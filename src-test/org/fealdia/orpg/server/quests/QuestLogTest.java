package org.fealdia.orpg.server.quests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.xml.bind.JAXBException;

import junit.framework.TestCase;

import org.fealdia.orpg.common.xml.XMLUtil;
import org.junit.Test;

public class QuestLogTest extends TestCase {
	private static final String QUEST_TITLE = "Quest title";

	@Test
	public void testSerialization() throws JAXBException {
		QuestLog questLog = new QuestLog();
		Quest quest = new Quest();
		quest.setId(1);
		quest.setTitle(QUEST_TITLE);
		quest.setDescription("Quest description");
		questLog.startQuest(quest);
		questLog.finishQuest(quest);

		Quest quest2 = new Quest();
		quest2.setId(2);
		quest2.setTitle("Second quest");
		quest2.addObjective(new KillObjective("rat", 10));
		questLog.startQuest(quest2);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		XMLUtil.toXML(questLog, out);
		System.out.println("Serialized: " + out.toString());

		QuestLog o_questLog = XMLUtil.fromXML(QuestLog.class, new ByteArrayInputStream(out.toByteArray()));

		List<QuestInstance> active = o_questLog.getActiveQuests();
		QuestInstance qi = active.get(0);
		Quest o_q = qi.getQuest();
		assert(o_q.getTitle() == QUEST_TITLE);
	}

	public void testLoading() throws JAXBException {
		final String data = "<questLog><active><questinstance><questId>1</questId><createdStamp>50</createdStamp></questinstance></active><finished/></questLog>";

		ByteArrayInputStream in = new ByteArrayInputStream(data.getBytes());

		QuestLog questLog = XMLUtil.fromXML(QuestLog.class, in);

		List<QuestInstance> finished = questLog.getFinishedQuests();
		assert(finished.size() == 1);
	}
}
