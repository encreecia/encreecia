package org.fealdia.orpg.server.maps.entities;

import static org.junit.Assert.assertEquals;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;

import org.fealdia.orpg.server.maps.InstanceFixture;
import org.junit.Test;

public class CreatureTest extends InstanceFixture {
	@Test
	public void targetIsClearedWhenItChangesInstance() {
		Creature hunter = new Monster();
		hunter.setInstance(instance);
		
		Creature victim = new Monster();
		victim.setInstance(instance);
		
		hunter.setTarget(victim.getID());

		assertEquals(victim, hunter.getTarget());

		victim.moveOutOfWorld();

		assertEquals(null, hunter.getTarget());
	}

	@Test
	public void healthChangeIsReportedToPropertyChangeListener() {
		Creature creature = new Monster();
		creature.setHealth(50);
		creature.setInstance(instance);

		HealthListener listener = new HealthListener();
		creature.addPropertyChangeListener(listener);

		creature.setHealth(20);

		assertEquals(true, listener.called);
	}

	@Test
	public void targetHealthChangeIsReportedToTargetter() {
		final List<Creature> reporters = new LinkedList<Creature>();
		Creature hunter = new Monster() {
			protected void onTargetHealthChange(Creature target, int difference) {
				reporters.add(target);
			};
		};
		hunter.setInstance(instance);

		Creature victim = new Monster();
		victim.setHealth(50);
		victim.setInstance(instance);

		hunter.setTarget(victim);

		victim.setHealth(10);

		assertEquals(true, reporters.size() > 0);
	}

	private static class HealthListener implements PropertyChangeListener {
		public boolean called = false;

		public void propertyChange(PropertyChangeEvent evt) {
			if (Creature.WatchableProperty.HEALTH.toString().equals(evt.getPropertyName())) {
				called = true;
			}
		}
	}

}
