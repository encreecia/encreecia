package org.fealdia.orpg.server.maps.entities.persistence;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import junit.framework.TestCase;

import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.PlayerManager;
import org.fealdia.orpg.server.maps.entities.Item;
import org.fealdia.orpg.server.maps.entities.MapEntity;
import org.junit.Test;

public class XmlReadersTest extends TestCase {
	@Test
	public void testInstanceOverlayReading() throws XMLStreamException, FactoryConfigurationError {
		final String xml = "<entities><entity archetype=\"healthpotion\"><x>14</x><y>3</y></entity><entity type=\"armor\"></entity></entities>";

		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		List<MapEntity> entities = XmlReaders.processEntities(reader);
		reader.close();

		Item item = (Item) entities.get(0);

		assertEquals(2, entities.size());
		assertEquals("healthpotion", item.getArchetype());
	}

	@Test
	public void testPlayerReading() throws XMLStreamException, FactoryConfigurationError {
		final String xml = "<player><x>29</x><inventory><entity type=\"armor\"></entity></inventory></player>";

		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		Player player = XmlReaders.processPlayer(reader);
		reader.close();

		assertEquals(true, player != null);
	}

	@Test
	public void testPlayerWritingAndReading() throws FactoryConfigurationError, Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Player player = new Player();
		player.setName("tmptestcase");
		player.getInventory().add((Item) MapEntity.createFromArchetype("healthpotion"));

		XMLEntityWriter writer = new XMLEntityWriter(out);
		writer.writeEntity("player", player);
		writer.close();

		final String xml = out.toString();
		System.out.println("Player XML:\n" + xml);

		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new ByteArrayInputStream(xml.getBytes()));
		Player playerOut = XmlReaders.processPlayer(reader);
		reader.close();

		assertEquals(player.getName(), playerOut.getName());
		assertEquals(player.getInventory().size(), playerOut.getInventory().size());
	}

	@Test
	public void testPlayerXmlFileReading() throws Exception {
		PlayerManager.loadFromFileXml("guest");
	}
}
