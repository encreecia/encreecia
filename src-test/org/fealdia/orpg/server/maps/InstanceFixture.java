package org.fealdia.orpg.server.maps;

import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.server.maps.Instance.Uniqueness;
import org.junit.Before;

public class InstanceFixture {
	protected Scene scene;
	protected Instance instance;

	@Before
	public void setUp() {
		scene = new Scene(4, 4);
		instance = new Instance(scene, Uniqueness.NORMAL, null);
	}
}
