package org.fealdia.orpg.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

import junit.framework.TestCase;

import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.xml.XMLUtil;
import org.fealdia.orpg.server.quests.KillObjective;
import org.fealdia.orpg.server.quests.Quest;
import org.junit.Test;

public class XMLSerializationTest extends TestCase {
	@Test
	public void testSerialization() throws Exception {
		XMLUtil.toXML(new Quest(), System.out);
	}
	
	@Test
	public void testMarshalAndUnmarshal() throws Exception {
		Quest quest = new Quest();
		quest.setId(5);
		quest.setTitle("Help the town guards");
		quest.setDescription("desc");
		quest.getObjectives().add(new KillObjective("rat", 10));
		quest.addDependency(1);
		quest.addDependency(2);
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		XMLUtil.toXML(quest, out);
		System.out.println("Serialized: >>>\n" + out.toString() + "\n<<<");
		
		Quest q = XMLUtil.<Quest>fromXML(Quest.class, new ByteArrayInputStream(out.toByteArray()));
		
		System.out.println("obj " + q);
		System.out.println(" id " + q.getId());
	}
	
	@Test
	public void testLoadFromFile() throws Exception {
		Quest q = XMLUtil.<Quest>fromXML(Quest.class, PathConfig.getMapDir() + File.separator + "quests" + File.separator + "1.xml");
		System.out.println("quest " + q);
	}

}
