package org.fealdia.orpg.server;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class GameRulesTest {
	@Test
	public void testDRCalculations() {
		// 50% 66% 75% DR%
		int[] pct = { 50, 66, 75 };
		int[][] table = {
				{ 6, 12, 18 },
				{ 9, 18, 27 },
		};
		
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				int level = i + 1;
				System.out.println("Level " + level + " col " + j + " = %" + pct[j] + " AP " + table[i][j]);
				assertEquals(pct[j], GameRules.getDamageReduction(table[i][j], level));
			}
		}
		
		/*
		assertEquals(50, GameRules.getDamageReduction(10, 1));
		assertEquals(75, GameRules.getDamageReduction(20, 1));
		
		assertEquals(50, GameRules.getDamageReduction(15, 2));
		assertEquals(75, GameRules.getDamageReduction(30, 2));
		*/
	}
	
	@Test
	public void testExperience() {
		long exp = 0;
		int lvl = 0;
		
		// Cross-check experience methods
		for (int i = 1; i <= GameRules.getHighestLevel(); i++) {
			exp = GameRules.getExperienceForLevel(i);
			lvl = GameRules.getLevelFromExperience(exp);
			System.out.println(i + " " + exp);
			assertEquals(i, lvl);
		}
		
		assertEquals(exp, GameRules.getExperienceForLevel(GameRules.getHighestLevel() + 1));
	}
}
