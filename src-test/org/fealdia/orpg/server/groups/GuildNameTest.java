package org.fealdia.orpg.server.groups;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class GuildNameTest {
	
	@Test
	public void testGuildTags() {
		String[] valid_tags = { "DM", "FoaF" };
		for (String tag : valid_tags) {
			assertTrue(Guild.isValidTag(tag));
		}
		
		String[] invalid_tags = { "Fooba", "F BA", "" };
		for (String tag : invalid_tags) {
			assertFalse(Guild.isValidTag(tag));
		}
	}

	@Test
	public void testGuildName() {
		String[] valid_names = { "Dungeon Masters", "Foobar" };
		for (String name : valid_names) {
			assertTrue(Guild.isValidName(name));
		}
		
		String[] invalid_names = { "' booya", " space at start", "space at end ", "way too long guild name that should fail", "1st guild" };
		for (String name : invalid_names) {
			assertFalse(Guild.isValidName(name));
		}
	}
	
}
