package org.fealdia.orpg.common.util;

import org.junit.Assert;
import org.junit.Test;

public class Sha1UtilTest {
	
	@Test
	public void testSha1ForFile() throws Exception {
		Assert.assertEquals("03b254c872b95141751f414e353a25c2ac261b51", Sha1Util.getSha1ForFile("lib/log4j-1.2.14.jar"));
		
		Assert.assertEquals("78e50e186b04c8fe1defaa098f1c192181b3d837", Sha1Util.getSha1ForFile("doc/agpl-3.0.txt"));
	}
	
	@Test
	public void testSha1ForString() {
		Assert.assertEquals("0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33", Sha1Util.getSha1ForString("foo"));
	}

}
