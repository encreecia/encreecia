package org.fealdia.orpg.common.util;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class StringUtilTest {
	
	@Test
	public void testCapitalize() {
		assertEquals("Capitalized string", StringUtil.capitalize("CAPITALIZED StrING"));
	}
	
	@Test
	public void testJoin() {
		List<String> strings = new LinkedList<String>();
		assertEquals("", StringUtil.join(strings, ","));
		strings.add("foo");
		assertEquals("foo", StringUtil.join(strings, ","));
		strings.add("bar");
		assertEquals("foo,bar", StringUtil.join(strings, ","));
	}
	
	@Test
	public void testSplitIntoParams() {
		String[] res1 = StringUtil.splitIntoParameters("\"Foo bar\" baz");
		assertEquals(2, res1.length);
		assertEquals("Foo bar", res1[0]);
		assertEquals("baz", res1[1]);
		
		String[] res2 = StringUtil.splitIntoParameters("foo bar baz");
		assertEquals(3, res2.length);
		
		String[] res3 = StringUtil.splitIntoParameters("fo\"bar baz");
		assertEquals(2, res3.length);
		assertEquals("fo\"bar", res3[0]);
		
		String[] res4 = StringUtil.splitIntoParameters(" foo  baz   bar ");
		assertEquals(3, res4.length);
		assertEquals("baz", res4[1]);
		assertEquals("bar", res4[2]);
	}
	
	@Test
	public void testTrimLeft() {
		assertEquals("foo", StringUtil.trimLeft("  foo"));
		assertEquals("foo", StringUtil.trimLeft("foo"));
		assertEquals("foo  ", StringUtil.trimLeft("foo  "));
		assertEquals("foo  ", StringUtil.trimLeft("  foo  "));
		assertEquals("foo", StringUtil.trimLeft("  \t  foo"));
	}

}
