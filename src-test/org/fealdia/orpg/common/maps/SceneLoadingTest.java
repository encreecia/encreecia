package org.fealdia.orpg.common.maps;

import org.junit.Before;
import org.junit.Test;

/**
 * Test scene loading. 
 */
public class SceneLoadingTest {
	private SceneManager sceneManager;

	@Before
	public void setUp() throws Exception {
		sceneManager = SceneManager.getInstance();
	}
	
	@Test
	public void testLoad() {
		sceneManager.getScene("chunktest");
	}

}
