package org.fealdia.orpg.common.maps;

import javax.xml.bind.JAXBException;

import junit.framework.TestCase;

import org.fealdia.orpg.common.xml.XMLUtil;

public class SceneEntityTest extends TestCase {
	public void testMarshalAndUnmarshal() throws JAXBException {
		SceneEntity entity = new SceneEntity("spawn", 5, 3);
		entity.putProperty("archetype", "skeleton");
		entity.putProperty("_name", "jaxbkiller");

		final String marshaled = XMLUtil.toXMLString(entity, true);

		System.out.println("Marshaled:\n" + marshaled);

		SceneEntity result = XMLUtil.fromXMLString(SceneEntity.class, marshaled);

		System.out.println("Unmarshaled:\n" + result);
		System.out.println("Marshaled again:\n" + XMLUtil.toXMLString(result, true));

		assertEquals(entity.getArchetype(), result.getArchetype());
		assertEquals(entity.getX(), result.getX());
		assertEquals(entity.getProperties(), result.getProperties());
	}

	public void testUnmarshallingCompatiblityIsNotBroken() throws JAXBException {
		final String frozenEntity = "<entity x=\"4\" y=\"3\" archetype=\"spawn\"><property key=\"archetype\" value=\"guard\"/></entity>";

		SceneEntity result = XMLUtil.fromXMLString(SceneEntity.class, frozenEntity);
		assertEquals(4, result.getX());
		assertEquals("spawn", result.getArchetype());
		assertEquals("guard", result.getProperty("archetype"));

	}
}
