package org.fealdia.orpg.common.maps;

import java.io.ByteArrayInputStream;

import javax.xml.bind.JAXBException;

import junit.framework.TestCase;

import org.fealdia.orpg.common.xml.XMLUtil;
import org.junit.Before;
import org.junit.Test;

public class AnimationTest extends TestCase {
	private Animation animation;
	private Animation bounce;

	@Before
	public void setUp() {
		animation = new Animation();
		animation.name = "guard";
		animation.type = Animation.Type.cycle;
		animation.speed = 4;
		animation.frames.add("face1");
		animation.frames.add("face2");

		bounce = new Animation();
		bounce.type = Animation.Type.bounce;
		bounce.name = "boing";
		bounce.speed = 1;
		bounce.frames.add("face1");
		bounce.frames.add("face2");
		bounce.frames.add("face3");
	}

	@Test
	public void testSerializationFormatValid() throws JAXBException {
		String xml = XMLUtil.toXMLString(animation, false);
		System.out.println("out:[[ " + xml + " ]]");
		assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><animation type=\"cycle\" sync=\"false\" speed=\"4\" name=\"guard\"><frame>face1</frame><frame>face2</frame></animation>", xml);
	}

	public void testDeserialization() throws JAXBException {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><animation type=\"bounce\" sync=\"false\" speed=\"8\" name=\"guard\"><frame>face1</frame><frame>face2</frame></animation>";
		Animation animation = XMLUtil.fromXML(Animation.class, new ByteArrayInputStream(xml.getBytes()));

		assertEquals(Animation.Type.bounce, animation.type);
		assertEquals(8, animation.speed);
	}

	@Test
	public void testGetFrame() {
		// Just check that no out of bounds etc
		for (int time = 0; time < 5000; time++) {
			animation.getFrame(time, 0);
		}
	}

	@Test
	public void testBounceType() {
		assertEquals("face1", bounce.getFrame(0, 0));
		assertEquals("face2", bounce.getFrame(1, 0));
		assertEquals("face3", bounce.getFrame(2, 0));
		assertEquals("face3", bounce.getFrame(3, 0));
		assertEquals("face2", bounce.getFrame(4, 0));
		assertEquals("face1", bounce.getFrame(5, 0));
	}
}
