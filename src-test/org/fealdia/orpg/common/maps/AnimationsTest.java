package org.fealdia.orpg.common.maps;

import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import junit.framework.TestCase;

import org.fealdia.orpg.common.xml.XMLUtil;
import org.junit.Test;

public class AnimationsTest extends TestCase {
	@Test
	public void testLoad() throws FileNotFoundException, JAXBException {
		Animations animations = XMLUtil.fromXML(Animations.class, Animations.FILENAME);
		assertTrue(animations.animations.size() > 0);
		System.out.println("Serialized back: " + XMLUtil.toXMLString(animations, true));
	}
}
