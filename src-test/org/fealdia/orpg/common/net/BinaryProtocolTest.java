package org.fealdia.orpg.common.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;

import junit.framework.TestCase;

import org.fealdia.orpg.common.net.data.FaceInfo;
import org.fealdia.orpg.common.net.packets.ClientHello;
import org.fealdia.orpg.common.net.packets.FaceInfoPacket;
import org.fealdia.orpg.common.net.packets.FilePacket;

public class BinaryProtocolTest extends TestCase {
	private static final int FILESIZE = 1025;

	public void testWritingAndReading() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ProtocolOutputStream pos = new ProtocolOutputStream(baos);

		// client hello: version, client(string), java_version, os_name, os_version
		ProtoPacket packetHello = new ClientHello(1, "testclient", "javaver", "osname", "osversion");
		pos.writePacket(packetHello);

		pos.writePacket(new FaceInfoPacket(new FaceInfo(1, "monsters/humanoid/warrior1", -1)));

		final byte[] filedata = new byte[FILESIZE];
		pos.writePacket(new FilePacket("filename", filedata));

		// parse the content
		byte[] data = baos.toByteArray();

		ProtocolInputStream pis = new ProtocolInputStream(new ByteArrayInputStream(data));
		try {
			for (ProtoPacket packet = pis.readPacket(); packet != null; packet = pis.readPacket()) {
				System.out.println("Packet: " + packet);
				if (packet instanceof FaceInfoPacket) {
					FaceInfoPacket faceInfo = (FaceInfoPacket) packet;
					System.out.println(" faceinfo " + faceInfo.faceInfo.name);
				}
				else if (packet instanceof FilePacket) {
					assertEquals(FILESIZE, ((FilePacket) packet).data.length);
				}
			}
		} catch (EOFException e) {
			System.out.println("EOF");
		}
	}
}
