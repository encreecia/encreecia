#!/bin/bash
echo "Regenerating images..."

for i in 1 2 3
do
	convert "shore1.png" -rotate "$[ 90 * $i ]" "shore$[$i + 1].png"
done

for i in 1 2 3
do
	convert "shore5.png" -rotate "$[ 90 * $i ]" "shore$[$i + 5].png"
done

for i in 1 2 3
do
	convert "shore9.png" -rotate "$[ 90 * $i ]" "shore$[$i + 9].png"
done

