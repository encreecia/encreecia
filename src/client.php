<?php
$path = dirname($_SERVER['REQUEST_URI']);
$codebase = "http://$_SERVER[SERVER_NAME]$path";
$href = 'client.php';
$title = 'Encreecia client';
$homepage = 'http://encreecia.fealdia.org';
$description = 'Client for encreecia';
if (strlen($_SERVER['QUERY_STRING']) > 0) {
	$href .= '?'. $_SERVER['QUERY_STRING'];
}

header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
header('Last-Modified: '. gmdate('D, d M Y H:i:s') . ' GMT');
header('Content-type: application/x-java-jnlp-file');
header('Cache-Control: no-store,no-cache,must-revalidate');
header('Pragma: no-cache');
//header('Content-disposition: Inline; fileName=client.jnlp');

echo '<?xml version="1.0" encoding="utf-8"?>'."\n";
?>
<jnlp spec="1.5+" codebase="<?php echo $codebase?>" href="<?php echo $href?>">
	<information>
		<title><?php echo $title?></title>
		<vendor>fealdia.org</vendor>
		<homepage href="<?php echo $homepage?>"/>
		<description><?php echo $description?></description>
		<icon href="webstart.gif"/>
	</information>

	<security>
		<all-permissions/>
	</security>

	<resources>
		<j2se version="1.6+"/>
		<jar href="client.jar"/>
		<jar href="common.jar"/>
		<jar href="doc.jar"/>
		<jar href="log4j-1.2.14.jar"/>
		<jar href="substance.jar"/>
	</resources>

	<application-desc main-class="org.fealdia.orpg.client.ClientMain">
		<argument>-webstart</argument>
<?php
if (isset($_REQUEST['defaultserver'])) {
	$server = $_REQUEST['defaultserver'];
?>
		<argument>-defaultserver</argument>
		<argument><?php echo $server?></argument>
<?php
}
if (isset($_REQUEST['easyplay'])) {
?>
		<argument>-easyplay</argument>
<?php
}
?>
	</application-desc>
</jnlp>

