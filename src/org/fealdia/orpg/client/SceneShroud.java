package org.fealdia.orpg.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.BitSet;

import org.apache.commons.io.IOUtils;
import org.fealdia.orpg.common.maps.Chunk;
import org.fealdia.orpg.common.maps.Scene;

public class SceneShroud {
	private static String EXTENSION = ".shroud";

	private final int widthChunks;
	private final int heightChunks;
	private final BitSet bitSet; // true for visited spots
	private final Scene scene;

	public static SceneShroud loadOrCreateSceneShroud(Scene scene) throws IOException {
		final String filename = scene.getFilename() + EXTENSION;

		File file = new File(filename);
		if (!file.exists()) {
			return new SceneShroud(scene);
		}

		return new SceneShroud(scene, filename);
	}
	
	public SceneShroud(Scene scene) {
		this.scene = scene;

		widthChunks = scene.getWidthInChunks();
		heightChunks = scene.getHeightInChunks();

		bitSet = new BitSet(widthChunks * heightChunks);
	}

	public SceneShroud(Scene scene, String filename) throws IOException {
		this.scene = scene;
		widthChunks = scene.getWidthInChunks();
		heightChunks = scene.getHeightInChunks();

		DataInputStream dis = new DataInputStream(new FileInputStream(filename));

		try {
			int version = dis.readUnsignedShort();
			if (version != 1) {
				throw new IOException("Unexpected version in SceneShroud");
			}
			int oldWidthChunks = dis.readInt();
			int oldHeightChunks = dis.readInt();
			BitSet oldBitSet = BitSet.valueOf(IOUtils.toByteArray(dis));

			if (oldWidthChunks != widthChunks || oldHeightChunks != heightChunks) {
				bitSet = new BitSet(widthChunks * heightChunks);

				// Go through old BitSet and set each true bit to new BitSet
				for (int x = 0; x < oldWidthChunks; x++) {
					for (int y = 0; y < oldHeightChunks; y++) {
						if (oldBitSet.get(y * oldHeightChunks + x)) {
							bitSet.set(y * widthChunks + x);
						}
					}
				}
			}
			else {
				bitSet = oldBitSet;
			}
		} finally {
			dis.close();
		}
	}

	public boolean isShrouded(int tileX, int tileY) {
		if (!scene.isOnScene(tileX, tileY)) {
			throw new IllegalArgumentException("Position out of Scene");
		}

		int chunkX = tileX / Chunk.getSize();
		int chunkY = tileY / Chunk.getSize();
		return !bitSet.get(chunkY * widthChunks + chunkX);
	}

	public void unshroud(int tileX, int tileY) {
		int chunkX = tileX / Chunk.getSize();
		int chunkY = tileY / Chunk.getSize();

		if (isShrouded(chunkX, chunkY)) {
			bitSet.set(chunkY * widthChunks + chunkX);
			try {
				saveToFile();
			} catch (IOException e) {
				throw new RuntimeException("Failed to save SceneShroud", e);
			}
		}
	}

	private String getFilename() {
		return scene.getFilename() + EXTENSION;
	}

	public void saveToFile() throws IOException {
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(getFilename()));
		try {
			dos.writeShort(1); // Version
			dos.writeInt(widthChunks);
			dos.writeInt(heightChunks);
			dos.write(bitSet.toByteArray());
		} finally {
			dos.close();
		}
	}
}
