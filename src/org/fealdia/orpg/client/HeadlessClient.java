package org.fealdia.orpg.client;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.BackingStoreException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.fealdia.orpg.client.ClientEntity.Status;
import org.fealdia.orpg.client.ui.ClientImageMapper;
import org.fealdia.orpg.client.ui.LoginDetails;
import org.fealdia.orpg.common.maps.Animations;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.maps.SceneManager;
import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.ProtoSocket;
import org.fealdia.orpg.common.net.data.ChatType;
import org.fealdia.orpg.common.net.data.FaceInfo;
import org.fealdia.orpg.common.net.data.FaceMapper;
import org.fealdia.orpg.common.net.data.MusicHint;
import org.fealdia.orpg.common.net.data.ProtoQuest;
import org.fealdia.orpg.common.net.packets.CheckFilePacket;
import org.fealdia.orpg.common.net.packets.ClientChatPacket;
import org.fealdia.orpg.common.net.packets.ClientHello;
import org.fealdia.orpg.common.net.packets.CommandPacket;
import org.fealdia.orpg.common.net.packets.ContainerPacket;
import org.fealdia.orpg.common.net.packets.ContainerPacket.ContainerItem;
import org.fealdia.orpg.common.net.packets.CoordinatePacket;
import org.fealdia.orpg.common.net.packets.EntityEffectPacket;
import org.fealdia.orpg.common.net.packets.EntityHealthPacket;
import org.fealdia.orpg.common.net.packets.EntityInfoPacket;
import org.fealdia.orpg.common.net.packets.EntityMovePacket;
import org.fealdia.orpg.common.net.packets.EntityReferencePacket;
import org.fealdia.orpg.common.net.packets.ExperiencePacket;
import org.fealdia.orpg.common.net.packets.FaceInfoPacket;
import org.fealdia.orpg.common.net.packets.FacePacket;
import org.fealdia.orpg.common.net.packets.FilePacket;
import org.fealdia.orpg.common.net.packets.FlagPacket;
import org.fealdia.orpg.common.net.packets.GetBackgroundFaceInfoPacket;
import org.fealdia.orpg.common.net.packets.GetImagePacket;
import org.fealdia.orpg.common.net.packets.InstanceNamePacket;
import org.fealdia.orpg.common.net.packets.ItemGetPacket;
import org.fealdia.orpg.common.net.packets.LoginPacket;
import org.fealdia.orpg.common.net.packets.MovementVectorPacket;
import org.fealdia.orpg.common.net.packets.MusicHintPacket;
import org.fealdia.orpg.common.net.packets.PartyMarkerPacket;
import org.fealdia.orpg.common.net.packets.PartyMemberPositionPacket;
import org.fealdia.orpg.common.net.packets.PartyMembersPacket;
import org.fealdia.orpg.common.net.packets.PlayerHealthPacket;
import org.fealdia.orpg.common.net.packets.PlayerPositionPacket;
import org.fealdia.orpg.common.net.packets.PopupPacket;
import org.fealdia.orpg.common.net.packets.QuestLogPacket;
import org.fealdia.orpg.common.net.packets.QuestOfferPacket;
import org.fealdia.orpg.common.net.packets.ServerChatPacket;
import org.fealdia.orpg.common.net.packets.ServerHello;
import org.fealdia.orpg.common.net.packets.SimplePacket;
import org.fealdia.orpg.common.net.packets.TextPacket;
import org.fealdia.orpg.common.util.FileUtil;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.Sha1Util;
import org.fealdia.orpg.common.util.StreamUtil;

/**
 * Contains all client<->server logic that does not concern itself with UI.
 * 
 * All construction of data packets to be sent to server should happen in this class, in the send*() methods.
 * 
 * Corresponding class at the server side is ClientThread.
 */
public abstract class HeadlessClient extends ProtoSocket implements ActionListener, Runnable {
	private static Logger logger = Logger.getLogger(HeadlessClient.class);
	private static Logger chatLogger = Logger.getLogger("chat");
	
	private boolean attackMode = false;
	private Scene scene = null;
	private SceneShroud sceneShroud = null;
	
	private long target = 0;
	private long experience = 0;
	private int health = 0;
	private boolean master = false;
	private int maxHealth = 0;
	
	private long requestedTarget = -1;
	
	/**
	 * Entity cache.
	 * id -> {instance, x, y, type, face, name, status}
	 */
	private Map<Long, ClientEntity> entityCache = Collections.synchronizedMap(new HashMap<Long, ClientEntity>());
	
	/**
	 * Container cache.
	 * id -> [{id, name, face, ...}] 
	 */
	private Map<Long,List<ContainerItem>> containerCache = new HashMap<Long,List<ContainerItem>>();

	private int level = 1;
	
	private String instanceName = null;

	private long experience_next = 0;

	private long experience_this = 0;
	
	private List<Long> partyMembers = new LinkedList<Long>();
	private Map<Long, Point> partyPositions = new HashMap<Long, Point>();
	
	private String marker_map = null;
	private int marker_x = 0;
	private int marker_y = 0;
	
	private String serverName = null;

	private FaceMapper faceMapper = ClientImageMapper.getInstance().faceMapper;

	private Set<Integer> requestedBackgrounds = new HashSet<Integer>();
	private Set<Integer> requestedImages = new HashSet<Integer>();

	protected PasswordStore passwordStore = new PasswordStore();

	public HeadlessClient() {
	}
	
	public boolean connect(String server, int port) {
		logger.debug("connect(" + server + ", " + port + ")");
		disconnect();
		showMessage("Connecting to " + server + ":" + port);
		try {
			setSocket(new Socket(server, port));
			sendVersion();
		} catch (UnknownHostException e) {
			showMessage("Unknown host: " + server + ":" + port + " - make sure you've written the address properly.");
			return false;
		} catch (ConnectException e) {
			// Connection refused
			logger.debug("Connection refused");
			showMessage("Connection refused");
			return false;
		} catch (IOException e) {
			logger.error("Failed to connect()", e);
			showMessage("Failed to connect");
			return false;
		}
		showMessage("Connected to " + server + ":" + port);
		serverName = server + ":" + port;
		return true;
	}
	
	public void run() {
		try {
			while (true) {
				if (getSocket() != null && getSocket().isConnected()) {
					try {
						readObjects();
					} catch (EOFException e) {
						// Normal disconnect
					} catch (SocketException e) {
						// Also normal - socket closed while reading etc
					} catch (IOException e) {
						logger.error("Exception while reading packets", e);
					} catch (ClassNotFoundException e) {
						logger.error("Class not found while reading packets", e);
					} catch (Exception e) {
						logger.error("Unexpected exception while reading server packets", e);
					} finally {
						disconnect();
					}
				}
				else {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {}
				}
			}
		}
		catch (Exception e) {
			logger.fatal("Exception in HeadlessClient.run()", e);
		}
	}
	
	private void sendVersion() {
		sendMessage(new ClientHello(PacketType.getProtocolVersion(), "jfealdia java client", System.getProperty("java.version"), System.getProperty("os.name"), System.getProperty("os.version")));
	}

	@Override
	protected void handleMessage(ProtoPacket packet) throws Exception {
		PacketType packetType = packet.getPacketType();

		if (packet instanceof ServerHello) {
			ServerHello serverHello = (ServerHello) packet;
			if (serverHello.protocolVersion != PacketType.getProtocolVersion()) {
				showMessage("Server is using incompatible protocol version (server:" + serverHello.protocolVersion + ", client: " + PacketType.getProtocolVersion() + ")");
				disconnect();
				return;
			}
			// Global map-related files are requested before login
			checkFile("maps/chunks");
			checkFile("maps/backgrounds.xml");
			checkFile(Animations.FILENAME);
			onLoginRequested();
			runHook("connect");
		}
		else if (packet instanceof SimplePacket && packetType == PacketType.LOGINSUCCESS) {
			logger.info("Logged in successfully");
			runHook("login");
		}
		else if (packet instanceof SimplePacket && packetType == PacketType.ENTITYCACHECLEAR) {
			clearEntityCache();
		}
		else if (packet instanceof InstanceNamePacket) {
			InstanceNamePacket instanceNamePacket = (InstanceNamePacket) packet;
			String instance = instanceNamePacket.instanceName;
			logger.info("Instance is '" + instance + "'");

			setInstanceName(instance);
			setScene(null);

			boolean got1 = checkFile("maps/" + instance);
			boolean got2 = checkFile("maps/" + instance + ".chunks");

			// Load and/or download the map
			if (got1 && got2) {
				// Load and set matching scene
				Scene scene = SceneManager.getInstance().getScene(instance);
				if (scene != null) {
					setScene(scene);
				}
			}

			clearEntityCache();

			// Clear party positions
			partyPositions.clear();
			onPartyChanged();
		}
		else if (packet instanceof PlayerPositionPacket) {
			PlayerPositionPacket playerPositionPacket = (PlayerPositionPacket) packet;
			setPosition(playerPositionPacket.x, playerPositionPacket.y);
		}
		else if (packetType == PacketType.NOTICE) {
			showMessage(((TextPacket) packet).text);
		}
		else if (packetType == PacketType.LOGINFAILURE) {
			TextPacket textPacket = (TextPacket) packet;
			logger.error("Login failure, reason: " + textPacket.text);
			showMessage("Login failure, reason: " + textPacket.text);
			onLoginRequested();
		}
		else if (packetType == PacketType.LOGIN) {
			// EASYPLAY details from server ride a LoginPacket
			LoginPacket login = (LoginPacket) packet;

			// save credentials for future use
			final String username = login.username;
			final String password = login.password;
			try {
				passwordStore.storeLoginDetails(serverName, new LoginDetails(username, password));
			} catch (BackingStoreException e) {
				logger.error("Failed to save easyplay username/password", e);
				showMessage("Failed to save generated username/password, write them down: " + username + " / " + password);
			}
		}
		else if (packet instanceof PopupPacket) {
			PopupPacket popup = (PopupPacket) packet;
			showPopup(popup.text);
		}
		else if (packet instanceof EntityEffectPacket) {
			EntityEffectPacket entityEffect = (EntityEffectPacket) packet;

			getEntityInfo(entityEffect.entityId).addEffect(faceMapper.getFaceById(entityEffect.faceId).name, entityEffect.time, entityEffect.text);
			onEntityCacheUpdated();
		}
		else if (packet instanceof EntityMovePacket) {
			EntityMovePacket entityMovePacket = (EntityMovePacket) packet;
			long id = entityMovePacket.id;
			int x = entityMovePacket.x;
			int y = entityMovePacket.y;
			logger.trace("Entity " + id + " moved to " + x + "," + y);
			if (entityMovePacket.dst_visible) {
				handleEntityMove(id, x, y);
			}
			else {
				getEntityCache().remove(id);
				onEntityCacheUpdated();
				onFloorChanged();
			}
		}
		else if (packet instanceof EntityInfoPacket) {
			EntityInfoPacket entityInfo = (EntityInfoPacket) packet;

			long id = 0;
			synchronized (getEntityCache()) {
				id = entityInfo.id;
				String name = entityInfo.name;
				String entitytype = entityInfo.entityType;

				ClientEntity info = getEntityInfo(id);
				info.setFace(entityInfo.face);
				info.setName(name);
				info.setType(entitytype);
				info.setCreature(entityInfo.isCreature);
				info.setItem(entityInfo.isItem);
				info.setLayer(info.isItem() ? 2 : (info.isCreature() ? 0 : 1));
				
				if (entityInfo.guild.length() > 0) {
					info.setGuild(entityInfo.guild);
				}
				if (entityInfo.nameColor != null) {
					info.setNameColor(entityInfo.nameColor);
				}
				if (entityInfo.overlayId != -1) {
					info.setOverlay(faceMapper.getFaceById(entityInfo.overlayId).name);
				}

				info.setStatus(Status.COMPLETE);
			}
				
			if (id == 0) {
				onPlayerInfoChanged();
			}
			onEntityCacheUpdated();
			onFloorChanged();
		}
		else if (packet instanceof EntityHealthPacket) {
			EntityHealthPacket entityHealth = (EntityHealthPacket) packet;
			
			ClientEntity en = getEntityInfo(entityHealth.id);
			en.setHealth(entityHealth.percentage);
		}
		else if (packetType == PacketType.TARGET) {
			setTarget(((EntityReferencePacket) packet).id);
		}
		else if (packet instanceof ExperiencePacket) {
			ExperiencePacket experience = (ExperiencePacket) packet;
			setExperience(experience.experience, experience.experienceThis, experience.experienceNext, experience.level);
		}
		else if (packet instanceof PlayerHealthPacket) {
			PlayerHealthPacket playerHealth = (PlayerHealthPacket) packet;
			setHealth(playerHealth.currentHealth, playerHealth.maxHealth);
		}
		else if (packet instanceof ContainerPacket) {
			ContainerPacket containerPacket = (ContainerPacket) packet;
			handleContainer(containerPacket.containerId, containerPacket.items);
		}
		else if (packet instanceof FilePacket) {
			FilePacket file = (FilePacket) packet;
			String filename = file.filename;
			byte[] data = file.data;

			logger.info("Received file '" + filename + "' with " + data.length + " bytes");

			if (isValidDownloadablePath(filename)) {
				synchronized (ImageMapper.getInstance()) {
					// TODO save to temporary file, then rename (otherwise other threads might read the partial file)
					if (!FileUtil.saveByteArrayToFile(data, PathConfig.getRootDir() + filename)) {
						logger.warn("Failed to save file: " + filename);
					}
				}

				// If it's a map, reload our map
				if (filename.startsWith("maps/") && getInstanceName() != null) {
					if (getScene() == null) {
						logger.debug("Loading Scene from scratch");
						setScene(SceneManager.getInstance().getScene(getInstanceName()));
					}
					else {
						logger.debug("Reloading Scene");
						getScene().reload();
					}
				}
				else if (filename.startsWith("images/")) {
					// When image updating is implemented, this may be needed
					//ImageMapper.getInstance().reload();

					// Redraw inventory & floor
					onFloorChanged();
					onContainerChanged(0);
				}
			}
			else {
				logger.error("Server tried to send file '" + filename + "' which is not allowed");
			}
		}
		else if (packet instanceof FlagPacket && packet.getPacketType() == PacketType.ATTACKMODE) {
			setAttackMode(((FlagPacket) packet).value);
		}
		else if (packetType == PacketType.LOGOUT) {
			handleLogout();
			runHook("logout");
		}
		else if (packet instanceof FlagPacket && packet.getPacketType() == PacketType.MASTER) {
			setMaster(((FlagPacket) packet).value);
		}
		else if (packet instanceof ServerChatPacket) {
			ServerChatPacket chat = (ServerChatPacket) packet;
			
			showChat(chat.who, chat.text, chat.chatType);
		}
		else if (packet instanceof PartyMembersPacket) {
			PartyMembersPacket partyMembersPacket = (PartyMembersPacket) packet;
			partyMembers = partyMembersPacket.ids;
			onPartyChanged();
		}
		else if (packet instanceof PartyMemberPositionPacket) {
			PartyMemberPositionPacket pos = (PartyMemberPositionPacket) packet;
			partyPositions.put(pos.id, new Point(pos.x, pos.y));
			onPartyChanged();
		}
		else if (packetType == PacketType.PARTYLEFT) {
			partyPositions.clear();
			partyMembers.clear();
			onPartyChanged();
		}
		else if (packetType == PacketType.PARTYMEMBERPOSCLEAR) {
			partyPositions.remove(((EntityReferencePacket) packet).id);
			onPartyChanged();
		}
		else if (packetType == PacketType.CONTAINERCLOSED) {
			long id = ((EntityReferencePacket) packet).id;
			containerCache.remove(id);
			onContainerClosed(id);
		}
		else if (packet instanceof PartyMarkerPacket) {
			PartyMarkerPacket marker = (PartyMarkerPacket) packet;
			setPartyMarker(marker.sceneName, marker.x, marker.y);
		}
		else if (packet instanceof QuestOfferPacket) {
			QuestOfferPacket offer = (QuestOfferPacket) packet;
			offerQuest(offer.quest.id, offer.quest.title, offer.quest.description);
		}
		else if (packet instanceof QuestLogPacket) {
			onQuestLogReceived(((QuestLogPacket) packet).quests);
		}
		else if (packet instanceof FaceInfoPacket) {
			FaceInfo faceInfo = ((FaceInfoPacket) packet).faceInfo;
			faceMapper.addFaceMapping(faceInfo);
		}
		else if (packet instanceof FacePacket) {
			FacePacket facePacket = (FacePacket) packet;
			FaceInfo faceInfo = faceMapper.getFaceById(facePacket.faceId);
			byte[] imageData = facePacket.imageData;

			String filename = getImageFilename(faceInfo.name);

			FileUtils.writeByteArrayToFile(new File(filename), imageData);
			logger.debug("Face received: " + faceInfo.name + " (" + imageData.length + " bytes)");
			onFaceReceived();
		}
		else if (packet instanceof MusicHintPacket) {
			MusicHintPacket musicHintPacket = (MusicHintPacket) packet;
			onMusicHintReceived(musicHintPacket.musicHint);
		}
		else {
			logger.error("Could not handle message: " + packet);
		}
	}

	protected void showPopup(String text) {
	}

	private void clearEntityCache() {
		// Clear entity cache, except player
		synchronized (getEntityCache()) {
			ClientEntity pl = getEntityInfo(0);
			entityCache.clear();
			entityCache.put(Long.valueOf(0), pl);
		}
	}

	protected void handleLogout() {
		clearContainerCache();
		partyMembers.clear();
		partyPositions.clear();
		marker_map = null;
		clearEntityCache();
		setScene(null);
		setMaster(false);
		showMessage("You have been logged out.");
	}

	protected void onPlayerInfoChanged() {}

	/**
	 * Ask the user to input username/password & send it. Must call login() with the username/password.
	 */
	abstract protected void askLogin();
	
	public final void login(String username, String password) {
		sendMessage(new LoginPacket(username, password));
	}
	
	protected void setPosition(int x, int y) {
		if (sceneShroud != null) {
			sceneShroud.unshroud(x, y);
		}
		logger.trace("Setting position x = " + x + ", y = " + y);
		synchronized (getEntityCache()) {
			ClientEntity info = getEntityInfo(0);
			info.setPosition(x, y);
			info.setStatus(Status.COMPLETE);
			info.setLayer(0);
		}
		
		// notify UI that redraw is needed
		onMapChanged();
		onFloorChanged();
	}

	public Scene getScene() {
		return scene;
	}

	public SceneShroud getSceneShroud() {
		return sceneShroud;
	}

	public int getX() {
		return getEntityInfo(0).getX();
	}

	public int getY() {
		return getEntityInfo(0).getY();
	}
	
	protected void onMapChanged() {}
	protected void showMessage(String text) {}

	public void setScene(Scene scene) {
		this.scene = scene;
		if (scene != null) {
			try {
				this.sceneShroud = SceneShroud.loadOrCreateSceneShroud(scene);
			} catch (IOException e) {
				logger.error("Failed to create SceneShroud", e);
			}
		}
		// notify UI of scene change
		onMapChanged();
	}

	public void sendChat(String text, ChatType type) {
		sendMessage(new ClientChatPacket(type, text));
	}
	
	public void sendCommand(String command) {
		sendMessage(new CommandPacket(command));
	}
	
	/**
	 * Get (or create) a modifiable Map containing the entity info. 
	 */
	public ClientEntity getEntityInfo(long id) {
		synchronized (getEntityCache()) {
			if (!entityCache.containsKey(id)) {
				entityCache.put(id, new ClientEntity());
			}
			return entityCache.get(id);
		}
	}
	
	public boolean isEntityComplete(long id) {
		ClientEntity en = getEntityInfo(id);
		return en.isComplete();
	}
	
	public Map<Long, ClientEntity> getEntityCache() {
		return entityCache;
	}
	
	private void handleEntityMove(long id, int x, int y) {
		synchronized (getEntityCache()) {
			ClientEntity info = getEntityInfo(id);
			info.setPosition(x, y);
			//info.put("instance", getScene().getName());

			// If entity is not known, query for info
			if (info.getStatus() == Status.INIT) {
				sendEntityQuery(id);
			}
		}
		
		onEntityCacheUpdated();
	}
	
	private void sendEntityQuery(long id) {
		sendMessage(new EntityReferencePacket(PacketType.ENTITYQUERY, id));
		
		ClientEntity info = getEntityInfo(id);
		info.setStatus(Status.REQUESTED);
	}
	
	private void sendSetTarget(long id) {
		sendMessage(new EntityReferencePacket(PacketType.TARGET, id));
	}
	
	protected void onEntityCacheUpdated() {}
	protected void onContainerChanged(long id) {}
	
	/**
	 * Decide what to do when user clicks the given coordinate.
	 */
	public void handleClick(int x, int y, boolean ctrl, int clickCount) {
		logger.trace("Click on x=" + x + ", y=" + y + ", ctrl=" + ctrl);
		
		// Doubleclick: talk?
		if (clickCount == 2) {
			sendUse(x, y);
			return;
		}
		
		if (ctrl) {
			sendLook(x, y);
			return;
		}
		
		long id = findCreatureAt(x, y);
		if (id > 0) {
			requestTarget(id);
			return;
		}
		
		// Clear target
		requestTarget(0);
	}
	
	public void handleShiftHover(int x, int y) {
		long id = findCreatureAt(x, y);
		if (id > 0) {
			requestTarget(id);
		}
	}
	
	/**
	 * Return creature id at given coordinates, or 0 if none.
	 */
	public long findCreatureAt(int x, int y) {
		synchronized (getEntityCache()) {
			for (Long id : getEntityCache().keySet()) {
				ClientEntity info = getEntityInfo(id);
				int entity_x = info.getX();
				int entity_y = info.getY();

				if (entity_x == x && entity_y == y && info.isCreature()) {
					return id;
				}
			}
		}
		return 0;
	}

	public long getTarget() {
		return target;
	}

	public void setTarget(long target) {
		this.target = target;
		requestedTarget = target;
		
		onEntityCacheUpdated();
	}

	public long getExperience() {
		return experience;
	}

	public void setExperience(long experience, long experience_this, long experience_next, int level) {
		this.experience = experience;
		this.experience_this = experience_this;
		this.experience_next = experience_next;
		this.level  = level;
		
		showMessage("Experience: " + experience + "(level " + level + ")");
	}
	
	public void setHealth(int health, int maxHealth) {
		this.health = health;
		this.maxHealth = maxHealth;
	}

	public int getHealth() {
		return health;
	}

	public int getMaxHealth() {
		return maxHealth;
	}
	
	public void sendItemApply(long itemId) {
		sendMessage(new EntityReferencePacket(PacketType.ITEMAPPLY, itemId));
	}
	
	public void sendItemGet(long itemId, int quantity) {
		sendMessage(new ItemGetPacket(itemId, quantity));
	}
	
	protected void handleContainer(long id, List<ContainerItem> itemList) {
		containerCache.put(id, itemList);
		
		onContainerChanged(id);
	}
	
	protected void clearContainerCache() {
		containerCache.clear();
		onContainerChanged(0);
	}
	
	public List<ContainerItem> getContainerItems(long id) {
		if (containerCache.containsKey(id)) {
			return containerCache.get(id);
		}
		return new LinkedList<ContainerItem>();
	}

	public int getLevel() {
		return level;
	}
	
	public void sendLook(int x, int y) {
		sendMessage(new CoordinatePacket(PacketType.LOOKAT, x, y));
	}
	
	public void sendExamineItem(long id) {
		sendMessage(new EntityReferencePacket(PacketType.EXAMINEITEM, id));
	}
	
	/**
	 * Check whether the file exists, if not, request it from server.
	 * If it exists, send the sha1 to server to get an updated one if it does not match.
	 * 
	 * @return true if file exists
	 */
	public boolean checkFile(String filename) throws Exception {
		logger.debug("Checking file: " + filename);
		File f = new File(PathConfig.getRootDir() + filename);
		if (f.exists()) {
			byte[] sha1 = Sha1Util.getSha1ByteArrayForFile(PathConfig.getRootDir() + filename);
			sendCheckFile(filename, sha1);
			return true;
		}
		else {
			sendGetFile(filename);
			return false;
		}
	}
	
	/**
	 * Request the given filename from server.
	 */
	public void sendGetFile(String filename) {
		sendMessage(new TextPacket(PacketType.GETFILE, filename));
	}
	
	public void sendCheckFile(String filename, byte[] sha1) {
		sendMessage(new CheckFilePacket(filename, sha1));
	}
	
	public void sendGetImage(int faceId) {
		sendMessage(new GetImagePacket(faceId));
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public boolean isAttackMode() {
		return attackMode;
	}

	public void setAttackMode(boolean attackMode) {
		this.attackMode = attackMode;
	}
	
	public void sendUse(int x, int y) {
		sendMessage(new CoordinatePacket(PacketType.USE, x, y));
	}

	public long getExperienceNext() {
		return experience_next;
	}
	
	public long getExperienceThis() {
		return experience_this;
	}
	
	public void sendMapClicked(int x, int y) {
		sendMessage(new CoordinatePacket(PacketType.MAPCLICKED, x, y));
	}
	
	public void showChat(String who, String text, ChatType chatType) {
		chatLogger.info(chatType + " <" + who + "> " + text);
	}
	
	/**
	 * Called when the party has changed.
	 */
	public void onPartyChanged() {
	}

	public Map<Long, Point> getPartyPositions() {
		return partyPositions;
	}

	public List<Long> getPartyMembers() {
		return partyMembers;
	}
	
	public void sendMovementVector(int dx, int dy) {
		sendMessage(new MovementVectorPacket(dx, dy));
	}
	
	protected void onContainerClosed(long id) {}
	
	public void setPartyMarker(String map, int x, int y) {
		marker_map = map;
		marker_x = x;
		marker_y = y;
		
		onPartyChanged();
	}
	
	public boolean isMarkerOnSameMap() {
		return (marker_map != null && getInstanceName().equals(marker_map));
	}
	
	public String getMarkerMap() {
		return marker_map;
	}
	
	public int getMarkerX() {
		return marker_x;
	}
	
	public int getMarkerY() {
		return marker_y;
	}
	
	public long getRequestedtarget() {
		return requestedTarget;
	}
	
	public void requestTarget(long id) {
		if (requestedTarget != id) {
			requestedTarget = id;
			sendSetTarget(id);
		}
	}

	@Override
	protected void onDisconnect() {
		super.onDisconnect();
		
		requestedTarget = -1;
		faceMapper.clear();
		requestedBackgrounds.clear();
		requestedImages.clear();
	}
	
	public void setMaster(boolean master) {
		this.master = master;
		onMapChanged();
	}
	
	public boolean isMaster() {
		return master;
	}
	
	protected void offerQuest(int questId, String title, String description) {}

	/**
	 * Allow adding the client as an ActionListener. The command should be a client command sent to server.
	 */
	public void actionPerformed(ActionEvent event) {
		sendCommand(event.getActionCommand());
	}
	
	public void handleCommand(String command) {
		// Handle any client side commands
		String[] parts = command.split(" +");
		String rest = command.substring(parts[0].length());
		
		// Commands when not connected
		if ("connect".equals(parts[0])) {
			String host = "localhost";
			int port = 7080;
			String serverport = System.getProperty("jfealdia.defaultserver");
			if (parts.length > 1) {
				serverport = parts[1];
			}
			if (serverport.contains(":")) {
				String[] dst = serverport.split(":");
				host = dst[0];
				port = Integer.parseInt(dst[1]);
			} else {
				host = parts[1];
			}
			connect(host, port);
		}
		else if ("fullscreen".equals(parts[0])) {
			toggleFullScreen();
		}
		else if ("screenshot".equals(parts[0])) {
			String filename = null;
			if (parts.length > 1) {
				filename = parts[1] + ".png";
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_kkmmss");
				String stamp = sdf.format(new Date());
				filename = "client-" + stamp + ".png";
			}

			String shotDir = PathConfig.getClientScreenshotDir();
			new File(shotDir).mkdirs();
			String fullpath = shotDir + filename;
			if (saveScreenshot(fullpath)) {
				showMessage("Screenshot saved to " + fullpath);
			}
		}
		
		// Connected
		else if (isConnected()) {
			if ("clear".equals(parts[0])) {
				clearText();
			}
			else if ("disconnect".equals(parts[0])) {
				disconnect();
			}
			else if ("g".equals(parts[0])) {
				sendChat(rest, ChatType.GUILD);
			}
			else if ("login".equals(parts[0])) {
				onLoginRequested();
			}
			else if ("p".equals(parts[0])) {
				sendChat(rest, ChatType.PARTY);
			}
			else if ("whisper".equals(parts[0])) {
				sendChat(rest, ChatType.WHISPER);
			}
			else if ("shout".equals(parts[0])) {
				sendChat(rest, ChatType.SHOUT);
			}
			else if ("say".equals(parts[0])) {
				sendChat(rest, ChatType.SAY);
			}
			else {
				// Send command to server
				sendCommand(command);
			}
		}
	}

	protected void clearText() {}

	protected void toggleFullScreen() {
	}

	protected boolean saveScreenshot(String fullpath) {
		return false;
	}

	public void handleConsoleLine(String command) {
		if (command.startsWith("/")) {
			handleCommand(command.substring(1));
		}
		// Send as "default" chattype
		else {
			if (command.length() > 0) {
				sendChat(command, ChatType.DEFAULT);
			}
		}
	}
	
	private void runHook(String hook) {
		logger.debug("Checking hook for " + hook);
		File f = new File (PathConfig.getUserHookDir() + hook);
		if (f.exists()) {
			logger.debug("Executing commands for hook " + hook);
			try {
				BufferedReader in = new BufferedReader(new FileReader(f));
				String line;
				while ((line = StreamUtil.readLine(in)) != null) {
					handleCommand(line);
				}
			} catch (FileNotFoundException e) {
				logger.warn(e);
			} catch (IOException e) {
				logger.warn(e);
			}
		}
	}

	abstract protected void onQuestLogReceived(List<ProtoQuest> quests);

	public String getServerName() {
		return serverName;
	}

	public FaceMapper getFaceMapper() {
		return faceMapper;
	}

	protected void sendGetBackgroundFaceInfo(int backgroundId) {
		sendMessage(new GetBackgroundFaceInfoPacket(backgroundId));
	}

	public void requestBackgroundFaceInfo(int backgroundId) {
		if (requestedBackgrounds.contains(backgroundId)) {
			return;
		}

		logger.debug("Requesting FaceInfo for missing background: " + backgroundId);
		requestedBackgrounds.add(backgroundId);
		sendGetBackgroundFaceInfo(backgroundId);
	}

	protected void onFloorChanged() {
		List<ContainerItem> items = new LinkedList<ContainerItem>();
		synchronized (getEntityCache()) {
			for (Long id : getEntityCache().keySet()) {
				if (id == 0 || !isEntityComplete(id)) {
					continue;
				}
				ClientEntity info = getEntityInfo(id);
				int en_x = info.getX();
				int en_y = info.getY();
				//String type = (String) info.get("type");
				boolean is_item = info.isItem();

				// Add only items at our position to the container
				if (getX() == en_x && getY() == en_y && is_item) {
					int quantity = info.getQuantity();
					items.add(new ContainerItem(id, info.getName(), info.getFace(), quantity, false));
				}
			}
		}
		handleContainer(-1, items);
	}

	protected boolean handleEasyplay() {
		// Easyplay: login automatically, either using saved login details or by creating new on server
		if (System.getProperty("jfealdia.easyplay") != null) {
			LoginDetails loginDetails = passwordStore.getLoginDetails(getServerName());
			System.clearProperty("jfealdia.easyplay"); // To avoid an infinite loop with server on login failure
			if (loginDetails != null) {
				// if username/password saved on client -> use them to log in automatically
				login(loginDetails.getUsername(), loginDetails.getPassword());
				return true;
			}
			else {
				// if no username/password saved -> generate new on server side
				sendSimpleMessage(PacketType.EASYPLAY);
				return true;
			}
		}
		return false;
	}

	protected void onLoginRequested() {
		if (!handleEasyplay()) {
			askLogin();
		}
	}

	public PasswordStore getPasswordStore() {
		return passwordStore;
	}

	public void onImageMissing(int faceId) {
		if (!requestedImages.contains(faceId)) {
			logger.debug("Requesting missing image: " + faceId);
			requestedImages.add(faceId);
			sendGetImage(faceId);
		}
	}

	public String getImageFilename(String faceName) {
		return ImageMapper.getInstance().getImageFilename(faceName);
	}

	protected void onFaceReceived() {
		// Redraw containers
		onContainerChanged(0);
		onFloorChanged();
	}

	protected void onMusicHintReceived(MusicHint musicHint) {
	}
}
