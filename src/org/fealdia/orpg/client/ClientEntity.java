package org.fealdia.orpg.client;

import java.awt.Color;


/**
 * Client's idea of a map entity. This is a convenience wrapper for Map<String,Object>.
 * 
 * This class must be properly synchronized.
 */
public class ClientEntity {
	//private static Logger logger = Logger.getLogger(ClientEntity.class);
	
	private int face = -1;
	
	private Status status = Status.INIT;
	
	private int x = -1;
	private int y = -1;
	
	private int layer = 0;
	
	private boolean isCreature = false;
	private boolean isItem = false;
	
	private int quantity = 0;
	
	private String name = null;
	private Color nameColor = Color.PINK;
	
	private String type = null;
	
	private String effectFace = null;
	private long effectExpires = 0;

	private int health = 0;
	
	private String guild = null; // Guild tag for players
	
	private String overlay = null;
	
	public enum Status {
		INIT,
		REQUESTED,
		COMPLETE,
	}

	public synchronized int getFace() {
		return face;
	}
	
	public synchronized int getX() {
		return x;
	}
	
	public synchronized int getY() {
		return y;
	}
	
	public synchronized int getLayer() {
		return layer;
	}
	
	public synchronized boolean isItem() {
		return isItem;
	}
	
	public synchronized Status getStatus() {
		return status;
	}

	public synchronized void setStatus(Status status) {
		this.status = status;
	}
	
	public synchronized boolean isComplete() {
		return getStatus() == Status.COMPLETE;
	}

	public synchronized void setFace(int face) {
		this.face = face;
	}
	
	public synchronized void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public synchronized void setLayer(int layer) {
		this.layer = layer;
	}

	public synchronized void setItem(boolean isItem) {
		this.isItem = isItem;
	}

	public synchronized int getQuantity() {
		return quantity;
	}

	public synchronized void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public synchronized String getName() {
		return name;
	}

	public synchronized void setName(String name) {
		this.name = name;
	}

	public synchronized String getType() {
		return type;
	}

	public synchronized void setType(String type) {
		this.type = type;
	}
	
	public synchronized boolean hasEffect() {
		long stamp = effectExpires;
		long now = System.currentTimeMillis();
		return (now < stamp);
	}
	
	public synchronized void addEffect(String face, int time, String text) {
		effectFace = face;
		effectExpires = System.currentTimeMillis() + time;
	}

	public synchronized String getEffectFace() {
		return effectFace;
	}

	public synchronized void setEffectFace(String effectFace) {
		this.effectFace = effectFace;
	}
	
	public synchronized boolean isPlayer() {
		return "player".equals(getType());
	}

	public void setHealth(int pct) {
		this.health = pct;
	}
	
	/**
	 * Get health in percentage (0-100).
	 */
	public int getHealth() {
		return health;
	}

	public boolean isCreature() {
		return isCreature;
	}

	public synchronized void setCreature(boolean isCreature) {
		this.isCreature = isCreature;
	}

	public String getGuild() {
		return guild;
	}

	public void setGuild(String guild) {
		this.guild = guild;
	}
	
	public String getVisibleName() {
		if (getGuild() != null) {
			return "[" + getGuild() + "]" + getName();
		}
		return getName();
	}

	public Color getNameColor() {
		return nameColor;
	}

	public void setNameColor(Color nameColor) {
		this.nameColor = nameColor;
	}

	public String getOverlay() {
		return overlay;
	}

	public void setOverlay(String overlay) {
		this.overlay = overlay;
	}
	
}
