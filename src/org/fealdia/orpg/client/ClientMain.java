package org.fealdia.orpg.client;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.fealdia.orpg.common.util.PathConfig;

public class ClientMain {
	// NOTE: do NOT use log4j logger here, "log4j.configuration" property needs to be set before loading log4j

	public static void main(String[] args) {
		boolean themed = true;
		System.setProperty("jfealdia.defaultserver", "localhost:7080");

		// Change root directory to ~/.jfealdia/
		PathConfig.initUserRootDir();
		
		for (int i = 0; i < args.length; i++) {
			if ("-notheme".equals(args[i])) {
				themed = false;
			}
			else if ("-webstart".equals(args[i])) {
				System.setProperty("log4j.configuration", "log4j-client.properties");
			}
			else if ("-defaultserver".equals(args[i])) {
				System.setProperty("jfealdia.defaultserver", args[i+1]);
				i++;
			}
			else if ("-easyplay".equals(args[i])) {
				System.setProperty("jfealdia.easyplay", "1");
			}
			else if ("-wipecache".equals(args[i])) {
				PathConfig.wipeUserCache();
			}
		}
		
		if (themed) {
			//JFrame.setDefaultLookAndFeelDecorated(true);
			JDialog.setDefaultLookAndFeelDecorated(true);
			try {
				UIManager.setLookAndFeel("org.jvnet.substance.skin.SubstanceEmeraldDuskLookAndFeel");
			}
			catch (Exception e) {}
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new ClientFrame();
				
			}
		});
	}

}
