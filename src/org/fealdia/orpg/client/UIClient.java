package org.fealdia.orpg.client;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.ui.ContainerView;
import org.fealdia.orpg.client.ui.ContainerViewListener;
import org.fealdia.orpg.client.ui.CustomTextArea;
import org.fealdia.orpg.client.ui.LoginDialog;
import org.fealdia.orpg.client.ui.PlayView;
import org.fealdia.orpg.client.ui.QuestLogDialog;
import org.fealdia.orpg.client.ui.QuestOfferDialog;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.ImageMissingListener;
import org.fealdia.orpg.common.net.data.ChatType;
import org.fealdia.orpg.common.net.data.ProtoQuest;
import org.fealdia.orpg.common.net.packets.ContainerPacket.ContainerItem;
import org.fealdia.orpg.common.ui.SceneMinimap;
import org.fealdia.orpg.common.ui.SceneMinimapListener;
import org.fealdia.orpg.common.util.ScreenshotUtil;

/**
 * UI functionality on top of HeadlessClient.
 * 
 * This class also handles arrow movement keys in a really idiotic manner thanks to the folks at Sun being not able to admit they have a serious flaw:
 * 
 * http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=5011907
 * 
 * Needless to say this kind of "best guess" code is prone to breakage.
 */
public class UIClient extends HeadlessClient implements KeyListener, ContainerViewListener, ImageMissingListener, SceneMinimapListener {
	private static Logger logger = Logger.getLogger(UIClient.class);
	
	private static Color[] PartyMemberMarkerColors = { Color.BLUE, Color.WHITE, Color.MAGENTA, Color.YELLOW };
	
	private PlayView playView = null;
	
	private CustomTextArea generalTextArea = null;
	
	private int moveVectorX = 0;
	private int moveVectorY = 0;
	
	private SceneMinimap minimap = null;
	
	private ClientFrame clientFrame = null;
	
	private ContainerView activeContainer = null;
	
	private ContainerView inventoryContainer = null;
	
	private ContainerView floorContainer = null;
	
	private CustomTextArea chatTextArea;
	
	public UIClient() {
		ImageMapper.getInstance().setImageMissingListener(this);
	}
	
	@Override
	protected void askLogin() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				LoginDialog dialog = new LoginDialog(getClientFrame(), passwordStore, getServerName());
				if (dialog.askLogin()) {
					login(dialog.getUsername(), dialog.getPassword());
				}
			}
		});
	}

	public PlayView getPlayView() {
		return playView;
	}

	public void setPlayView(PlayView playView) {
		this.playView = playView;
		
		playView.setClient(this);
	}

	@Override
	protected void onMapChanged() {
		playView.setScene(getScene());
		playView.setPosition(getX(), getY());
		//getPlayView().repaint();
		
		getMinimap().setScene(getScene());
		updateMinimapMarkers();
		getMinimap().centerMinimapOn(getX(), getY());
	}

	private void updateMinimapMarkers() {
		getMinimap().clearMarkers();
		getMinimap().addMarker(getX(), getY(), Color.RED);
		
		// add party member markers
		Map<Long,Point> positions = getPartyPositions();
		List<Long> members = getPartyMembers();
		//for (Map.Entry<Long, Point> e : positions.entrySet()) {
		int index = 0;
		for (Long id : members) {
			Point p = positions.get(id);
			if (p != null) {
				getMinimap().addMarker(p.x, p.y, PartyMemberMarkerColors[index]);
				index++;
				if (index >= PartyMemberMarkerColors.length) {
					index = 0;
				}
			}
		}
		
		// add party marker (if in the same instance)
		if (isMarkerOnSameMap()) {
			getMinimap().addMarker(getMarkerX(), getMarkerY(), Color.BLACK);
		}
		
		getMinimap().repaint();
	}

	@Override
	protected void showMessage(String text) {
		String[] parts = text.split("\n");
		for (String p : parts) {
			addGeneralText(p);
		}
	}

	public CustomTextArea getGeneralTextArea() {
		return generalTextArea;
	}

	public void setGeneralTextArea(CustomTextArea generalTextArea) {
		this.generalTextArea = generalTextArea;
	}
	
	public void addGeneralText(String text) {
		getGeneralTextArea().addText(text);
	}

	public void keyPressed(KeyEvent event) {
		keyReallyPressed(event);
	}

	public void keyReleased(KeyEvent event) {
		//logger.debug("Released: " + event.getKeyCode());
		keyReallyReleased(event.getKeyCode());
	}
	public void keyTyped(KeyEvent event) {}
	
	public void keyReallyPressed(KeyEvent event) {
		//logger.debug("keyPressed " + KeyEvent.getKeyText(event.getKeyCode()) + " (" + event.getKeyCode() + ")");
		//logger.debug(": " + event);
		
		int oldX = moveVectorX;
		int oldY = moveVectorY;
		
		// Movement
		switch (event.getKeyCode()) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_A:
			moveVectorX = -1;
			break;
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_D:
			moveVectorX = 1;
			break;
		case KeyEvent.VK_UP:
		case KeyEvent.VK_W:
			moveVectorY = -1;
			break;
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_S:
			moveVectorY = 1;
			break;
		case KeyEvent.VK_CONTROL:
			Cursor cur = new Cursor(Cursor.HAND_CURSOR);
			getPlayView().setCursor(cur);
			getActiveContainer().setCursor(cur);
			getInventoryContainer().setCursor(cur);
			getFloorContainer().setCursor(cur);
			break;
		default:
			return;
		}
		
		// Only send the vector if it has changed
		if (oldX != moveVectorX || oldY != moveVectorY) {
			sendMovementVector();
		}
	}
	
	public void keyReallyReleased(int keyCode) {
		//logger.debug("key really released = " + keyCode);
		
		// Movement
		switch (keyCode) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_A:
		case KeyEvent.VK_D:
			moveVectorX = 0;
			break;
		case KeyEvent.VK_UP:
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_W:
		case KeyEvent.VK_S:
			moveVectorY = 0;
			break;
		case KeyEvent.VK_CONTROL:
			Cursor cur = new Cursor(Cursor.DEFAULT_CURSOR);
			getPlayView().setCursor(cur);
			getInventoryContainer().setCursor(cur);
			getFloorContainer().setCursor(cur);
			break;
		default:
			return;
		}
		
		sendMovementVector();
	}
	
	public void sendMovementVector() {
		if (!isConnected()) {
			return;
		}
		logger.trace("Sending vector dx = " + moveVectorX + " dy = " + moveVectorY);
		
		sendMovementVector(moveVectorX, moveVectorY);
	}
	
	@Override
	protected void onEntityCacheUpdated() {
		//getPlayView().repaint();
	}

	public SceneMinimap getMinimap() {
		return minimap;
	}

	public void setMinimap(SceneMinimap minimap) {
		this.minimap = minimap;
	}

	public ClientFrame getClientFrame() {
		return clientFrame;
	}

	public void setClientFrame(ClientFrame clientFrame) {
		this.clientFrame = clientFrame;
	}

	@Override
	public void setExperience(long experience, long experience_this, long experience_next, int level) {
		super.setExperience(experience, experience_this, experience_next, level);
		
		getClientFrame().setExperience(experience, experience_this, experience_next, level);
	}

	@Override
	public void setHealth(int health, int maxHealth) {
		super.setHealth(health, maxHealth);
		
		getClientFrame().setHealth(health, maxHealth);
	}

	public ContainerView getInventoryContainer() {
		return inventoryContainer;
	}

	public void setInventoryContainer(ContainerView inventoryContainer) {
		this.inventoryContainer = inventoryContainer;
		
		inventoryContainer.setListener(this);
	}

	public ContainerView getFloorContainer() {
		return floorContainer;
	}

	public void setFloorContainer(ContainerView floorContainer) {
		this.floorContainer = floorContainer;
		
		floorContainer.setListener(this);
	}

	public void actionItemApplied(ContainerView view, long item_id) {
		sendItemApply(item_id);
	}

	@Override
	protected void onFloorChanged() {
		// Update floor ContainerView
		getFloorContainer().clearItems();
		synchronized (getEntityCache()) {
			for (Long id : getEntityCache().keySet()) {
				if (id == 0 || !isEntityComplete(id)) {
					continue;
				}
				ClientEntity info = getEntityInfo(id);
				int en_x = info.getX();
				int en_y = info.getY();
				//String type = (String) info.get("type");
				boolean is_item = info.isItem();

				// Add only items at our position to the container
				if (getX() == en_x && getY() == en_y && is_item) {
					int quantity = info.getQuantity();
					getFloorContainer().addItem(new ContainerItem(id, info.getName(), info.getFace(), quantity, false));
				}
			}
		}
	}

	public void actionItemGet(ContainerView view, long item_id, int quantity) {
		sendItemGet(item_id, quantity);
	}

	@Override
	protected void onContainerChanged(long id) {
		// Inventory
		if (id == 0) {
			updateContainerView(id, getInventoryContainer());
		}
		// Active container
		else {
			updateContainerView(id, getActiveContainer());
			getActiveContainer().setVisible(true);
		}
	}
	
	@Override
	protected void onContainerClosed(long id) {
		getActiveContainer().setVisible(false);
	}
	
	private void updateContainerView(long container_id, ContainerView view) {
		List<ContainerItem> itemList = getContainerItems(container_id);
		view.setItems(itemList);
	}

	@Override
	protected void onPlayerInfoChanged() {
		getClientFrame().setPlayerName(getEntityInfo(0).getName());
	}

	public void actionItemExamined(ContainerView containerView, long id) {
		sendExamineItem(id);
	}

	public void onImageMissing(String name) {
		int faceId = getFaceMapper().getFaceByName(name).id;
		super.onImageMissing(faceId);
	}

	public void onBackgroundMissing(int backgroundId) {
		requestBackgroundFaceInfo(backgroundId);
	}

	public void onMinimapClicked(int x, int y) {
		sendMapClicked(x, y);
	}

	public void setChatTextArea(CustomTextArea chatTextArea) {
		this.chatTextArea = chatTextArea;
	}

	@Override
	public void showChat(String who, String text, ChatType chattype) {
		super.showChat(who, text, chattype);
		
		String line = null;
		
		if ("emote".equals(chattype)) {
			line = "* " + who + " " + text;
		} else {
			line = chattype + " <" + who + "> " + text;
		}
		
		chatTextArea.addText(line);
		showMessage(line);
	}

	@Override
	public void onPartyChanged() {
		updateMinimapMarkers();
	}

	public ContainerView getActiveContainer() {
		return activeContainer;
	}

	public void setActiveContainer(ContainerView activeContainer) {
		this.activeContainer = activeContainer;
		
		activeContainer.setListener(this);
	}
	
	@Override
	protected void onDisconnect() {
		super.onDisconnect();
		showMessage("Disconnected from server");
	}

	@Override
	protected void showPopup(final String text) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				final JTextArea area = new JTextArea(text);
				JScrollPane scroller = new JScrollPane(area);
				scroller.setPreferredSize(new Dimension(500, 300));
				JOptionPane.showMessageDialog(null, scroller);
			}
		});
	}
	
	@Override
	protected void offerQuest(final int questId, final String title, final String description) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				QuestOfferDialog dialog = new QuestOfferDialog(UIClient.this, questId, getClientFrame(), title, description);
				dialog.setVisible(true);
			}
		});
	}
	
	@Override
	protected boolean saveScreenshot(String fullpath) {
		return ScreenshotUtil.saveScreenshot(getClientFrame(), fullpath);
	}
	
	@Override
	protected void clearText() {
		getClientFrame().clearText();
	}

	@Override
	protected void onQuestLogReceived(final List<ProtoQuest> quests) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new QuestLogDialog(getClientFrame(), quests).setVisible(true);
			}
		});
	}
}
