package org.fealdia.orpg.client;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.fealdia.orpg.client.ui.LoginDetails;

/**
 * Store for player login details on game servers.
 */
public class PasswordStore {
	private static final String PASSWORD = "password";
	private static final String USERNAME = "username";

	public void clearLoginDetails(final String server) throws BackingStoreException {
		getPrefs(server).clear();
	}

	/**
	 * Get stored login details for the given server, or return null if none.
	 */
	public LoginDetails getLoginDetails(final String server) {
		Preferences prefs = getPrefs(server);
		if (prefs.get(USERNAME, null) != null) {
			return new LoginDetails(prefs.get(USERNAME, null), prefs.get(PASSWORD, null));
		}
		return null;
	}

	public void storeLoginDetails(final String server, final LoginDetails loginDetails) throws BackingStoreException {
		Preferences prefs = getPrefs(server);
		prefs.put(USERNAME, loginDetails.getUsername());
		prefs.put(PASSWORD, loginDetails.getPassword());
		prefs.sync();
	}

	private Preferences getPrefs(final String server) {
		return Preferences.userNodeForPackage(PasswordStore.class).node("PasswordStore/" + server);
	}
}
