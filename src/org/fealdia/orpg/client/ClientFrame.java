package org.fealdia.orpg.client;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.actions.FileActions;
import org.fealdia.orpg.client.actions.KeysAction;
import org.fealdia.orpg.client.ui.ClientImageMapper;
import org.fealdia.orpg.client.ui.ColoredBar;
import org.fealdia.orpg.client.ui.ContainerView;
import org.fealdia.orpg.client.ui.CustomTextArea;
import org.fealdia.orpg.client.ui.PlayView;
import org.fealdia.orpg.common.VersionInfo;
import org.fealdia.orpg.common.actions.AboutAction;
import org.fealdia.orpg.common.actions.ExitAction;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.ui.BindListener;
import org.fealdia.orpg.common.ui.BindManager;
import org.fealdia.orpg.common.ui.BindManagerGlue;
import org.fealdia.orpg.common.ui.JCommandLine;
import org.fealdia.orpg.common.ui.KeyListenerProxy;
import org.fealdia.orpg.common.ui.SceneMinimap;

/**
 * Main frame for the client.
 */
public class ClientFrame extends JFrame implements ActionListener, BindListener {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(ClientFrame.class);
	
	private UIClient headlessClient;

	private CustomTextArea generalTextArea;

	private JTextField commandField;
	
	private SceneMinimap minimap;
	
	private JLabel experienceLabel;
	private JLabel healthLabel;
	private JLabel levelLabel;

	private ColoredBar healthBar;

	private JLabel nameLabel;

	private ColoredBar experienceBar;

	private CustomTextArea chatTextArea;

	public ClientFrame() {
		ImageMapper.setInstance(ClientImageMapper.getInstance());
		headlessClient = new UIClient();

		setTitle("JFealdia Client " + VersionInfo.getVersion());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		getClient().setClientFrame(this);
		
		createMenu();
		
		// Create UI elements
		JSplitPane mainPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		
		// Top: main window, character info pane, inventory
		
		JSplitPane topSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		mainPane.setTopComponent(topSplitPane);
		
		// Create PlayView
		JPanel pvPanel = new JPanel();
		PlayView pv = new PlayView(headlessClient.getFaceMapper());
		pvPanel.add(pv);
		getClient().setPlayView(pv);
		topSplitPane.setLeftComponent(pvPanel);
		
		// Right part of the splitpane
		JPanel rightPane = new JPanel();
		rightPane.setLayout(new BoxLayout(rightPane, BoxLayout.Y_AXIS));
		
		// Characters stats
		JPanel statsPane = new JPanel();
		statsPane.setLayout(new BoxLayout(statsPane, BoxLayout.Y_AXIS));
		statsPane.setMinimumSize(new Dimension(300, 150));
		//statsPane.setPreferredSize(new Dimension(300, 150));
		//statsPane.setMaximumSize(new Dimension(600, 150));
		//statsPane.setBackground(new Color(100, 255, 100));
		statsPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		nameLabel = new JLabel("Name: player");
		statsPane.add(nameLabel);
		levelLabel = new JLabel("Level: 1");
		statsPane.add(levelLabel);
		healthLabel = new JLabel("Health: ?/?");
		statsPane.add(healthLabel);

		healthBar = new ColoredBar();
		healthBar.setPosition(80);
		healthBar.setPreferredSize(new Dimension(300, 20));
		healthBar.setMaximumSize(new Dimension(600, 20));
		statsPane.add(healthBar);
		
		experienceLabel = new JLabel("Experience: 0");
		statsPane.add(experienceLabel);
		
		experienceBar = new ColoredBar(Color.YELLOW, new Color(180, 180, 0));
		experienceBar.setPreferredSize(new Dimension(300, 20));
		experienceBar.setMaximumSize(new Dimension(600, 20));
		statsPane.add(experienceBar);
		
		rightPane.add(statsPane);
		
		// Active container
		ContainerView activeView = new ContainerView(headlessClient.getFaceMapper());
		//activeView.setPreferredSize(new Dimension(200, 150));
		//activeView.setMaximumSize(new Dimension(600, 150));
		activeView.setBackground(Color.BLACK);
		activeView.setVisible(false);
		
		JScrollPane activeScroller = new JScrollPane(activeView);
		//activeScroller.setPreferredSize(new Dimension(200, 150));
		activeScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		activeScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		activeScroller.addComponentListener(activeView);
		
		rightPane.add(activeScroller);
		getClient().setActiveContainer(activeView);
		
		// Inventory
		ContainerView inventoryView = new ContainerView(headlessClient.getFaceMapper());
		//inventoryView.setPreferredSize(new Dimension(200, 150));
		//inventoryView.setMaximumSize(new Dimension(600, 150));
		inventoryView.setBackground(Color.BLACK);
		
		JScrollPane inventoryScroller = new JScrollPane(inventoryView);
		//inventoryScroller.setPreferredSize(new Dimension(200, 150));
		inventoryScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		inventoryScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		inventoryScroller.addComponentListener(inventoryView);

		rightPane.add(inventoryScroller);
		getClient().setInventoryContainer(inventoryView);
		
		// Floor
		ContainerView floorView = new ContainerView(headlessClient.getFaceMapper());
		//floorView.setPreferredSize(new Dimension(200, 150));
		//floorView.setMaximumSize(new Dimension(600, 150));
		floorView.setBackground(Color.BLACK);
		
		JScrollPane floorScroller = new JScrollPane(floorView);
		//floorScroller.setPreferredSize(new Dimension(200, 150));
		floorScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		floorScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		floorScroller.addComponentListener(floorView);
		
		rightPane.add(floorScroller);
		getClient().setFloorContainer(floorView);
		
		// minimap
		minimap = new SceneMinimap();
		minimap.setListener(headlessClient);
		JScrollPane minimapScroller = new JScrollPane(minimap);
		minimapScroller.setPreferredSize(new Dimension(150, 150));
		minimapScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		minimapScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		getClient().setMinimap(minimap);
		rightPane.add(minimapScroller);
		
		topSplitPane.setRightComponent(rightPane);
		
		// Bottom
		JPanel bottomPane = new JPanel();
		bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.Y_AXIS));
		
		JTabbedPane bottomTab = new JTabbedPane();
		bottomTab.setPreferredSize(new Dimension(800, 600));
		
		generalTextArea = new CustomTextArea();
		getClient().setGeneralTextArea(generalTextArea);
		bottomTab.add("General", new JScrollPane(generalTextArea));
		
		chatTextArea = new CustomTextArea();
		getClient().setChatTextArea(chatTextArea);
		bottomTab.add("Chat", new JScrollPane(chatTextArea));
		
		bottomPane.add(bottomTab);
		mainPane.setBottomComponent(bottomPane);
		
		commandField = new JCommandLine();
		commandField.setPreferredSize(new Dimension(600, 20));
		commandField.addActionListener(this);
		bottomPane.add(commandField);

		JPanel rootPane = new JPanel();
		rootPane.setLayout(new BoxLayout(rootPane, BoxLayout.Y_AXIS));
		rootPane.add(mainPane);
		add(rootPane);
		
		setPreferredSize(new Dimension(1024, 910));
		pack();
		
		mainPane.setDividerLocation(0.8);
		
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Thread ct = new Thread(getClient());
		ct.setName("Client");
		ct.start();
		
		getRootPane().registerKeyboardAction(this, "enter", KeyStroke.getKeyStroke("ENTER"), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		getRootPane().registerKeyboardAction(this, "escape", KeyStroke.getKeyStroke("ESCAPE"), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		
		// Movement keybindings. Could perhaps bind to the PlayView only.
		//pv.addKeyListener(getClient());
		pv.addKeyListener(new KeyListenerProxy(getClient()));
		/*
		pv.registerKeyboardAction(getClient(), "left", KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), JComponent.WHEN_FOCUSED);
		pv.registerKeyboardAction(getClient(), "right", KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), JComponent.WHEN_FOCUSED);
		pv.registerKeyboardAction(getClient(), "up", KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), JComponent.WHEN_FOCUSED);
		pv.registerKeyboardAction(getClient(), "down", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), JComponent.WHEN_FOCUSED);
		*/
		
		// Focus PlayView initially
		pv.requestFocusInWindow();
		
		BindManager bm = new BindManager();
		bm.loadFromFile("client.binds");
		bm.addListener(this);
		BindManagerGlue glue = new BindManagerGlue(bm);
		pv.addKeyListener(new KeyListenerProxy(glue));
		
		logger.trace("Frame created");
		getClient().handleCommand("connect " + System.getProperty("jfealdia.defaultserver"));
	}

	protected void createMenu() {
		// Actions
		FileActions fileActions = new FileActions(this);
		ExitAction exitAction = new ExitAction();
		
		// Menu
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		// File
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('f');
		menuBar.add(fileMenu);
		fileMenu.add(fileActions.getConnectAction());
		fileMenu.addSeparator();
		fileMenu.add(exitAction);
		
		// Right align rest
		menuBar.add(Box.createHorizontalGlue());
		
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('h');
		menuBar.add(helpMenu);
		helpMenu.add(new KeysAction());
		helpMenu.addSeparator();
		helpMenu.add(new AboutAction());
	}

	public UIClient getClient() {
		return headlessClient;
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == commandField) {
			String command = event.getActionCommand();
			getClient().handleConsoleLine(command);
			commandField.setText("");
			getClient().getPlayView().requestFocus();
		}
		else if (event.getActionCommand().equals("enter")) {
			if (!commandField.hasFocus()) {
				commandField.requestFocusInWindow();
			} /* else {
				String command = commandField.getText();
				handleCommand(command);
				commandField.setText("");
				getClient().getPlayView().requestFocus();
			} */
		}
		else if (event.getActionCommand().equals("escape")) {
			if (commandField.hasFocus()) {
				commandField.setText("");
				getClient().getPlayView().requestFocus();
			}
		}
	}
	
	public SceneMinimap getMinimap() {
		return minimap;
	}
	
	public void setExperience(long experience, long experience_this, long experience_next, int level) {
		experienceLabel.setText("Experience: " + experience + "/" + experience_next);
		levelLabel.setText("Level: " + level);
		
		int pos = (int) (100 * (experience - experience_this) / (experience_next - experience_this));
		
		experienceBar.setPosition(pos);
	}
	
	public void setHealth(int health, int maxHealth) {
		healthLabel.setText("Health: " + health + "/" + maxHealth);
		
		healthBar.setPosition((Math.min(health, maxHealth) * 100) / maxHealth);
	}
	
	public void setPlayerName(String name) {
		nameLabel.setText("Name: " + name);
	}

	public void bindPressed(String bind, String command) {
		if (command.startsWith("!")) {
			// Prefill commandline
			commandField.setText(command.substring(1));
			commandField.requestFocus();
		} else {
			getClient().handleCommand(command);
		}
	}
	
	public void clearText() {
		generalTextArea.setText(null);
	}
}
