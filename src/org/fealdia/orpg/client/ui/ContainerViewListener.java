package org.fealdia.orpg.client.ui;

/**
 * Listens to actions on a ContainerView (apply, get etc.).
 */
public interface ContainerViewListener {
	public void actionItemApplied(ContainerView view, long item_id);
	
	public void actionItemExamined(ContainerView containerView, long id);
	
	public void actionItemGet(ContainerView view, long item_id, int quantity);
}
