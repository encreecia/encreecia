package org.fealdia.orpg.client.ui;

import java.awt.Font;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTextArea;

/**
 * A custom text area for the client.
 * 
 * This may eventually do some formatting for the text based on wiki-like markup syntax.
 */
public class CustomTextArea extends JTextArea {
	private static final long serialVersionUID = 1L;
	
	//private Style boldStyle = null;
	
	public CustomTextArea() {
		setEditable(false);
		setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
		
		/*
		StyledDocument doc = (StyledDocument) getDocument();
		
		boldStyle = doc.addStyle("bold", null);
		StyleConstants.setBold(boldStyle, true);
		*/
	}

	/**
	 * Adds one or more lines of text and scrolls to the bottom.
	 */
	public void addText(String text) {
		Format formatter = new SimpleDateFormat("HH:mm:ss");
		String stamp = formatter.format(new Date());

		String line = stamp + " " + text + "\n";
		
		//addStyledText(line, null);
		
		append(line);
		
		scrollToBottom();
	}
	
	public void scrollToBottom() {
		setCaretPosition(getDocument().getLength());
	}
	
	/*
	public void addStyledText(String text, Style style) {
		StyledDocument doc = (StyledDocument) getDocument();
		
		try {
			doc.insertString(doc.getLength(), text, style);
		} catch (BadLocationException e) {
		}
	}
	*/
}
