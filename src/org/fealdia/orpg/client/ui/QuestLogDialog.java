package org.fealdia.orpg.client.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fealdia.orpg.common.net.data.ProtoQuest;

@SuppressWarnings("serial")
public class QuestLogDialog extends JDialog implements ActionListener {
	private final JPanel contentPanel = new JPanel();
	private final List<ProtoQuest> quests;
	private JList titleList;
	private JTextArea descriptionTextArea;
	private JLabel questTitleLabel;

	/**
	 * Create the dialog.
	 */
	public QuestLogDialog(JFrame parent, final List<ProtoQuest> quests) {
		super(parent);
		getRootPane().registerKeyboardAction(this, "close", KeyStroke.getKeyStroke("ESCAPE"), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		getRootPane().registerKeyboardAction(this, "close", KeyStroke.getKeyStroke("L"), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		this.quests = quests;
		setTitle("Quest log");
		setBounds(100, 100, 700, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		{
			JSplitPane splitPane = new JSplitPane();
			contentPanel.add(splitPane);
			{
				JPanel panel = new JPanel();
				panel.setPreferredSize(new Dimension(250, 10));
				splitPane.setRightComponent(panel);
				panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
				{
					JPanel panel_1 = new JPanel();
					panel.add(panel_1);
					panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
					{
						questTitleLabel = new JLabel("(select quest)");
						panel_1.add(questTitleLabel);
					}
				}
				{
					JScrollPane scrollPane = new JScrollPane();
					scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
					panel.add(scrollPane);
					{
						descriptionTextArea = new JTextArea();
						scrollPane.setViewportView(descriptionTextArea);
					}
				}
				{
					JPanel buttonPanel = new JPanel();
					panel.add(buttonPanel);
					buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
					JButton btnAbandon = new JButton("Abandon");
					btnAbandon.setEnabled(false);
					buttonPanel.add(btnAbandon);
					{
						Component horizontalGlue = Box.createHorizontalGlue();
						buttonPanel.add(horizontalGlue);
					}
					{
						JButton btnShareQuest = new JButton("Share quest");
						btnShareQuest.setEnabled(false);
						buttonPanel.add(btnShareQuest);
					}
					{
						JButton btnClose = new JButton("Close");
						getRootPane().setDefaultButton(btnClose);
						btnClose.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								setVisible(false);
							}
						});
						buttonPanel.add(btnClose);
					}
				}
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setMinimumSize(new Dimension(200, 22));
				splitPane.setLeftComponent(scrollPane);
				{
					titleList = new JList(new QuestTitleListModel());
					titleList.addListSelectionListener(new ListSelectionListener() {
						public void valueChanged(ListSelectionEvent arg0) {
							int index = titleList.getSelectedIndex();
							ProtoQuest q = quests.get(index);
							questTitleLabel.setText(q.title);
							descriptionTextArea.setText(q.description);
						}
					});
					scrollPane.setViewportView(titleList);
				}
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
		}

		fillData();

		setLocationRelativeTo(null); // Center
		setModal(false);
	}

	private void fillData() {
		for (ProtoQuest quest : quests) {
			titleList.add(new JLabel((String) quest.title));
		}
	}

	private class QuestTitleListModel extends AbstractListModel {
		public Object getElementAt(int index) {
			return quests.get(index).title;
		}

		public int getSize() {
			return quests.size();
		}
	}

	public void actionPerformed(ActionEvent e) {
		if ("close".equals(e.getActionCommand())) {
			setVisible(false);
		}
	}
}
