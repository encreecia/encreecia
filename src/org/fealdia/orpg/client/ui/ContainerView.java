package org.fealdia.orpg.client.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.Scrollable;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.net.data.FaceMapper;
import org.fealdia.orpg.common.net.packets.ContainerPacket.ContainerItem;

/**
 * This allows viewing and selecting container items.
 * 
 * Examples: inventory, floor, containers
 */
public class ContainerView extends JComponent implements MouseListener, Scrollable, ComponentListener {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(ContainerView.class);
	
	private int selected = 0;
	
	private List<ContainerItem> items = new LinkedList<ContainerItem>();
	
	private ContainerViewListener listener = null;

	private FaceMapper faceMapper;
	
	public ContainerView(FaceMapper faceMapper) {
		this.faceMapper = faceMapper;
		setBackground(Color.green);
		setOpaque(true);
		
		addMouseListener(this);
	}
	
	private int getItemSize() {
		return ImageMapper.getInstance().getTileSize();
	}
	
	/**
	 * Calculate and resize to size required by the amount of items.
	 */
	private void adjustSize() {
		int w = 0;
		if (getParent() != null) {
			w = getParent().getWidth();
		} else {
			w = 200;
		}
		int items_per_row = w / getItemSize();
		int rows_needed = items.size() / items_per_row + 1;
		if (rows_needed < 1) {
			rows_needed = 1;
		}
		int h = rows_needed * getItemSize() + 1;
		
		setPreferredSize(new Dimension(w, h));
		setSize(new Dimension(w, h));
		
		logger.trace("adjustSize() items = " + items.size() + ", rows_needed = " + rows_needed + ", w = " + w + ", h = " + h);
		
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		int slotSize = ImageMapper.getInstance().getTileSize();
		
		g.setColor(getBackground());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		
		int slots_x = getWidth() / slotSize;
		int slots_y = getHeight() / slotSize;
		
		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
		
		//logger.debug("Slots x = " + slots_x + ", y = " + slots_y);
		
		for (int i = 0; i < slots_x; i++) {
			for (int j = 0; j < slots_y; j++) {
				g.setColor(Color.GRAY);
				g.drawRect(i*slotSize, j*slotSize, slotSize, slotSize);
				
				int numItem = j * slots_x + i;
				ContainerItem item;
				synchronized (items) {
					if (items.size() <= numItem) {
						continue;
					}
					item = items.get(numItem);
				}
				BufferedImage image = ImageMapper.getInstance().getImage(faceMapper.getFaceById(item.faceId).name);
				g.drawImage(image, i*slotSize, j*slotSize, null);
				
				// If quantity > 1 draw it
				FontMetrics metrics = g.getFontMetrics();
				int fontHeight = metrics.getHeight();
				if (item.quantity > 1) {
					g.setColor(Color.WHITE);
					g.drawString(String.valueOf(item.quantity), i * slotSize + 2, j * slotSize + fontHeight);
				}
				
				// If equipped, draw 'A'
				if (item.equipped) {
					int aWidth = metrics.charWidth('A');
					g.setColor(Color.GREEN);
					g.drawString("A", i*slotSize + slotSize - aWidth - 2, j*slotSize + slotSize);
				}
				
				if (numItem == getSelected()) {
					g.setColor(Color.GREEN);
					g.drawRect(i*slotSize, j*slotSize, slotSize - 1, slotSize - 1);
				}
			}
		}
	}

	public int getSelected() {
		return selected;
	}

	public void setSelected(int selected) {
		this.selected = selected;
		
		repaint();
	}
	
	public void clearItems() {
		items.clear();
		
		adjustSize();
		repaint();
	}

	public void addItem(ContainerItem containerItem) {
		items.add(containerItem);

		adjustSize();
		repaint();
	}

	public void mouseClicked(MouseEvent event) {
		int slotSize = ImageMapper.getInstance().getTileSize();
		int slots_x = getWidth() / slotSize;
		//int slots_y = getHeight() / slotSize;

		int x = (event.getX() / slotSize);
		int y = (event.getY() / slotSize);
		int selected = y * slots_x + x;
		
		if (selected >= items.size()) {
			return;
		}
		
		// ctrl+left: examine
		if (event.getButton() == MouseEvent.BUTTON1 && (event.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0) {
			logger.trace("Ctrl+Button 1 on " + selected);
			actionExamine(selected);
		}
		// left: select
		else if (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 1) {
			logger.trace("Button 1 on " + selected);
			setSelected(selected);
		}
		// middle or left doubleclick: apply
		else if (event.getButton() == MouseEvent.BUTTON2 || (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 2)) {
			logger.trace("Button 2 on " + selected);
			actionApply(selected);
		}
		// right: get/drop
		else if (event.getButton() == MouseEvent.BUTTON3) {
			logger.trace("Button 3 on " + selected);
			actionGet(selected);
		}
	}

	private void actionExamine(int item_number) {
		long id = getItemId(item_number);
		
		// Notify listener
		if (listener != null) {
			listener.actionItemExamined(this, id);
		}
	}

	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}
	
	public long getItemId(int position) {
		return items.get(position).id;
	}
	
	public void actionApply(int item_number) {
		long id = getItemId(item_number);
		
		logger.trace("Apply action on id = " + id);
		
		// Notify listener
		if (listener != null) {
			listener.actionItemApplied(this, id);
		}
	}
	
	public void actionGet(int item_number) {
		long id = getItemId(item_number);
		
		logger.trace("Apply action on id = " + id);
		
		int item_quantity = items.get(item_number).quantity;
		if (item_quantity > 1) {
			String howmany = JOptionPane.showInputDialog("How many?", item_quantity);
			try {
				item_quantity = Integer.parseInt(howmany);
			} catch (NumberFormatException e) {
				item_quantity = 0;
			}
		}
		
		// Notify listener
		if (listener != null) {
			listener.actionItemGet(this, id, item_quantity);
		}
	}

	public ContainerViewListener getListener() {
		return listener;
	}

	public void setListener(ContainerViewListener listener) {
		this.listener = listener;
	}

	public Dimension getPreferredScrollableViewportSize() {
		return new Dimension(200, 50);
	}

	public int getScrollableBlockIncrement(Rectangle arg0, int arg1, int arg2) {
		return getItemSize();
	}

	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	public int getScrollableUnitIncrement(Rectangle arg0, int arg1, int arg2) {
		return getItemSize();
	}

	public void componentHidden(ComponentEvent arg0) {}
	public void componentMoved(ComponentEvent arg0) {}
	public void componentShown(ComponentEvent arg0) {}

	public void componentResized(ComponentEvent arg0) {
		adjustSize();
	}

	public void setItems(List<ContainerItem> itemList) {
		items = itemList;

		adjustSize();
		repaint();
	}

}
