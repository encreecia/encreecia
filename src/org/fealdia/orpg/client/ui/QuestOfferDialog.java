package org.fealdia.orpg.client.ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class QuestOfferDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	public QuestOfferDialog(ActionListener listener, int questId, JFrame parent, String title, String description) {
		super(parent);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Quest offer");
		
		JPanel mainPane = new JPanel();
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		
		// Title
		JLabel titleLabel = new JLabel(title);
		titleLabel.setFont(new Font(Font.SERIF, Font.PLAIN, 20));
		mainPane.add(titleLabel);
		
		// Description
		JTextArea descriptionArea = new JTextArea(description);
		JScrollPane scrollPane = new JScrollPane(descriptionArea);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		descriptionArea.setWrapStyleWord(true);
		descriptionArea.setLineWrap(true);
		descriptionArea.setEditable(false);
		descriptionArea.setFont(new Font("Dialog", Font.ITALIC, 14));
		mainPane.add(scrollPane);
		
		// Accept/Reject buttons
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		mainPane.add(buttonPane);
		JButton acceptButton = new JButton("Accept");
		acceptButton.setActionCommand("acceptquest " + questId);
		acceptButton.addActionListener(this);
		acceptButton.addActionListener(listener);
		getRootPane().setDefaultButton(acceptButton);
		buttonPane.add(acceptButton);
		buttonPane.add(Box.createHorizontalGlue());
		JButton rejectButton = new JButton("Reject");
		rejectButton.setActionCommand("rejectquest " + questId);
		rejectButton.addActionListener(this);
		rejectButton.addActionListener(listener);
		buttonPane.add(rejectButton);
		
		add(mainPane);
		
		setSize(400, 600);
		setLocationRelativeTo(null); // Center
		setModal(false);
	}

	public void actionPerformed(ActionEvent e) {
		setVisible(false);
	}

}
