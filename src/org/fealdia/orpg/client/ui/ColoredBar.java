package org.fealdia.orpg.client.ui;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.BorderFactory;
import javax.swing.JComponent;

/**
 * A simple colored bar for health and other bars.
 */
public class ColoredBar extends JComponent {
	private static final long serialVersionUID = 1L;
	
	private int position = 100;
	
	private Color color;
	private Color colorTo;

	public ColoredBar() {
		setBorder(BorderFactory.createRaisedBevelBorder());
		this.color = new Color(255, 100, 100);
		this.colorTo = new Color(180, 0, 0);
	}

	public ColoredBar(Color color, Color colorTo) {
		setBorder(BorderFactory.createRaisedBevelBorder());
		this.color = color;
		this.colorTo = colorTo;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		
		GradientPaint gp = new GradientPaint(0, 0, color, 0, getHeight() / 2, colorTo, true);
		g2.setPaint(gp);
		g2.fill(new Rectangle2D.Double(0, 0, getWidth(), getHeight()));

		// clear the missing part
		g.setColor(Color.GRAY);
		int left = (getWidth() / 100) * position;
		g.fillRect(left, 0, getWidth() - left, getHeight());
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	
}
