package org.fealdia.orpg.client.ui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.Timer;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.ClientEntity;
import org.fealdia.orpg.client.UIClient;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.LineOfSight;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.net.data.FaceMapper;
import org.fealdia.orpg.common.ui.ScenePainter;

/**
 * Player's view of the map. 
 */
public class PlayView extends JComponent implements MouseListener, MouseMotionListener, ActionListener {
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(PlayView.class);
	
	private Scene scene = null;
	
	private UIClient client = null;
	
	// player's position - should be center of the view
	private int x = 0;
	private int y = 0;
	
	private Timer timer = null;

	private long tick = 0;

	private FaceMapper faceMapper;

	public PlayView(FaceMapper faceMapper) {
		this.faceMapper = faceMapper;
		setOpaque(true);
		setPreferredSize(new Dimension(672, 672));
		
		addMouseListener(this);
		addMouseMotionListener(this);
		
		timer = new Timer(100, this);
		timer.start();
	}

	/**
	 * Paints the player's view.
	 * TODO: This shares code with SceneViewer - merge?
	 */
	@Override
	protected void paintComponent(Graphics g) {
		// Default background color
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// No scene = don't draw anything
		Scene scene = getScene();
		if (scene == null) {
			return;
		}
		
		LineOfSight los = new LineOfSight(scene, getClient().getX(), getClient().getY());
		
		g.setFont(new Font("Verdana", Font.BOLD, 10));
		
		// Draw background tiles
		ImageMapper im = ImageMapper.getInstance();
		int tileSize = im.getTileSize();
		
		int offset_x = getOffsetX();
		int offset_y = getOffsetY();
		//logger.debug("offset x = " + offset_x + ", y = " + offset_y);
		
		ScenePainter.paintSceneBackground(getScene(), g, offset_x, offset_y, getWidth(), getHeight());
		
		Map<Long, ClientEntity> entityCache = getClient().getEntityCache();
		synchronized (entityCache) {
			for (int i = 0; i < getWidth(); i += tileSize) {
				int real_x = i / tileSize + offset_x;

				if (real_x < 0 || real_x >= scene.getWidth()) { continue; }

				for (int j = 0; j < getHeight(); j += tileSize) {
					int real_y = j / tileSize + offset_y;

					if (real_y < 0 || real_y >= scene.getHeight()) { continue; }

					// Go through entityCache and draw anything in there. This includes the player itself
					for (int layer = 2; layer >= 0; layer--) {
						for (Long id : entityCache.keySet()) {
							if (!getClient().isEntityComplete(id)) {
								continue;
							}
							ClientEntity info = getClient().getEntityInfo(id);
							int en_x = info.getX();
							int en_y = info.getY();
							int en_layer = info.getLayer();

							if (en_layer == layer && en_x == real_x && en_y == real_y) {
								// Draw the entity face
								String face = faceMapper.getFaceById(info.getFace()).name;
								if (face != null && (getClient().isMaster() || los.isVisible(en_x, en_y))) {
									ScenePainter.paintFace(face, g, i, j, tick, id);
								}
								
								// If our player character is here, draw a red box in attack mode
								if (id == 0) {
									if (getClient().isAttackMode()) {
										g.setColor(Color.RED);
										g.fillRect(i, j + tileSize - 3, tileSize - 1, 2);
									}
								}

								// Draw "glowing" box around the entity if it's our target
								if (id != 0 && id == getClient().getTarget()) {
									final int STEPS = 16;

									// This is 1,2..16,15..1
									int adjust = (int) (tick % STEPS);
									if (tick % (STEPS * 2) >= STEPS) {
										adjust = STEPS - adjust;
									}

									Color c = new Color(127 + (128 / STEPS) * adjust, 0, 0);
									g.setColor(c);
									g.drawRect(i, j, tileSize - 1, tileSize - 1);
									g.drawRect(i + 1, j + 1, tileSize - 3, tileSize - 3);
									
									// Draw health bar
									int health = info.getHealth();
									if (health > 0) {
										g.setColor(Color.RED);
										g.fillRect(i + 1, j + tileSize - 5, ((tileSize - 1) * health) / 100, 5);
									}
								}

								// Draw any effect on the entity
								if (info.hasEffect()) {
									BufferedImage effect_image = im.getImage(info.getEffectFace());
									g.drawImage(effect_image, i, j, null, null);
								}
								
								// Draw any overlay on the entity
								if (info.getOverlay() != null) {
									BufferedImage overlay_image = im.getImage(info.getOverlay());
									g.drawImage(overlay_image, i, j, null, null);
								}
								
								// Draw name on players
								if (info.isPlayer() && info.getName() != null) {
									g.setColor(info.getNameColor());
									g.drawString(info.getVisibleName(), i, j);
								}
							}
						}
					}
					
					// if no LoS, shroud
					if (!los.isVisible(real_x, real_y)) {
						Graphics2D g2d = (Graphics2D) g;
						AlphaComposite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f);
						Composite old = g2d.getComposite();
						g2d.setComposite(alpha);
						g.setColor(Color.GRAY);
						g.fillRect(i, j, tileSize, tileSize);
						g2d.setComposite(old);
					}
				}
			}
		}
		
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getPlayerX() {
		return x;
	}

	public int getPlayerY() {
		return y;
	}
	
	public void setClient(UIClient client) {
		this.client = client;
	}
	
	public UIClient getClient() {
		return client;
	}

	public void mousePressed(MouseEvent event) {
		if (!getClient().isConnected()) {
			return;
		}
		
		int[] pos = translateToSceneXY(event.getX(), event.getY());
		
		boolean ctrl = (event.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0;
		
		getClient().handleClick(pos[0], pos[1], ctrl, event.getClickCount());
	}

	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mouseClicked(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}
	
	/**
	 * Get X coordinate where top left corner should be so that we draw player in the middle.
	 */
	private int getOffsetX() {
		ImageMapper im = ImageMapper.getInstance();
		int tileSize = im.getTileSize();
		
		// about half of the tiles should be to left/top from player 
		int xadjust = (getWidth() / tileSize) / 2; // 10
		
		int offset_x = getPlayerX() - xadjust;
		
		return offset_x;
	}
	
	private int getOffsetY() {
		ImageMapper im = ImageMapper.getInstance();
		int tileSize = im.getTileSize();
		
		// about half of the tiles should be to left/top from player 
		int yadjust = (getWidth() / tileSize) / 2; // 10
		
		int offset_y = getPlayerY() - yadjust;
		
		return offset_y;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == null) {
			tick = e.getWhen() / 100;
			repaint();
		}
		
	}

	public void mouseDragged(MouseEvent arg0) {}

	/**
	 * If shift is held down while moving mouse, target creatures under the cursor (if any).
	 */
	public void mouseMoved(MouseEvent event) {
		boolean shift = (event.getModifiersEx() & MouseEvent.SHIFT_DOWN_MASK) != 0;
		if (!shift) {
			return;
		}
		
		if (!getClient().isConnected()) {
			return;
		}
		
		int[] pos = translateToSceneXY(event.getX(), event.getY());

		getClient().handleShiftHover(pos[0], pos[1]);
	}
	
	/**
	 * Translate a pixel coordinate on the PlayView to Scene coordinate.
	 */
	private int[] translateToSceneXY(int pixel_x, int pixel_y) {
		int tileSize = ImageMapper.getInstance().getTileSize();
		int tile_x = pixel_x / tileSize;
		int tile_y = pixel_y / tileSize;
		
		int real_x = tile_x + getOffsetX();
		int real_y = tile_y + getOffsetY();
		
		int[] res = { real_x, real_y };
		return res;
	}
}
