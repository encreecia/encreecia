package org.fealdia.orpg.client.ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.fealdia.orpg.client.PasswordStore;

/**
 * Allow user to enter player name and password.
 */
public class LoginDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(LoginDialog.class);
	private JTextField username;
	private JTextField password;
	private JCheckBox rememberBox;
	private boolean wasAccepted = true;
	private PasswordStore passwordStore;
	private String serverName;
	
	public LoginDialog(JFrame parent, PasswordStore passwordStore, final String serverName) {
		super(parent);
		this.passwordStore = passwordStore;
		this.serverName = serverName;
		
		setTitle("Log in to " + serverName);
		//setSize(310, 310);
		
		// Username: [       ]
		// Password: [       ]
		// [x] Remember username and password
		//                   [Log in][Cancel]

		JPanel mainPane = new JPanel();
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		
		// Username & password in grid layout
		JPanel inputPane = new JPanel();
		GridLayout inputLayout = new GridLayout(0, 2);
		inputPane.setLayout(inputLayout);
		inputPane.add(new JLabel("Username:"));
		username = new JTextField();
		inputPane.add(username);
		inputPane.add(new JLabel("Password:"));
		password = new JTextField();
		inputPane.add(password);
		mainPane.add(inputPane);
		
		rememberBox = new JCheckBox("Remember username and password");
		rememberBox.setMnemonic('r');
		mainPane.add(rememberBox);
		
		// Buttons in BoxLayout
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		mainPane.add(buttonPane);
		
		JButton logButton = new JButton("Log in");
		logButton.setActionCommand("login");
		logButton.addActionListener(this);
		logButton.setMnemonic('l');
		getRootPane().setDefaultButton(logButton);
		buttonPane.add(Box.createHorizontalGlue());
		buttonPane.add(logButton);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("cancel");
		cancelButton.addActionListener(this);
		cancelButton.setMnemonic('c');
		buttonPane.add(cancelButton);
		
		// Note about registration
		JPanel infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.LINE_AXIS));
		mainPane.add(infoPane);
		
		JLabel infoLabel = new JLabel("<html>To create a new player, type in a new name<br>and a password. Once logged, F1 for help.");
		infoPane.add(infoLabel);
		infoPane.add(Box.createHorizontalGlue());
		
		mainPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		add(mainPane);
		
		pack();
		setLocationRelativeTo(null); // Center
		
		setModal(true);
	}

	/**
	 * Input the user.
	 * 
	 * @return true if username & password was given
	 */
	public boolean askLogin() {
		wasAccepted = false;
		
		// If remember checked, fill username & password
		LoginDetails loginDetails = passwordStore.getLoginDetails(serverName);
		if (loginDetails != null) {
			username.setText(loginDetails.getUsername());
			username.selectAll();
			password.setText(loginDetails.getPassword());
			rememberBox.setSelected(true);
		}
		
		setVisible(true);
		
		return wasAccepted && username.getText().length() > 0;
	}
	
	public String getUsername() {
		return username.getText();
	}
	
	public String getPassword() {
		return password.getText();
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand() == "login") {
			if (rememberBox.isSelected()) {
				try {
					passwordStore.storeLoginDetails(serverName, new LoginDetails(username.getText(), password.getText()));
				} catch (BackingStoreException e) {
					logger.error("Failed to save prefs", e);
				}
			}
			else {
				try {
					passwordStore.clearLoginDetails(serverName);
				} catch (BackingStoreException e) {
					logger.error("Failed to clear prefs", e);
				}
			}
			wasAccepted = true;
			setVisible(false);
		}
		else if (event.getActionCommand() == "cancel") {
			setVisible(false);
		}
	}

	public Preferences getPrefs() {
		return Preferences.userNodeForPackage(this.getClass());
	}
}
