package org.fealdia.orpg.client.ui;

import java.awt.image.BufferedImage;
import java.io.File;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.BackgroundMapper;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.net.data.FaceMapper;
import org.fealdia.orpg.common.util.StringUtil;

public class ClientImageMapper extends ImageMapper {
	private static final Logger logger = Logger.getLogger(ClientImageMapper.class);

	private static ClientImageMapper instance;

	public final FaceMapper faceMapper = new FaceMapper();

	public static ClientImageMapper getInstance() {
		if (instance == null) {
			instance = new ClientImageMapper("32x32");
		}
		return instance;
	}

	public ClientImageMapper(String size) {
		super(size);
	}

	@Override
	public BufferedImage getBackgroundImage(int backgroundId) {
		// Check faceMapper for background faceName
		String faceName = BackgroundMapper.getInstance().getTile(backgroundId).getFace();
		if (faceMapper.getFaceByName(faceName) != null) {
			return getImage(faceName);
		}

		// Notify ImageMissingListener
		if (getImageMissingListener() != null) {
			getImageMissingListener().onBackgroundMissing(backgroundId);
		}
		else {
			logger.warn("No FaceInfo for background: " + backgroundId);
		}

		return null;
	}
	
	// TODO override getImage(name, seed) to use faceMapper

	@Override
	public String getImageFilename(String faceName) {
		String crc = StringUtil.toHexString(faceMapper.getFaceByName(faceName).crc32);

		return getImageDir() + File.separatorChar + faceName + "_" + crc + ".png";
	}
}
