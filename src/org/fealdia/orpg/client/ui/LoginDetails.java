package org.fealdia.orpg.client.ui;

public class LoginDetails {
	private final String username;
	private final String password;

	public LoginDetails(final String username, final String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
