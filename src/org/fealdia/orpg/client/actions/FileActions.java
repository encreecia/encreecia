package org.fealdia.orpg.client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;

import org.fealdia.orpg.client.ClientFrame;


/**
 * Container for file actions. 
 */
public class FileActions {
	//private static Logger logger = Logger.getLogger(FileActions.class);
	
	private ClientFrame clientFrame;
	private ConnectAction connectAction;

	public FileActions(ClientFrame clientFrame) {
		this.clientFrame = clientFrame;
		this.connectAction = new ConnectAction();
	}
	
	protected class ConnectAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public ConnectAction() {
			putValue(Action.NAME, "Connect...");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		}

		public void actionPerformed(ActionEvent e) {
			// ask for server & port
			String connectTo = JOptionPane.showInputDialog("Give server:port to connect to.", System.getProperty("jfealdia.defaultserver"));
			if (connectTo == null) {
				return;
			}
			
			clientFrame.getClient().handleCommand("connect " + connectTo);
		}
	}

	public AbstractAction getConnectAction() {
		return connectAction;
	}
	
}
