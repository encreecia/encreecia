package org.fealdia.orpg.client.actions;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

import org.fealdia.orpg.common.util.FileUtil;

public class KeysAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	
	public KeysAction() {
		putValue(Action.NAME, "Keys...");
		putValue(Action.MNEMONIC_KEY, KeyEvent.VK_K);
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("F1"));
	}

	public void actionPerformed(ActionEvent arg0) {
		String content = FileUtil.getFileOrResourceContentAsString("doc/client-help.txt");
		
		JTextArea area = new JTextArea(content);
		JScrollPane scroller = new JScrollPane(area);
		scroller.setPreferredSize(new Dimension(600, 400));
		
		JOptionPane.showMessageDialog(null, scroller);
	}

}
