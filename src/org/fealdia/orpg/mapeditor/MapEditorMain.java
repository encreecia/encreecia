package org.fealdia.orpg.mapeditor;

import javax.swing.JFrame;

import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.mapeditor.ui.MapEditorFrame;

public class MapEditorMain {

	public static void main(String[] args) {
		try {
			//UIManager.setLookAndFeel("org.jvnet.substance.skin.SubstanceGreenMagicLookAndFeel");
		}
		catch (Exception e) {
		}

		ImageMapper.getInstance();
		
		JFrame frame = MapEditorFrame.getInstance();
		frame.setVisible(true);
	}

}
