package org.fealdia.orpg.mapeditor.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.actions.AboutAction;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.ui.SceneMinimap;
import org.fealdia.orpg.mapeditor.actions.ChunkActions;
import org.fealdia.orpg.mapeditor.actions.FileActions;

/**
 * Main frame of the Map Editor.
 * 
 * TODO Change cruft here to use Actions
 */
public class MapEditorFrame extends JFrame implements ActionListener, ChangeListener, InternalFrameListener, VetoableChangeListener, ComponentListener {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(MapEditorFrame.class);
	
	private static MapEditorFrame instance;
	
	public static MapEditorFrame getInstance() {
		if (instance == null) {
			instance = new MapEditorFrame();
		}
		return instance;
	}

	private JTabbedPane tabPane;

	private JDesktopPane desktopPane;

	private ChunkTool chunkTool;

	private TileTool tileTool;

	private SceneMinimap minimap;

	private AbstractAction close;

	private AbstractAction closeAll;

	private AbstractAction newEditor;

	private JMenu windowMenu;
	
	private JLabel statusBar;

	private ArchetypeTool archetypeTool;
	
	private int toolMode = 0;

	private JSplitPane mainPane;

	private JSplitPane rightPanel;

	public MapEditorFrame() {
		setTitle("Map editor");
		setSize(new Dimension(1024, 768));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null); // Center on screen
		
		createMenu();
		
		// Status bar
		statusBar = new JLabel();
		statusBar.setPreferredSize(new Dimension(800, 20));
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(statusBar, BorderLayout.SOUTH);
		
		// Main panel containing the mapview and tool
		mainPane = new JSplitPane();
		mainPane.setOneTouchExpandable(true);
		getContentPane().add(mainPane, BorderLayout.CENTER);
		
		desktopPane = new JDesktopPane();
		desktopPane.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		desktopPane.setBackground(Color.GRAY);
		mainPane.setLeftComponent(desktopPane);
		
		// Right side of the pane: JTabbedPane of tools, minimap
		rightPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		//rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		mainPane.setRightComponent(rightPanel);
		
		// TODO add keylistener so that TAB changes between the... tabs?
		tabPane = new JTabbedPane();
		// add listener for tabPane that notifies SceneEditor of which tool is selected
		tabPane.addChangeListener(this);
		
		chunkTool = new ChunkTool();
		JScrollPane chunkToolScroller = new JScrollPane(chunkTool);
		chunkToolScroller.setName("Chunks");
		chunkToolScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tabPane.add(chunkToolScroller);
		tabPane.setMnemonicAt(tabPane.getComponentCount() - 1, KeyEvent.VK_1);
		
		tileTool = new TileTool();
		JScrollPane tileToolScroller = new JScrollPane(tileTool);
		tileToolScroller.setName("Tile");
		tileToolScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tabPane.add(tileToolScroller);
		tabPane.setMnemonicAt(tabPane.getComponentCount() - 1, KeyEvent.VK_2);
		
		archetypeTool = new ArchetypeTool();
		JScrollPane archetypeToolScroller = new JScrollPane(archetypeTool);
		archetypeToolScroller.setName("Archetypes");
		archetypeToolScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tabPane.add(archetypeToolScroller);
		tabPane.setMnemonicAt(tabPane.getComponentCount() - 1, KeyEvent.VK_3);
		
		//rightPanel.add(tabPane);
		//rightPanel.add(new SceneMinimap());
		rightPanel.setTopComponent(tabPane);
		
		minimap = new SceneMinimap();
		minimap.setShowUniqueChunks(true);
		JScrollPane minimapScroller = new JScrollPane(minimap);
		minimapScroller.setPreferredSize(new Dimension(100, 100));
		minimapScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		minimapScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		rightPanel.setBottomComponent(minimapScroller);
		
		//mainPane.setRightComponent(tabPane);
		resetDividers();
		
		checkActions();
		
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(MapEditorKeyboardManager.getInstance());
		
		addComponentListener(this);
		
		logger.debug("Constructor done");
	}

	public void actionPerformed(ActionEvent e) {
		logger.trace("Action performed: " + e.getActionCommand());
		
		if ("Exit".equals(e.getActionCommand())) {
			System.exit(0);
		}
		else if ("map resize".equals(e.getActionCommand())) {
			SceneEditor se = getActiveSceneEditor();
			if (se == null) {
				return;
			}
			Scene scene = se.getScene();
			
			// Ask for new size and resize
			ResizeDialog resizeDialog = new ResizeDialog(this, scene.getWidthInChunks(), scene.getHeightInChunks());
			if (resizeDialog.askResize()) {
				logger.debug("Resize requested");
				scene.resize(resizeDialog.getNewWidth(), resizeDialog.getNewHeight(), resizeDialog.getOffsetX(), resizeDialog.getOffsetY());
				scene.notifyObservers();
				repaint();
			}
		}
		else {
			logger.error("FIXME: Unimplemented action command: " + e.getActionCommand());
		}
	}

	public void stateChanged(ChangeEvent e) {
		// JTabbedPane tab changed
		
		setToolMode(tabPane.getSelectedIndex());
	}
	
	// TODO add menu item for informative statistics (count of references to chunks, tiles etc)
	// TODO Make Chunks menu appear also on JTabbedPane when right clicked (setComponentPopupMenu()?)
	private void createMenu() {
		// Actions
		FileActions fileActions = new FileActions(this);
		ChunkActions chunkActions = new ChunkActions(this);
		
		newEditor = new AbstractAction("New editor") {
					private static final long serialVersionUID = 1L;
					public void actionPerformed(ActionEvent e) {
						// Is there a selected SceneEditorIFrame?
						JInternalFrame frame = getDesktopPane().getSelectedFrame();
						if (frame != null && frame instanceof SceneEditorIFrame) {
							// Get Scene from current editor, and create new editor with it
							Scene scene = ((SceneEditorIFrame) frame).getSceneEditor().getScene();
							SceneEditorIFrame newFrame = new SceneEditorIFrame(scene);
							addSceneEditorIFrame(newFrame);
						}
					}
				};
		newEditor.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		newEditor.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl D"));
		
		close = new AbstractAction("Close") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				// Attempt to close the active frame
				JInternalFrame frame = getDesktopPane().getSelectedFrame();
				if (frame != null && frame instanceof SceneEditorIFrame) {
					closeSceneEditorIFrame((SceneEditorIFrame)frame);
				}
			}
		};
		close.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		close.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl W"));
		
		AbstractAction closeOthers = new AbstractAction("Close others") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent arg0) {
				JInternalFrame[] frames = getDesktopPane().getAllFrames();
				JInternalFrame selected = getDesktopPane().getSelectedFrame();
				for (int i = 0; i < frames.length; i++) {
					JInternalFrame frame = frames[i];
					if (frame != selected && frame instanceof SceneEditorIFrame) {
						closeSceneEditorIFrame((SceneEditorIFrame) frame);
					}
				}
			}
		};
		closeOthers.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		
		closeAll = new AbstractAction("Close all") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				// Go through internal frames and close them
				JInternalFrame[] frames = getDesktopPane().getAllFrames();
				for (int i = 0; i < frames.length; i++) {
					if (frames[i] instanceof SceneEditorIFrame) {
						closeSceneEditorIFrame((SceneEditorIFrame) frames[i]);
					}
				}
			}
		};
		closeAll.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		// File
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic('f');
		menuBar.add(fileMenu);
		
		fileMenu.add(fileActions.getNewMapAction());
		fileMenu.add(fileActions.getOpenMapAction());
		fileMenu.addSeparator();
		fileMenu.add(fileActions.getSaveAction());
		fileMenu.add(fileActions.getSaveAsAction());
		fileMenu.addSeparator();
		fileMenu.add(fileActions.getReloadAction());
		fileMenu.addSeparator();
		fileMenu.add(fileActions.getExportMinimapAction());
		fileMenu.addSeparator();
		fileMenu.add(fileActions.getExitAction());
		
		// Chunks
		JMenu chunksMenu = new JMenu("Chunks");
		chunksMenu.setMnemonic('c');
		menuBar.add(chunksMenu);
		
		chunksMenu.add(chunkActions.getCreateChunkAction());
		
		chunksMenu.add(chunkActions.getCreateFilledChunkAction());
		
		chunksMenu.add(chunkActions.getDuplicateChunkAction());
		
		AbstractAction pruneChunks = new AbstractAction("Prune unused private chunks") {
			private static final long serialVersionUID = 1L;
			public void actionPerformed(ActionEvent e) {
				JInternalFrame frame = getDesktopPane().getSelectedFrame();
				if (frame != null && frame instanceof SceneEditorIFrame) {
					((SceneEditorIFrame)frame).getSceneEditor().getScene().prunePrivateChunks();
				}
			}
		};
		chunksMenu.add(pruneChunks);
		
		// Map
		JMenu mapMenu = new JMenu("Map");
		mapMenu.setMnemonic('m');
		menuBar.add(mapMenu);
		
		JMenuItem resizeMapItem = new JMenuItem("Resize...");
		resizeMapItem.setMnemonic('r');
		resizeMapItem.setActionCommand("map resize");
		resizeMapItem.addActionListener(this);
		mapMenu.add(resizeMapItem);
		
		windowMenu = new JMenu("Window");
		windowMenu.setMnemonic('w');
		menuBar.add(windowMenu);
		
		windowMenu.add(newEditor);
		windowMenu.addSeparator();
		windowMenu.add(close);
		windowMenu.add(closeOthers);
		windowMenu.add(closeAll);
		windowMenu.addSeparator();
		
		// Right align rest
		menuBar.add(Box.createHorizontalGlue());
		
		// Help
		JMenu helpMenu = new JMenu("Help");
		helpMenu.setMnemonic('h');
		menuBar.add(helpMenu);
		
		helpMenu.add(new AboutAction());
	}

	/**
	 * (Re)create SceneEditorIFrame items in Window menu. 
	 */
	private void createSceneEditorMenuItems() {
		logger.debug("Updating Window menu editor items");
		
		// Remove all items from windowMenu, below last separator
		while (true) {
			int count = windowMenu.getItemCount();
			if (count == 0) {
				break;
			}
			Component com = windowMenu.getMenuComponent(count - 1);
			if (com == null || com instanceof JSeparator) {
				break;
			}
			logger.debug("Removing menu item");
			windowMenu.remove(com);
		}
		
		// Create items for all SceneEditorIFrames
		JInternalFrame[] frames = getDesktopPane().getAllFrames();
		for (int i = 0; i < frames.length; i++) {
			if (frames[i] instanceof SceneEditorIFrame) {
				final SceneEditorIFrame editor = (SceneEditorIFrame) frames[i];
				AbstractAction action = new AbstractAction() {
					private static final long serialVersionUID = 1L;
					public void actionPerformed(ActionEvent e) {
						//logger.debug("SceneEditorIFrame menu item action, trying to select frame");
						try {
							editor.setSelected(true);
						} catch (PropertyVetoException e1) {
							logger.debug("editor.setSelected() exception", e1);
						}
						//getDesktopPane().setSelectedFrame(editor);
					}
				};
				action.putValue(Action.NAME, (i + 1) + ": " + editor.getTitle());
				if (i < 9) {
					action.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_1 + i);
					action.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift " + (i + 1)));
				}
				JMenuItem item = new JMenuItem(action);
				windowMenu.add(item);
			}
		}
	}

	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}
	
	public SceneEditor getActiveSceneEditor() {
		JInternalFrame frame = desktopPane.getSelectedFrame();
		if (frame != null && frame instanceof SceneEditorIFrame) {
			return ((SceneEditorIFrame)frame).getSceneEditor();
		}
		
		return null;
	}
	
	/**
	 * Get active Scene, if any. 
	 */
	public Scene getActiveScene() {
		SceneEditor se = getActiveSceneEditor();
		if (se != null) {
			return se.getScene();
		}
		return null;
	}
	
	/**
	 * Adds a SceneEditorIFrame to the desktop, initializes it properly and registers listeners. 
	 */
	public void addSceneEditorIFrame(SceneEditorIFrame frame) {
		// set chunk and tile tools
		frame.getSceneEditor().setChunkTool(chunkTool);
		frame.getSceneEditor().setTileTool(tileTool);
		frame.getSceneEditor().setArchetypeTool(archetypeTool);
		
		frame.addInternalFrameListener(this);
		frame.addVetoableChangeListener(this);
		
		logger.debug("Adding SceneEditorIFrame");
		desktopPane.add(frame);
		
		frame.show();
		
		try {
			frame.setMaximum(true);
		} catch (PropertyVetoException e) {
			logger.debug("Failed to maximize iframe", e);
		}
	}

	public void internalFrameActivated(InternalFrameEvent arg0) {
		checkActions();
		//logger.debug("Internal frame activated");
		updateMinimap();
	}

	public void internalFrameClosed(InternalFrameEvent arg0) {
		createSceneEditorMenuItems();
	}

	public void internalFrameClosing(InternalFrameEvent arg0) {}

	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		checkActions();
		//logger.debug("Internal frame deactivated");
		updateMinimap();
	}

	public void internalFrameDeiconified(InternalFrameEvent arg0) {}

	public void internalFrameIconified(InternalFrameEvent arg0) {}

	public void internalFrameOpened(InternalFrameEvent arg0) {
		checkActions();
		logger.debug("Internal frame opened");
		updateMinimap();
		createSceneEditorMenuItems();
	}

	public ChunkTool getChunkTool() {
		return chunkTool;
	}

	public TileTool getTileTool() {
		return tileTool;
	}
	
	private void updateMinimap() {
		minimap.setScene(getActiveScene());
		minimap.setViewer(getActiveSceneEditor());
	}
	
	private void closeSceneEditorIFrame(SceneEditorIFrame frame) {
		logger.debug("Close for SceneEditorIFrame requested");
		
		try {
			frame.setClosed(true);
		} catch (PropertyVetoException e) {
		}
	}

	public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
		//logger.debug("Vetoable change property " + evt.getPropertyName());
		Object src = evt.getSource();
		
		// Is a SceneEditorIFrame being closed (new closed = true)?
		if ("closed".equals(evt.getPropertyName()) && evt.getNewValue().equals(Boolean.TRUE) &&
				src instanceof SceneEditorIFrame) {
			SceneEditorIFrame frame = (SceneEditorIFrame) src;
			
			// check whether the scene is modified
			if (((SceneEditorIFrame)frame).getSceneEditor().getScene().isModified()) {
				// prompt for save/cancel
				String[] options = { "Discard changes", "Cancel" };
				if (JOptionPane.showOptionDialog(this, "The current map has unsaved changes.\nDiscard changes?",
						"Discard changes?", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]) == 1) {
					throw new PropertyVetoException("Close aborted", evt);
				}
			}
		}
		
	}
	
	/**
	 * Go through all actions and enable/disable them as required.
	 */
	private void checkActions() {
		// Is a SceneEditorIFrame selected?
		JInternalFrame frame = getDesktopPane().getSelectedFrame();
		boolean haveFrame = (frame != null && frame instanceof SceneEditorIFrame);

		// Go through all iframe actions and setEnabled(haveFrame)
		newEditor.setEnabled(haveFrame);
		close.setEnabled(haveFrame);
		closeAll.setEnabled(haveFrame);
	}
	
	public void setStatusbarText(String text) {
		statusBar.setText(text);
	}

	public ArchetypeTool getArchetypeTool() {
		return archetypeTool;
	}

	public int getToolMode() {
		return toolMode;
	}

	public void setToolMode(int toolMode) {
		this.toolMode = toolMode;
	}

	public JTabbedPane getTabPane() {
		return tabPane;
	}
	
	public MapTool getActiveMapTool() {
		if (getToolMode() == 0) {
			return getChunkTool();
		}
		else if (getToolMode() == 1) {
			return getTileTool();
		}
		return getArchetypeTool();
	}

	public SceneMinimap getMinimap() {
		return minimap;
	}

	public void componentHidden(ComponentEvent arg0) {}
	public void componentMoved(ComponentEvent arg0) {}
	public void componentShown(ComponentEvent arg0) {}

	public void componentResized(ComponentEvent arg0) {
		resetDividers();
	}
	
	private void resetDividers() {
		mainPane.setDividerLocation((int) (getWidth() * 0.7));
		rightPanel.setDividerLocation((int) (getHeight() * 0.6));
	}

}
