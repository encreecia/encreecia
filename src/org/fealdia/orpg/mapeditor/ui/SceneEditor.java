package org.fealdia.orpg.mapeditor.ui;

import java.awt.Graphics;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.BufferedReader;
import java.io.StringReader;
import java.io.StringWriter;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Chunk;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.SceneEntity;
import org.fealdia.orpg.common.maps.SceneEntityManager;
import org.fealdia.orpg.common.ui.SceneViewer;

/**
 * Provides editing on top of SceneViewer.
 */
public class SceneEditor extends SceneViewer implements MouseWheelListener {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SceneEditor.class);
	
	private TileTool tileTool;
	private ChunkTool chunkTool;
	private ArchetypeTool archetypeTool;
	
	public SceneEditor() {
		setChunkGrid(true);
		setTileGrid(false);
		
		addMouseWheelListener(this);
	}

	@Override
	public void tileClicked(int x, int y) {
		super.tileClicked(x, y);
		
		// Depending on tool mode (chunk or tile), set chunk or tile
		if (getMode() == 0) {
			if (getScene().setChunk(x, y, chunkTool.getSelected())) {
				repaint();
			}
		}
		else if (getMode() == 1) {
			if (getTileTool() == null) {
				logger.error("tileClicked() called, but no tile tool set");
				return;
			}
			
			if (getScene().setTile(x, y, getTileTool().getSelected())) {
				repaint();
			}
		}
		else if (getMode() == 2) {
			if (getArchetypeTool() == null) {
				logger.error("tileClicked() called, but no archetype tool set");
				return;
			}
			
			// Add entity
			getScene().getEntityManager().addEntity(new SceneEntity(getArchetypeTool().getSelectedArchetype(), x, y));
			getScene().setModified(true);
			
			repaint();
		}
		
		getScene().notifyObservers();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int realx = e.getX() / ImageMapper.getInstance().getTileSize() + getOffsetX();
		int realy = e.getY() / ImageMapper.getInstance().getTileSize() + getOffsetY();

		// "Color picker": Ctrl-button1
		if (e.getButton() == MouseEvent.BUTTON1 && (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0) {
			logger.debug("Color picker");
			int bg = getScene().getBackgroundId(realx, realy);
			logger.debug(" tile " + bg);
			getTileTool().setSelected(bg);
			return;
		}
		
		// "Flood fill": Shift-button1
		if (e.getButton() == MouseEvent.BUTTON1 && (e.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK) != 0) {
			int chunkid = getScene().getChunkId(realx, realy);
			logger.debug("Flood filling chunk " + chunkid + " with " + getTileTool().getSelected());
			getScene().floodFill(realx, realy, getTileTool().getSelected());
			repaint();
			return;
		}
		
		// Map entity editing: middle click
		if (e.getButton() == MouseEvent.BUTTON2 & (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) == 0) {
			// Get entity at position
			SceneEntityManager em = getScene().getEntityManager();
			SceneEntity en = em.getFirstEntityAt(realx, realy);
			if (en != null) {
				// Get entity properties as text
				StringWriter sw = new StringWriter();
				SceneEntityManager.saveEntityToStream(en, sw, true);
				String props = sw.toString();
				logger.debug("Entity properties: " + props);
				
				// Show a crude dialog to modify the properties
				JTextArea area = new JTextArea(props);
				JScrollPane pane = new JScrollPane(area);
				JOptionPane.showMessageDialog(this, pane, "Entity: " + en.getArchetype(), JOptionPane.PLAIN_MESSAGE);
				String newProps = area.getText();
				
				// If text was changed, modify the entity
				if (!newProps.equals(props)) {
					logger.debug("Modifying entity: " + newProps);
					
					StringReader in = new StringReader(newProps);
					SceneEntityManager.modifyEntityFromStream(en, new BufferedReader(in));
					getScene().setModified(true);
					getScene().notifyObservers();
					repaint();
				}
			}
			
			return;
		}
		
		// Map entity deletion: ctrl-middle click
		if (e.getButton() == MouseEvent.BUTTON2 && (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0) {
			SceneEntityManager em = getScene().getEntityManager();
			
			// Since we don't go this through in reverse order, we actually delete the bottom-most one
			SceneEntity en = em.getFirstEntityAt(realx, realy);
			if (en != null) {
				em.getEntities().remove(en);
				getScene().setModified(true);
				repaint();
			}
			
			return;
		}
		
		super.mousePressed(e);
		
		// Right click moves the map towards that direction (relative to center)
		if (e.getButton() == MouseEvent.BUTTON3) {
			logger.trace("Right button down");
			
			int diff_x = (e.getX() - (getWidth() / 2)) / ImageMapper.getInstance().getTileSize();
			int diff_y = (e.getY() - (getHeight() / 2)) / ImageMapper.getInstance().getTileSize();
			adjustOffset(diff_x, diff_y);
			
			//setOffset(x, y);
		}

	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// Draw dot in the center
		/*
		int dotSize = 2;
		g.setColor(Color.RED);
		g.drawRect(getWidth() / 2 - dotSize, getHeight() / 2 - dotSize, 2*dotSize, 2*dotSize);
		*/
		
		// Could make this an option?
		// Draw yellow '+' on the screen
		/*
		g.setColor(Color.YELLOW);
		g.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2); // -
		g.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight()); // |
		*/
	}

	public void setTileTool(TileTool tileTool) {
		this.tileTool = tileTool;
	}

	public TileTool getTileTool() {
		return tileTool;
	}

	public void setChunkTool(ChunkTool chunkTool) {
		this.chunkTool = chunkTool;
	}

	public ChunkTool getChunkTool() {
		return chunkTool;
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		int realx = e.getX() / ImageMapper.getInstance().getTileSize() + getOffsetX();
		int realy = e.getY() / ImageMapper.getInstance().getTileSize() + getOffsetY();
		
		int chunkx = realx / Chunk.getSize();
		int chunky = realy / Chunk.getSize();
		
		MapEditorFrame.getInstance().setStatusbarText("x = " + realx + ", y = " + realy + " chunk x = " + chunkx + ", y = " + chunky);
	}

	public boolean isUniqueMode() {
		return getScene().isUniqueMode();
	}

	public void setUniqueMode(boolean uniqueMode) {
		getScene().setUniqueMode(uniqueMode);
	}

	public ArchetypeTool getArchetypeTool() {
		return archetypeTool;
	}

	public void setArchetypeTool(ArchetypeTool archetypeTool) {
		this.archetypeTool = archetypeTool;
	}

	public int getMode() {
		return MapEditorFrame.getInstance().getToolMode();
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		int amount = e.getWheelRotation();
		
		// shift down: scroll horizontally
		if ((e.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK) != 0) {
			adjustOffset(amount, 0);
		}
		// no shift: scroll vertically
		else {
			adjustOffset(0, amount);
		}
	}
	
}
