package org.fealdia.orpg.mapeditor.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import org.fealdia.orpg.common.maps.BackgroundMapper;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.Tile;

/**
 * Allows selection of a background tile.
 * 
 * TODO add visual information displaying which of the tiles are walls (speed=0) and which are not? (eg. selection color changes?)
 */
public class TileTool extends MapTool {
	private static final long serialVersionUID = 1L;
	
	public TileTool() {
		setItemSize(ImageMapper.getInstance().getTileSize());
	}
	
	@Override
	protected void paintItem(Graphics g, int item, int x, int y) {
		ImageMapper im = ImageMapper.getInstance();
		BackgroundMapper bm = BackgroundMapper.getInstance();

		Image img = im.getImage(bm.getTile(item).getFace());
		g.drawImage(img, x, y, Color.RED, null);
	}

	@Override
	public void setSelected(int item) {
		if (!isValidSelection(item)) {
			return;
		}
		
		Tile tile = BackgroundMapper.getInstance().getTile(item);
		
		// Show properties of the selected tile on tooltip
		StringBuffer tip = new StringBuffer();
		tip.append("<html>Name: " + tile.getName() + "<br>");
		tip.append("Face: " + tile.getFace() + "<br>");
		tip.append("Speed: " + tile.getSpeed());
		tip.append("</html>");
		
		setToolTipText(tip.toString());
		
		super.setSelected(item);
	}

	@Override
	public int getItemCount() {
		return BackgroundMapper.getInstance().getBackgroundCount();
	}

}
