package org.fealdia.orpg.mapeditor.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Scene;

/**
 * Creates internal frame for scene editor.
 * 
 * Observer for: Scene
 */
public class SceneEditorIFrame extends JInternalFrame implements Observer, ActionListener {
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = Logger.getLogger(SceneEditorIFrame.class);
	
	private static int nextPosition = 0;
	
	private SceneEditor sceneEditor;

	private JToggleButton chunkGrid;

	private JToggleButton tileGrid;

	private JToggleButton uniqueMode;

	/**
	 * Creates a new SceneEditorIFrame with a new Scene.
	 */
	public SceneEditorIFrame() {
		super("untitled", true, true, true, true);
		logger.debug("Created with new Scene");
		create(new Scene());
	}
	
	public SceneEditorIFrame(Scene scene) {
		super(scene.getName(), true, true, true, true);
		logger.debug("Created with given Scene");
		create(scene);
	}
	
	private void create(Scene scene) {
		setSize(300, 300);
		
		scene.addObserver(this);
		
		sceneEditor = new SceneEditor();
		sceneEditor.setScene(scene);
		
		JScrollPane scroller = new JScrollPane(sceneEditor);
		scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		add(scroller);
		
		updateTitle();
		
		// don't create all iframes at 0,0
		final int diff = 20;
		setLocation(diff * nextPosition, diff * nextPosition);
		nextPosition++;
		if (nextPosition > diff) {
			nextPosition = 0;
		}
		
		//createMenu();
		createToolbar();
	}
	
	public SceneEditor getSceneEditor() {
		return sceneEditor;
	}

	@Override
	public void dispose() {
		super.dispose();
		logger.debug("Dispose");
		
		sceneEditor.getScene().deleteObserver(this);
	}
	
	private void updateTitle() {
		setTitle(sceneEditor.getScene().getTitle());
	}

	public void update(Observable o, Object arg) {
		if (o instanceof Scene) {
			updateTitle();
		}
	}
	
	/*
	private void createMenu() {
		JMenuBar bar = new JMenuBar();
		
		// File
		JMenu file = new JMenu("File");
		file.setMnemonic('f');
		bar.add(file);
		
		// Window
		JMenu window = new JMenu("Window");
		window.setMnemonic('w');
		bar.add(window);
		
		setJMenuBar(bar);
	}
	*/
	
	private void createToolbar() {
		JToolBar bar = new JToolBar();
		//bar.setPreferredSize(new Dimension(20, 20));
		
		chunkGrid = new JToggleButton("Chunk grid");
		chunkGrid.setSelected(true);
		chunkGrid.setActionCommand("chunkGrid");
		chunkGrid.addActionListener(this);
		chunkGrid.setIcon(new ImageIcon(getClass().getResource("/toolbarButtonGraphics/table/ColumnDelete24.gif")));
		bar.add(chunkGrid);
		
		tileGrid = new JToggleButton("Tile grid");
		tileGrid.setSelected(false);
		tileGrid.setActionCommand("tileGrid");
		tileGrid.addActionListener(this);
		tileGrid.setIcon(new ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/AlignCenter24.gif")));
		bar.add(tileGrid);
		
		uniqueMode = new JToggleButton("Unique mode");
		uniqueMode.setActionCommand("uniqueMode");
		uniqueMode.addActionListener(this);
		uniqueMode.setIcon(new ImageIcon(getClass().getResource("/toolbarButtonGraphics/development/WebComponent24.gif")));
		bar.add(uniqueMode);

		add(bar, BorderLayout.PAGE_START);
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("chunkGrid".equals(cmd)) {
			getSceneEditor().setChunkGrid(chunkGrid.isSelected());
		}
		else if ("tileGrid".equals(cmd)) {
			getSceneEditor().setTileGrid(tileGrid.isSelected());
		}
		else if ("uniqueMode".equals(cmd)) {
			getSceneEditor().setUniqueMode(uniqueMode.isSelected());
		}
	}
}
