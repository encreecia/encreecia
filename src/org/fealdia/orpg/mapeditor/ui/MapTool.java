package org.fealdia.orpg.mapeditor.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JComponent;
import javax.swing.Scrollable;

import org.apache.log4j.Logger;

/**
 * Base class for map editing tools as ChunkTool and TileTool. These tools provide a set of items that
 * user can select one from.
 * 
 * TODO add hotkeys for 'z' and 'x' to select prev/next item.
 * FIXME does not work for tools that contain more items than currently visible area in JTabbedPane can contain
 */
public abstract class MapTool extends JComponent implements MouseListener, MouseWheelListener, Scrollable {
	private static final long serialVersionUID = 1L;
	
	private final static Logger logger = Logger.getLogger(MapTool.class);
	
	private int itemCount = 0;
	private int itemSize = 0;
	private int selected = 0;
	private int borderSize = 1;

	public MapTool() {
		setOpaque(true);
		
		setSelected(0);
		
		addMouseListener(this);
		addMouseWheelListener(this);
	}
	
	@Override
	public Dimension getPreferredSize() {
		logger.trace("getPreferredSize(), cursize = " + getWidth() + " x " + getHeight());
		final int spacefor_x = Math.max(getWidth() / getItemSizeBorder(), 1); // Width is determined by JScrollPane/tab container
		int need_y = getItemCount() / spacefor_x;
		if (getItemCount() % spacefor_x != 0) {
			need_y++;
		}
		return new Dimension(getWidth(), need_y * getItemSizeBorder());
	}

	/**
	 * Get (index of) currently selected item. 
	 */
	public int getSelected() {
		return selected;
	}
	
	/**
	 * Check whether index is a valid selection.
	 */
	public final boolean isValidSelection(int index) {
		return (index >= 0 && index < getItemCount());
	}

	/**
	 * Change currently selected item. Triggers a repaint if selection changed.
	 */
	public void setSelected(int selected) {
		if (this.selected == selected || !isValidSelection(selected)) {
			return;
		}
		
		this.selected = selected;
		repaint();
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

	public int getItemSize() {
		return itemSize;
	}

	public void setItemSize(int itemSize) {
		this.itemSize = itemSize;
		
		setMinimumSize(new Dimension(itemSize, itemSize));
	}

	/**
	 * Mouse click changes selection to whatever is currently under the cursor.
	 */
	public void mousePressed(MouseEvent e) {
		
		int tile_x = e.getX() / getItemSizeBorder();
		int tile_y = e.getY() / getItemSizeBorder();
		
		int tile = tile_y * (getWidth() / getItemSizeBorder()) + tile_x;
		
		int maxTile = getItemCount() - 1;
		
		if (tile > maxTile) {
			tile = maxTile;
		}
		
		logger.debug("mousePressed on tile = " + tile);
		setSelected(tile);
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		logger.debug("Mouse wheel moved, rotation = " + e.getWheelRotation() + ", amount = " + e.getScrollAmount());
		
		int diff = e.getWheelRotation();
		
		int index = getSelected() + diff;
		if (isValidSelection(index)) {
			setSelected(index);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		logger.trace("paintComponent width = " + getWidth() + " x " + getHeight());
		// Background
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());

		int itemCount = getItemCount();
		int itemSize = getItemSizeBorder();
		
		// ok, draw the tiles visible in the clip bounds
		int i = 0;
		for (int y = 0; y + itemSize <= getHeight(); y += itemSize) {
			for (int x = 0; x + itemSize <= getWidth(); x += itemSize) {
				if (i >= itemCount) {
					break;
				}
				
				paintItem(g, i, x + 1, y + 1);
				
				// "Grid" between tiles
				if (getBorderSize() > 0) {
					g.setColor(Color.GRAY);
					int b = getBorderSize() - 1;
					g.drawRect(x, y, b, itemSize);
					g.drawRect(x, y, itemSize, b);
				}
				
				// Show which of the tiles is selected
				if (i == getSelected()) {
					g.setColor(Color.CYAN);
					g.drawRect(x + 1, y + 1, itemSize - 2, itemSize - 2);
				}
				
				i++;
			}
		}
	}
	
	protected abstract void paintItem(Graphics g, int item, int x, int y);

	public int getBorderSize() {
		return borderSize;
	}

	public void setBorderSize(int borderSize) {
		this.borderSize = borderSize;
	}
	
	public int getItemSizeBorder() {
		return getItemSize() + getBorderSize();
	}

	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public boolean getScrollableTracksViewportWidth() {
		return true;
	}

	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return getItemSize() * 2;
	}

	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return getItemSize();
	}
}
