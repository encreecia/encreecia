package org.fealdia.orpg.mapeditor.ui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.List;

import org.fealdia.orpg.common.maps.ArchetypeManager2;
import org.fealdia.orpg.common.maps.ImageMapper;

/**
 * Archetype selection tool.
 */
public class ArchetypeTool extends MapTool {
	private static final long serialVersionUID = 1L;
	
	public ArchetypeTool() {
		setItemSize(ImageMapper.getInstance().getTileSize());
		setBorderSize(0);
	}
	
	@Override
	public int getItemCount() {
		return ArchetypeManager2.getInstance().size();
	}
	
	public String getSelectedArchetype() {
		return ArchetypeManager2.getInstance().getArchetypeList().get(getSelected());
	}
	
	@Override
	protected void paintItem(Graphics g, int item, int x, int y) {
		ArchetypeManager2 am = ArchetypeManager2.getInstance();
		List<String> archetypes = am.getArchetypeList();

		String face = am.getArchetypeFace(archetypes.get(item));
		if (face != null) {
			BufferedImage im = ImageMapper.getInstance().getImage(face);
			g.drawImage(im, x, y, null, null);
		}
	}
	
}
