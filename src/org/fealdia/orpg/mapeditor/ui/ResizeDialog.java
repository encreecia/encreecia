package org.fealdia.orpg.mapeditor.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class ResizeDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lblX;
	private JSpinner spinnerOffsetY;
	private JSpinner spinnerOffsetX;
	private JLabel lblY;
	private final int current_width;
	private final int current_height;
	private JSpinner spinnerWidth;
	private JSpinner spinnerHeight;
	private boolean resized = false;
	private PreviewComponent previewComponent;

	/**
	 * Scene resize dialog.
	 * 
	 * TODO add real models to offset spinners
	 */
	public ResizeDialog(JFrame parent, int current_width, int current_height) {
		super(parent);
		this.current_width = current_width;
		this.current_height = current_height;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		setTitle("Resize map");
		setBounds(100, 100, 450, 450);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		{
			JPanel panel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel.getLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			contentPanel.add(panel);
			{
				JLabel lblCurrentSize = new JLabel("Current size: " + current_width + " x " + current_height + " chunks");
				panel.add(lblCurrentSize);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
			{
				JLabel label = new JLabel("New Size");
				panel.add(label);
			}
		}
		{
			JPanel panel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel.getLayout();
			flowLayout.setHgap(10);
			flowLayout.setAlignment(FlowLayout.LEFT);
			contentPanel.add(panel);
			{
				JLabel lblNewLabel_1 = new JLabel("Width");
				lblNewLabel_1.setFont(new Font("Dialog", Font.BOLD, 10));
				panel.add(lblNewLabel_1);
			}
			{
				spinnerWidth = new JSpinner();
				spinnerWidth.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						previewComponent.repaint();
					}
				});
				spinnerWidth.setPreferredSize(new Dimension(48, 20));
				spinnerWidth.setModel(new SpinnerNumberModel(current_width, new Integer(1), null, new Integer(1)));
				panel.add(spinnerWidth);
			}
			{
				JLabel lblNewLabel_2 = new JLabel("Height");
				lblNewLabel_2.setFont(new Font("Dialog", Font.BOLD, 10));
				panel.add(lblNewLabel_2);
			}
			{
				spinnerHeight = new JSpinner();
				spinnerHeight.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						previewComponent.repaint();
					}
				});
				spinnerHeight.setPreferredSize(new Dimension(48, 20));
				spinnerHeight.setModel(new SpinnerNumberModel(current_height, new Integer(1), null, new Integer(1)));
				panel.add(spinnerHeight);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
			{
				JLabel label = new JLabel("Offset");
				panel.add(label);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			{
				lblY = new JLabel("Y");
				lblY.setFont(new Font("Dialog", Font.BOLD, 10));
			}
			{
				spinnerOffsetY = new JSpinner();
				spinnerOffsetY.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						previewComponent.repaint();
					}
				});
				spinnerOffsetY.setPreferredSize(new Dimension(48, 20));
			}
			{
				lblX = new JLabel("X");
				lblX.setFont(new Font("Dialog", Font.BOLD, 10));
			}
			panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 5));
			panel.add(lblX);
			{
				spinnerOffsetX = new JSpinner();
				spinnerOffsetX.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent e) {
						previewComponent.repaint();
					}
				});
				spinnerOffsetX.setPreferredSize(new Dimension(48, 20));
			}
			panel.add(spinnerOffsetX);
			panel.add(lblY);
			panel.add(spinnerOffsetY);
		}
		{
			JPanel previewPanel = new JPanel();
			previewComponent = new PreviewComponent();
			previewPanel.add(previewComponent);
			contentPanel.add(previewPanel);
			previewPanel.setLayout(new BorderLayout(0, 0));
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Resize");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						resized = true;
						setVisible(false);
					}
				});
				okButton.setActionCommand("resize");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						resized = false;
						setVisible(false);
					}
				});
				cancelButton.setActionCommand("close");
				buttonPane.add(cancelButton);
			}
		}
		setLocationRelativeTo(null); // center
		setModal(true);
	}

	public boolean askResize() {
		setVisible(true);
		return resized;
	}

	public int getNewWidth() {
		return (Integer) spinnerWidth.getValue();
	}

	public int getNewHeight() {
		return (Integer) spinnerHeight.getValue();
	}

	public int getOffsetX() {
		return (Integer) spinnerOffsetX.getValue();
	}

	public int getOffsetY() {
		return (Integer) spinnerOffsetY.getValue();
	}

	private class PreviewComponent extends JComponent implements ChangeListener {
		public PreviewComponent() {
			setPreferredSize(new Dimension(200, 200));
			setSize(new Dimension(200, 200));
		}
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.GRAY);
			g.fillRect(0, 0, getWidth(), getHeight());

			// Map box
			final int diffwidth = Math.abs(current_width - getNewWidth());
			final int diffheight = Math.abs(current_height - getNewHeight());
			final int diff_x = Math.max(current_width - getNewWidth(), 0);
			final int diff_y = Math.max(current_height - getNewHeight(), 0);

			// Virtual width/height of the preview component before scaling
			final int virtualwidth = current_width + diffwidth;
			final int virtualheight = current_height + diffheight;

			// Scale to the preview component dimensions
			final int realmapboxwidth = (int) ((((float) current_width) / virtualwidth) * getWidth());
			final int realmapboxheight = (int) ((((float) current_height) / virtualheight) * getHeight());
			final int realmapoffsetx = (int) ((((float) diff_x + getOffsetX()) / (current_width + diffwidth)) * getWidth());
			final int realmapoffsety = (int) ((((float) diff_y + getOffsetY()) / (current_height + diffheight)) * getHeight());

			g.setColor(Color.GREEN);
			g.fillRect(realmapoffsetx, realmapoffsety, realmapboxwidth, realmapboxheight);

			// crop "box" is needed if either width/height is smaller
			// It has margin equal to size difference
			final int previewrelativediffx = (int) ((((float) diff_x) / (current_width + diffwidth)) * getWidth());
			final int previewrelativediffy = (int) ((((float) diff_y) / (current_height + diffheight)) * getHeight());

			if (diff_x > 0 || diff_y > 0) {
				g.setColor(Color.RED);
				g.drawRect(previewrelativediffx, previewrelativediffy, getWidth() - 2 * previewrelativediffx, getHeight() - 2 * previewrelativediffy);
			}
		}

		public void stateChanged(ChangeEvent e) {
			repaint();
		}
	}
}
