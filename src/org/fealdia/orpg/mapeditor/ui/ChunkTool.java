package org.fealdia.orpg.mapeditor.ui;

import java.awt.Graphics;

import org.fealdia.orpg.common.maps.BackgroundMapper;
import org.fealdia.orpg.common.maps.Chunk;
import org.fealdia.orpg.common.maps.ChunkList;
import org.fealdia.orpg.common.maps.Tile;

/**
 * Provides a list of chunks with preview of each, and allows selection of one.
 */
public class ChunkTool extends MapTool {
	private static final long serialVersionUID = 1L;

	private static final int SCALE = 2;

	public ChunkTool() {
		setItemSize(Chunk.getSize() * SCALE);
	}
	
	@Override
	public void paintItem(Graphics g, int item, int x, int y) {
		// Draw the actual tile colors
		Chunk chunk = ChunkList.getInstance().getChunk(item);
		BackgroundMapper bm = BackgroundMapper.getInstance();
		
		// TODO Optimize! This is probably quite slow
		
		// Go through all tiles in the chunk and draw each as one pixel
		for (int b = 0; b < Chunk.getSize(); b++) {
			for (int a = 0; a < Chunk.getSize(); a++) {
				int tile_id = chunk.getTile(a, b);
				Tile tile = bm.getTile(tile_id);
				g.setColor(tile.getColor());
				g.drawRect(x + a * SCALE, y + b * SCALE, SCALE - 1, SCALE - 1);
			}
		}
	}

	@Override
	public int getItemCount() {
		return ChunkList.getInstance().size();
	}

}
