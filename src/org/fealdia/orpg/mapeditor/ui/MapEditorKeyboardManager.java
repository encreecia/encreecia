package org.fealdia.orpg.mapeditor.ui;

import java.awt.DefaultKeyboardFocusManager;
import java.awt.event.KeyEvent;

import javax.swing.JTabbedPane;

/**
 * Handle global keybindings.
 */
public class MapEditorKeyboardManager extends DefaultKeyboardFocusManager {
	//private static Logger logger = Logger.getLogger(MapEditorKeyboardManager.class);
	
	private static MapEditorKeyboardManager instance = null;
	
	public static MapEditorKeyboardManager getInstance() {
		if (instance == null) {
			instance = new MapEditorKeyboardManager();
		}
		return instance;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		//logger.debug("TODO: handle keybind: " + event.paramString());
		
		boolean ctrl = (event.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0;
		
		if (event.getID() == KeyEvent.KEY_PRESSED) {
			if (event.getKeyCode() == KeyEvent.VK_ADD || event.getKeyCode() == KeyEvent.VK_X) {
				MapTool tool = MapEditorFrame.getInstance().getActiveMapTool();
				tool.setSelected(tool.getSelected() + 1);
			}
			else if (event.getKeyCode() == KeyEvent.VK_SUBTRACT || event.getKeyCode() == KeyEvent.VK_Z) {
				MapTool tool = MapEditorFrame.getInstance().getActiveMapTool();
				tool.setSelected(tool.getSelected() - 1);
			}
			// Tab/shift-tab cycles tabs
			else if (event.getKeyCode() == KeyEvent.VK_TAB && (event.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == 0) {
				JTabbedPane pane = MapEditorFrame.getInstance().getTabPane();
				
				int index = pane.getSelectedIndex();
				if ((event.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) != 0) {
					index -= 1;
				} else {
					index += 1;
				}
				
				if (index >= pane.getComponentCount()) {
					index = 0;
				}
				if (index < 0) {
					index = pane.getComponentCount() - 1;
				}
				
				pane.setSelectedIndex(index);
			}
			else if (event.getKeyCode() == KeyEvent.VK_1 && ctrl) {
				MapEditorFrame.getInstance().getTabPane().setSelectedIndex(0);
			}
			else if (event.getKeyCode() == KeyEvent.VK_2 && ctrl) {
				MapEditorFrame.getInstance().getTabPane().setSelectedIndex(1);
			}
			else if (event.getKeyCode() == KeyEvent.VK_3 && ctrl) {
				MapEditorFrame.getInstance().getTabPane().setSelectedIndex(2);
			}
		}
		
		return super.dispatchKeyEvent(event);
	}
	
}
