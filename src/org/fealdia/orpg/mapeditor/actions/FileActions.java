package org.fealdia.orpg.mapeditor.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.ArchetypeManager2;
import org.fealdia.orpg.common.maps.BackgroundMapper;
import org.fealdia.orpg.common.maps.ChunkList;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.ui.ExtensionlessFileFilter;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.ScreenshotUtil;
import org.fealdia.orpg.mapeditor.ui.MapEditorFrame;
import org.fealdia.orpg.mapeditor.ui.SceneEditorIFrame;

/**
 * Container for file action classes.
 */
public class FileActions {
	private static Logger logger = Logger.getLogger(FileActions.class);

	private NewMapAction newMapAction;
	private ExitAction exitAction;
	private SaveAction saveAction;
	private SaveAsAction saveAsAction;

	private MapEditorFrame frame;

	private OpenMapAction openMapAction;

	private ReloadAction reloadAction;
	
	private ExportMinimapAction exportMinimapAction;

	public FileActions(MapEditorFrame frame) {
		this.frame = frame;

		this.newMapAction = new NewMapAction();
		this.openMapAction = new OpenMapAction();
		this.saveAction = new SaveAction();
		this.saveAsAction = new SaveAsAction();
		this.reloadAction = new ReloadAction();
		this.exportMinimapAction = new ExportMinimapAction();
		this.exitAction = new ExitAction();
	}

	/**
	 * Get JFileChooser common for opening and saving.
	 */
	protected JFileChooser getMapChooser(String title) {
		// Initial directory: maps/
		JFileChooser chooser = new JFileChooser(PathConfig.getMapDir());
		chooser.setFileFilter(new ExtensionlessFileFilter());
		chooser.setDialogTitle(title);
		return chooser;
	}

	protected class NewMapAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public NewMapAction() {
			putValue(Action.NAME, "New map");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl N"));
			// putValue(Action.SMALL_ICON, UIManager.getIcon("Tree.leafIcon"));
			putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(
					"/toolbarButtonGraphics/general/New16.gif")));
		}

		public void actionPerformed(ActionEvent e) {
			// TODO ask for size and name?
			
			// create new SceneEditorIFrame, and add it to JDesktopPane
			frame.addSceneEditorIFrame(new SceneEditorIFrame());
		}
	}

	protected class OpenMapAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public OpenMapAction() {
			putValue(Action.NAME, "Open map...");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl O"));
			// putValue(Action.SMALL_ICON,
			// UIManager.getIcon("FileView.directoryIcon"));
			putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(
					"/toolbarButtonGraphics/general/Open16.gif")));
		}

		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = getMapChooser("Open map...");

			// Ask user to cancel if current map is modified
			/*
			if (sceneEditor.getScene().isModified()) {
				if (JOptionPane
						.showConfirmDialog(
								frame,
								"The current map has unsaved changes.\nDiscard changes?",
								"Discard changes?", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
					return;
				}
			}
			*/

			chooser.showOpenDialog(frame);

			if (chooser.getSelectedFile() == null) {
				logger.info("User cancelled 'open map' file choosing");
				return;
			}

			Scene scene = new Scene();
			scene.loadFromFile(chooser.getSelectedFile().getAbsolutePath());
			scene.loadEntities();
			frame.addSceneEditorIFrame(new SceneEditorIFrame(scene));
		}
	}

	protected class SaveAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public SaveAction() {
			putValue(Action.NAME, "Save");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl S"));
			putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(
					"/toolbarButtonGraphics/general/Save16.gif")));
		}

		public void actionPerformed(ActionEvent e) {
			// Save the map with the name it has
			getActiveScene().saveToFile();
			
			// Save entities
			getActiveScene().saveEntities();

			// Save global chunk list here
			ChunkList.getInstance().saveToFile();
		}
	}

	protected class SaveAsAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public SaveAsAction() {
			putValue(Action.NAME, "Save as...");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
			putValue(Action.ACCELERATOR_KEY, KeyStroke
					.getKeyStroke("ctrl shift S"));
			putValue(Action.SMALL_ICON, new ImageIcon(getClass().getResource(
					"/toolbarButtonGraphics/general/SaveAs16.gif")));
		}

		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = getMapChooser("Save map as...");

			// Set initial name to that of the Scene
			Scene scene = getActiveScene();
			if (scene == null) {
				logger.warn("SaveAsAction with no active scene");
				return;
			}
			
			chooser.setSelectedFile(new File(scene.getName()));

			// Did the user cancel?
			if (chooser.showSaveDialog(frame) != JFileChooser.APPROVE_OPTION) {
				logger.info("User cancelled 'save as' file choosing, aborting");
				return;
			}

			// If file exists, confirm overwrite before calling save
			File selectedFile = chooser.getSelectedFile();
			if (selectedFile.exists()) {
				String message = "You chose to save the map to '"
						+ selectedFile.getAbsolutePath()
						+ "', however, the file already exists.\n\nOverwrite file?";
				if (JOptionPane.showConfirmDialog(frame, message,
						"Overwrite file?", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
					logger
							.info("User cancelled 'save as' before overwriting a file");
					return;
				}
			}

			// Set selected file name and perform SaveAction
			getActiveScene().setNameFromFile(selectedFile.getAbsolutePath());

			// Save the map in the given file
			logger.debug("Save as: " + selectedFile.getName());
			logger.debug(" Full path: " + selectedFile.getAbsolutePath());
			// scene.saveToFile(chooser.getSelectedFile().getAbsolutePath());

			getSaveAction().actionPerformed(null);
		}
	}

	protected class ReloadAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public ReloadAction() {
			putValue(Action.NAME, "Reload");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_R);
			putValue(Action.SHORT_DESCRIPTION, "Reload resources");
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl R"));
		}

		public void actionPerformed(ActionEvent e) {
			BackgroundMapper.getInstance().reload();
			ImageMapper.getInstance().reload();
			ArchetypeManager2.getInstance().reload();
		}
	}
	
	protected class ExportMinimapAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		
		public ExportMinimapAction() {
			putValue(Action.NAME, "Export minimap");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_M);
			putValue(Action.SHORT_DESCRIPTION, "Export image of the minimap");
		}
		
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Export minimap");
			String filename = "minimap.png";
			Scene active = frame.getActiveScene();
			if (active != null) {
				filename = "minimap-" + active.getName() + "-" + new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date()) + ".png";
			}
			chooser.setSelectedFile(new File(filename));
			
			if (chooser.showSaveDialog(frame) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			
			File selected = chooser.getSelectedFile();
			
			ScreenshotUtil.saveScreenshot(frame.getMinimap(), selected.getAbsolutePath());
		}
	}

	protected class ExitAction extends AbstractAction {
		private static final long serialVersionUID = 1L;

		public ExitAction() {
			putValue(Action.NAME, "Exit");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl Q"));
		}

		public void actionPerformed(ActionEvent e) {
			System.exit(0); // TODO ask to save first?
		}
	}

	public AbstractAction getNewMapAction() {
		return newMapAction;
	}

	public AbstractAction getExitAction() {
		return exitAction;
	}

	public AbstractAction getSaveAction() {
		return saveAction;
	}

	public AbstractAction getSaveAsAction() {
		return saveAsAction;
	}

	protected Scene getActiveScene() {
		if (frame.getActiveSceneEditor() == null) {
			logger.error("Scene editor not set, but active scene requested");
			return null;
		}

		return frame.getActiveSceneEditor().getScene();
	}

	public AbstractAction getOpenMapAction() {
		return openMapAction;
	}

	public AbstractAction getReloadAction() {
		return reloadAction;
	}

	public ExportMinimapAction getExportMinimapAction() {
		return exportMinimapAction;
	}

}
