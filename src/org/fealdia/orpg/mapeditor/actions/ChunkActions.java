package org.fealdia.orpg.mapeditor.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Chunk;
import org.fealdia.orpg.common.maps.ChunkList;
import org.fealdia.orpg.mapeditor.ui.MapEditorFrame;

/**
 * Container for chunk action classes.
 */
public class ChunkActions {
	private static Logger logger = Logger.getLogger(ChunkActions.class);
	
	private MapEditorFrame frame;
	
	private CreateChunkAction createChunkAction;
	private CreateFilledChunkAction createFilledChunkAction;
	private DuplicateChunkAction duplicateChunkAction;

	public ChunkActions(MapEditorFrame frame) {
		this.frame = frame;
		
		this.createChunkAction = new CreateChunkAction();
		this.createFilledChunkAction = new CreateFilledChunkAction();
		this.duplicateChunkAction = new DuplicateChunkAction();
		
		logger.debug("Created");
	}
	
	protected class CreateChunkAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		
		public CreateChunkAction() {
			putValue(Action.NAME, "Create new chunk");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl 1"));
		}

		public void actionPerformed(ActionEvent e) {
			ChunkList.getInstance().addChunk(new Chunk());
			frame.getChunkTool().repaint();
		}
	}

	protected class CreateFilledChunkAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		
		public CreateFilledChunkAction() {
			putValue(Action.NAME, "Create new filled chunk");
			putValue(Action.SHORT_DESCRIPTION, "Create a new chunk and fill it with currently selected tile");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_F);
		}

		public void actionPerformed(ActionEvent e) {
			Chunk chunk = new Chunk();
			chunk.fill(frame.getTileTool().getSelected());
			ChunkList.getInstance().addChunk(chunk);
			frame.getChunkTool().repaint();
		}
	}
	
	protected class DuplicateChunkAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		
		public DuplicateChunkAction() {
			putValue(Action.NAME, "Duplicate selected chunk");
			putValue(Action.MNEMONIC_KEY, KeyEvent.VK_D);
		}

		public void actionPerformed(ActionEvent e) {
			// Duplicate the selected chunk
			ChunkList.getInstance().duplicateChunk(frame.getChunkTool().getSelected());
			frame.getChunkTool().repaint();
		}
	}

	public AbstractAction getCreateChunkAction() {
		return createChunkAction;
	}

	public AbstractAction getCreateFilledChunkAction() {
		return createFilledChunkAction;
	}

	public AbstractAction getDuplicateChunkAction() {
		return duplicateChunkAction;
	}

}
