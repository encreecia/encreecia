package org.fealdia.orpg.common.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Helper POJO for JAXB HashMap marshaling.
 */
@XmlRootElement
@XmlType(propOrder = { "key", "value" })
public class MapEntry {
	@XmlAttribute
	public String key;

	@XmlAttribute
	public String value;

	public MapEntry(final String key, final String value) {
		this.key = key;
		this.value = value;
	}

	// JAXB
	@SuppressWarnings("unused")
	private MapEntry() {
	}

	public static MapEntry[] mapToArray(final Map<String,String> properties) {
		if (properties.size() == 0) {
			return null;
		}

		List<MapEntry> list = new ArrayList<MapEntry>(properties.size());
		for (Entry<String,String> mapEntry : properties.entrySet()) {
			list.add(new MapEntry(mapEntry.getKey(), mapEntry.getValue()));
		}

		return list.toArray(new MapEntry[list.size()]);
	}

	public static HashMap<String,String> fromArray(MapEntry[] mapentries) {
		HashMap<String,String> result = new HashMap<String,String>(mapentries.length);

		for (MapEntry mapEntry : mapentries) {
			result.put(mapEntry.key, mapEntry.value);
		}
		return result;
	}
}
