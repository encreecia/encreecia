package org.fealdia.orpg.common.xml;

import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MapAdapter extends XmlAdapter<MapEntry[],Map<String,String>> {
	@Override
	public MapEntry[] marshal(Map<String,String> map) throws Exception {
		return MapEntry.mapToArray(map);
	}

	@Override
	public Map<String,String> unmarshal(MapEntry[] entries) throws Exception {
		return MapEntry.fromArray(entries);
	}

}
