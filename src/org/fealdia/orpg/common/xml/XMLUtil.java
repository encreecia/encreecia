package org.fealdia.orpg.common.xml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XMLUtil {
	public static <T> T fromXML(Class<?> clazz, String filename) throws JAXBException, FileNotFoundException {
		return fromXML(clazz, new FileInputStream(new File(filename)));
	}
	
	public static <T> T fromXML(Class<?> clazz, InputStream input) throws JAXBException {
		Unmarshaller u = JAXBContext.newInstance(clazz).createUnmarshaller();
		@SuppressWarnings("unchecked")
		T ele = (T) u.unmarshal(input);
		return ele;
	}
	
	public static <T> T fromXMLString(Class<?> clazz, final String data) throws JAXBException {
		ByteArrayInputStream in = new ByteArrayInputStream(data.getBytes());
		return fromXML(clazz, in);
	}

	public static void toXML(Object object, OutputStream stream) throws JAXBException {
		toXML(object, stream, true);
	}

	public static void toXML(Object object, OutputStream stream, boolean formatted) throws JAXBException {
		Marshaller m = JAXBContext.newInstance(object.getClass()).createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatted);
		m.marshal(object, stream);
	}

	public static String toXMLString(Object object, boolean formatted) throws JAXBException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		toXML(object, out, formatted);
		return out.toString();
	}
}
