package org.fealdia.orpg.common.xml;

import java.util.LinkedList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Extends DefaultHandler of SAX.
 */
public class SAXHandler extends DefaultHandler {
	StringBuffer charactersInElement = new StringBuffer();
	
	List<String> openTags = new LinkedList<String>();
	
	/**
	 * Returns characters in last element. Should be called before endElement().
	 */
	public String getCharactersInElement() {
		return charactersInElement.toString();
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		super.characters(ch, start, length);
		
		charactersInElement.append(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
		
		if (charactersInElement.length() > 0) {
			charactersInElement = new StringBuffer();
		}
		openTags.remove(openTags.size() - 1);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		
		if (charactersInElement.length() > 0) {
			charactersInElement = new StringBuffer();
		}
		openTags.add(qName);
	}
	
}
