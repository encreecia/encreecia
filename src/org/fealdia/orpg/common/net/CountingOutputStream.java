package org.fealdia.orpg.common.net;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * A wrapper for OutputStream that counts how much data has passed through the stream.
 */
public class CountingOutputStream extends FilterOutputStream {
	private long written = 0;
	private long timestamp = 0;
	
	public CountingOutputStream(OutputStream out) {
		super(out);
	}

	@Override
	public void write(int b) throws IOException {
		super.write(b);
		setWritten(getWritten() + 1);
	}
	
	public long getWritten() {
		return written;
	}

	public void setWritten(long written) {
		this.written = written;
		if (timestamp == 0) {
			timestamp = System.currentTimeMillis();
		}
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		out.write(b, off, len);
		setWritten(getWritten() + len);
	}

	@Override
	public void write(byte[] b) throws IOException {
		out.write(b);
		setWritten(getWritten() + b.length);
	}

	/**
	 * Get average bytes/sec, starting from first byte sent.
	 */
	public int getBytesPerSecond() {
		long now = System.currentTimeMillis();
		float diff_secs = ((float)(now - timestamp)) / 1000;
		
		float bps = getWritten() / diff_secs;
		
		return (int) bps;
	}
}
