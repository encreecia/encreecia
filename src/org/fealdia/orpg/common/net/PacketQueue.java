package org.fealdia.orpg.common.net;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Packet queue for ProtoSocket.
 */
public class PacketQueue implements Runnable {
	private static Logger logger = Logger.getLogger(PacketQueue.class);
	
	private List<ProtoPacket> queue = Collections.synchronizedList(new LinkedList<ProtoPacket>());
	
	private ProtocolOutputStream pos = null;
	private CountingOutputStream countingStream;
	
	private boolean die = false;
	
	private int packetsSent = 0;
	
	public PacketQueue(OutputStream out) throws IOException {
		countingStream = new CountingOutputStream(out);
		this.pos = new ProtocolOutputStream(new BufferedOutputStream(countingStream));
	}
	
	/**
	 * Send a packet over socket
	 */
	private synchronized void sendMessage(ProtoPacket packet) throws IOException {
		if (logger.isTraceEnabled()) {
			logger.trace(String.format("sendMessage[%d]: %s", countingStream.getWritten(), packet));
		}

		pos.writePacket(packet);
		pos.flush();
		packetsSent++;
	}
	
	/**
	 * Queue a message to be sent by the sender thread.
	 */
	public void queueMessage(ProtoPacket message) {
		queue.add(message);
		synchronized (queue) {
			queue.notify(); // wake up sender thread
		}
	}

	public void run() {
		logger.trace("Started");
		
		try {
			// Keep sending packets from queue forever
			while (!die) {
				while (!die && queue.size() > 0) {
					ProtoPacket packet = queue.remove(0);
					sendMessage(packet);
				}

				// If no messages left, sleep
				synchronized (queue) {
					if (queue.size() > 0 || die) {
						continue;
					}
					try {
						queue.wait();
					} catch (InterruptedException e) {
						logger.warn("Interrupted", e);
					}
				}
			}
		}
		catch (Exception e) {
			logger.error("PacketQueue.run() exiting because of exception", e);
		}
		logger.trace("Dying");
	}
	
	/**
	 * Have this thread die.
	 */
	public void die() {
		die = true;
		synchronized (queue) {
			queue.notify();
		}
	}
	
	public void clear() {
		queue.clear();
	}
	
	public int getPacketsSent() {
		return packetsSent;
	}
	
	public long getBytesSent() {
		return countingStream.getWritten();
	}
	
	public int getBytesSentPerSecond() {
		return countingStream.getBytesPerSecond();
	}
	
}
