package org.fealdia.orpg.common.net.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ProtoQuest {
	public final int id;
	public final String title;
	public final String description;

	public ProtoQuest(int id, String title, String description) {
		this.id = id;
		this.title = title;
		this.description = description;
	}

	public ProtoQuest(DataInputStream dis) throws IOException {
		this(dis.readInt(), dis.readUTF(), dis.readUTF());
	}

	public void writeToStream(DataOutputStream dos) throws IOException {
		dos.writeInt(id);
		dos.writeUTF(title);
		dos.writeUTF(description);
	}
}
