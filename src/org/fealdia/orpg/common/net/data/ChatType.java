package org.fealdia.orpg.common.net.data;

public enum ChatType {
	/** Equals GLOBAL until there are enough players to change this to SAY */
	DEFAULT,
	/** Sent to all players. May be removed, or limited to higher levels in the future. */
	GLOBAL,
	GUILD,
	PARTY,
	/** All players within <= 1 squares */
	WHISPER,
	/** Four times radius of say */
	SHOUT,
	/** Heard within MapEntity.VISIBLE_RADIUS */
	SAY,
	EMOTE,
}
