package org.fealdia.orpg.common.net.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Temporary mapping of faceId<->faceName. Persists until client-server connection is lost.
 */
public class FaceInfo {
	public final int id;
	public final String name;
	public final long crc32;

	public FaceInfo(int id, String name, long crc32) {
		this.id = id;
		this.name = name;
		this.crc32 = crc32;
	}

	public FaceInfo(DataInputStream data) throws IOException {
		this(data.readInt(), data.readUTF(), data.readLong());
	}

	public void writeToStream(DataOutputStream out) throws IOException {
		out.writeInt(id);
		out.writeUTF(name);
		out.writeLong(crc32);
	}
}
