package org.fealdia.orpg.common.net.data;

import java.util.HashMap;
import java.util.Map;

public class FaceMapper {
	private Map<Integer,FaceInfo> idMap = new HashMap<Integer,FaceInfo>();
	private Map<String,FaceInfo> faceMap = new HashMap<String,FaceInfo>();

	public void addFaceMapping(FaceInfo faceInfo) {
		if (idMap.put(faceInfo.id, faceInfo) != null || faceMap.put(faceInfo.name, faceInfo) != null) {
			throw new IllegalArgumentException("Face id already in FaceMapper. Is server or client persisting FaceMapper over sessions?");
		}
	}

	public FaceInfo getFaceById(final int faceId) {
		return idMap.get(faceId);
	}

	public FaceInfo getFaceByName(final String faceName) {
		return faceMap.get(faceName);
	}

	public void clear() {
		idMap.clear();
		faceMap.clear();
	}
}
