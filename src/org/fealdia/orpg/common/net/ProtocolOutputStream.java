package org.fealdia.orpg.common.net;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * See ProtocolInputStream
 */
public class ProtocolOutputStream extends DataOutputStream {

	public ProtocolOutputStream(OutputStream inputStream) {
		super(inputStream);
	}

	public void writePacket(final ProtoPacket protoPacket) throws IOException {
		byte[] data = protoPacket.constructBytes();
		int length = data.length;

		// For short packets, length is stored in first two bytes
		if (length < ProtocolInputStream.MAX_UNSIGNED_SHORT) {
			writeShort(length); // can be read with readUnsignedShort()
		}
		// For longer ones, two bytes only tell length is longer, and is stored in the next four bytes
		else {
			writeShort(ProtocolInputStream.MAX_UNSIGNED_SHORT);
			writeInt(length);
		}

		// Now the packet type
		writeShort(protoPacket.getPacketType().ordinal());

		// And payload
		if (data != null && data.length > 0) {
			write(data);
		}
	}
}
