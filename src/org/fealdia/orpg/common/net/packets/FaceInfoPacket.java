package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.data.FaceInfo;

public class FaceInfoPacket extends ProtoPacket {
	public final FaceInfo faceInfo;

	public FaceInfoPacket(FaceInfo faceInfo) {
		super(PacketType.FACEINFO);
		this.faceInfo = faceInfo;
	}

	public FaceInfoPacket(DataInputStream data) throws IOException {
		this(new FaceInfo(data));
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		faceInfo.writeToStream(dataOutput);
	}
}
