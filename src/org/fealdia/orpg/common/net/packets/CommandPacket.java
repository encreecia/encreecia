package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class CommandPacket extends ProtoPacket {
	public final String command;

	public CommandPacket(String command) {
		super(PacketType.COMMAND);
		this.command = command;
	}

	public CommandPacket(DataInputStream dis) throws IOException {
		this(dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(command);
	}
}
