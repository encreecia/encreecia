package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class ExperiencePacket extends ProtoPacket {
	public final long experience;
	public final long experienceNext;
	public final long experienceThis;
	public final int level;

	public ExperiencePacket(long experience, long experienceNext, long experienceThis, int level) {
		super(PacketType.EXPERIENCE);
		this.experience = experience;
		this.experienceNext = experienceNext;
		this.experienceThis = experienceThis;
		this.level = level;
	}

	public ExperiencePacket(DataInputStream dis) throws IOException {
		this(dis.readLong(), dis.readLong(), dis.readLong(), dis.readUnsignedShort());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(experience);
		dataOutput.writeLong(experienceNext);
		dataOutput.writeLong(experienceThis);
		dataOutput.writeShort(level);
	}

}
