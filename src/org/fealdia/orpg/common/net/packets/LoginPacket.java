package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class LoginPacket extends ProtoPacket {
	public final String username;
	public final String password;

	public LoginPacket(String username, String password) {
		super(PacketType.LOGIN);
		this.username = username;
		this.password = password;
	}

	public LoginPacket(DataInputStream dis) throws IOException {
		this(dis.readUTF(), dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(username);
		dataOutput.writeUTF(password);
	}
}
