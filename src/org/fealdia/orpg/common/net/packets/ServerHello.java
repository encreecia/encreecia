package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class ServerHello extends ProtoPacket {
	public final int protocolVersion;
	public final String serverName;

	public ServerHello(final int protocolVersion, final String serverName) {
		super(PacketType.SERVERHELLO);
		this.protocolVersion = protocolVersion;
		this.serverName = serverName;
	}

	public ServerHello(DataInputStream dis) throws IOException {
		this(dis.readInt(), dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(protocolVersion);
		dataOutput.writeUTF(serverName);
	}
}
