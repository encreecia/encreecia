package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.data.ProtoQuest;

public class QuestOfferPacket extends ProtoPacket {
	public final String fromWho;
	public final ProtoQuest quest;

	public QuestOfferPacket(String fromWho, ProtoQuest protoQuest) {
		super(PacketType.QUESTOFFER);
		this.fromWho = fromWho;
		this.quest = protoQuest;
	}

	public QuestOfferPacket(DataInputStream dis) throws IOException {
		this(dis.readUTF(), new ProtoQuest(dis));
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(fromWho);
		quest.writeToStream(dataOutput);
	}
}
