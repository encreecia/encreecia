package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class ContainerPacket extends ProtoPacket {
	public final long containerId;
	public final List<ContainerItem> items;

	public ContainerPacket(long containerId, List<ContainerItem> items) {
		super(PacketType.CONTAINER);
		this.containerId = containerId;
		this.items = items;
	}

	public ContainerPacket(DataInputStream dis) throws IOException {
		super(PacketType.CONTAINER);
		this.containerId = dis.readLong();

		int count = dis.readInt();

		this.items = new LinkedList<ContainerItem>();

		// Read items
		for (int i = 0; i < count; i++) {
			long id = dis.readLong();
			String name = dis.readUTF();
			int face = dis.readInt();
			int quantity = dis.readInt();
			boolean equipped = dis.readBoolean();
			ContainerItem containerItem = new ContainerItem(id, name, face, quantity, equipped);
			items.add(containerItem);
		}
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(containerId);
		dataOutput.writeInt(items.size());

		// Write items
		for (ContainerItem containerItem : items) {
			dataOutput.writeLong(containerItem.id);
			dataOutput.writeUTF(containerItem.name);
			dataOutput.writeInt(containerItem.faceId);
			dataOutput.writeInt(containerItem.quantity);
			dataOutput.writeBoolean(containerItem.equipped);
		}
	}

	public static class ContainerItem {
		public final long id;
		public final String name;
		public final int faceId;
		public final int quantity;
		public final boolean equipped;

		public ContainerItem(long id, String name, int faceId, int quantity, boolean equipped) {
			this.id = id;
			this.name = name;
			this.faceId = faceId;
			this.quantity = quantity;
			this.equipped = equipped;
		}
	}
}
