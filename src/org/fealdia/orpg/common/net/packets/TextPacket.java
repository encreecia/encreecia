package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class TextPacket extends ProtoPacket {
	public final String text;

	public TextPacket(PacketType packetType, String text) {
		super(packetType);
		this.text = text;
	}

	public TextPacket(PacketType packetType, DataInputStream dis) throws IOException {
		this(packetType, dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(text);
	}
}
