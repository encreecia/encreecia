package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class CheckFilePacket extends ProtoPacket {
	public final String filename;
	public final byte[] sha1;

	public CheckFilePacket(String filename, byte[] sha1) {
		super(PacketType.CHECKFILE);
		this.filename = filename;
		this.sha1 = sha1;
	}

	public CheckFilePacket(DataInputStream dis) throws IOException {
		super(PacketType.CHECKFILE);
		this.filename = dis.readUTF();
		byte[] buf = new byte[dis.available()];
		dis.readFully(buf);
		this.sha1 = buf;
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(filename);
		dataOutput.write(sha1);
	}
}
