package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.data.ChatType;

public class ServerChatPacket extends ProtoPacket {
	public final String who;
	public final ChatType chatType;
	public final String text;

	public ServerChatPacket(String who, ChatType chatType, String text) {
		super(PacketType.SERVERCHAT);
		this.who = who;
		this.chatType = chatType;
		this.text = text;
	}

	public ServerChatPacket(DataInputStream dis) throws IOException {
		this(dis.readUTF(), ChatType.values()[dis.readChar()], dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(who);
		dataOutput.writeChar(chatType.ordinal());
		dataOutput.writeUTF(text);
	}
}
