package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

/**
 * Packet that consists of PacketType alone.
 */
public class SimplePacket extends ProtoPacket {
	public SimplePacket(PacketType packetType) {
		super(packetType);
	}

	public SimplePacket(PacketType packetType, DataInputStream dis) {
		super(packetType);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + packetType + "]";
	}
}
