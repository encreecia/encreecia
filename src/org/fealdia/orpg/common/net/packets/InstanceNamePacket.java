package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class InstanceNamePacket extends ProtoPacket {
	public final String instanceName;

	public InstanceNamePacket(String instanceName) {
		super(PacketType.INSTANCENAME);
		this.instanceName = instanceName;
	}

	public InstanceNamePacket(DataInputStream dis) throws IOException {
		this(dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(instanceName);
	}
}
