package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class PlayerPositionPacket extends ProtoPacket {
	public final int x;
	public final int y;

	public PlayerPositionPacket(int x, int y) {
		super(PacketType.PLAYERPOSITION);
		this.x = x;
		this.y = y;
	}

	public PlayerPositionPacket(DataInputStream dis) throws IOException {
		this(dis.readInt(), dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(x);
		dataOutput.writeInt(y);
	}
}
