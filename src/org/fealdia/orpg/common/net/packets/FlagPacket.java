package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

/**
 * Simple packet for carrying a boolean flag.
 */
public class FlagPacket extends ProtoPacket {
	public final boolean value;

	public FlagPacket(PacketType packetType, boolean value) {
		super(packetType);
		this.value = value;
	}

	public FlagPacket(PacketType packetType, DataInputStream dis) throws IOException {
		this(packetType, dis.readBoolean());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeBoolean(value);
	}
}
