package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class PopupPacket extends ProtoPacket {
	public final String text;

	public PopupPacket(String text) {
		super(PacketType.POPUP);
		this.text = text;
	}

	public PopupPacket(DataInputStream dis) throws IOException {
		this(dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(text);
	}
}
