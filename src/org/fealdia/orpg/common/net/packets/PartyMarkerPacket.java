package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class PartyMarkerPacket extends ProtoPacket {
	public final String sceneName;
	public final int x;
	public final int y;

	public PartyMarkerPacket(String sceneName, int x, int y) {
		super(PacketType.PARTYMARKER);
		this.sceneName = sceneName;
		this.x = x;
		this.y = y;
	}

	public PartyMarkerPacket(DataInputStream dis) throws IOException {
		this(dis.readUTF(), dis.readInt(), dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(sceneName);
		dataOutput.writeInt(x);
		dataOutput.writeInt(y);
	}
}
