package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class ItemGetPacket extends ProtoPacket {
	public final long itemId;
	public final int quantity;

	public ItemGetPacket(long itemId, int quantity) {
		super(PacketType.ITEMGET);
		this.itemId = itemId;
		this.quantity = quantity;
	}

	public ItemGetPacket(DataInputStream dis) throws IOException {
		this(dis.readLong(), dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(itemId);
		dataOutput.writeLong(quantity);
	}
}
