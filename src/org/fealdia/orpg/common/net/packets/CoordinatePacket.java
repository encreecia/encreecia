package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

/**
 * Generic packet for sending map coordinates related to some action.
 */
public class CoordinatePacket extends ProtoPacket {
	public final int x;
	public final int y;

	public CoordinatePacket(PacketType packetType, int x, int y) {
		super(packetType);
		this.x = x;
		this.y = y;
	}

	public CoordinatePacket(PacketType packetType, DataInputStream dis) throws IOException {
		this(packetType, dis.readInt(), dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(x);
		dataOutput.writeInt(y);
	}
}
