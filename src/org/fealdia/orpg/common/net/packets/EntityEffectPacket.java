package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class EntityEffectPacket extends ProtoPacket {
	public final long entityId;
	public final int faceId;
	public final int time;
	public final String text;

	public EntityEffectPacket(long entityId, int faceId, int time, String text) {
		super(PacketType.ENTITYEFFECT);
		this.entityId = entityId;
		this.faceId = faceId;
		this.time = time;
		this.text = text;
	}

	public EntityEffectPacket(DataInputStream dis) throws IOException {
		this(dis.readLong(), dis.readInt(), dis.readInt(), dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(entityId);
		dataOutput.writeInt(faceId);
		dataOutput.writeInt(time);
		dataOutput.writeUTF(text == null ? "" : text);
	}
}
