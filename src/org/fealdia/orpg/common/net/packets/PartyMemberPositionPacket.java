package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class PartyMemberPositionPacket extends ProtoPacket {
	public final long id;
	public final int x;
	public final int y;

	public PartyMemberPositionPacket(long id, int x, int y) {
		super(PacketType.PARTYMEMBERPOS);
		this.id = id;
		this.x = x;
		this.y = y;
	}

	public PartyMemberPositionPacket(DataInputStream dis) throws IOException {
		this(dis.readLong(), dis.readInt(), dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(id);
		dataOutput.writeInt(x);
		dataOutput.writeInt(y);
	}
}
