package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.data.MusicHint;

public class MusicHintPacket extends ProtoPacket {
	public final MusicHint musicHint;

	public MusicHintPacket(MusicHint musicHint) {
		super(PacketType.MUSICHINT);
		this.musicHint = musicHint;
	}

	public MusicHintPacket(DataInputStream dis) throws IOException {
		this(MusicHint.values()[dis.readShort()]);
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeShort(musicHint.ordinal());
	}
}
