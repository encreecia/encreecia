package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.data.ProtoQuest;

public class QuestLogPacket extends ProtoPacket {
	public final List<ProtoQuest> quests;

	public QuestLogPacket(List<ProtoQuest> quests) {
		super(PacketType.QUESTLOG);
		this.quests = quests;
	}

	public QuestLogPacket(DataInputStream dis) throws IOException {
		super(PacketType.QUESTLOG);
		int count = dis.readInt();

		List<ProtoQuest> quests = new LinkedList<ProtoQuest>();
		for (int i = 0; i < count; i++) {
			quests.add(new ProtoQuest(dis));
		}

		this.quests = quests;
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(quests.size());
		for (ProtoQuest protoQuest : quests) {
			protoQuest.writeToStream(dataOutput);
		}
	}
}
