package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class FilePacket extends ProtoPacket {
	public final String filename;
	public final byte[] data;

	public FilePacket(String filename, byte[] data) {
		super(PacketType.FILE);
		this.filename = filename;
		this.data = data;
	}

	public FilePacket(DataInputStream dis) throws IOException {
		super(PacketType.FILE);
		this.filename = dis.readUTF();
		byte[] data = new byte[dis.available()];
		dis.readFully(data);
		this.data = data;
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeUTF(filename);
		dataOutput.write(data);
	}
}
