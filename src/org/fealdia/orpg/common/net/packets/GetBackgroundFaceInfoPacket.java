package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class GetBackgroundFaceInfoPacket extends ProtoPacket {
	public final int backgroundId;

	public GetBackgroundFaceInfoPacket(int backgroundId) {
		super(PacketType.GETBACKGROUNDFACEINFO);
		this.backgroundId = backgroundId;
	}

	public GetBackgroundFaceInfoPacket(DataInputStream dis) throws IOException {
		this(dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(backgroundId);
	}
}
