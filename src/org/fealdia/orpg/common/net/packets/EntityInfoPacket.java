package org.fealdia.orpg.common.net.packets;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class EntityInfoPacket extends ProtoPacket {
	public final long id;
	public final String entityType;
	public final boolean isCreature;
	public final boolean isItem;
	public final String name;
	public final int face;
	public final int overlayId;
	public final int quantity;
	public final String guild;
	public final Color nameColor;

	public EntityInfoPacket(long id, String entityType, boolean isCreature, boolean isItem, String name, int face, int overlayId, int quantity, String guild, Color color) {
		super(PacketType.ENTITYINFO);
		this.id = id;
		this.entityType = entityType;
		this.isCreature = isCreature;
		this.isItem = isItem;
		this.name = name;
		this.face = face;
		this.overlayId = overlayId;
		this.quantity = quantity;
		this.guild = guild;
		this.nameColor = color;
	}

	public EntityInfoPacket(DataInputStream dis) throws IOException {
		this(dis.readLong(), dis.readUTF(), dis.readBoolean(), dis.readBoolean(), dis.readUTF(), dis.readInt(), dis.readInt(), dis.readInt(), dis.readUTF(), colorFromValue(dis.readInt()));
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(id);
		dataOutput.writeUTF(entityType);
		dataOutput.writeBoolean(isCreature);
		dataOutput.writeBoolean(isItem);
		dataOutput.writeUTF(name);
		dataOutput.writeInt(face);
		dataOutput.writeInt(overlayId);
		dataOutput.writeInt(quantity);
		dataOutput.writeUTF(guild);
		dataOutput.writeInt(nameColor == null ? -1 : nameColor.getRGB());
	}

	private static Color colorFromValue(int sRGB) {
		return sRGB == -1 ? null : new Color(sRGB);
	}
}
