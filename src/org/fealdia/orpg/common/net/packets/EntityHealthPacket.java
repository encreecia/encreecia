package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class EntityHealthPacket extends ProtoPacket {
	public final long id;
	public final int percentage;

	public EntityHealthPacket(long id, int percentage) {
		super(PacketType.ENTITYHEALTH);
		this.id = id;
		this.percentage = percentage;
	}

	public EntityHealthPacket(DataInputStream dis) throws IOException {
		this(dis.readLong(), dis.readShort());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(id);
		dataOutput.writeShort(percentage);
	}
}
