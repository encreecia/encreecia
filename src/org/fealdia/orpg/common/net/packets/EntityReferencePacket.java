package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

/**
 * Generic packet that only contains reference to an entity.
 */
public class EntityReferencePacket extends ProtoPacket {
	public final long id;

	public EntityReferencePacket(PacketType packetType, long id) {
		super(packetType);
		this.id = id;
	}

	public EntityReferencePacket(PacketType packetType, DataInputStream dis) throws IOException {
		this(packetType, dis.readLong());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(id);
	}
}
