package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class EntityMovePacket extends ProtoPacket {
	public final long id;
	public final int x;
	public final int y;
	public final boolean src_visible;
	public final boolean dst_visible;

	public EntityMovePacket(long id, int x, int y, boolean src_visible, boolean dst_visible) {
		super(PacketType.ENTITYMOVE);
		this.id = id;
		this.x = x;
		this.y = y;
		this.src_visible = src_visible;
		this.dst_visible = dst_visible;
	}

	public EntityMovePacket(DataInputStream dis) throws IOException {
		this(dis.readLong(), dis.readInt(), dis.readInt(), dis.readBoolean(), dis.readBoolean());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeLong(id);
		dataOutput.writeInt(x);
		dataOutput.writeInt(y);
		dataOutput.writeBoolean(src_visible);
		dataOutput.writeBoolean(dst_visible);
	}

}
