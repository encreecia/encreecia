package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class FacePacket extends ProtoPacket {
	public final int faceId;
	public final byte[] imageData;

	public FacePacket(int faceId, byte[] imageData) {
		super(PacketType.FACE);
		this.faceId = faceId;
		this.imageData = imageData;
	}

	public FacePacket(DataInputStream dis) throws IOException {
		super(PacketType.FACE);

		this.faceId = dis.readInt();
		byte[] data = new byte[dis.available()];
		dis.readFully(data);
		this.imageData = data;
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(faceId);
		dataOutput.write(imageData);
	}
}
