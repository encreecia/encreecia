package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class MovementVectorPacket extends ProtoPacket {
	public final int dx;
	public final int dy;

	public MovementVectorPacket(int dx, int dy) {
		super(PacketType.MOVE);
		this.dx = dx;
		this.dy = dy;
	}

	public MovementVectorPacket(DataInputStream dis) throws IOException {
		this(dis.readShort(), dis.readShort());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeShort(dx);
		dataOutput.writeShort(dy);
	}
}
