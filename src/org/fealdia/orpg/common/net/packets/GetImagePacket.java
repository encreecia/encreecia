package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class GetImagePacket extends ProtoPacket {
	public final int id;

	public GetImagePacket(int id) {
		super(PacketType.GETIMAGE);
		this.id = id;
	}

	public GetImagePacket(DataInputStream dis) throws IOException {
		this(dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(id);
	}
}
