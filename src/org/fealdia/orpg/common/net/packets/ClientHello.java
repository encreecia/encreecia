package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class ClientHello extends ProtoPacket {
	public final int protocolVersion;
	public final String clientName;
	public final String javaVersion;
	public final String osName;
	public final String osVersion;

	public ClientHello(int protocolVersion, String clientName, String javaVersion, String osName, String osVersion) {
		super(PacketType.CLIENTHELLO);
		this.protocolVersion = protocolVersion;
		this.clientName = clientName;
		this.javaVersion = javaVersion;
		this.osName = osName;
		this.osVersion = osVersion;
	}

	public ClientHello(DataInputStream dis) throws IOException {
		this(dis.readInt(), dis.readUTF(), dis.readUTF(), dis.readUTF(), dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(protocolVersion);
		dataOutput.writeUTF(clientName);
		dataOutput.writeUTF(javaVersion);
		dataOutput.writeUTF(osName);
		dataOutput.writeUTF(osVersion);
	}
}
