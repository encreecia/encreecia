package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class PlayerHealthPacket extends ProtoPacket {
	public final int currentHealth;
	public final int maxHealth;

	public PlayerHealthPacket(int currentHealth, int maxHealth) {
		super(PacketType.PLAYERHEALTH);
		this.currentHealth = currentHealth;
		this.maxHealth = maxHealth;
	}

	public PlayerHealthPacket(DataInputStream dis) throws IOException {
		this(dis.readInt(), dis.readInt());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeInt(currentHealth);
		dataOutput.writeInt(maxHealth);
	}
}
