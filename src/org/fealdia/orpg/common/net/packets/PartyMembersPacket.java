package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;

public class PartyMembersPacket extends ProtoPacket {
	public final List<Long> ids;

	public PartyMembersPacket(List<Long> ids) {
		super(PacketType.PARTYMEMBERS);
		this.ids = ids;
	}

	public PartyMembersPacket(DataInputStream dis) throws IOException {
		super(PacketType.PARTYMEMBERS);
		int count = dis.available() / 8;
		List<Long> ids = new ArrayList<Long>(count);
		for (int i = 0; i < count; i++) {
			ids.add(dis.readLong());
		}
		this.ids = ids;
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		for (Long id : ids) {
			dataOutput.writeLong(id);
		}
	}
}
