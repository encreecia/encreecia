package org.fealdia.orpg.common.net.packets;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.data.ChatType;

public class ClientChatPacket extends ProtoPacket {
	public final ChatType chatType;
	public final String text;

	public ClientChatPacket(ChatType chatType, String text) {
		super(PacketType.CHAT);
		this.chatType = chatType;
		this.text = text;
	}

	public ClientChatPacket(DataInputStream dis) throws IOException {
		this(ChatType.values()[dis.readChar()], dis.readUTF());
	}

	@Override
	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
		dataOutput.writeChar(chatType.ordinal());
		dataOutput.writeUTF(text);
	}
}
