package org.fealdia.orpg.common.net;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ProtocolInputStream extends DataInputStream {
	static final int MAX_UNSIGNED_SHORT = 65535;

	public ProtocolInputStream(final InputStream inputStream) {
		super(inputStream);
	}

	public ProtoPacket readPacket() throws IOException {
		// Packet length is usually 2 bytes...
		int length = readUnsignedShort();

		// ... but if the length is maximum, it's a sign of actually larger packet, so read the next 4 bytes as new length
		if (length == MAX_UNSIGNED_SHORT) {
			length = readInt();
		}

		// Packet type
		int packetTypeValue = readUnsignedShort();
		PacketType packetType = PacketType.values()[packetTypeValue];

		// Now read the (optional) payload
		byte[] data = new byte[length];
		readFully(data, 0, length);

		return ProtoPacket.create(packetType, data);
	}
}
