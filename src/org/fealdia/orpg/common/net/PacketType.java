package org.fealdia.orpg.common.net;

import org.fealdia.orpg.common.net.packets.CheckFilePacket;
import org.fealdia.orpg.common.net.packets.ClientChatPacket;
import org.fealdia.orpg.common.net.packets.ClientHello;
import org.fealdia.orpg.common.net.packets.CommandPacket;
import org.fealdia.orpg.common.net.packets.ContainerPacket;
import org.fealdia.orpg.common.net.packets.CoordinatePacket;
import org.fealdia.orpg.common.net.packets.EntityEffectPacket;
import org.fealdia.orpg.common.net.packets.EntityHealthPacket;
import org.fealdia.orpg.common.net.packets.EntityInfoPacket;
import org.fealdia.orpg.common.net.packets.EntityMovePacket;
import org.fealdia.orpg.common.net.packets.EntityReferencePacket;
import org.fealdia.orpg.common.net.packets.ExperiencePacket;
import org.fealdia.orpg.common.net.packets.FaceInfoPacket;
import org.fealdia.orpg.common.net.packets.FacePacket;
import org.fealdia.orpg.common.net.packets.FilePacket;
import org.fealdia.orpg.common.net.packets.FlagPacket;
import org.fealdia.orpg.common.net.packets.GetBackgroundFaceInfoPacket;
import org.fealdia.orpg.common.net.packets.GetImagePacket;
import org.fealdia.orpg.common.net.packets.InstanceNamePacket;
import org.fealdia.orpg.common.net.packets.ItemGetPacket;
import org.fealdia.orpg.common.net.packets.LoginPacket;
import org.fealdia.orpg.common.net.packets.MovementVectorPacket;
import org.fealdia.orpg.common.net.packets.MusicHintPacket;
import org.fealdia.orpg.common.net.packets.PartyMarkerPacket;
import org.fealdia.orpg.common.net.packets.PartyMemberPositionPacket;
import org.fealdia.orpg.common.net.packets.PartyMembersPacket;
import org.fealdia.orpg.common.net.packets.PlayerHealthPacket;
import org.fealdia.orpg.common.net.packets.PlayerPositionPacket;
import org.fealdia.orpg.common.net.packets.PopupPacket;
import org.fealdia.orpg.common.net.packets.QuestLogPacket;
import org.fealdia.orpg.common.net.packets.QuestOfferPacket;
import org.fealdia.orpg.common.net.packets.ServerChatPacket;
import org.fealdia.orpg.common.net.packets.ServerHello;
import org.fealdia.orpg.common.net.packets.SimplePacket;
import org.fealdia.orpg.common.net.packets.TextPacket;

public enum PacketType {
	LOGIN(LoginPacket.class),
	LOGINFAILURE(TextPacket.class),
	LOGINSUCCESS(SimplePacket.class),
	INSTANCENAME(InstanceNamePacket.class),
	PLAYERPOSITION(PlayerPositionPacket.class),
	NOTICE(TextPacket.class),
	POPUP(PopupPacket.class),
	ENTITYCACHECLEAR(SimplePacket.class),
	ENTITYMOVE(EntityMovePacket.class),
	ENTITYINFO(EntityInfoPacket.class),
	ENTITYHEALTH(EntityHealthPacket.class),
	TARGET(EntityReferencePacket.class),
	EXPERIENCE(ExperiencePacket.class),
	CONTAINER(ContainerPacket.class),
	ENTITYEFFECT(EntityEffectPacket.class),
	FILE(FilePacket.class),
	ATTACKMODE(FlagPacket.class),
	LOGOUT(SimplePacket.class),
	MASTER(FlagPacket.class),
	CHAT(ClientChatPacket.class),
	PARTYMEMBERS(PartyMembersPacket.class),
	PARTYMEMBERPOS(PartyMemberPositionPacket.class),
	PARTYLEFT(SimplePacket.class),
	PARTYMEMBERPOSCLEAR(EntityReferencePacket.class),
	CONTAINERCLOSED(EntityReferencePacket.class),
	PARTYMARKER(PartyMarkerPacket.class),
	QUESTOFFER(QuestOfferPacket.class),
	QUESTLOG(QuestLogPacket.class),
	COMMAND(CommandPacket.class),
	ENTITYQUERY(EntityReferencePacket.class),
	ITEMAPPLY(EntityReferencePacket.class),
	ITEMGET(ItemGetPacket.class),
	LOOKAT(CoordinatePacket.class),
	EXAMINEITEM(EntityReferencePacket.class),
	GETIMAGE(GetImagePacket.class),
	USE(CoordinatePacket.class),
	MAPCLICKED(CoordinatePacket.class),
	GETFILE(TextPacket.class),
	CHECKFILE(CheckFilePacket.class),
	MOVE(MovementVectorPacket.class),
	EASYPLAY(SimplePacket.class),
	FACEINFO(FaceInfoPacket.class),
	FACE(FacePacket.class),
	SERVERHELLO(ServerHello.class),
	CLIENTHELLO(ClientHello.class),
	PLAYERHEALTH(PlayerHealthPacket.class),
	SERVERCHAT(ServerChatPacket.class),
	GETBACKGROUNDFACEINFO(GetBackgroundFaceInfoPacket.class),
	MUSICHINT(MusicHintPacket.class),
	;
	public final Class<? extends ProtoPacket> classType;

	private PacketType(Class<? extends ProtoPacket> clazz) {
		this.classType = clazz;
	}

	public static int getProtocolVersion() {
		return 0x20000 + values().length;
	}
}
