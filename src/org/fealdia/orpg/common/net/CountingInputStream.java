package org.fealdia.orpg.common.net;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A wrapper for InputStream that counts how much data has passed through the stream.
 */
public class CountingInputStream extends FilterInputStream {
	private long bytesRead = 0;
	private long timestamp = 0;
	
	public CountingInputStream(InputStream in) {
		super(in);
	}

	public long getBytesRead() {
		return bytesRead;
	}
	
	public void setBytesRead(long bytes) {
		this.bytesRead = bytes;
		if (timestamp == 0) {
			timestamp = System.currentTimeMillis();
		}
	}

	@Override
	public int read() throws IOException {
		int c = super.read();
		setBytesRead(getBytesRead() + 1);
		return c;
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int res = super.read(b, off, len);
		
		setBytesRead(getBytesRead() + res);
		return res;
	}

	@Override
	public int read(byte[] b) throws IOException {
		int res = super.read(b);
		
		setBytesRead(getBytesRead() + res);
		return res;
	}

	@Override
	public long skip(long n) throws IOException {
		long res = skip(n);
		
		setBytesRead(getBytesRead() + res);
		return res;
	}
	
	/**
	 * Get average bytes/sec, starting from first byte sent.
	 */
	public int getBytesPerSecond() {
		long now = System.currentTimeMillis();
		float diff_secs = ((float)(now - timestamp)) / 1000;
		
		float bps = getBytesRead() / diff_secs;
		
		return (int) bps;
	}

}
