package org.fealdia.orpg.common.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.net.packets.SimplePacket;

/**
 * Should contain common things of server and client communication. 
 */
public abstract class ProtoSocket {
	private static Logger logger = Logger.getLogger(ProtoSocket.class);
	
	private Socket socket;
	
	private PacketQueue queue;
	private CountingInputStream inputStream;
	
	public enum ClientState {
		// Server: waiting for hello from client
		// Client: waiting for hello ack from server
		INIT,
		
		// Client has sent supported protocol version, server has accepted it and
		// sent ack
		READY,
		
		// Client has succesfully logged in
		LOGGED
	}
	
	protected ClientState state = ClientState.INIT;

	/**
	 * Called to handle one packet received from other end.
	 */
	protected abstract void handleMessage(ProtoPacket message) throws Exception;
	
	/**
	 * Send a packet over socket
	 */
	public void sendMessage(ProtoPacket message) {
		queue.queueMessage(message);
	}
	
	public void sendSimpleMessage(PacketType type) {
		sendMessage(new SimplePacket(type));
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
		
		if (socket != null) {
			try {
				inputStream = new CountingInputStream(socket.getInputStream());
				startSenderThread(socket.getOutputStream());
			} catch (IOException e) {
				logger.error("Error starting sender thread, disconnecting", e);
				disconnect();
			}
		} else {
			inputStream = null;
		}
	}
	
	public boolean isConnected() {
		if (getSocket() == null) {
			return false;
		}
		return getSocket().isConnected();
	}

	/**
	 * Check that the given path is valid "downloadable" path.
	 * 
	 * This is needed because we don't want the server to trust the client, or the client to trust the server.
	 */
	public boolean isValidDownloadablePath(String filename) {
		// images/ and maps/ are okay, as long as they don't contain ".."
		// maps/<something>
		// maps/<something>.<otherthing>
		return filename.matches("^(images|maps)/[^.]+(\\.[^.]+)?");
	}
	
	protected void startSenderThread(OutputStream out) throws IOException {
		queue = new PacketQueue(out);
		Thread t = new Thread(queue);
		t.setDaemon(true);
		t.setName("ProtoSocketSender");
		
		t.start();
		logger.trace("Sender thread started");
	}
	
	public synchronized void disconnect() {
		if (getSocket() == null) {
			return;
		}
		try {
			if (getSocket() != null) {
				getSocket().close();
			}
		} catch (IOException e) {
			logger.warn("Socket close threw exception", e);
		}
		setSocket(null);
		if (queue != null) {
			queue.die(); // Kill the sender thread
			queue.clear();
		}
		
		logger.trace("Disconnected");
		onDisconnect();
	}
	
	protected void onDisconnect() { }
	
	public long getBytesSent() {
		return queue.getBytesSent();
	}
	
	public int getBytesSentPerSecond() {
		return queue.getBytesSentPerSecond();
	}
	
	public InputStream getInputStream() throws IOException {
		return inputStream;
	}
	
	public long getBytesReceived() {
		return inputStream.getBytesRead();
	}
	
	public int getBytesReceivedPerSecond() {
		return inputStream.getBytesPerSecond();
	}

	public String getRemoteIP() {
		if (getSocket() != null) {
			InetSocketAddress addr = (InetSocketAddress) getSocket().getRemoteSocketAddress();
			return addr.getAddress().getHostAddress();
		}
		return null;
	}

	/**
	 * Read objects from socket until EOF.
	 */
	protected void readObjects() throws Exception {
		ProtocolInputStream pis = new ProtocolInputStream(getInputStream());
		ProtoPacket packet = pis.readPacket();
		while (packet != null) {
			if (logger.isTraceEnabled()) {
				logger.trace("Object received [" + getBytesReceived() + "]: " + packet);
			}

			handleMessage(packet);

			packet = pis.readPacket();
		}
		pis.close();
	}
}
