package org.fealdia.orpg.common.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Encreecia protocol packet base class.
 * 
 * Format:
 * 
 * <length:2-6 bytes>
 * <type:2 bytes>
 * <payload:0-N bytes>
 */
public abstract class ProtoPacket {
	protected final PacketType packetType;

	protected ProtoPacket(PacketType packetType) {
		this.packetType = packetType;
		if (!getClass().equals(packetType.classType)) {
			throw new IllegalArgumentException("Bug in code! PacketType classType does not equal actual class. Class: " + getClass().getSimpleName() + ", PacketType " + packetType);
		}
	}

	public static ProtoPacket create(PacketType packetType, byte[] data) throws IOException {
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));

		// constructor(PacketType, DataInputStream)
		try {
			ProtoPacket protoPacket = packetType.classType.getConstructor(PacketType.class, DataInputStream.class).newInstance(packetType, dis);
			return protoPacket;
		} catch (NoSuchMethodException e) {
			// Do nothing, check next constructor
		} catch (Exception e) {
			throw new IllegalArgumentException("SimplePacket constructor call failed", e);
		}

		// constructor(DataInputStream)
		try {
			ProtoPacket protoPacket = packetType.classType.getConstructor(DataInputStream.class).newInstance(dis);
			return protoPacket;
		} catch (NoSuchMethodException e) {
			throw new IllegalArgumentException("Bug in code! Class " + packetType.classType.getSimpleName() + " does not implement required constructor", e);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	public PacketType getPacketType() {
		return packetType;
	}

	/**
	 * Called by ProtocolOutputStream to serialize the object as byte array, right before sending it.
	 */
	public byte[] constructBytes() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		constructBytes(dos);
		return baos.toByteArray();
	}

	protected void constructBytes(DataOutputStream dataOutput) throws IOException {
	}
}
