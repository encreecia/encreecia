package org.fealdia.orpg.common.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.Scene;

/**
 * Responsible for some Scene painting in Client & Map editor
 */
public class ScenePainter {
	
	/**
	 * Paint Scene background tiles.
	 */
	public static void paintSceneBackground(Scene scene, Graphics g, int scene_x, int scene_y, int width, int height) {
		// Draw background tiles
		ImageMapper im = ImageMapper.getInstance();
		int tileSize = im.getTileSize();
		
		for (int i = 0; i < width; i += tileSize) {
			int real_x = i / tileSize + scene_x;

			if (real_x < 0 || real_x >= scene.getWidth()) { continue; }

			for (int j = 0; j < height; j += tileSize) {
				int real_y = j / tileSize + scene_y;

				if (real_y < 0 || real_y >= scene.getHeight()) { continue; }

				// Get tile at coordinates from Scene, tile image from ImageMapper
				int bg_id = scene.getBackgroundId(real_x, real_y);
				Image img = im.getBackgroundImage(bg_id);

				g.drawImage(img, i, j, null);
			}
		}

	}
	
	public static void paintFace(String face, Graphics g, int x, int y, long tick, long seed) {
		ImageMapper im = ImageMapper.getInstance();
		BufferedImage en_image = im.getImage(face, seed);
		g.drawImage(en_image, x, y, null, null);
	}
}
