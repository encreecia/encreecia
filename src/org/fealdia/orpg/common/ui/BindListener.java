package org.fealdia.orpg.common.ui;

/**
 * Listener for custom BindManager keybindings.
 */
public interface BindListener {
	public void bindPressed(String bind, String command);
}
