package org.fealdia.orpg.common.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComponent;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Chunk;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.maps.SceneEntity;
import org.fealdia.orpg.common.maps.SceneEntityManager;
import org.fealdia.orpg.common.maps.Tile;

/**
 * Provides a zoomed-out preview of a Scene. Each tile is presented as a pixel.
 * This is meant to be put inside a JScrollPane, so the component is as big as
 * the map itself.
 * 
 * Observer for: Scene
 */
public class SceneMinimap extends JComponent implements MouseListener, Observer, Scrollable, MouseMotionListener {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SceneMinimap.class);
	
	private SceneMinimapListener listener;
	private Scene scene;
	private SceneViewer viewer;
	private boolean showUniqueChunks = false;
	
	// Markers on the minimap
	Map<Point, Color> markers = new HashMap<Point, Color>();

	public SceneMinimap() {
		setOpaque(true);
		addMouseListener(this);
		setAutoscrolls(true); // enable synthetic drag events
		addMouseMotionListener(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (logger.isTraceEnabled()) {
			logger.trace("paintComponent(), clip bounds = " + g.getClipBounds());
		}
		
		Rectangle rect = g.getClipBounds();
		
		g.setColor(Color.PINK);
		g.fillRect(rect.x, rect.y, rect.width, rect.height);
		
		// Get and check scene here, because it may change while redrawing
		Scene scene = getScene();
		if (scene == null) {
			return;
		}
		
		for (int x = rect.x; x < rect.x + rect.width; x++) {
			if (x >= scene.getWidth()) {
				break;
			}
			
			for (int y = rect.y; y < rect.y + rect.height; y++) {
				if (y >= scene.getHeight()) {
					break;
				}
				
				Tile tile = scene.getBackgroundTile(x, y);
				g.setColor(tile.getColor());
				
				//g.setColor(Color.YELLOW);
				g.drawRect(x, y, 0, 0);
			}
		}
		
		// Draw all markers
		for (Map.Entry<Point, Color> e : markers.entrySet()) {
			Point p = e.getKey();
			
			// Draw only markers intersecting with the clip bounds
			if (rect.intersects(p.x - 2, p.y - 2, 4, 4)) {
				g.setColor(e.getValue());
				g.drawLine(p.x - 2, p.y, p.x + 2, p.y);
				g.drawLine(p.x, p.y - 2, p.x, p.y + 2);
			}
		}
		
		// Mark all unique chunks
		if (isShowUniqueChunks()) {
			Color fog = new Color(255, 0, 0, 64);
			g.setColor(fog);
			for (int x = 0; x < scene.getWidthInChunks(); x++) {
				for (int y = 0; y < scene.getHeightInChunks(); y++) {
					// Draw only chunks intersecting with the clip bounds
					if (rect.intersects(x * Chunk.getSize(), y * Chunk.getSize(), Chunk.getSize(), Chunk.getSize())) {
						int chunk_id = scene.getChunkId(x * Chunk.getSize(), y * Chunk.getSize());
						if (scene.isUniqueChunk(chunk_id)) {
							g.fillRect(x * Chunk.getSize(), y * Chunk.getSize(), Chunk.getSize(), Chunk.getSize());
						}
					}
				}
			}
		}
		
		// Mark all entities from scene entity manager. Client doesn't have any.
		SceneEntityManager em = scene.getEntityManager();
		g.setColor(Color.RED);
		for (SceneEntity entity : em.getEntities()) {
			final int x = entity.getX();
			final int y = entity.getY();
			
			// Draw only marks intersecting with the clip bounds
			if (rect.intersects(x - 1, y - 1, 2, 2)) {
				g.drawLine(x - 1, y, x + 1, y);
				g.drawLine(x, y - 1, x, y + 1);
			}
		}
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		if (this.scene != null) {
			this.scene.deleteObserver(this);
		}
		this.scene = scene;
		if (scene != null) {
			scene.addObserver(this);
		}
		adjustSize();
		
		repaint();
	}

	public void update(Observable o, Object arg) {
		if (o instanceof Scene) {
			adjustSize();
			repaint();
		}
	}
	
	protected void adjustSize() {
		// Resize this component
		Dimension dim = null;
		if (scene != null) {
			dim = new Dimension(scene.getWidth(), scene.getHeight());
			logger.trace("Size set to " + dim);
		} else {
			dim = new Dimension(0, 0);
		}
		setPreferredSize(dim);
		setSize(dim);
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		
		if (e.getButton() == MouseEvent.BUTTON2){
			setShowUniqueChunks(!showUniqueChunks);
			repaint();
			return;
		}
		// Right click centers the clicked position in JScrollPane
		if (e.getButton() == MouseEvent.BUTTON3) {
			centerMinimapOn(x, y);
			return;
		}
		
		// If we have a SceneViewer, move it
		if (viewer != null) {
			logger.debug("Moving viewer to " + x + ", " + y);
			viewer.setOffsetAtMiddle(x, y);
		}
		
		if (getListener() != null) {
			getListener().onMinimapClicked(x, y);
		}
	}

	/**
	 * Get SceneViewer linked to this minimap, if any.
	 */
	public SceneViewer getViewer() {
		return viewer;
	}

	public void setViewer(SceneViewer viewer) {
		this.viewer = viewer;
	}

	public Dimension getPreferredScrollableViewportSize() {
		return new Dimension(100, 100);
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		int a = 0;
		if (orientation == SwingConstants.HORIZONTAL) {
			a = visibleRect.width;
		}
		else {
			a = visibleRect.height;
		}
		return (a / Chunk.getSize()) * Chunk.getSize();
	}

	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return Chunk.getSize();
	}

	public SceneMinimapListener getListener() {
		return listener;
	}

	public void setListener(SceneMinimapListener listener) {
		this.listener = listener;
	}

	public void clearMarkers() {
		markers.clear();
	}

	/**
	 * Adds a colored marker on the minimap.
	 * @note repaint() needs to be called manually
	 */
	public void addMarker(int x, int y, Color color) {
		markers.put(new Point(x, y), color);
	}

	public boolean isShowUniqueChunks() {
		return showUniqueChunks;
	}

	public void setShowUniqueChunks(boolean showUniqueChunks) {
		this.showUniqueChunks = showUniqueChunks;
	}
	
	/**
	 * Scroll the container of SceneMinimap so that the given coordinate is at the center.
	 */
	public void centerMinimapOn(final int x, final int y) {
		logger.trace("centerMinimapOn(" + x + "," + y + ")");
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Rectangle visible = getVisibleRect();
				scrollRectToVisible(new Rectangle(x - visible.width / 2, y - visible.height / 2, visible.width, visible.height));
			};
		});
	}

	public void mouseDragged(MouseEvent e) {
		Rectangle r = new Rectangle(e.getX(), e.getY(), 1, 1);
		scrollRectToVisible(r);
	}

	public void mouseMoved(MouseEvent arg0) {
	}
}
