package org.fealdia.orpg.common.ui;

import java.awt.DefaultKeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * The glue that makes BindManager receive KeyEvents.
 */
public class BindManagerGlue extends DefaultKeyboardFocusManager implements KeyListener {
	private BindManager bm = null;
	
	public BindManagerGlue(BindManager bm) {
		this.bm = bm;
		
		//KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this);
	}
	
	/*
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		//checkBinds(event);
		
		return super.dispatchKeyEvent(event);
	}
	*/
	
	@Override
	public boolean postProcessKeyEvent(KeyEvent event) {
		//logger.debug("postProcessKeyEvent()" + e.isConsumed());
		bm.checkBinds(event);
		
		return super.postProcessKeyEvent(event);
	}

	public void keyPressed(KeyEvent arg0) {
		bm.checkBinds(arg0);
	}

	public void keyReleased(KeyEvent arg0) {
		bm.checkBinds(arg0);
	}

	public void keyTyped(KeyEvent arg0) {
		bm.checkBinds(arg0);
	}
	
}
