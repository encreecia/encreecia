package org.fealdia.orpg.common.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JComponent;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.ArchetypeManager2;
import org.fealdia.orpg.common.maps.Chunk;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.maps.SceneEntity;
import org.fealdia.orpg.common.maps.SceneEntityManager;
import org.fealdia.orpg.server.quests.QuestAvailability;

/**
 * Provides a view of a Scene. Map editor should have one instance of this per Scene.
 * 
 * Observer for: Scene
 * 
 * TODO Implement Scrollable?
 * TODO This could be modified to use JScrollPane/JViewPort; client could only use JViewPort (possibly a custom one)...?
 */
public class SceneViewer extends JComponent implements MouseMotionListener, MouseListener, Observer {
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(SceneViewer.class);

	// Current position of top-left corner
	private int offset_x = 0;
	private int offset_y = 0;
	
	private Scene scene = null;
	
	private boolean chunkGrid = false;
	private boolean tileGrid = false;
	
	public SceneViewer() {
		setOpaque(true); // don't try to paint behind this
		scene = new Scene(); // None given, create new
		addMouseListener(this);
		addMouseMotionListener(this);
		
		logger.debug("Created");
	}
	
	@Override
	public Dimension getMinimumSize() {
		return new Dimension(320, 320);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		logger.trace("paintComponent");
		
		// Draw background color
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		ImageMapper im = ImageMapper.getInstance();
		int tileSize = im.getTileSize();
		
		//Random gen = new Random();
		
		// Background
		ScenePainter.paintSceneBackground(getScene(), g, offset_x, offset_y, getWidth(), getHeight());
		
		// Draw entities & grids
		for (int i = 0; i < getWidth(); i += tileSize) {
			int real_x = i / tileSize + offset_x;
			
			if (real_x < 0 || real_x >= scene.getWidth()) { continue; }
			
			for (int j = 0; j < getHeight(); j += tileSize) {
				int real_y = j / tileSize + offset_y;
				
				if (real_y < 0 || real_y >= scene.getHeight()) { continue; }
				
				// Draw any entities
				SceneEntityManager em = getScene().getEntityManager();
				for (SceneEntity entity : em.getEntities()) {
					int x = entity.getX();
					int y = entity.getY();
					if ( x == real_x && y == real_y) {

						// Draw the entity face based on archetype
						BufferedImage en_image = im.getImage("misc/orb");
						final String archetype = entity.getArchetype();
						if (archetype != null) {
							String face = em.getEntityEditorFace(entity);
							if (face != null) {
								en_image = im.getImage(face);
							}
						}
						
						g.drawImage(en_image, i, j, null, null);
						
						// If monster, draw a red circle
						if (archetype != null) {
							String cl = ArchetypeManager2.getInstance().getFullArchetype(archetype).get("class");
							if ("spawn".equals(cl) || "monster".equals(cl)) {
								g.setColor(Color.RED);
								g.drawOval(i, j, tileSize - 1, tileSize - 1);
							}
						}

						// Quest marker
						if (entity.containsKey("_quests") || entity.containsKey("_questsgiven") || entity.containsKey("_queststaken")) {
							g.drawImage(im.getImage(QuestAvailability.AVAILABLE.getFace()), i, j, null, null);
						}
					}
				}
				
				// Draw grid(s)?
				if (tileGrid) {
					g.setColor(Color.WHITE);
					g.drawLine(i, j, i, j + tileSize);
					g.drawLine(i, j, i + tileSize, j);
				}
				if (chunkGrid) {
					g.setColor(Color.RED);
					if (real_x % Chunk.getSize() == 0) {
						g.drawLine(i, j, i, j + tileSize);
					}
					if (real_y % Chunk.getSize() == 0) {
						g.drawLine(i, j, i + tileSize, j);
					}
				}
			}
		}
		
		// Draw spawn radiuses
		for (int i = 0; i < getWidth(); i += tileSize) {
			int real_x = i / tileSize + offset_x;
			
			if (real_x < 0 || real_x >= scene.getWidth()) { continue; }
			
			for (int j = 0; j < getHeight(); j += tileSize) {
				int real_y = j / tileSize + offset_y;
				
				if (real_y < 0 || real_y >= scene.getHeight()) { continue; }
				
				// Draw any entities
				SceneEntityManager em = getScene().getEntityManager();
				for (SceneEntity entity : em.getEntities()) {
					int x = entity.getX();
					int y = entity.getY();
					if ( x == real_x && y == real_y) {
						// draw radius for spawns
						int radius = entity.getRadius();
						if (radius != -1) {
							g.setColor(Color.YELLOW);
							g.drawRect(i  - tileSize * radius, j - tileSize * radius, (2 * radius + 1) * tileSize, (2 * radius + 1) * tileSize);
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Called when user clicks a tile with left mouse button. 
	 */
	public void tileClicked(int x, int y) {
		logger.trace("tileClicked(" + x + "," + y + ")");
	}

	public void mousePressed(MouseEvent e) {
		int realx = e.getX() / ImageMapper.getInstance().getTileSize() + offset_x;
		int realy = e.getY() / ImageMapper.getInstance().getTileSize() + offset_y;
		
		if (e.getButton() == MouseEvent.BUTTON1) {
			tileClicked(realx, realy);
		}
		if (e.getButton() == MouseEvent.BUTTON3) {
			logger.trace("Right button down");
		}
	}
	
	public void mouseDragged(MouseEvent e) {
		int realx = e.getX() / ImageMapper.getInstance().getTileSize() + offset_x;
		int realy = e.getY() / ImageMapper.getInstance().getTileSize() + offset_y;
		
		// When mouse1 is down while dragging, call tileClicked
		if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) != 0) {
			tileClicked(realx, realy);
		}
	}

	public void mouseMoved(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}

	public boolean isChunkGrid() {
		return chunkGrid;
	}

	public void setChunkGrid(boolean chunkGrid) {
		this.chunkGrid = chunkGrid;
		repaint();
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		if (this.scene != null) {
			this.scene.deleteObserver(this);
		}
		this.scene = scene;
		if (scene != null) {
			scene.addObserver(this);
		}
		repaint();
	}

	public void setTileGrid(boolean tileGrid) {
		this.tileGrid = tileGrid;
		repaint();
	}

	public boolean isTileGrid() {
		return tileGrid;
	}
	
	public int getOffsetX() {
		return offset_x;
	}
	
	public int getOffsetY() {
		return offset_y;
	}
	
	public void setOffset(int x, int y) {
		logger.trace("setOffset(" + x + "," + y + ")");
		this.offset_x = x;
		this.offset_y = y;
		repaint();
	}
	
	/**
	 * Set given coordinate to be at the middle of the view.
	 */
	public void setOffsetAtMiddle(int x, int y) {
		int width = getWidth() / ImageMapper.getInstance().getTileSize();
		int height = getHeight() / ImageMapper.getInstance().getTileSize();
		
		setOffset(x - width / 2, y - height / 2);
	}
	
	public void adjustOffset(int diff_x, int diff_y) {
		setOffset(offset_x + diff_x, offset_y + diff_y);
	}

	public void update(Observable o, Object arg) {
		if (o instanceof Scene) {
			repaint();
		}
	}
}
