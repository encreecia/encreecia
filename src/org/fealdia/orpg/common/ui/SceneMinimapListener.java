package org.fealdia.orpg.common.ui;

/**
 * Listener for SceneMinimap click events.
 */
public interface SceneMinimapListener {
	public void onMinimapClicked(int x, int y);
}
