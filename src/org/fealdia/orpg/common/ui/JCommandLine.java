package org.fealdia.orpg.common.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTextField;

/**
 * A JTextField with arrow-browseable history. Up and down arrow keys browse the history.
 */
public class JCommandLine extends JTextField implements ActionListener, KeyListener {
	private static final long serialVersionUID = 1L;
	
	private List<String> history = new LinkedList<String>();
	
	private int historyPos = 0; // bottom

	public JCommandLine() {
		addActionListener(this);
		addKeyListener(this);
	}

	public void actionPerformed(ActionEvent arg0) {
		String cmd = arg0.getActionCommand();
		if (cmd.length() > 0) {
			history.add(cmd);
		}
		historyPos = 0;
	}

	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == KeyEvent.VK_UP) {
			if (historyPos < history.size()) {
				if (historyPos == 0 && getText().length() > 0) {
					history.add(getText());
					historyPos++;
				}
				historyPos++;
				showHistory();
			}
		}
		else if (arg0.getKeyCode() == KeyEvent.VK_DOWN) {
			if (historyPos > 0) {
				historyPos--;
				showHistory();
			} else if (getText().length() > 0) {
				history.add(getText());
				setText("");
			}
		}
	}

	public void keyReleased(KeyEvent arg0) {}
	public void keyTyped(KeyEvent arg0) {}
	
	public void showHistory() {
		if (historyPos == 0) {
			setText("");
		} else {
			setText(history.get(history.size() - historyPos));
		}
	}
}
