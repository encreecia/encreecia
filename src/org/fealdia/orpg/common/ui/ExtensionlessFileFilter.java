package org.fealdia.orpg.common.ui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Shows only files that have no extension, also excluding "chunks".
 */
public class ExtensionlessFileFilter extends FileFilter {

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		
		String name = file.getName();
		if (name.indexOf('.') == -1 && !"chunks".equals(name)) {
			return true;
		}
		
		return false;
	}

	@Override
	public String getDescription() {
		return "Extensionless files";
	}

}
