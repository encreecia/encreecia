package org.fealdia.orpg.common.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Timer;

import org.apache.log4j.Logger;

/**
 * A workaround for Sun's broken KeyListener implementation.
 * 
 * Linux, case 1 (one key held down):
 * keyPressed, keyReleased are repeated
 * 
 * Linux, case 2 (2 keys held down):
 * keyPressed A
 * keyPressed B, keyReleased B - repeated
 * keyReleased A
 * 
 * Windows, case 1 (one key held down):
 * keyPressed - repeated
 * keyReleased
 * 
 * Thus, on Linux we create or reset a release timer at keyReleased, and keyPressed is not considered valid if there is a release timer.
 * 
 * On Windows, keyReleased triggers a release immediately.
 * 
 * @see http://forums.sun.com/thread.jspa?threadID=790206&messageID=4495526
 * @see http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4153069
 * @see http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4817479
 */
public class KeyListenerProxy implements ActionListener, KeyListener {
	/**
	 * Milliseconds. The workaround for proper key release generation works only if the system's key repeat rate is smaller than this.
	 *
	 * This also defines how fast the UI reacts to released keys. 
	 */
	private static final int REPEAT_RATE = 10;
	
	private static Logger logger = Logger.getLogger(KeyListenerProxy.class);
	
	private boolean brokenRepeat = false;
	
	private Map<Integer, Timer> keyReleaseTimers = new HashMap<Integer,Timer>();
	private Map<Integer, KeyEvent> keyReleaseEvents = new HashMap<Integer, KeyEvent>();
	
	private KeyListener listener = null;
	
	protected KeyListenerProxy() {}
	
	public KeyListenerProxy(KeyListener target) {
		listener = target;
		
		if ("Linux".equals(System.getProperty("os.name"))) {
			logger.debug("Enabling KeyListenerProxy workaround");
			brokenRepeat = true;
		}
	}
	
	public void actionPerformed(ActionEvent event) {
		String cmd = event.getActionCommand();
		
		// Key release timer
		if (cmd == null) {
			// Iterate through all timers and remove expired ones, releasing the corresponding keys
			for (Iterator<Integer> iter = keyReleaseTimers.keySet().iterator(); iter.hasNext();) {
				int keyCode = iter.next();
				
				Timer t = keyReleaseTimers.get(keyCode);
				if (!t.isRunning()) {
					// Call actual listener's keyReleased
					logger.trace("Key really released: " + keyCode);
					listener.keyReleased(keyReleaseEvents.get(keyCode));
					
					iter.remove();
					keyReleaseTimers.remove(keyCode);
				}
			}
			return;
		}
	}

	public void keyPressed(KeyEvent event) {
		logger.trace("keyPressed " + event.getKeyCode());
		//if (resetOrCreateReleaseTimer(event.getKeyCode())) {
		if (!hasReleaseTimer(event.getKeyCode())) {
			logger.trace("Key really pressed: " + event.getKeyCode());
			listener.keyPressed(event);
			
		} else {
			// Stop timer - no key release without a KeyReleased event
			keyReleaseTimers.get(event.getKeyCode()).stop();
			keyReleaseTimers.remove(event.getKeyCode());
		}
		
		keyReleaseEvents.put(event.getKeyCode(), event);
	}

	public void keyReleased(KeyEvent event) {
		logger.trace("Released: " + event.getKeyCode());

		keyReleaseEvents.put(event.getKeyCode(), event);
		
		resetOrCreateReleaseTimer(event.getKeyCode());
	}

	public void keyTyped(KeyEvent event) {
		// TODO: Handle keyTyped repetition as well
		listener.keyTyped(event);
	}

	/**
	 * Set the actual listener.
	 */
	public KeyListener getListener() {
		return listener;
	}

	public void setListener(KeyListener listener) {
		this.listener = listener;
	}
	
	protected boolean hasReleaseTimer(int keyCode) {
		/*
		Timer t = keyReleaseTimers.get(keyCode);
		return (t != null && t.isRepeats());
		*/
		
		return keyReleaseTimers.containsKey(keyCode);
	}
	
	/**
	 * Reset or create a key release timer.
	 * 
	 * @return true if timer was created
	 */
	protected boolean resetOrCreateReleaseTimer(int keyCode) {
		// Reset earlier timer if any
		if (hasReleaseTimer(keyCode)) {
			Timer oldTimer = keyReleaseTimers.get(keyCode);
			oldTimer.restart();
			return false;
		} else {
			// Add new timer to release the key
			int time = REPEAT_RATE;
			if (!brokenRepeat) {
				time = 1;
			}
			
			Timer timer = new Timer(time, this);
			timer.setRepeats(false);
			keyReleaseTimers.put(keyCode, timer);
			timer.start();
		}
		return true;
	}

}
