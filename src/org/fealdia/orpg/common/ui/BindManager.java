package org.fealdia.orpg.common.ui;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JTextField;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.FileUtil;
import org.fealdia.orpg.common.util.StreamUtil;

/**
 * Custom keyboard bind manager. This provides a way to map keys into commands. For each bind, BindListeners are notified of the command.
 * 
 * TODO negative modifiers: !ctrl+shift+A
 * TODO get rid of keyrepeat
 */
public class BindManager {
	private Logger logger = Logger.getLogger(BindManager.class);
	
	private Map<String,String> binds = new HashMap<String,String>();
	
	private List<BindListener> listeners = new LinkedList<BindListener>();
	
	public void addListener(BindListener listener) {
		listeners.add(listener);
	}
	
	public void addBind(String bind, String command) {
		if (logger.isTraceEnabled()) {
			logger.trace("Adding bind '" + bind + "' -> '" + command + "'");
		}
		binds.put(bind, command);
	}
	
	public boolean checkBinds(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.VK_UNDEFINED) {
			return false;
		}
		if (event.getSource() instanceof JTextField) {
			return false;
		}
		
		StringBuffer str = new StringBuffer();
		if (event.isConsumed()) {
			str.append("@");
		}
		if ((event.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
			str.append("ctrl+");
		}
		if ((event.getModifiersEx() & KeyEvent.ALT_DOWN_MASK) != 0) {
			str.append("alt+");
		}
		if ((event.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) != 0) {
			str.append("shift+");
		}
		if (event.getID() == KeyEvent.KEY_RELEASED) {
			str.append("^");
		}
		str.append(KeyEvent.getKeyText(event.getKeyCode()));
		
		String keyText = str.toString();
		//logger.debug("Key: " + keyText);
		
		if (binds.containsKey(keyText)) {
			notifyListeners(keyText, binds.get(keyText));
			return true;
		}
		
		return false;
	}
	
	protected void notifyListeners(String bind, String command) {
		for (BindListener l : listeners) {
			l.bindPressed(bind, command);
		}
	}
	
	public void removeBind(String bind) {
		binds.remove(bind);
	}

	/**
	 * Load bindings from the given file. First try the PathConfig root directory, and then class loader.
	 */
	public boolean loadFromFile(String filename) {
		logger.trace("loadFromFile(): " + filename);
		
		BufferedReader reader = FileUtil.getFileOrResourceAsReader(filename);
		try {
			return loadFromStream(reader);
		} catch (IOException e) {
			logger.debug("IOException while reading from stream", e);
		}
		return false;
	}
	
	public boolean loadFromStream(BufferedReader in) throws IOException {
		for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
			String[] parts = line.split("[ \t]+", 2);
			addBind(parts[0], parts[1]);
		}
		
		return false;
	}
	
	// TODO saveToFile()
}
