package org.fealdia.orpg.common.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import org.fealdia.orpg.common.VersionInfo;
import org.fealdia.orpg.common.util.FileUtil;

/**
 * Common "About" dialog.
 */
public class AboutDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JTabbedPane tabPane;

	public AboutDialog(JFrame parent) {
		super(parent);
		
		setTitle("About JFealdia " + VersionInfo.getVersion());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		createComponents();

		pack();
		setSize(new Dimension(640, 400));
		setLocationRelativeTo(null); // Center
		setModal(true);
	}
	
	protected void createComponents() {
		JPanel mainPane = new JPanel();
		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
		add(mainPane);
		
		tabPane = new JTabbedPane();
		mainPane.add(tabPane);
		
		addTabWithResourceFile("Authors", "doc/AUTHORS");
		addTabWithResourceFile("License", "doc/LICENSE");
		addTabWithResourceFile("AGPLv3", "doc/agpl-3.0.txt");
		addTabWithResourceFile("Substance license", "Substance.license");
		addTabWithText("Debug", getDebugText());
		
		JButton closeButton = new JButton("Close");
		closeButton.setActionCommand("close");
		closeButton.addActionListener(this);
		closeButton.setAlignmentX(CENTER_ALIGNMENT);
		mainPane.add(closeButton);
		
		getRootPane().setDefaultButton(closeButton);
	}

	public void actionPerformed(ActionEvent arg0) {
		setVisible(false);
	}
	
	public void addTabWithText(String title, String text) {
		JTextArea textArea = new JTextArea(text);
		textArea.setEditable(false);
		
		JScrollPane scroller = new JScrollPane(textArea);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tabPane.add(title, scroller);
	}
	
	public void addTabWithFile(String title, String filename) {
		addTabWithText(title, FileUtil.getFileContentAsString(filename));
	}
	
	public void addTabWithResourceFile(String title, String resource) {
		String content = FileUtil.getFileOrResourceContentAsString(resource);
		addTabWithText(title, content);
	}
	
	public String getDebugText() {
		StringBuffer res = new StringBuffer();
		
		res.append("=== System properties ===\n\n");
		Properties props = System.getProperties();
		for (Map.Entry<Object, Object> e : props.entrySet()) {
			res.append(e.getKey() + " = " + e.getValue() + "\n");
		}
		
		return res.toString();
	}
	
}
