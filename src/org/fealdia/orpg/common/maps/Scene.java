package org.fealdia.orpg.common.maps;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;

/**
 * Represents a single map, which may have multiple Instances. 
 * Maximum size of the Scene is 65535x65535.
 * This class is Observable; when the map is modified setChanged will be set.
 * 
 * NOTE: This class shouldn't concern itself with how it is drawn.
 * 
 * Scene background format in NBO:
 * <2:version><2:size=2><2:width><2:height><width*height*2:chunkrefs>
 * 
 * TODO allow creation of Scene from file directly, without a 2x2 size that is resized
 */
public class Scene extends Observable {
	private static Logger logger = Logger.getLogger(Scene.class);
	
	private static final int MAX_DIMENSION = 65535; // 2^16-1 (max value for short)
	
	private static final int UNIQUE_INDEX = 32768;
	
	private boolean modified = true;
	
	private String name = "untitled";
	
	private int width_chunks = 0;
	private int height_chunks = 0;
	
	// map data (chunk references, each reference is 2 bytes, short)
	private byte[] data = null;
	
	private boolean uniqueMode = false;
	
	private ChunkList uniqueChunks = new ChunkList();
	
	private int usageCount = 0;
	
	private SceneEntityManager entityManager = new SceneEntityManager();
	
	public Scene() {
		resize(2, 2);
	}
	
	public Scene(int chunk_width, int chunk_height) {
		resize(chunk_width, chunk_height);
	}
	
	public Chunk getChunk(int index) {
		// Map-specific chunk?
		if (index < 0) {
			return uniqueChunks.getChunk(index + UNIQUE_INDEX);
		}
		
		return ChunkList.getInstance().getChunk(index);
	}
	
	public int getChunkId(int tile_x, int tile_y) {
		//logger.trace("getChunkId(tile_x = " + tile_x + ", tile_y = "+ tile_y + ")");
		int chunk_x = tile_x / Chunk.getSize();
		int chunk_y = tile_y / Chunk.getSize();
		
		if (tile_x < 0 || tile_y < 0 || chunk_x >= width_chunks || chunk_y >= height_chunks) {
			logger.debug("getChunkId() out of map");
			return 0;
		}
		
		// get right short from data
		int res = ByteBuffer.wrap(data).getShort((chunk_y * width_chunks + chunk_x) * 2);
		
		return res;
	}
	
	/**
	 * Get chunk id at coordinates for editing. This should be called when something is about to modify the tile at coordinates.
	 * In unique mode, a copy of the chunk will be created first and inserted at the coordinates.
	 */
	public int getChunkIdForEditing(int tile_x, int tile_y) {
		int chunk_id = getChunkId(tile_x, tile_y);
		
		// If unique mode, make a copy of the chunk, and change the ref under cursor
		if (isUniqueMode()) {
			// Check if the chunk is unique
			boolean is_unique = isUniqueChunk(chunk_id);
			
			// Make a copy of the chunk if not
			if (!is_unique) {
				chunk_id = makeUniqueChunk(chunk_id);
				
				// Change the ref under cursor
				setChunk(tile_x, tile_y, chunk_id);
			}
		}
		
		return chunk_id;
	}
	
	/**
	 * (Try to) load the scene by name. Name is either a name relative to maps/ root directory, or an absolute
	 * path. 
	 */
	public boolean loadByName(String name) {
		if (name.startsWith("/")) {
			return loadFromFile(name);
		}
		return loadFromFile(PathConfig.getMapDir() + name);
	}
	
	/**
	 * Loads the map data, and map-specific chunks from another file (.chunks).
	 */
	public boolean loadFromFile(String name) {
		logger.debug("loadFromFile(" + name + ")");
		
		DataInputStream dis = null;
		try {
			dis = new DataInputStream(new BufferedInputStream(new FileInputStream(name)));
		} catch (FileNotFoundException e1) {
			logger.warn("Failed to load map from file '" + name + "', file not found.");
			return false;
		}
		
		try {
			// Read header: version, size, width, height
			short version = dis.readShort();
			if (version != 0) {
				logger.warn("Invalid version " + version + " in map file '" + name + "'");
				return false;
			}
			short size = dis.readShort();
			if (size != 2) {
				logger.warn("Invalid chunk reference size " + size + " in map file '" + name + "'");
				return false;
			}
			short width_chunks = dis.readShort();
			short height_chunks = dis.readShort();
			resize(width_chunks, height_chunks);
			
			// Read the chunk references
			for (int i = 0; i < width_chunks * height_chunks; i++) {
				ByteBuffer.wrap(data).putShort(i*2, dis.readShort());
			}
		} catch (IOException e) {
			logger.error("IOException while loading from file", e);
			return false;
		}
		finally {
			try {
				dis.close();
			} catch (IOException e) {
				logger.error("IOException when closing file", e);
			}
		}
		
		// Load unique chunklist
		uniqueChunks.loadFromFile(name + ".chunks");
		
		setModified(false);
		setNameFromFile(name);
		
		return true;
	}
	
	/**
	 * Get the background id of tile at given coordinates. This is the index in backgrounds.xml 
	 */
	public int getBackgroundId(int tile_x, int tile_y) {
		int chunk_id = getChunkId(tile_x, tile_y);
		Chunk chunk = getChunk(chunk_id);
		if (chunk == null) {
			return 0;
		}
		int bg_id = getChunk(chunk_id).getTile(tile_x % Chunk.getSize(), tile_y % Chunk.getSize());
		
		return bg_id;
	}
	
	public Tile getBackgroundTile(int tile_x, int tile_y) {
		return BackgroundMapper.getInstance().getTile(getBackgroundId(tile_x, tile_y));
	}

	/**
	 * Get width in tiles.
	 */
	public int getWidth() {
		return width_chunks * Chunk.getSize();
	}
	
	public int getWidthInChunks() {
		return width_chunks;
	}

	/**
	 * Get height in tiles.
	 */
	public int getHeight() {
		return height_chunks * Chunk.getSize();
	}
	
	public int getHeightInChunks() {
		return height_chunks;
	}
	
	/**
	 * Attempts to save the Scene to a file.
	 * @param name full path to the target file
	 */
	public boolean saveToFile(String name) {
		logger.debug("saveToFile(" + name + ")");
		try {
			FileOutputStream fos = new FileOutputStream(name);
			DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(fos));
			
			// Write headers: version, size, width, height
			dos.writeShort(0); // version
			dos.writeShort(2); // size = 2 (how much one chunk reference takes, eg. 2 bytes)
			
			dos.writeShort(width_chunks);
			dos.writeShort(height_chunks);
			logger.debug(" Size: " + width_chunks + " x " + height_chunks);
			
			// Write chunks
			ByteBuffer buf = ByteBuffer.wrap(data);
			
			for (int i = 0; i < width_chunks * height_chunks; i++) {
				//logger.debug(" Chunk ref " + i + ": " + buf.getShort(i*2));
				dos.writeShort(buf.getShort(i*2));
			}
			
			dos.close();
		} catch (FileNotFoundException e) {
			logger.error("saveToFile() File not found: " + name);
			return false;
		} catch (IOException e) {
			logger.error("IOException while saving to file", e);
			return false;
		}
		
		// Save chunklist
		uniqueChunks.saveToFile(name + ".chunks");
		
		setNameFromFile(name);
		logger.info("Scene saved to file " + name);
		setModified(false);
		setChanged();
		notifyObservers();
		
		return true;
	}
	
	/**
	 * Save scene to the file it was loaded from (if any). 
	 */
	public boolean saveToFile() {
		if (getName() == null) {
			return false;
		}
		return saveToFile(PathConfig.getMapDir() + getName());
	}

	/**
	 * Resize the map. This may destroy some data if the new size is smaller than the old one.
	 * @return true if the size was changed 
	 */
	public boolean resize(int chunk_width, int chunk_height) {
		return resize(chunk_width, chunk_height, 0, 0);
	}

	/**
	 * Resize the map, and move old map to given offset. Allows growing the map to left and up.
	 */
	public boolean resize(final int chunk_width, final int chunk_height, final int offset_x, final int offset_y) {
		logger.trace("resize(" + chunk_width + ", " + chunk_height + ")");
		
		// Check that the requested size is not too large (for the binary format), or too small
		if (chunk_width < 1 || chunk_height < 1 || chunk_width > MAX_DIMENSION || chunk_height > MAX_DIMENSION) {
			logger.warn("Invalid resize dimensions (" + chunk_width + ", " + chunk_height + "), not resizing");
			return false;
		}
		
		if (data != null) {
			// copy old data to new map
			
			byte[] target = new byte[chunk_width * chunk_height * 2];
			ByteBuffer srcBuf = ByteBuffer.wrap(data);
			ByteBuffer dstBuf = ByteBuffer.wrap(target);
			
			// Copy everything that fits in the new map from the old map
			for (int y = 0; y < this.height_chunks; y++) {
				for (int x = 0; x < this.width_chunks; x++) {
					// Is this position outside map for *new* map?
					if (x + offset_x < 0 || x + offset_x >= chunk_width || y + offset_y < 0 || y + offset_y >= chunk_height) {
						continue;
					}

					dstBuf.putShort(((y + offset_y) * chunk_width + (x + offset_x)) * 2, srcBuf.getShort((y * this.width_chunks + x) * 2));
				}
			}
			
			data = target;
		}
		else {
			data = new byte[chunk_width * chunk_height * 2];
		}
		this.width_chunks = chunk_width;
		this.height_chunks = chunk_height;
		
		// Move entities
		if (offset_x != 0 || offset_y != 0) {
			entityManager.moveAllByOffset(offset_x * 16, offset_y * 16);
		}

		setChanged();
		notifyObservers();
		setModified(true);
		return true;
	}

	/**
	 * Set chunk at given tile coordinates.
	 * 
	 * @return true if chunk was actually changed
	 */
	public boolean setChunk(int tile_x, int tile_y, int i) {
		if (tile_x >= getWidth() || tile_y >= getHeight()) {
			logger.warn("setChunk() called with invalid x = " + tile_x + ", y = " + tile_y);
			return false;
		}
		
		int chunk_x = tile_x / Chunk.getSize();
		int chunk_y = tile_y / Chunk.getSize();
		logger.trace("Setting chunk at chunk_x = " + chunk_x + ", chunk_y = " + chunk_y + " to " + i);
		
		int index = (chunk_y * width_chunks + chunk_x) * 2;
		
		ByteBuffer buf = ByteBuffer.wrap(data);
		
		if (buf.getShort(index) == i) {
			return false;
		}
		buf.putShort(index, (short)i);
		
		setModified(true);
		logger.debug("Chunk at chunk_x = " + chunk_x + ", chunk_y = " + chunk_y + " changed to " + i);
		setChanged();
		return true;
	}

	/**
	 * Change the tile at given position.
	 * @return true if the tile was changed to different value
	 */
	public boolean setTile(int tile_x, int tile_y, int i) {
		logger.trace("setTile(tile_x = " + tile_x + ", tile_y = " + tile_y + ", i = " + i + ") uniqueMode = " + isUniqueMode());
		if (tile_x >= getWidth() || tile_y >= getHeight()) {
			logger.warn("setTile() called with invalid tile_x = " + tile_x + ", tile_y = " + tile_y);
			return false;
		}
		
		int chunk_id = getChunkIdForEditing(tile_x, tile_y);
		
		boolean res = getChunk(chunk_id).setTile(tile_x % Chunk.getSize(), tile_y % Chunk.getSize(), i);
		
		if (res) {
			setChanged();
			setModified(true);
			logger.debug("Tile at tile_x = " + tile_x + ", tile_y = " + tile_y + " changed to " + i);
		}
		return res;
	}

	/**
	 * Check whether the file has been modified (after loading). In other words, whether an up-to-date version
	 * of this Scene has been saved to a file (if any). 
	 */
	public boolean isModified() {
		return modified;
	}

	public void setModified(boolean modified) {
		this.modified = modified;
		if (modified) {
			setChanged();
		}
	}

	/**
	 * Get Scene name. This should be relative to the maps/ directory. 
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		logger.trace("Scene name set to: " + name);
	}
	
	public void setNameFromFile(String fileName) {
		String mapdir = PathConfig.getMapDir();
		
		if (fileName.indexOf(mapdir) != -1) {
			setName(fileName.substring(fileName.indexOf(mapdir) + mapdir.length()));
		}
		else {
			logger.warn("Failed to guess map name from filename '" + fileName + "', using absolute name");
			setName(fileName);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		logger.debug("finalize");
	}
	
	public boolean floodFill(int tile_x, int tile_y, int tile_id) {
		logger.debug("floodFill x = " + tile_x + ", y = " + tile_y + " tile = " + tile_id);
		
		// Create list of points to check, and fill it with the initial one
		List <int[]> points = new LinkedList<int[]>();
		int[] tmp = { tile_x, tile_y };
		points.add(tmp);
		
		// What tile id to flood fill
		int filltarget = getBackgroundId(tile_x, tile_y);
		logger.debug(" fill target type = " + filltarget);
		
		boolean modified = false;
		int iter = 0;
		while (points.size() > 0) {
			iter++;
			if (iter == 500) {
				logger.error("floodFill() exiting because of maxed iterations");
				break;
			}
			int[] p = points.remove(0);
			logger.debug(" checking " + p[0] + "," + p[1] + " (" + points.size() + " left)");
			
			if (getBackgroundId(p[0], p[1]) != filltarget) {
				logger.warn("Checking already filled (?) chunk");
				continue;
			}
			
			if (setTile(p[0], p[1], tile_id)) {
				modified = true;
			}
			
			// Tiles near us to check
			int[][] nearby = {
					{-1, 0 },
					{ 1, 0 },
					{ 0,-1 },
					{ 0, 1 },
			};
			
			// Add all tiles to list that contain the same tile type
			for (int i = 0; i < nearby.length; i++) {
				int[] checkp = { p[0] + nearby[i][0], p[1] + nearby[i][1] };
				// Out of scene?
				if (!isOnScene(checkp[0], checkp[1])) {
					logger.debug("  -> out of scene");
					continue;
				}
				
				// Check whether we are within the same chunk
				if (tile_x / 16 != checkp[0] / 16 || tile_y / 16 != checkp[1] / 16) {
					logger.debug("  -> different chunk, ignoring");
					continue;
				}
				
				if (getBackgroundId(checkp[0], checkp[1]) == filltarget) {
					boolean add = true;
					for (int[] p1 : points) {
						if (p1[0] == checkp[0] && p1[1] == checkp[1]) {
							logger.debug("  -> already queued, ignoring");
							add = false;
							break;
						}
					}
					if (add) {
						points.add(checkp);
					}
				}
			}
		}
		
		if (modified) {
			setChanged();
			setModified(true);
			notifyObservers();
		}
		
		return modified;
	}
	
	/**
	 * Fill the chunk at target with given tile.
	 */
	public boolean floodFillChunk(int tile_x, int tile_y, int tile_id) {
		logger.debug("floodFillChunk x = " + tile_x + ", y = " + tile_y + " tile = " + tile_id);
		
		int chunk_id = getChunkIdForEditing(tile_x, tile_y);
		Chunk chunk = getChunk(chunk_id);
		
		boolean modified = false;
		for (int i = 0; i < Chunk.getSize(); i++) {
			for (int j = 0; j < Chunk.getSize(); j++) {
				if (chunk.setTile(i, j, tile_id)) {
					modified = true;
				}
			}
		}
		if (modified) {
			setModified(true);
			setChanged();
		}
		return modified;
	}

	/**
	 * Get title suitable for showing in UI.
	 */
	public String getTitle() {
		StringBuffer sb = new StringBuffer();
		if (isModified()) {
			sb.append('*');
		}
		sb.append(getName() + " - [" + getWidth() + "x" + getHeight() + "]");
		return sb.toString();
	}

	/**
	 * Check whether the given position can be moved into, with the given Mobility.
	 */
	public boolean isBlockedFor(int x, int y, Mobility mobility) {
		if (isOutsideScene(x, y)) {
			return true;
		}
		
		Tile tile = getBackgroundTile(x, y);
		return !mobility.canPass(tile.getMoveType());
	}
	
	public boolean isOutsideScene(int x, int y) {
		return (x < 0 || y < 0 || x > getWidth() || y > getHeight());
	}
	
	public int getSpeed(int x, int y) {
		return getBackgroundTile(x, y).getSpeed();
	}

	public boolean isUniqueMode() {
		return uniqueMode;
	}

	public void setUniqueMode(boolean uniqueMode) {
		this.uniqueMode = uniqueMode;
	}
	
	/**
	 * Return true if only 0-1 references to the given chunk are found.
	 */
	public boolean isUniqueChunk(int chunk_id) {
		/*
		int refs = 0;
		
		for (int chunk_x = 0; chunk_x < getWidthInChunks(); chunk_x++) {
			for (int chunk_y = 0; chunk_y < getHeightInChunks(); chunk_y++) {
				if (getChunkId(chunk_x * Chunk.getSize(), chunk_y * Chunk.getSize()) == chunk_id) {
					refs++;
				}
			}
		}
		return (refs <= 1);
		*/
		return (chunk_id > UNIQUE_INDEX || chunk_id < 0);
	}
	
	public int makeUniqueChunk(int chunk_id) {
		Chunk chunk = getChunk(chunk_id);
		Chunk newChunk = new Chunk(chunk);
		
		uniqueChunks.addChunk(newChunk);
		int newchunk_id = uniqueChunks.size() - 1 - UNIQUE_INDEX;
		logger.debug("Made new unique chunk with index " + newchunk_id + " (" + uniqueChunks.size() + " unique chunks)");
		
		return newchunk_id;
	}
	
	public boolean loadEntities() {
		// Try loading from XML file first
		try {
			SceneEntityManager em = SceneEntityManager.loadFromFile(PathConfig.getMapDir() + getName() + ".xml");
			this.entityManager = em;
			return true;
		} catch (FileNotFoundException e) {
			// Ok, will try to load legacy file next
		} catch (JAXBException e) {
			e.printStackTrace();
			logger.error("Failed to load Scene entities from XML", e);
			return false;
		}

		// Legacy fallback
		return entityManager.loadFromTxtFile(PathConfig.getMapDir() + getName() + ".txt");
	}
	
	public boolean saveEntities() {
		try {
			entityManager.saveToFile(PathConfig.getMapDir() + getName() + ".xml");
		} catch (Exception e) {
			logger.error("Failed to save Scene entities", e);
			return false;
		}
		return true;
	}

	public SceneEntityManager getEntityManager() {
		return entityManager;
	}
	
	public boolean reload() {
		return loadByName(getName());
	}

	/**
	 * Number of instances or whatever are using this Scene.
	 * @see SceneManager.unloadUnusedScenes()
	 */
	public int getUsageCount() {
		return usageCount;
	}
	
	public synchronized void incrementUsageCount() {
		usageCount++;
	}
	
	public synchronized void decrementUsageCount() {
		usageCount--;
	}

	/**
	 * Get filename based on the current map name.
	 */
	public String getFilename() {
		return PathConfig.getMapDir() + getName();
	}
	
	public boolean isOnScene(int tile_x, int tile_y) {
		return (tile_x >= 0 && tile_y >= 0 && tile_x <= getWidth() && tile_y <= getHeight());
	}

	/**
	 * Prune unused private chunks.
	 * @return true if any chunks were removed
	 */
	public boolean prunePrivateChunks() {
		int count_before = uniqueChunks.size();
		int pruned = 0;
		
		logger.debug("Pruning unique chunks, before = " + count_before);
		for (int i = uniqueChunks.size(); i > 0; i--) {
			int chunk_id = i - 1 - UNIQUE_INDEX;
			int refs = countChunkRefs(chunk_id);
			logger.debug("Chunk " + i + " has " + refs + " refs");
			if (refs == 0) {
				// Remove the chunk, fix the indexes
				logger.debug("Pruning chunk " + i);
				for (int chunk_x = 0; chunk_x < getWidthInChunks(); chunk_x++) {
					for (int chunk_y = 0; chunk_y < getHeightInChunks(); chunk_y++) {
						int old_chunk_id = getChunkId(chunk_x * Chunk.getSize(), chunk_y * Chunk.getSize());
						// Reindex all old chunks after us
						if (old_chunk_id < 0 && old_chunk_id > chunk_id) {
							setChunk(chunk_x * Chunk.getSize(), chunk_y * Chunk.getSize(), old_chunk_id - 1);
						}
					}
				}
				uniqueChunks.getChunks().remove(i - 1);
				
				pruned++;
			}
		}
		
		if (pruned > 0) {
			setModified(true);
			logger.debug("Pruned " + pruned + " chunks of " + count_before);
		}
		return (pruned > 0);
	}
	
	public int countChunkRefs(int chunk_id) {
		int refs = 0;
		
		for (int chunk_x = 0; chunk_x < getWidthInChunks(); chunk_x++) {
			for (int chunk_y = 0; chunk_y < getHeightInChunks(); chunk_y++) {
				if (getChunkId(chunk_x * Chunk.getSize(), chunk_y * Chunk.getSize()) == chunk_id) {
					refs++;
				}
			}
		}
		return refs;
	}
}
