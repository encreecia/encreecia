package org.fealdia.orpg.common.maps;

import java.awt.Color;

/**
 * Represents properties of one background tile. See the constructor for default values.
 */
public class Tile {
	/**
	 * @see Mobility
	 */
	public enum MoveType {
		LAND,
		WALL,
		MOUNTAIN,
		WATER,
	}
	
	/**
	 * Line of Sight type.
	 */
	public enum LOSType {
		NORMAL, // default, density applies
		WINDOW, // can't be seen through, unless next to it
	}
	
	private String face = null;
	private String name;
	private int speed;
	private int density;
	private Color color;
	
	private MoveType moveType = MoveType.LAND;
	private LOSType losType = LOSType.NORMAL;
	
	public Tile() {
		// Defaults
		name = "unknown";
		speed = 0;
		density = 0;
		color = Color.BLACK;
	}
	
	public int getDensity() {
		return density;
	}

	public void setDensity(int density) {
		this.density = density;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getFace() {
		return face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	public MoveType getMoveType() {
		return moveType;
	}

	public void setMoveType(MoveType moveType) {
		this.moveType = moveType;
	}

	public LOSType getLosType() {
		return losType;
	}

	public void setLosType(LOSType losType) {
		this.losType = losType;
	}
	
}
