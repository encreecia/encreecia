package org.fealdia.orpg.common.maps;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.xml.bind.annotation.XmlAccessOrder;
import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorOrder(XmlAccessOrder.ALPHABETICAL)
public class Animation {
	public final static String PREFIX = "anim:";

	@XmlElement(name = "frame")
	List<String> frames = new LinkedList<String>();
	@XmlAttribute
	String name;
	@XmlAttribute
	long speed = 4;
	@XmlAttribute
	Type type = Type.cycle;
	@XmlAttribute
	/**
	 * Whether to synchronize this animation with others of same type.
	 */
	boolean sync = false;

	public enum Type {
		cycle,
		bounce,
		random,
	};

	public String getFrame() {
		return getFrameWithSeed(0);
	}

	/**
	 * Seed is unique id (eg. entity id) used to make sure animation is not synced with others of the same type, unless sync property is set.
	 */
	public String getFrameWithSeed(long seed) {
		if (sync) {
			seed = 0;
		}
		return getFrame(System.currentTimeMillis() / 100, seed);
	}

	public String getFrame(long time, long seed) {
		long period = speed * frames.size();
		int frame = (int) (((time + seed) % period) / speed);
		switch (type) {
			case cycle:
				break;
			case bounce:
				period *= 2;
				frame = (int) (((time + seed) % period) / speed);
				if (frame >= frames.size()) {
					frame = (2 * frames.size() - 1) - frame;
				}
				break;
			case random:
				// seed random so that it changes only every "speed"
				frame = new Random(time / speed).nextInt(frames.size());
				break;
		}
		return frames.get(frame);
	}

	public List<String> getUniqueFrames() {
		return new LinkedList<String>(new HashSet<String>(frames));
	}

	public String getName() {
		return name;
	}
}
