package org.fealdia.orpg.common.maps;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;

/**
 * List of chunks. There is one global list shared by all maps, and private one for each.
 * 
 * File format in NBO:
 * <2:version><2:size=2><2:chunksize=16><2:chunks><chunksize^2*chunks*2:tilerefs>
 */
public class ChunkList {
	// Path to global chunk file (common to all maps)
	private final static String GLOBAL_FILE = PathConfig.getMapDir() + "chunks";
	
	private static ChunkList instance = null;
	
	private static Logger logger = Logger.getLogger(ChunkList.class);
	
	private List<Chunk> chunks = new LinkedList<Chunk>();
	
	public static synchronized ChunkList getInstance() {
		if (instance == null) {
			instance = new ChunkList();
			if (!instance.loadFromFile(GLOBAL_FILE)) {
				instance.addChunk(new Chunk());
			}
		}
		return instance;
	}
	
	public ChunkList() {
	}
	
	public boolean loadFromFile(String name) {
		chunks.clear();
		DataInputStream dis = null;
		try {
			dis = new DataInputStream(new BufferedInputStream(new FileInputStream(name)));
			
			// Read "header" information: version, size, chunksize
			dis.readShort(); // version
			short size = dis.readShort();
			if (size != 2) {
				throw new RuntimeException("Size != 2");
			}
			short chunksize = dis.readShort();
			if (chunksize != 16) {
				throw new RuntimeException("chunksize != 16");
			}
			
			// Read the chunks
			short numchunks = dis.readShort();
			for (int i = 0; i < numchunks; i++) {
				// create new chunk from stream
				chunks.add(new Chunk(dis));
			}
			
			dis.close();
		} catch (FileNotFoundException e) {
			logger.warn("Failed to load chunks from " + name);
			return false;
		} catch (IOException e) {
			logger.warn("Failed to load chunks from " + name, e);
			return false;
		} finally {
			try {
				dis.close();
			} catch (Exception e) {
			}
		}
		
		logger.info("Loaded " + chunks.size() +" chunks from " + name);
		
		return true;
	}
	
	public boolean saveToFile() {
		return saveToFile(GLOBAL_FILE);
	}
	
	public boolean saveToFile(String name) {
		logger.debug("saveToFile(" + name + ") size = " + chunks.size());
		try {
			FileOutputStream fos = new FileOutputStream(name);
			DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(fos));
			
			// Write headers: version, size, chunksize, number of chunks
			dos.writeShort(0); // version
			dos.writeShort(2); // size = 2
			dos.writeShort(16); // chunksize = 16
			dos.writeShort(chunks.size()); // chunks
			
			// Write chunks
			for (int i = 0; i < chunks.size(); i++) {
				chunks.get(i).saveToDataOutputStream(dos);
			}
			
			dos.close();
		} catch (FileNotFoundException e) {
			logger.error("saveToFile() for " + name + " failed", e);
			return false;
		} catch (IOException e) {
			logger.error("saveToFile() for " + name + " failed", e);
			return false;
		}
		
		return true;
	}

	public Chunk getChunk(int chunk_id) {
		if (chunk_id >= chunks.size()) {
			return null;
		}
		return chunks.get(chunk_id);
	}

	public List<Chunk> getChunks() {
		return chunks;
	}
	
	/**
	 * Duplicates a chunk and returns the new index.
	 */
	public int duplicateChunk(int index) {
		Chunk chunk = getChunk(index);
		getChunks().add(new Chunk(chunk));
		return getChunks().size() - 1;
	}
	
	public void addChunk(Chunk chunk) {
		getChunks().add(chunk);
	}
	
	public int size() {
		return getChunks().size();
	}
}
