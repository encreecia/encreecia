package org.fealdia.orpg.common.maps;

/**
 * Notified when ImageMapper fails to serve an image.
 */
public interface ImageMissingListener {
	public void onImageMissing(String name);

	public void onBackgroundMissing(int backgroundId);
}
