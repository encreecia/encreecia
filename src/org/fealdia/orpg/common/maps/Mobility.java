package org.fealdia.orpg.common.maps;

/**
 * Describes the type of movement a Creature is capable of. For other entities, the terrain it can be on.
 *
 * @see Tile.MoveType
 */
public enum Mobility {
	NORMAL (true, false, false, false),
	GHOST (true, true, false, false),
	WATER (false, false, false, true),
	FLIGHT (true, false, true, true),
	DM (true, true, true, true),
	;
	
	private boolean[] vals;
	
	Mobility(boolean... vals) {
		assert(vals.length == Tile.MoveType.values().length);
		
		this.vals = vals;
	}
	
	/**
	 * Can this type of Mobility pass onto the given MoveType?
	 */
	public boolean canPass(Tile.MoveType moveType) {
		return vals[moveType.ordinal()];
	}
}
