package org.fealdia.orpg.common.maps;


/**
 * Provides Line of Sight calculations for Scenes. This actually calculates a Field of View.
 */
public class LineOfSight {
	//private static Logger logger = Logger.getLogger(LineOfSight.class);
	
	private Scene scene = null;
	private int radius = 10; // TODO get from server
	private int player_x = -1;
	private int player_y = -1;
	
	private int[] matrix = null;

	public LineOfSight(Scene scene) {
		setScene(scene);
	}
	
	public LineOfSight(Scene scene, int player_x, int player_y) {
		setScene(scene);
		setPlayerPosition(player_x, player_y);
	}
	
	protected synchronized void calculate() {
		if (matrix != null) {
			return;
		}
		
		matrix = new int[getMatrixWidth()*getMatrixWidth()];
		//logger.debug("Calculating matrix for " + matrix.length + " at x = " + this.x + " y = " + this.y);
		
		// Nothing can be seen at first
		for (int i = 0; i < matrix.length; i++) {
			matrix[i] = -100;
		}
		
		floodFill(player_x, player_y, 10, true);
	}
	
	protected void floodFill(int x, int y, int left, boolean first) {
		//logger.debug("floodFill(x = " + x + ", y = " + y + ", left = " + left);
		if (!first) {
			Tile tile = scene.getBackgroundTile(x, y);
			if (tile.getLosType() == Tile.LOSType.WINDOW) {
				// If we are not standing next to the window, it blocks our view like any wall
				int dx = Math.abs(player_x - x);
				int dy = Math.abs(player_y - y);
				if (!(dx <= 1 && dy <= 1 && dx + dy <= 1)) {
					left -= 10;
				}
				// else we see through it
			} else {
				int density = tile.getDensity();
				left -= density;
			}
		}
		
		int matrix = getMatrix(x, y);
		if (left > matrix) {
			setMatrix(x, y, left);
		}
		else {
			return;
		}
		if (left > 0) {
			int[][] places = {
					{-1, 0 },
					{ 1, 0 },
					{ 0,-1 },
					{ 0, 1 },
					{-1,-1 },
					{-1, 1 },
					{ 1,-1 },
					{ 1, 1 },
			};
			
			for (int i = 0; i < places.length; i++) {
				int tx = x + places[i][0];
				int ty = y + places[i][1];
				if (isWithinMatrix(tx, ty)) {
					floodFill(tx, ty, left, false);
				}
			}
		}
	}
	
	protected int getMatrix(int x, int y) {
		int relx = getRadius() + x - this.player_x;
		int rely = getRadius() + y - this.player_y;
		
		//int index = rely*getMatrixWidth() + relx;
		//logger.debug("Matrix size: " + matrix.length + ", index = " + index);
		return matrix[rely*getMatrixWidth() + relx];
	}
	
	protected int getMatrixWidth() {
		return getRadius()*2 + 1;
	}
	
	public int getRadius() {
		return radius;
	}
	
	public Scene getScene() {
		return scene;
	}
	
	protected void setMatrix(int x, int y, int value) {
		int relx = getRadius() + x - this.player_x;
		int rely = getRadius() + y - this.player_y;
		
		matrix[rely*getMatrixWidth() + relx] = value;
	}
	
	public synchronized void setPlayerPosition(int x, int y) {
		this.player_x = x;
		this.player_y = y;
		
		matrix = null;
	}
	
	public synchronized void setScene(Scene scene) {
		this.scene = scene;
		
		matrix = null;
	}
	
	public synchronized boolean isVisible(int scene_x, int scene_y) {
		if (!isWithinMatrix(scene_x, scene_y)) {
			return false;
		}
		calculate();
		
		int matrix = getMatrix(scene_x, scene_y);
		return (matrix >= 0); 
	}
	
	public synchronized boolean isWithinMatrix(int scene_x, int scene_y) {
		int leftEdge = Math.max(0, this.player_x - getRadius());
		int rightEdge = Math.min(getScene().getWidth(), this.player_x + getRadius());
		int topEdge = Math.max(0, this.player_y - getRadius());
		int bottomEdge = Math.min(getScene().getHeight(), this.player_y + getRadius());
		
		return (scene_x >= leftEdge && scene_x <= rightEdge && scene_y >= topEdge && scene_y <= bottomEdge);
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
}
