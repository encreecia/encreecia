package org.fealdia.orpg.common.maps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.StreamUtil;
import org.fealdia.orpg.common.xml.XMLUtil;

/**
 * Handles loading & saving of Scene entity list.
 */
@XmlRootElement(name = "scene")
public class SceneEntityManager {
	private static Logger logger = Logger.getLogger(SceneEntityManager.class);
	
	@Deprecated
	public final static String KEY_TYPE = "_ARCHETYPE";
	
	@XmlElementWrapper(name = "entities")
	@XmlElement(name = "entity")
	private List<SceneEntity> entities = new LinkedList<SceneEntity>();
	
	public void addEntity(SceneEntity entity) {
		entities.add(entity);
	}
	
	public List<SceneEntity> getEntities() {
		return entities;
	}
	
	/**
	 * Get first entity at the given position, if any.
	 */
	public SceneEntity getFirstEntityAt(int x, int y) {
		for (SceneEntity en : getEntities()) {
			if (en.getX() == x && en.getY() == y) {
				return en;
			}
		}
		return null;
	}
	
	public static SceneEntityManager loadFromFile(final String filename) throws FileNotFoundException, JAXBException {
		return XMLUtil.fromXML(SceneEntityManager.class, filename);
	}

	// For reading legacy txt formats
	@Deprecated
	public boolean loadFromTxtFile(String filename) {
		logger.debug("Loading from file: " + filename);
		
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			logger.warn("Entity file not found: " + filename);
			return false;
		}
		try {
			Map<String,String> props = null;
			for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
				String[] parts = line.split(" +", 2);
				
				if (props != null) {
					if (line.startsWith("</")) {
						SceneEntity sceneEntity = new SceneEntity(props.get(KEY_TYPE), Integer.parseInt(props.get("x")), Integer.parseInt(props.get("y")));
						for (String key : props.keySet()) {
							if (key.equals(KEY_TYPE) || key.equals("x") || key.equals("y")) {
								continue;
							}
							sceneEntity.putProperty(key, props.get(key));
						}
						entities.add(sceneEntity);
						props = null;
					}
					else {
						props.put(parts[0], parts[1]);
					}
				}
				else if (line.startsWith("entity ")) {
					String type = parts[1];
					props = new HashMap<String,String>();
					props.put(KEY_TYPE, type);
				}
				else {
					logger.warn("Unhandled line: " + line);
				}
			}
		} catch (IOException e) {
			logger.warn("loadFromFile() exception", e);
		}
		
		try {
			in.close();
		} catch (IOException e) {}
		
		logger.debug("Loaded " + entities.size() + " entities from file");
		return true;
	}

	/**
	 * Save properties of single entity to stream.
	 */
	public static boolean saveEntityToStream(SceneEntity en, Writer out, boolean skipArchetype) {
		try {
			List<String> keys = new LinkedList<String>(en.getProperties().keySet());
			Collections.sort(keys);
			
			out.write("x " + en.getX() + "\n");
			out.write("y " + en.getY() + "\n");
			if (!skipArchetype) {
				out.write("archetype " + en.getArchetype() + "\n");
			}
			for (String key : keys) {
				out.write(key + " " + en.getProperty(key) + "\n");
			}
		} catch (IOException e) {
			logger.error("saveEntityToStream() failed", e);
			return false;
		}
		return true;
	}
	
	public void saveToFile(final String filename) throws JAXBException, IOException {
		logger.debug("Saving to file: " + filename);

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(filename);
			XMLUtil.toXML(this, fos, true);
		} finally {
			fos.close();
		}

		// Success, now remove legacy .txt
		String removeFile = filename.replaceFirst("\\.xml$", ".txt");
		File file = new File(removeFile);
		if (file.exists() && file.delete()) {
			logger.info("Legacy file removed after successful save to XML: " + removeFile);
		}
	}

	public static boolean modifyEntityFromStream(SceneEntity en, BufferedReader in) {
		en.clearProperties();
		try {
			for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
				String parts[] = line.split(" +", 2);
				if (parts.length == 2) {
					if (parts[0].equals("x")) {
						en.setX(Integer.parseInt(parts[1]));
						continue;
					}
					if (parts[0].equals("y")) {
						en.setY(Integer.parseInt(parts[1]));
						continue;
					}
					en.putProperty(parts[0], parts[1]);
				} else {
					logger.warn("Unhandled line: " + line);
				}
			}
		} catch (IOException e) {
			logger.error("modifyEntityFromStream() failed", e);
			return false;
		}
		return true;
	}
	
	/**
	 * Get map editor face for the entity.
	 *
	 * For spawns, this shows the face of the spawned archetype.
	 */
	public String getEntityEditorFace(SceneEntity entity) {
		ArchetypeManager2 am = ArchetypeManager2.getInstance();
		
		Map<String,String> props = am.getFullArchetype(entity.getArchetype());
		
		// For spawns, show the actual spawning monster
		String className = props.get("class");
		if ("spawn".equals(className)) {
			String arch = entity.getProperty("archetype");
			if (arch != null) {
				return am.getArchetypeFace(arch);
			}
		}
		
		// If face has been overridden from archetype, show it
		if (entity.containsKey("face")) {
			return entity.getProperty("face");
		}
		
		// For the rest, show the archetype face
		return am.getArchetypeFace(entity.getArchetype());
	}
	
	@Deprecated
	public static int getEntityRadius(SceneEntity entity) {
		if (entity.containsKey("radius")) {
			return Integer.parseInt(entity.getProperty("radius"));
		}
		return -1;
	}

	public void moveAllByOffset(int offset_x, int offset_y) {
		for (SceneEntity entity : entities) {
			entity.setX(entity.getX() + offset_x);
			entity.setY(entity.getY() + offset_y);
		}
	}
}
