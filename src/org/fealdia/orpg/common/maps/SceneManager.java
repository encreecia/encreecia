package org.fealdia.orpg.common.maps;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * In charge of loading of scenes. This class has no idea about any live
 * things on the map; it knows only the "static images" of maps. Maybe InstanceManager should
 * be in charge of unloading maps?
 * 
 * Used by map editor and server.
 * 
 * @note InstanceManager uses this class.
 */
public class SceneManager {
	private static SceneManager instance;
	private static Logger logger = Logger.getLogger(SceneManager.class);
	
	private List<Scene> scenes = new LinkedList<Scene>();

	public static SceneManager getInstance() {
		if (instance == null) {
			instance = new SceneManager();
		}
		return instance;
	}
	
	/**
	 * Get Scene with the given name, possibly loading it.
	 * @param name name relative to the maps root directory (eg. "default")
	 * @return scene, or null if the scene cannot be loaded
	 */
	public synchronized Scene getScene(String name) {
		if (name == null) {
			logger.warn("getScene() called with null name");
			return null;
		}
		
		// If the scene has already been loaded, return it
		for (Scene scene : scenes) {
			if (name.equals(scene.getName())) {
				return scene;
			}
		}
		
		// (Try to) load it
		Scene scene = new Scene();
		if (!scene.loadByName(name)) {
			logger.warn("Failed to load scene '" + name + "'");
			return null;
		}
		scenes.add(scene);
		logger.info("Successfully loaded scene '" + scene.getName() + "', " + scenes.size() + " scenes loaded");
		
		return scene;
	}

	public List<Scene> getScenes() {
		return scenes;
	}
	
	public int countScenes() {
		return scenes.size();
	}
	
	/**
	 * Unload all Scenes with usage count of zero.
	 */
	public synchronized void unloadUnusedScenes() {
		for (Iterator<Scene> iter = getScenes().iterator(); iter.hasNext(); ) {
			Scene scene = iter.next();
			synchronized (scene) {
				if (scene.getUsageCount() == 0) {
					logger.info("Unloading scene: " + scene.getName());
					iter.remove();
				}
			}
		}
	}
}
