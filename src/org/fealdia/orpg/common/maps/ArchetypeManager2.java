package org.fealdia.orpg.common.maps;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.StreamUtil;

/**
 * Simple, non-XML archetype manager.
 * 
 * Archetypes are basically key/value pairs of strings. They also have single inheritance.
 * 
 * TODO: cache full archetypes
 */
public class ArchetypeManager2 {
	private static ArchetypeManager2 instance = null;
	
	private static Logger logger = Logger.getLogger(ArchetypeManager2.class);
	
	private static final String INHERITS = "inherits";
	
	/**
	 * archetype_name -> {key -> value}
	 */
	private Map<String,Map<String,String>> archetypes = Collections.synchronizedMap(new HashMap<String,Map<String,String>>());
	
	/**
	 * List of archetype names, in the order they were loaded from file.
	 */
	private List<String> archetypeList = Collections.synchronizedList(new LinkedList<String>());
	
	public static ArchetypeManager2 getInstance() {
		if (instance == null) {
			instance = new ArchetypeManager2();
		}
		return instance;
	}
	
	public ArchetypeManager2() {
		loadFromFile();
	}
	
	public synchronized Map<String,String> getArchetype(String name) {
		return archetypes.get(name);
	}
	
	public synchronized String getArchetypeFace(String name) {
		Map<String,String> props = getFullArchetype(name);
		if (props != null && props.containsKey("face")) {
			return props.get("face");
		}
		return null;
	}
	
	public List<String> getArchetypeList() {
		return archetypeList;
	}
	
	/**
	 * Get archetype with inheritance.
	 */
	public synchronized Map<String,String> getFullArchetype(String name) {
		Map<String,String> baseArch = getArchetype(name);
		if (baseArch == null) {
			return null;
		}
		
		Map<String,String> result = new HashMap<String,String>(baseArch);
		
		// now the inheritance part
		String parentName = name;
		String inherits = result.get(INHERITS);
		while (inherits != null) {
			Map<String,String> parent = getArchetype(inherits);
			if (parent == null) {
				logger.warn("Archetype '" + inherits + "' inherited by '" + parentName + "' is not defined, ignored");
				break;
			}
			
			// Copy all non-existing keys from parent
			for (Map.Entry<String,String> pair : parent.entrySet()) {
				if (!result.containsKey(pair.getKey())) {
					result.put(pair.getKey(), pair.getValue());
				}
			}
			
			parentName = inherits;
			inherits = parent.get(INHERITS);
		}
		result.remove(INHERITS);
		
		return result;
	}
	
	public synchronized void loadFromFile() {
		archetypeList.clear();
		archetypes.clear();
		
		String filename = PathConfig.getXmlDir() + "archetypes.txt";
		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			logger.error("Archetypes file not found, raising exception");
		}
		
		String in_arch = null;
		Map<String,String> archMap = null;
		try {
			for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
				String[] parts = line.split(" +", 2);
				String key = parts[0];
				String value = null;
				if (parts.length > 1) {
					value = parts[1];
				}
				
				if (in_arch != null) {
					if (line.startsWith("</")) {
						logger.trace("Added archetype '" + in_arch + "' with " + archMap.size() + " properties");
						archetypes.put(in_arch, archMap);
						archetypeList.add(in_arch);
						in_arch = null;
						archMap = null;
					}
					else {
						archMap.put(key, value);
					}
				}
				else {
					if ("archetype".equals(key)) {
						in_arch = value;
						archMap = new HashMap<String,String>();
					}
					else {
						logger.error("Unhandled line: " + line);
					}
				}
			}
		} catch (IOException e1) {
			logger.error("loadFromFile() failed", e1);
		}
		
		try {
			in.close();
		} catch (IOException e) {
			logger.error("loadFromFile() close failed", e);
		}
		
		logger.debug(archetypes.size() + " archetypes loaded");
	}
	
	public int size() {
		return archetypeList.size();
	}

	public void reload() {
		loadFromFile();
	}
}
