package org.fealdia.orpg.common.maps;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * Represents a 16x16 piece of a Scene background.
 * 
 * TODO Figure out a way/place to cache Image representations of Chunks (own class?)
 */
public class Chunk {
	private static Logger logger = Logger.getLogger(Chunk.class);
	
	// References to tiles in backgrounds.xml
	private short[] data = null;
	
	// Note: if this is changed, the binary-saved maps will break
	public static int getSize() {
		return 16;
	}
	
	public Chunk() {
		data = new short[getSize() * getSize()];
		fill(0);
	}
	
	/**
	 * Attempt to create Chunk from binary data stream.
	 */
	public Chunk(DataInputStream dis) throws IOException {
		data = new short[getSize() * getSize()];
		fill(0);
		
		for (int i = 0; i < getSize() * getSize(); i++) {
			data[i] = dis.readShort();
		}
	}
	
	/**
	 * Create a new Chunk like the given one.
	 */
	public Chunk(Chunk src) {
		data = new short[getSize() * getSize()];
		
		// Copy data
		for (int i = 0; i < getSize() * getSize(); i++) {
			data[i] = src.data[i];
		}
	}

	public void fill(int tile) {
		for (int i = 0; i < getSize() * getSize(); i++) {
			data[i] = (short)tile;
		}
	}

	public void saveToDataOutputStream(DataOutputStream dos) throws IOException {
		for (int i = 0; i < getSize() * getSize(); i++) {
			dos.writeShort(data[i]);
		}
	}

	public int getTile(int x, int y) {
		return data[y*getSize() + x];
	}

	/**
	 * Changes said tile to the given one.
	 * @return true if the tile was changed (to a different value)
	 */
	public boolean setTile(int x, int y, int tile) {
		if (x < 0 || y < 0 || x > getSize() || y > getSize()) {
			logger.warn("setTile() called with invalid parameters");
			return false;
		}
		if (data[y * getSize() + x] == tile) {
			return false;
		}
		
		data[y * getSize() + x ] = (short)tile;
		return true;
	}
}
