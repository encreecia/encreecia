package org.fealdia.orpg.common.maps;

import java.awt.Color;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.xml.SAXHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 * Reads backgrounds.xml and serves as a mapper between binary references to background tiles 
 * in maps (chunks) and actual tiles.
 */
public class BackgroundMapper extends SAXHandler {
	private static BackgroundMapper instance = null;
	
	private static Logger logger = Logger.getLogger(BackgroundMapper.class);
	
	private List<Tile> tiles = new LinkedList<Tile>();
	
	private Tile tile = null;
	
	public static BackgroundMapper getInstance() {
		if (instance == null) {
			instance = new BackgroundMapper();
		}
		return instance;
	}
	
	public BackgroundMapper() {
		loadBackgrounds();
	}
	
	public Tile getTile(int tile_id) {
		return tiles.get(tile_id);
	}
	
	public int getBackgroundCount() {
		return tiles.size();
	}
	
	public boolean loadBackgrounds() {
		tiles.clear();
		
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			parser.parse(new File(PathConfig.getMapDir() + "backgrounds.xml"), this);
		}
		catch (Exception e) {
			logger.error("Background loading failed", e);
			return false;
		}
		logger.info(tiles.size() + " background tiles loaded.");
		return true;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		
		if ("bg".equals(qName) && tile == null) {
			tile = new Tile();
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		//logger.debug("Element " + qName + " ends with " + getCharactersInElement());
		String content = getCharactersInElement();
		
		super.endElement(uri, localName, qName);
		
		if ("bg".equals(qName) && tile != null) {
			tiles.add(tile);
			tile = null;
		}
		else if ("density".equals(qName)) {
			tile.setDensity(Integer.parseInt(content));
		}
		else if ("face".equals(qName)) {
			tile.setFace(content);
		}
		else if ("lostype".equals(qName)) {
			tile.setLosType(Tile.LOSType.valueOf(content.toUpperCase()));
		}
		else if ("movetype".equals(qName)) {
			tile.setMoveType(Tile.MoveType.valueOf(content.toUpperCase()));
		}
		else if ("name".equals(qName)) {
			tile.setName(content);
		}
		else if ("speed".equals(qName)) {
			tile.setSpeed(Integer.parseInt(content));
		}
		else if ("color".equals(qName)) {
			
			// Hex format: "#ff0000" or "#f00"
			if (content.startsWith("#")) {
				int r = 0, g = 0, b = 0;
				if (content.length() == 4) {
					String rs = content.substring(1, 2);
					r = Integer.parseInt(rs + rs, 16);
					String gs = content.substring(2, 3);
					g = Integer.parseInt(gs + gs, 16);
					String bs = content.substring(3, 4);
					b = Integer.parseInt(bs + bs, 16);
				}
				else if (content.length() == 7) {
					r = Integer.parseInt(content.substring(1, 3), 16);
					g = Integer.parseInt(content.substring(3, 5), 16);
					b = Integer.parseInt(content.substring(5, 7), 16);
				}
				else {
					logger.warn("Invalid color value: " + content);
				}
				//logger.debug("hex color read: " + r + ", " + g + ", " + b);
				tile.setColor(new Color(r, g, b));
			}
			
			// Integer format: "255 0 0"
			else {
				String[] parts = content.split(" +");
				tile.setColor(new Color(
						Integer.parseInt(parts[0]),
						Integer.parseInt(parts[1]),
						Integer.parseInt(parts[2])
				));
			}
		}
		else if (!"backgrounds".equals(qName)) {
			logger.warn("Ignored element " + qName);
		}
	}
	
	public void reload() {
		loadBackgrounds();
	}
	
}
