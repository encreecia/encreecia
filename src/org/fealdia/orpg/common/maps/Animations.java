package org.fealdia.orpg.common.maps;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.xml.XMLUtil;

@XmlRootElement
public class Animations {
	public static final String FILENAME = "maps/animations.xml";
	private static Animations instance = null;

	@XmlElement(name = "animation")
	List<Animation> animations = new LinkedList<Animation>();

	synchronized public static Animations getInstance() {
		if (instance == null) {
			try {
				instance = XMLUtil.fromXML(Animations.class, PathConfig.getRootDir() + Animations.FILENAME);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return instance;
	}

	public Animation getAnimation(String name) {
		for (Animation animation : animations) {
			if (animation.getName().equals(name)) {
				return animation;
			}
		}
		return null;
	}
}
