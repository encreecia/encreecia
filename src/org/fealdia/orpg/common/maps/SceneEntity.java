package org.fealdia.orpg.common.maps;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.fealdia.orpg.common.xml.MapEntry;

/**
 * SceneEntity is for MapEntity what Scene is for Instance.
 * 
 * Scenes have entities, and when an Instance is created from a Scene, the SceneEntities are created as MapEntities as well.
 */
@XmlRootElement(name = "entity")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "archetype", "x", "y", "propertiesAsMapEntryArray" })
public class SceneEntity {
	@XmlAttribute
	private String archetype;
	@XmlAttribute
	private int y = 0;
	@XmlAttribute
	private int x = 0;
	@XmlTransient
	//@XmlJavaTypeAdapter(MapAdapter.class) // Instead of this, we use a workaround below
	private HashMap<String,String> properties = new HashMap<String,String>();

	// JAXB constructor
	@SuppressWarnings("unused")
	private SceneEntity() {
	}

	public SceneEntity(final String archetype, final int x, final int y) {
		this.archetype = archetype;
		this.x = x;
		this.y = y;
	}

	public String getArchetype() {
		return archetype;
	}

	public void setArchetype(String archetype) {
		this.archetype = archetype;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public HashMap<String,String> getProperties() {
		return properties;
	}

	public void setProperties(HashMap<String,String> properties) {
		this.properties = properties;
	}

	public boolean containsKey(final String key) {
		return properties.containsKey(key);
	}

	public String getProperty(final String key) {
		return properties.get(key);
	}

	public void putProperty(final String key, final String value) {
		properties.put(key, value);
	}

	public void clearProperties() {
		properties.clear();
	}

	// JAXB workaround to prevent generation of element wrapper (XmlAdapter would do that)
	@XmlElement(name = "property")
	private MapEntry[] getPropertiesAsMapEntryArray() {
		return MapEntry.mapToArray(properties);
	}

	@SuppressWarnings("unused")
	private void setPropertiesAsMapEntryArray(final MapEntry[] mapentries) {
		this.properties = MapEntry.fromArray(mapentries);
	}

	// Helper methods for properties

	// Spawns
	public int getRadius() {
		if (properties.containsKey("radius")) {
			return Integer.parseInt(properties.get("radius"));
		}
		return -1;
	}
}
