package org.fealdia.orpg.common.maps;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;

/**
 * Scans images directory, and allows mapping of "short names" to actual filenames and images.
 * Also loads and caches images after first request.
 */
public class ImageMapper {
	private static ImageMapper instance = null;
	
	private static Logger logger = Logger.getLogger(ImageMapper.class);
	
	private String size;
	
	// name -> image
	private Map<String, BufferedImage> imageCache = new HashMap<String, BufferedImage>();
	
	private ImageMissingListener imageMissingListener = null;
	
	// TODO move getInstance and setInstance to ImageMapperFactory?
	public static ImageMapper getInstance() {
		if (instance == null) {
			instance = new ImageMapper("32x32");
		}
		return instance;
	}
	
	public static void setInstance(ImageMapper imageMapper) {
		if (instance != null) {
			throw new RuntimeException("ImageMapper instance can not be set twice");
		}
		instance = imageMapper;
	}

	public ImageMapper(String size) {
		logger.trace("Creating instance with size = " + size);
		this.size = size;
	}
	
	public int getTileSize() {
		return 32;
	}
	
	public String getImageDir() {
		return PathConfig.getImageDir() + size;
	}

	public BufferedImage getBackgroundImage(int backgroundId) {
		return getImage(BackgroundMapper.getInstance().getTile(backgroundId).getFace());
	}

	public BufferedImage getImage(String name) {
		return getImage(name, 0);
	}

	public BufferedImage getImage(String name, long seed) {
		if (name.startsWith(Animation.PREFIX)) {
			String anim = name.substring(5);
			name = Animations.getInstance().getAnimation(anim).getFrameWithSeed(seed);
		}
		if (imageCache.containsKey(name)) {
			return imageCache.get(name);
		}
		
		// Load it
		try {
			synchronized (this) {
				BufferedImage result = ImageIO.read(new File(getImageFilename(name)));
				if (logger.isTraceEnabled()) {
					logger.trace("Image loaded: " + name + " (" + imageCache.size() + " in cache)");
				}
				imageCache.put(name, result);
				return result;
			}
		} catch (IOException e) {
			if (getImageMissingListener() != null) {
				logger.debug("Failed to load image: " + name + ", notifying listener");
				getImageMissingListener().onImageMissing(name);
			} else {
				logger.warn("Failed to load image: " + name);
			}
		}
		
		return null;
	}
	
	/**
	 * Clears the image cache, allowing the images to be reloaded on next request.
	 */
	public void reload() {
		imageCache.clear();
	}

	public ImageMissingListener getImageMissingListener() {
		return imageMissingListener;
	}

	public void setImageMissingListener(ImageMissingListener imageMissingListener) {
		this.imageMissingListener = imageMissingListener;
	}
	
	public String getImageFilename(String faceName) {
		return getImageDir() + File.separatorChar + faceName + ".png";
	}
}
