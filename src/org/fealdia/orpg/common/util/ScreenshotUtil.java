package org.fealdia.orpg.common.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

/**
 * Utility methods for taking screenshots.
 */
public class ScreenshotUtil {
	private static Logger logger = Logger.getLogger(ScreenshotUtil.class);
	
	public static boolean saveScreenshot(Component component, String filename) {
		BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(), BufferedImage.TYPE_INT_RGB);
		
		// Paint the component to the BufferedImage
		Graphics2D g2d = image.createGraphics();
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
		component.paint(g2d);
		g2d.dispose();
		
		// Save the image to file
		try {
			ImageIO.write(image, "png", new File(filename));
		} catch (IOException e) {
			logger.error("saveScreenshot() failed", e);
			return false;
		}
		
		return true;
	}

}
