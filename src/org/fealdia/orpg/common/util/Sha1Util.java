package org.fealdia.orpg.common.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class Sha1Util {
	private static Logger logger = Logger.getLogger(Sha1Util.class);
	
	/**
	 * Get SHA1 checksum for a file.
	 */
	public static String getSha1ForFile(String filename) throws Exception {
		return getHexString(getSha1ByteArrayForFile(filename));
	}

	public static byte[] getSha1ByteArrayForFile(final String filename) throws NoSuchAlgorithmException, IOException {
		byte[] digest;
		MessageDigest sha1 = MessageDigest.getInstance("SHA1");

		DigestInputStream in = new DigestInputStream(new FileInputStream(filename), sha1);

		// Read until EOF
		byte[] buf = new byte[1024];
		while (in.read(buf) != -1) {
		}

		digest = sha1.digest();
		in.close();

		return digest;
	}

	public static String getSha1ForString(String data) {
		try {
			MessageDigest sha1 = MessageDigest.getInstance("SHA1");
			
			byte[] digest = sha1.digest(data.getBytes());
			
			return getHexString(digest);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Failed to get MessageDigest for SHA1", e);
		}
		return null;
	}
	
	/**
	 * Converts data into a hex string (two characters per byte).
	 */
	public static String getHexString(byte[] data) {
		StringBuffer res = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int upper = (data[i] & 0xf0) >>> 4;
			int lower = data[i] & 0x0f;
			res.append(Integer.toHexString(upper));
			res.append(Integer.toHexString(lower));
		}
		return res.toString();
	}

}
