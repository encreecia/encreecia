package org.fealdia.orpg.common.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * String-related utility methods.
 */
public class StringUtil {
	
	public static String capitalize(String text) {
		String lower = text.toLowerCase();
		return lower.substring(0, 1).toUpperCase() + lower.substring(1);
	}
	
	public static String join(Collection<String> strings, String delim) {
		if (strings.isEmpty()) {
			return "";
		}
		
		Iterator<String> iter = strings.iterator();
		StringBuffer buf = new StringBuffer(iter.next());
		while (iter.hasNext()) {
			buf.append(delim).append(iter.next());
		}
		return buf.toString();
	}

	/**
	 * Split given line into "parameters".
	 * 
	 * Example: "Foo bar" baz -> [Foo bar, baz]
	 */
	public static String[] splitIntoParameters(String line) {
		List<String> res = new LinkedList<String>();
		boolean in_quote = false;
		boolean in_word = false;
		int start = 0;
		for (int i = 0; i < line.length(); i++) {
			//System.out.println(i + " " + line.charAt(i) + " " + in_word + " " + in_quote);
			if (in_word) {
				if (in_quote) {
					if (line.charAt(i) == '"') {
						in_quote = false;
						in_word = false;
						res.add(line.substring(start, i));
						start = i + 1;
					}
					continue;
				}
				
				if (line.charAt(i) == ' ') {
					// word end
					res.add(line.substring(start, i));
					in_word = false;
					start = i + 1;
				}
				continue;
			}

			if (line.charAt(i) == ' ') {
				start = i + 1;
			}
			else if (line.charAt(i) == '"') {
				in_word = true;
				in_quote = true;
				start = i + 1;
			}
			else {
				in_word = true;
			}
		}
		if (in_word) {
			res.add(line.substring(start, line.length()));
		}
		
		return res.toArray(new String[0]);
	}
	
	public static String trimLeft(String s) {
		int start = 0;
		// seek until first non-whitespace is found
		for (start = 0; start < s.length() && (s.charAt(start) == ' ' || s.charAt(start) == '\t'); start++) {}
		if (start > 0) {
			return s.substring(start);
		}
		return s;
	}

	public static String toHexString(long value) {
		return String.format("%08x", value);
	}
}
