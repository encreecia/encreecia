package org.fealdia.orpg.common.util;

import java.io.File;
import java.io.IOException;

public class SaveHelper {
	public interface Saveable {
		public void saveToFile(String filename) throws Exception;
	}

	public static void saveToFileWithBackup(Saveable saveable, final String filename) throws Exception {
		final String tempfile = filename + ".tmp";

		saveable.saveToFile(tempfile);

		FileUtil.renameAsBackupWithException(filename);

		File f = new File(tempfile);
		boolean res = f.renameTo(new File(filename));
		if (!res) {
			throw new IOException("Rename from tmp to actual filename failed: " + filename);
		}
	}
}
