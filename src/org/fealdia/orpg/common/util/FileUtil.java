package org.fealdia.orpg.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.ByteBuffer;

import org.apache.log4j.Logger;

public class FileUtil {
	private final static Logger logger = Logger.getLogger(FileUtil.class);
	
	/**
	 * Get file content as ByteBuffer, or null on error.
	 * 
	 * Supports files up to Integer.MAX_VALUE in size (2 GiB).
	 */
	public static ByteBuffer getFileContentAsByteBuffer(String filename) {
		ByteBuffer buf = null;

		// Allocate ByteBuffer as long as the file
		File file = new File(filename);
		int fileSize = (int) file.length();
		buf = ByteBuffer.allocate(fileSize);
		
		try {
			DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)));

			try {
				while (true) {
					buf.put(in.readByte());
				}
			} catch (EOFException e) {}
		} catch (FileNotFoundException e) {
			logger.error("File not found: " + filename);
			return null;
		} catch (IOException e) {
			logger.error("Failure reading file contents: " + filename, e);
			throw new RuntimeException(e);
		}

		return buf;
	}
	
	/**
	 * Get file content as String, or null on errors.
	 */
	public static String getFileContentAsString(String filename) {
		ByteBuffer buf = getFileContentAsByteBuffer(filename);
		if (buf != null) {
			return new String(buf.array());
		}
		return null;
	}
	
	public static BufferedReader getFileOrResourceAsReader(String filename) {
		BufferedReader reader = null;

		// Filesystem
		File f = new File(PathConfig.getRootDir() + filename);
		if (f.exists()) {
			try {
				reader = new BufferedReader(new FileReader(f));
			} catch (FileNotFoundException e) {
				return null;
			}
		}
		
		// Resource
		else {
			InputStream is = FileUtil.class.getResourceAsStream("/" + filename);
			if (is != null) {
				reader = new BufferedReader(new InputStreamReader(is));
			} else {
				return null;
			}
		}
		
		return reader;
	}

	public static String getFileOrResourceContentAsString(String filename) {
		BufferedReader reader = getFileOrResourceAsReader(filename);
		if (reader == null) {
			return null;
		}
		
		// Read the lines
		StringBuffer res = new StringBuffer();
		try {
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				res.append(line);
				res.append("\n");
			}
		} catch (IOException e) {
			return null;
		}
		
		return res.toString();
	}
	
	/**
	 * Get first line in file as string, or null on error.
	 */
	public static String getFirstLineAsString(String filename) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();
			reader.close();
			return line;
		} catch (FileNotFoundException e) {
			logger.warn("getFirstLineAsString(): file not found: " + filename);
		} catch (IOException e) {
			logger.error("getFirstLineAsString(): " + filename, e);
		}
		return null;
	}
	
	public static String getURLContentAsString(URL url) {
		if (url == null) {
			return null;
		}
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			
			StringBuffer result = new StringBuffer();
			
			char[] data = new char[512];
			for (int len = in.read(data); len != -1; len = in.read(data)) {
				result.append(data, 0, len);
			}
			return result.toString();
		} catch (IOException e) {
			logger.error("getURLContentAsString() IOException", e);
		}
		
		return null;
	}
	
	/**
	 * Rename the given file to keep it as a backup.
	 */
	public static boolean renameAsBackup(String filename) {
		File oldFile = new File(filename);
		if (oldFile.exists()) {
			if (!oldFile.renameTo(new File(filename + "." + System.currentTimeMillis()))) {
				logger.error("renameAsBackup() failed for " + filename);
				return false;
			}
		}
		
		return true;
	}
	
	public static void renameAsBackupWithException(final String filename) throws IOException {
		if (!renameAsBackup(filename)) {
			throw new IOException("Failed to rename file " + filename);
		}
	}

	/**
	 * Saves the given data to the file.
	 */
	public static boolean saveByteArrayToFile(byte[] data, String filename) {
		// Create the directory
		File file = new File(filename);
		new File(file.getParent()).mkdirs();
		
		try {
			DataOutputStream out = new DataOutputStream(new FileOutputStream(filename));
			out.write(data);
			out.close();
		} catch (FileNotFoundException e) {
			logger.error("File not found: " + filename);
			return false;
		} catch (IOException e) {
			logger.error("Failed to write file:" + filename, e);
			return false;
		}
		
		return true;
	}
}
