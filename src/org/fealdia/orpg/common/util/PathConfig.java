package org.fealdia.orpg.common.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

/**
 * Path configuration for client, map editor and server.
 */
public class PathConfig {
	private static String rootDir = "";
	
	public static String getClientScreenshotDir() {
		return getUserRootDir() + "shots" + File.separatorChar;
	}

	/**
	 * Gets the root directory, which ends in '/' (unless empty). Default is empty root directory.
	 * 
	 * This is used as prefix for other paths.
	 */
	public static String getRootDir() {
		return rootDir;
	}

	public static void setRootDir(String rootDir) {
		if (rootDir.length() > 0 && !rootDir.endsWith(File.separator)) {
			rootDir += File.separator;
		}
		
		PathConfig.rootDir = rootDir;
	}
	
	public static String getEtcDir() {
		return getRootDir() + "etc" + File.separatorChar;
	}
	
	public static String getImageDir() {
		return getRootDir() + "images" + File.separatorChar;
	}
	
	/**
	 * Get the directory maps are in, ending in '/'.
	 */
	public static String getMapDir() {
		return getRootDir() + "maps" + File.separatorChar;
	}
	
	public static String getServerDataDir() {
		return getRootDir() + "data" + File.separatorChar;
	}
	
	/**
	 * Get platform-dependent jfealdia directory.
	 * @return
	 */
	public static String getUserRootDir() {
		return System.getProperty("user.home") + File.separator + ".jfealdia" + File.separator;
	}
	
	public static String getUserHookDir() {
		return System.getProperty("user.home") + File.separator + ".jfealdia" + File.separator + "hooks" + File.separator;
	}
	
	public static String getXmlDir() {
		return getRootDir() + "xml" + File.separatorChar;
	}
	
	public static String getDMPasswordPath() {
		return getServerDataDir() + "dm_password";
	}

	public static void initUserRootDir() {
		String userRoot = getUserRootDir();
		new File(userRoot).mkdirs();
		setRootDir(userRoot);
	}

	public static void wipeUserCache() {
		// Make sure we are not deleting files under encreecia checkout
		if ("".equals(PathConfig.getRootDir())) {
			throw new IllegalArgumentException("Refusing to wipe cache when root dir has not been set!");
		}

		// Delete downloaded images and maps
		try {
			FileUtils.deleteDirectory(new File(PathConfig.getMapDir()));
			FileUtils.deleteDirectory(new File(PathConfig.getImageDir()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
