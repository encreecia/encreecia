package org.fealdia.orpg.common.util;

/**
 * Time-related utility methods.
 */
public class TimeUtil {

	/**
	 * Breaks seconds into seconds, minutes, hours, days.
	 */
	public static int[] breakSecsToDHMS(int total_secs) {
		int secs = total_secs;
		int mins = 0;
		int hours = 0;
		int days = 0;
		days = secs / (3600*24);
		secs -= days * (3600*24);

		hours = secs / 3600;
		secs -= hours * 3600;

		mins = secs / 60;
		secs -= mins * 60;

		int[] res = { secs, mins, hours, days };
		return res;
	}
	
	/**
	 * Get "N days, N hours, N minutes, N seconds" from seconds.
	 */
	public static String getDHMSStringFromSecs(int total_secs) {
		int[] broken = breakSecsToDHMS(total_secs);
		return broken[3] + " days, " + broken[2] + " hours, " + broken[1] + " minutes, " + broken[0] + " seconds";
	}

}
