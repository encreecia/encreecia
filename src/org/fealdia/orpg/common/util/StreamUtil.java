package org.fealdia.orpg.common.util;

import java.io.BufferedReader;
import java.io.IOException;

public class StreamUtil {
	/**
	 * Read a line from specially formatted file/stream. Empty/comment lines are skipped
	 * and other lines trimmed.
	 */
	public static String readLine(BufferedReader in) throws IOException {
		while (true) {
			String line = in.readLine();
			if (line == null) {
				return line;
			}
			line = StringUtil.trimLeft(line);
			if (line.length() != 0 && !line.startsWith("#")) {
				return line;
			}
		}
	}
}
