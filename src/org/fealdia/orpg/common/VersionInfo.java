package org.fealdia.orpg.common;

import org.fealdia.orpg.common.util.FileUtil;

public class VersionInfo {
	public static String getVersion() {
		String version = FileUtil.getFileOrResourceContentAsString("version.txt");
		if (version == null) {
			return "development version";
		}
		return version;
	}

}
