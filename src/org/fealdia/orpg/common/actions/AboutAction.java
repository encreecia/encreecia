package org.fealdia.orpg.common.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.fealdia.orpg.common.ui.AboutDialog;

/**
 * Common Help->About action.
 */
public class AboutAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	
	public AboutAction() {
		putValue(Action.NAME, "About");
		putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
	}

	public void actionPerformed(ActionEvent e) {
		new AboutDialog(null).setVisible(true);
	}

}
