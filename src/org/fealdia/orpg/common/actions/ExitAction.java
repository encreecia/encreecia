package org.fealdia.orpg.common.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

/**
 * Common exit action that simply quits the application, with no confirmations. 
 */
public class ExitAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	
	public ExitAction() {
		putValue(Action.NAME, "Exit");
		putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl Q"));
	}

	public void actionPerformed(ActionEvent e) {
		System.exit(0);
	}

}
