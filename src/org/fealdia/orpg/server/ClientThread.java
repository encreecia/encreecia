package org.fealdia.orpg.server;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.StreamCorruptedException;
import java.math.BigInteger;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Animation;
import org.fealdia.orpg.common.maps.Animations;
import org.fealdia.orpg.common.maps.BackgroundMapper;
import org.fealdia.orpg.common.maps.ImageMapper;
import org.fealdia.orpg.common.net.PacketType;
import org.fealdia.orpg.common.net.ProtoPacket;
import org.fealdia.orpg.common.net.ProtoSocket;
import org.fealdia.orpg.common.net.data.ChatType;
import org.fealdia.orpg.common.net.data.FaceInfo;
import org.fealdia.orpg.common.net.data.FaceMapper;
import org.fealdia.orpg.common.net.data.MusicHint;
import org.fealdia.orpg.common.net.data.ProtoQuest;
import org.fealdia.orpg.common.net.packets.CheckFilePacket;
import org.fealdia.orpg.common.net.packets.ClientChatPacket;
import org.fealdia.orpg.common.net.packets.ClientHello;
import org.fealdia.orpg.common.net.packets.CommandPacket;
import org.fealdia.orpg.common.net.packets.ContainerPacket;
import org.fealdia.orpg.common.net.packets.ContainerPacket.ContainerItem;
import org.fealdia.orpg.common.net.packets.CoordinatePacket;
import org.fealdia.orpg.common.net.packets.EntityEffectPacket;
import org.fealdia.orpg.common.net.packets.EntityHealthPacket;
import org.fealdia.orpg.common.net.packets.EntityInfoPacket;
import org.fealdia.orpg.common.net.packets.EntityMovePacket;
import org.fealdia.orpg.common.net.packets.EntityReferencePacket;
import org.fealdia.orpg.common.net.packets.ExperiencePacket;
import org.fealdia.orpg.common.net.packets.FaceInfoPacket;
import org.fealdia.orpg.common.net.packets.FacePacket;
import org.fealdia.orpg.common.net.packets.FilePacket;
import org.fealdia.orpg.common.net.packets.FlagPacket;
import org.fealdia.orpg.common.net.packets.GetBackgroundFaceInfoPacket;
import org.fealdia.orpg.common.net.packets.GetImagePacket;
import org.fealdia.orpg.common.net.packets.InstanceNamePacket;
import org.fealdia.orpg.common.net.packets.ItemGetPacket;
import org.fealdia.orpg.common.net.packets.LoginPacket;
import org.fealdia.orpg.common.net.packets.MovementVectorPacket;
import org.fealdia.orpg.common.net.packets.MusicHintPacket;
import org.fealdia.orpg.common.net.packets.PartyMarkerPacket;
import org.fealdia.orpg.common.net.packets.PartyMemberPositionPacket;
import org.fealdia.orpg.common.net.packets.PartyMembersPacket;
import org.fealdia.orpg.common.net.packets.PlayerHealthPacket;
import org.fealdia.orpg.common.net.packets.PlayerPositionPacket;
import org.fealdia.orpg.common.net.packets.PopupPacket;
import org.fealdia.orpg.common.net.packets.QuestLogPacket;
import org.fealdia.orpg.common.net.packets.QuestOfferPacket;
import org.fealdia.orpg.common.net.packets.ServerChatPacket;
import org.fealdia.orpg.common.net.packets.ServerHello;
import org.fealdia.orpg.common.net.packets.TextPacket;
import org.fealdia.orpg.common.util.FileUtil;
import org.fealdia.orpg.common.util.Sha1Util;
import org.fealdia.orpg.server.PlayerManager.LoginException;
import org.fealdia.orpg.server.maps.entities.Creature;
import org.fealdia.orpg.server.maps.entities.Creature.WatchableProperty;
import org.fealdia.orpg.server.maps.entities.Item;
import org.fealdia.orpg.server.maps.entities.MapEntity;
import org.fealdia.orpg.server.maps.entities.Monster;
import org.fealdia.orpg.server.maps.entities.Wearable;
import org.fealdia.orpg.server.quests.Quest;
import org.fealdia.orpg.server.quests.QuestInstance;
import org.fealdia.orpg.server.quests.QuestManager;

/**
 * When a client connects to Server, an instance of this class is created. The purpose is to
 * communicate with the client until the connection is lost. This thread is responsible for
 * asking server to create a Player once the player has successfully logged in.
 *
 * All construction of data packets to be sent to client should happen in this class, in the send*() methods.
 * 
 * Corresponding class at the client side is HeadlessClient.
 */
public class ClientThread extends ProtoSocket implements PropertyChangeListener, Runnable {
	private static Logger logger = Logger.getLogger(ClientThread.class);
	private static Logger chatLogger = Logger.getLogger("chat");
	private static Logger playerLogger = Logger.getLogger("player");
	
	private Player player = null;
	private Server server;
	
	private String javaVersion = null;
	private String osName = null;
	private String osVersion = null;
	
	private FaceMapper faceMapper = new FaceMapper();
	private static int nextFaceId = 1;

	public ClientThread(Socket socket, Server server) {
		setSocket(socket);
		this.server = server;
	}
	
	public void run() {
		logger.trace("Run called");
		try {
			readObjects();
		}
		// Normal disconnect?
		catch (EOFException e) {
		}
		// Remote most likely disconnected while reading message, this is normal too
		catch (SocketException e) {
		}
		catch (StreamCorruptedException e) {
			logger.error("Corrupted stream data received, closing connection");
		}
		catch (Exception e) {
			logger.error("Unexpected exception", e);
		}
		
		// Save bandwidth stats
		getServer().incrementBytesTransferred(getBytesReceived(), getBytesSent());
		logger.info("Bandwidth statistics: IN " + getBytesReceivedPerSecond() + " bytes/sec, " + getBytesReceived() + " total, OUT " + getBytesSentPerSecond() + " bytes/sec, " + getBytesSent());
		playerLogger.info("BANDWIDTH " + getPlayerName() + " ip=" + getRemoteIP() + " in=" + getBytesReceived() + " out=" + getBytesSent() + " in/s=" + getBytesReceivedPerSecond() + " out/s=" + getBytesSentPerSecond());
		
		logger.info("IP " + getRemoteIP() + " disconnected");
		
		disconnect();
		
		// Log out the player as well
		logout();
		
		logger.trace("Exiting");
	}

	/**
	 * Handle a single message from client.
	 * 
	 * TODO create separate methods of longest stuff
	 */
	@Override
	protected void handleMessage(ProtoPacket packet) throws Exception {
		if (getPlayer() != null) {
			checkPeriodicalSave();
		}
		
		// Packets allowed before logging in
		final PacketType packetType = packet.getPacketType();

		if (packet instanceof ClientHello) {
			ClientHello hello = (ClientHello) packet;
			logger.debug("Hello received, client from IP " + getRemoteIP() + " claims to be running Java " + hello.javaVersion + " on " + hello.osName + " " + hello.osVersion);

			setJavaVersion(hello.javaVersion);
			setOsName(hello.osName);
			setOsVersion(hello.osVersion);
			sendVersion();
		}
		else if (packet instanceof CheckFilePacket) {
			CheckFilePacket checkFile = (CheckFilePacket) packet;
			logger.trace("Client requested '" + checkFile.filename + "' if sha1 is not " + checkFile.sha1);
			if (isValidDownloadablePath(checkFile.filename)) {
				byte[] actual_sha1 = Sha1Util.getSha1ByteArrayForFile(checkFile.filename);
				if (!actual_sha1.equals(checkFile.sha1)) {
					logger.info("Sending file '" + checkFile.filename + "' (" + actual_sha1 + ") to client");
					sendFile(checkFile.filename);
				}
			}
			else {
				logger.error("Client requested unallowed file: " + checkFile.filename);
			}
		}
		else if (packet instanceof LoginPacket) {
			LoginPacket loginPacket = (LoginPacket) packet;
			handleLogin(loginPacket.username, loginPacket.password);
		}
		else if (packetType == PacketType.EASYPLAY) {
			handleEasyplay();
		}
		else if (packetType == PacketType.GETFILE) {
			String filename = ((TextPacket) packet).text;
			if (isValidDownloadablePath(filename)) {
				logger.info("Sending file '" + filename + "' to client");
				sendFile(filename);
			} else {
				logger.error("Client requested unallowed file: " + filename);
			}
		}
		else if (getPlayer() == null) {
			// After /logout some packets may still arrive, ignore them
			logger.warn("Ignoring PacketType " + packetType + " that requires login: " + packet);
			return;
		}

		else if (packet instanceof ClientChatPacket) {
			ClientChatPacket chat = (ClientChatPacket) packet;

			ChatType chatType = chat.chatType;
			String text = chat.text;
			switch (chatType) {
				case DEFAULT:
				case GLOBAL:
					//PlayerManager.getInstance().sendGlobalNotice("<" + getPlayer().getName() + "> " + text);

					// Log chat
					chatLogger.info(chatType + " <" + getPlayer().getName() + "@" + getPlayer().getInstance().getUniqueName() + "> " + text);

					List<Player> players = PlayerManager.getInstance().getPlayers();
					synchronized (players) {
						for (Player p : players) {
							p.getClientThread().sendChat(getPlayer().getName(), text, ChatType.GLOBAL);
						}
					}
					break;
				case GUILD:
					if (getPlayer().isInGuild()) {
						getPlayer().getGuild().sendChat(getPlayer().getName(), text);
					} else {
						sendNotice("You are not in a guild.");
					}
					break;
				case PARTY:
					if (getPlayer().isInParty()) {
						for (Player p : getPlayer().getParty().getPlayers()) {
							p.getClientThread().sendChat(getPlayer().getName(), text, ChatType.PARTY);
						}
					} else {
						sendNotice("You are not in a party.");
					}
					break;
				case WHISPER:
					// Send to all players within <= 1 squares
					handleChat(text, ChatType.WHISPER, 1);
					break;
				case SHOUT:
					handleChat(text, ChatType.SHOUT, MapEntity.VISIBLE_RADIUS * 4);
					break;
				case SAY:
					handleChat(text, ChatType.SAY, MapEntity.VISIBLE_RADIUS);
					break;
				default:
					sendNotice("Unsupported chat type: " + chatType);
			};
		}
		else if (packet instanceof MovementVectorPacket) {
			MovementVectorPacket move = (MovementVectorPacket) packet;

			// Set movement vector for player
			player.setMovementVector(move.dx, move.dy);
		}
		else if (packetType == PacketType.ENTITYQUERY) {
			sendEntityInfo(((EntityReferencePacket) packet).id);
		}
		else if (packetType == PacketType.TARGET) {
			getPlayer().setTarget(((EntityReferencePacket) packet).id);
		}
		else if (packet instanceof EntityReferencePacket && packetType == PacketType.ITEMAPPLY) {
			getPlayer().tryToApplyItem(((EntityReferencePacket) packet).id);
		}
		else if (packet instanceof ItemGetPacket) {
			ItemGetPacket itemGet = (ItemGetPacket) packet;
			getPlayer().tryToGetItem(itemGet.itemId, itemGet.quantity);
		}
		else if (packet instanceof CommandPacket) {
			String command = ((CommandPacket) packet).command;
			try {
				getPlayer().handleClientCommand(command);
			}
			catch (Exception e) {
				logger.info("Player command caused exception: " + command, e);
				sendNotice("Failed to handle command: " + command);
			}
		}
		else if (packet instanceof CoordinatePacket && packetType == PacketType.LOOKAT) {
			CoordinatePacket coordinatePacket = (CoordinatePacket) packet;
			getPlayer().lookAt(coordinatePacket.x, coordinatePacket.y);
		}
		else if (packetType == PacketType.EXAMINEITEM) {
			getPlayer().examineItem(((EntityReferencePacket) packet).id);
		}
		else if (packet instanceof GetImagePacket) {
			GetImagePacket getImagePacket = (GetImagePacket) packet;
			int faceId = getImagePacket.id;
			sendFace(faceId);
		}
		else if (packet instanceof CoordinatePacket && packetType == PacketType.USE) {
			CoordinatePacket coordinatePacket = (CoordinatePacket) packet;
			getPlayer().handleUse(coordinatePacket.x, coordinatePacket.y);
		}
		else if (packet instanceof CoordinatePacket) {
			CoordinatePacket map = (CoordinatePacket) packet;
			getPlayer().handleMapClicked(map.x, map.y);
		}
		else if (packet instanceof GetBackgroundFaceInfoPacket) {
			int backgroundId = ((GetBackgroundFaceInfoPacket) packet).backgroundId;
			String faceName = BackgroundMapper.getInstance().getTile(backgroundId).getFace();
			getFaceIdOrSend(faceName); // Triggers sending of FaceInfo packet
		}
		else {
			logger.error("Unknown packet type '" + packetType + "': " + packet);
			// TODO: actually throw an exception / or send error to client?
		}
	}
	
	public void handleChat(String text, ChatType chatType, int radius) {
		// Send to all players within <= <radius> squares
		List<MapEntity> entities = getPlayer().getInstance().getEntitiesAtRadius(getPlayer().getX(), getPlayer().getY(), radius);
		for (MapEntity e : entities) {
			if (e instanceof Player) {
				Player p = (Player) e;
				p.getClientThread().sendChat(getPlayer().getName(), text, chatType);
			}
		}
	}
	
	protected void handleLogin(String username, String password) throws IOException {
		logout();
		try {
			synchronized (PlayerManager.getInstance()) {
				PlayerManager.getInstance().handleLogin(username, password, this);
			}
		} catch (LoginException e) {
			logger.info("Login failure (" + username + "): " + e.getMessage());
			sendLoginFailure(e.getMessage());
			return;
		}
		sendLoginSuccess();
		Thread.currentThread().setName("ClientThread:" + getPlayer().getName());
		sendEntityInfo(0);
	}
	
	private void handleEasyplay() throws IOException {
		synchronized (PlayerManager.getInstance()) {
			// create new username & password
			final String username = PlayerManager.getNextFreeGuestName();
			final String password = new BigInteger(64, new Random()).toString(32);

			// Call handleLogin() with the new credentials
			handleLogin(username, password);

			// send username & password to client
			sendNotice("New player created: " + username + " / " + password);
			sendEasyplay(username, password);
		}
	}

	private void sendEasyplay(final String username, final String password) {
		sendMessage(new LoginPacket(username, password));
	}

	private void sendVersion() {
		sendMessage(new ServerHello(PacketType.getProtocolVersion(), "jfealdia java server"));
	}
	
	private void sendLoginFailure(String reason) {
		sendMessage(new TextPacket(PacketType.LOGINFAILURE, reason));
	}
	
	private void sendLoginSuccess() {
		sendSimpleMessage(PacketType.LOGINSUCCESS);
		sendMusicHint(MusicHint.GENERAL);
	}

	public void sendInstanceName(String name) {
		logger.debug("Sending instance name to client: " + name);
		sendMessage(new InstanceNamePacket(name));
	}
	
	public void sendPosition(int x, int y) {
		//logger.debug("Sending position to client: x = " + x + ", y = " + y);
		sendMessage(new PlayerPositionPacket(x, y));
	}

	public Player getPlayer() {
		return player;
	}
	
	public String getPlayerName() {
		if (getPlayer() != null) {
			return getPlayer().getName();
		}
		return null;
	}

	public void setPlayer(Player player) {
		this.player = player;

		if (player != null) {
			player.addPropertyChangeListener(this);
		}
	}
	
	public void sendNotice(String text) {
		sendMessage(new TextPacket(PacketType.NOTICE, text));
	}

	public void sendNoticeOfEntityMovement(long id, int x, int y, boolean src_visible, boolean dst_visible) {
		sendMessage(new EntityMovePacket(id, x, y, src_visible, dst_visible));
	}
	
	public void sendEntityCacheClear() {
		sendSimpleMessage(PacketType.ENTITYCACHECLEAR);
	}
	
	public void sendEntityInfo(long id) throws IOException {
		if (getPlayer().getID() == id) {
			id = 0;
		}
		
		MapEntity entity = null;
		if (id == 0) {
			entity = getPlayer();
		} else {
			entity = getPlayer().getInstance().getEntityByID(id);
		}
		if (entity == null) {
			logger.warn("Not sending entity info for " + id);
			return;
		}
			
		// Any quests available?
		int overlayId = -1;
		if (entity instanceof Monster) {
			Monster m = (Monster) entity;
			String face = m.getQuestAvailabilityFor(getPlayer()).getFace();
			if (face != null) {
				overlayId = getFaceIdOrSend(face);
			}
		}
		int quantity = -1;
		if (entity instanceof Item) {
			Item item = (Item) entity;
			quantity = item.getQuantity();
		}
		String guild = "";
		Color nameColor = null;
		if (entity instanceof Player) {
			Player pl = (Player) entity;
			if (pl.isInGuild()) {
				guild = pl.getGuild().getTag();
			}
			nameColor = pl.getNameColor();
		}
		
		final int faceId = getFaceIdOrSend(entity.getVisibleFace());

		sendMessage(new EntityInfoPacket(id, entity.getEntityType(), entity instanceof Creature, (entity instanceof Item && ((Item) entity).canPickUp()), entity.getName(), faceId, overlayId, quantity, guild, nameColor));
	}
	
	public void sendTarget() {
		long id = getPlayer().getTarget() == null ? 0 : getPlayer().getTarget().getID();
		
		sendMessage(new EntityReferencePacket(PacketType.TARGET, id));
		
		// Send health too
		if (id != 0 && getPlayer().getTarget() != null) {
			sendTargetHealth(id, getPlayer().getTarget().getHealthPercentage());
		}
	}
	
	public void sendExperience() {
		sendMessage(new ExperiencePacket(getPlayer().getExperience(), GameRules.getExperienceForLevel(getPlayer().getLevel() + 1), GameRules.getExperienceForLevel(getPlayer().getLevel()), getPlayer().getLevel()));
	}
	
	public void sendHealth() {
		sendMessage(new PlayerHealthPacket(getPlayer().getHealth(), getPlayer().getMaxHealth()));
	}
	
	/**
	 * Send given items as container to client.
	 * @param id container id, or 0 for player inventory
	 */
	public void sendContainer(long id, List<Item> items) {
		List<ContainerItem> containerItems = new LinkedList<ContainerItem>();
		for (Item item : items) {
			ContainerItem containerItem = new ContainerItem(item.getID(), item.getName(), getFaceIdOrSend(item.getFace()), item.getQuantity(), item instanceof Wearable && ((Wearable) item).isEquipped());
			containerItems.add(containerItem);
		}
		sendMessage(new ContainerPacket(id, containerItems));
	}
	
	public void sendEntityEffect(long entityId, String face, int time, String text) {
		sendMessage(new EntityEffectPacket(entityId, getFaceIdOrSend(face), time, text));
	}

	public void sendTargetHealth(long id, int percentage) {
		sendMessage(new EntityHealthPacket(id, percentage));
		
	}
	
	/**
	 * Send the file to client.
	 */
	public void sendFile(String filename) {
		sendMessage(new FilePacket(filename, FileUtil.getFileContentAsByteBuffer(filename).array()));
	}
	
	public void sendAttackMode(boolean on) {
		sendMessage(new FlagPacket(PacketType.ATTACKMODE, on));
	}

	public void logout() {
		if (getPlayer() != null) {
			getPlayer().logout();
			PlayerManager.getInstance().logoutPlayer(getPlayer());
			sendLogout();
			setPlayer(null);
		}
	}
	
	public void sendLogout() {
		sendSimpleMessage(PacketType.LOGOUT);
	}
	
	public void sendChat(String who, String text, ChatType type) {
		sendMessage(new ServerChatPacket(who, type, text));
	}
	
	public void sendPartyMembers() {
		List<Player> players = getPlayer().getParty().getPlayers();
		List<Long> ids = new LinkedList<Long>();
		for (Player p: players) {
			if (p.getID() == getPlayer().getID()) {
				continue;
			}
			ids.add(p.getID());
		}
		sendMessage(new PartyMembersPacket(ids));
	}
	
	public void sendPartyMemberPos(long id, int x, int y) {
		sendMessage(new PartyMemberPositionPacket(id, x, y));
	}
	
	public void sendPartyLeft() {
		sendSimpleMessage(PacketType.PARTYLEFT);
	}
	
	public void sendPartyMemberPosClear(long id) {
		sendMessage(new EntityReferencePacket(PacketType.PARTYMEMBERPOSCLEAR, id));
	}
	
	public void sendContainerClosed(long id) {
		sendMessage(new EntityReferencePacket(PacketType.CONTAINERCLOSED, id));
	}
	
	/**
	 * Check whether this player has been saved recently, if not, save.
	 */
	protected void checkPeriodicalSave() {
		long lastSaveTime = getPlayer().getLastSaveTime();
		long savePeriod = ServerConfig.getInstance().getPropertyAsInt("player.save_period");
		long now = System.currentTimeMillis() / 1000;
		
		if (now > lastSaveTime + savePeriod) {
			logger.trace("Autosaving player");
			PlayerManager.saveOrLogError(getPlayer());
		}
	}
	
	public void sendPartyMarker(String map, int x, int y) {
		if (map != null) {
			sendMessage(new PartyMarkerPacket(map, x, y));
		}
	}

	public String getJavaVersion() {
		return javaVersion;
	}

	public void setJavaVersion(String javaVersion) {
		this.javaVersion = javaVersion;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public void sendPopup(String text) {
		sendMessage(new PopupPacket(text));
	}
	
	public Server getServer() {
		return server;
	}
	
	public void sendMaster() {
		sendMessage(new FlagPacket(PacketType.MASTER, getPlayer().isMaster()));
	}

	/**
	 * Send quest offer to client, showing the quest accept/reject window.
	 */
	public void sendQuestOffer(int questId, String fromWho) {
		Quest quest = QuestManager.getInstance().getQuest(questId);
		
		sendMessage(new QuestOfferPacket(fromWho, new ProtoQuest(questId, quest.getTitle(), quest.getFullDescription())));
	}

	public void sendQuestLog() {
		List<ProtoQuest> quests = new LinkedList<ProtoQuest>();
		for (QuestInstance qi : getPlayer().getQuestLog().getActiveQuests()) {
			Quest q = qi.getQuest();
			quests.add(new ProtoQuest(qi.getQuestId(), q.getTitle(), q.getDescription()));
		}
		sendMessage(new QuestLogPacket(quests));
	}

	private int getFaceIdOrSend(final String faceName) {
		if (faceMapper.getFaceByName(faceName) != null) {
			return faceMapper.getFaceByName(faceName).id;
		}

		// add a new mapping
		long crc32;
		if (faceName.startsWith(Animation.PREFIX)) {
			crc32 = 0;
			explodeAndSendAnimationFaceIds(faceName.substring(Animation.PREFIX.length()));
		}
		else {
			try {
				crc32 = FileUtils.checksumCRC32(new File(ImageMapper.getInstance().getImageFilename(faceName)));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		int faceId = nextFaceId++;
		FaceInfo faceInfo = new FaceInfo(faceId, faceName, crc32);
		faceMapper.addFaceMapping(faceInfo);

		sendMessage(new FaceInfoPacket(faceInfo));
		return faceId;
	}

	/**
	 * Make sure all animation faceIds have been sent to client.
	 */
	private void explodeAndSendAnimationFaceIds(final String animationName) {
		Animation animation = Animations.getInstance().getAnimation(animationName);
		for (String faceName : animation.getUniqueFrames()) {
			getFaceIdOrSend(faceName);
		}
	}

	private void sendFace(int faceId) throws IOException {
		String faceName = faceMapper.getFaceById(faceId).name;

		// Guess the filename, and send the file
		ImageMapper im = ImageMapper.getInstance();
		String filename = im.getImageDir() + "/" + faceName + ".png";
		sendMessage(new FacePacket(faceId, FileUtils.readFileToByteArray(new File(filename))));
	}

	public void sendMusicHint(MusicHint musicHint) {
		sendMessage(new MusicHintPacket(musicHint));
	}

	public void propertyChange(PropertyChangeEvent evt) {
		WatchableProperty property = WatchableProperty.valueOf(evt.getPropertyName());
		
		switch (property) {
			case HEALTH:
				sendHealth();
				break;
			case MAXHEALTH:
				sendHealth();
				break;
			case TARGET:
				sendTarget();
				break;
			default:
				logger.debug("Unhandled propertychange " + property);
				break;
		}
	}
}
