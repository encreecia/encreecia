package org.fealdia.orpg.server.commands;

/**
 * Enum for some simple commands.
 */
public enum Command {
	WHO(0, null, false),
	ABANDONQUEST(1, "INT", false),
	ACCEPTQUEST(1, "INT", false),
	ATTACKMODE(0, null, false),
	CAST(0, null, false),
	EMOTES(0, null, false),
	INSPECT(0, null, false),
	PLAYED(0, null, false),
	PVP(0, null, false),
	HELP(0, null, false),
	LOGOUT(0, null, false),
	PLAYERS(0, null, false),
	REJECTQUEST(1, "INT", false),
	SAVE(0, null, false),
	TARGETTARGET(0, null, false),
	WHERE(0, null, false),
	QUESTLOG(0, null, false),
	DM(1, null, false),
	
	// DM commands
	BANDWIDTH(0, null, true),
	CLEARQUESTLOG(0, null, true),
	CREATE(1, null, true),
	ENTITIES(0, null, true),
	DUMP(1, "ID", true),
	DUMPX(1, "ID", true),
	GOTO(3, "MAP INT INT", true),
	KICK(1, "PLAYER", true),
	KILL(1, "ID", true),
	MAPLIST(0, null, true),
	NODM(0, null, true),
	PATCH(3, "STRING STRING STRING", true),
	POPUP(2, "PLAYER STRING", true),
	POSSESS(1, "ID", true),
	RELOAD(0, null, true),
	RESPAWN(0, null, true),
	RESYNC(0, null, true),
	SERVERSTATUS(0, null, true),
	SUMMON(1, "PLAYER", true),
	TELEPORT(1, "PLAYER", true),
	UNLOADALL(0, null, true),
	UNPOSSESS(0, null, true),
	;
	
	private int minArgs;
	private String paramSpec;
	private boolean needDM;

	Command(int min_args, String param_spec, boolean need_dm) {
		this.minArgs = min_args;
		this.paramSpec = param_spec;
		this.needDM = need_dm;
	}
	
	public int getMinArgs() {
		return minArgs;
	}
	
	public String getParamSpec() {
		return paramSpec;
	}
	
	public boolean isDMCommand() {
		return needDM;
	}
	
	/*
	public boolean acceptsCommand(String command, String raw_line, String[] args, Player player) {
		return (this.command.equals(command) && args.length >= this.min_args);
	}
	*/
}
