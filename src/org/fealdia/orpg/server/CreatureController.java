package org.fealdia.orpg.server;

import org.fealdia.orpg.server.maps.entities.Creature;

/**
 * Interface for controlling creatures (monsters & players).
 *
 * The main idea is to separate controlling logic to allow easier possession / controlling of other creatures.
 */
public abstract class CreatureController {
	private Creature target;
	
	public boolean doMovement(Creature creature) {
		return false;
	}

	public Creature getTarget() {
		return target;
	}

	public void setTarget(Creature target) {
		this.target = target;
	}
	
}
