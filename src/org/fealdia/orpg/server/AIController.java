package org.fealdia.orpg.server;

import java.util.Random;

import org.fealdia.orpg.server.maps.entities.Creature;
import org.fealdia.orpg.server.maps.entities.Monster;
import org.fealdia.orpg.server.maps.entities.Monster.MovementOrders;

/**
 * Controller for Monsters.
 */
public class AIController extends CreatureController {

	@Override
	public boolean doMovement(Creature c) {
		Monster m = (Monster) c;
		
		if (m.getMovementOrders() == MovementOrders.WANDER) {
			// If we are too far from spawn location, move towards it
			if (m.distanceTo(m.getSpawnX(), m.getSpawnY()) > m.getWanderRadius()) {
				return m.moveTowards(m.getSpawnX(), m.getSpawnY());
			}

			// Otherwise just wander
			return m.doMovementWander();
		}

		if (m.getTarget() != null) {
			Creature en = m.getTarget();

			// If we can already attack the target, don't move.
			if (m.canAttack(en)) {
				return false;
			}

			// If we can still see the target, move towards it, otherwise clear target
			if (m.canSee(en)) {
				boolean couldMove = m.moveTowards(en.getX(), en.getY());
				if (!couldMove) {
					int dx = Math.abs(en.getX() - m.getX());
					int dy = Math.abs(en.getY() - m.getY());
					
					// If we are not trying to move diagonally, we can try moving sideways to the target
					
					// Randomize direction where to move sideways
					int r = new Random().nextInt(2); // 0..1
					if (r == 0) { r = -1; }
					
					if ((dx > 0 && dy == 0)) {
						if (!m.moveTowards(m.getX(), m.getY()+r)) {
							m.moveTowards(m.getX(), m.getY()-r);
						}
					}
					else if ((dx == 0 && dy > 0)) {
						if (!m.moveTowards(m.getX()+r, m.getY())) {
							m.moveTowards(m.getX()-r, m.getY());
						}
					} else {
						m.targetClosestEnemy();
					}
				}
				return couldMove;
			} else {
				m.setTarget(0);
			}
		}

		// Guards (STAND) should return to their post after losing target
		if (m.getMovementOrders() == MovementOrders.STAND && m.getTarget() == null) {
			// Move towards our spawn, if not already there
			if (m.getSpawnX() != m.getX() || m.getSpawnY() != m.getY()) {
				return m.moveTowards(m.getSpawnX(), m.getSpawnY());
			}
		}
		
		return false;
	}

}
