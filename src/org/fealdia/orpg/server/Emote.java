package org.fealdia.orpg.server;

/**
 * Emotes and their textual presentations.
 */
public enum Emote {
	AGREE ("agrees", "agrees with WHO"),
	BEG ("begs everyone in an unsightly manner", "begs WHO in an unsightly manner"),
	BLUSH ("becomes flushed", "blushes and looks down avoiding eye contact with WHO"),
	BORED ("looks bored. Oh the drudgery!", "becomes bored and looks at WHO, prompting ideas on what to do"),
	BOW ("bows down", "bows down in front of WHO"),
	BYE ("waves farewell to everyone", "waves farewell to WHO"),
	CHARGE ("tells everyone to charge", "tells everyone to charge at WHO"),
	CHEER ("gives a shout of approval", "shouts approvingly at WHO"),
	CLAP ("claps hands excitedly", "claps hands approvingly at WHO"),
	CRY ("sheds a tear", "cries on WHO's shoulder"),
	CURIOUS ("is curious about something", "is curious about WHO"),
	DISAGREE ("disagrees", "disagrees with WHO"),
	GRIN ("grins evilly", "gives a wicked grin at WHO"),
	NOD ("nods solemnly", "nods solemnly to WHO"),
	POKE ("wants to poke something, but fails to find a target", "pokes WHO - HEY!"),
	READY ("is ready", "gestures a ready signal to WHO"),
	SHAKE ("shakes hand with invisible someone", "shakes WHO's hand"),
	SHRUG ("shrugs with a puzzled look", "shrugs at WHO"),
	SMILE ("smiles wryly", "smiles warmly at WHO"),
	;
	
	private final String no_target;
	private final String target;
	
	Emote(String no_target, String target) {
		this.no_target = no_target;
		this.target = target;
	}
	
	public static Emote getEmote(String what) {
		try {
			return Emote.valueOf(what.toUpperCase());
		}
		catch (IllegalArgumentException e) {
			return null;
		}
	}
	
	public static String getEmoteList() {
		StringBuffer res = new StringBuffer();
		boolean first = true;
		for (Emote e : Emote.values()) {
			if (!first) {
				res.append(", ");
			} else {
				first = false;
			}
			res.append(e.toString().toLowerCase());
		}
		return res.toString();
	}
	
	public String withoutTarget() {
		return no_target;
	}
	
	public String withTarget(String who) {
		return target.replaceAll("WHO", who);
	}
}
