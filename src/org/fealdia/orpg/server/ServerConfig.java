package org.fealdia.orpg.server;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Server configuration values.
 */
public final class ServerConfig {
	private static ServerConfig instance = null;
	private final static Logger logger = Logger.getLogger(ServerConfig.class);
	
	private Properties props = null;
	
	public static ServerConfig getInstance() {
		if (instance == null) {
			instance = new ServerConfig();
		}
		return instance;
	}
	
	private ServerConfig() {
		try {
			props = new Properties();
			props.load(getClass().getResourceAsStream("/server.properties"));
			logger.debug("Loaded");
		} catch (IOException e) {
			logger.error("IOException while creating ServerConfig", e);
		}
	}

	public String getProperty(String key) {
		return props.getProperty(key);
	}
	
	public int getPropertyAsInt(String key) {
		return Integer.parseInt(getProperty(key));
	}

}
