package org.fealdia.orpg.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.FileUtil;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.SaveHelper;
import org.fealdia.orpg.common.util.SaveHelper.Saveable;
import org.fealdia.orpg.common.util.Sha1Util;
import org.fealdia.orpg.common.xml.XMLUtil;
import org.fealdia.orpg.server.maps.entities.persistence.XMLEntityWriter;
import org.fealdia.orpg.server.maps.entities.persistence.XmlReaders;
import org.fealdia.orpg.server.quests.QuestLog;

/**
 * Responsible for Player managing duties. Keeps track of what players there are in various instances etc.
 * 
 * Manages login/logout, loading/saving, player notices etc.
 */
public class PlayerManager {
	private static PlayerManager instance = null;
	private static Logger logger = Logger.getLogger(PlayerManager.class);
	private static Logger playerLogger = Logger.getLogger("player");
	
	private List<Player> players = Collections.synchronizedList(new LinkedList<Player>());
	
	private static final String PLAYERDIR = PathConfig.getServerDataDir() + "players" + File.separatorChar;
	
	public static PlayerManager getInstance() {
		if (instance == null) {
			instance = new PlayerManager();
		}
		return instance;
	}

	@SuppressWarnings("serial")
	public class LoginException extends Exception {
		public LoginException(String message) {
			super(message);
		}
	}

	/**
	 * Check whether the provided player name and password are correct.
	 */
	public static boolean isValidLogin(String player, String password) {
		if (!isPlayerRegistered(player)) {
			return false;
		}
		String filename = PLAYERDIR + player + "/password";
		String passInFile = FileUtil.getFirstLineAsString(filename);
		if (Sha1Util.getSha1ForString(password).equals(passInFile)) {
			return true;
		}
		
		return false;
	}

	public static boolean isValidName(String player) {
		return player.matches("[A-Za-z][A-Za-z0-9]{1,16}");
	}

	/**
	 * Check whether a player by the name is online. 
	 */
	public boolean isPlayerLogged(String playerName) {
		synchronized (players) {
			for (Player p : players) {
				if (playerName.equals(p.getName())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Check whether the given player exists.
	 */
	public static boolean isPlayerRegistered(String playerName) {
		String filename = PLAYERDIR + playerName + "/password";
		File f = new File(filename);
		return f.exists();
	}

	/**
	 * Handle a login request from client. Synchronized because may also register a player.
	 */
	public synchronized Player handleLogin(final String username, final String password, ClientThread clientThread) throws LoginException {
		// Allow only one client logging in at once
		if (username == null || password == null) {
			throw new LoginException("Empty username or password");
		}

		// Does the player exist?
		if (!isPlayerRegistered(username)) {
			// No, create it
			if (!isValidName(username)) {
				throw new LoginException("Invalid player name.");
			}

			if (!setPlayerPassword(username, password)) {
				throw new LoginException("Failed to create new player");
			}
		}

		// Yes - validate username/password
		boolean valid = isValidLogin(username, password);
		if (!valid) {
			throw new LoginException("Invalid login");
		}

		// Check that the player is not logged in already
		boolean logged = isPlayerLogged(username);
		if (logged) {
			throw new LoginException("Player already logged in");
		}

		// Check that we don't already have maximum number of players allowed.
		int player_limit = getMaxPlayers();
		if (player_limit > 0 && countPlayers() >= player_limit) {
			throw new LoginException("Server is full (" + countPlayers() + " players online)");
		}

		try {
			return loginPlayer(username, clientThread);
		} catch (Exception e) {
			logger.warn("Exception while logging player in", e);
			throw new LoginException("Exception while logging player in: " + e.getMessage());
		}
	}
	
	private synchronized Player loginPlayer(String name, ClientThread ct) throws Exception {
		Player p = loadFromFile(name, ct);
		if (p == null) {
			p = createNewPlayer(name, ct);
		}
		p.setName(name);
		p.setClientThread(ct);
		ct.setPlayer(p);
		players.add(p);

		ct.sendInstanceName(p.getInstance().getName());
		ct.sendPosition(p.getX(), p.getY());
		p.onInventoryChanged();
		p.resyncVisibleEntities();
		p.setAttackMode(true);
		ct.sendExperience();
		ct.sendHealth();
		
		sendGlobalNotice("Player " + name + " logged in, " + countPlayers() + " players online");
		logger.info("Player '" + name + "' logged in from IP " + p.getRemoteIP());
		playerLogger.info("LOGIN " + name + " ip=" + p.getRemoteIP() + " java=" + ct.getJavaVersion() + " os=" + ct.getOsName() + " os_version=" + ct.getOsVersion());
		
		return p;
	}
	
	public Player createNewPlayer(String name, ClientThread ct) {
		// Create the new player
		Player p = new Player();
		players.add(p);
		p.setName(name);
		p.setClientThread(ct);
		ct.setPlayer(p);

		// No savefile, set some defaults
		logger.info("New player '" + name + "' created");

		p.respawnAtStartingSpawn();

		p.setExperience(0); // set health etc
		p.setHealth(p.getMaxHealth());
		p.setFullyLoaded(true);

		return p;
	}

	public void logoutPlayer(Player p) {
		// Save the player
		saveOrLogError(p);
		
		p.moveOutOfWorld();
		
		players.remove(p);
		
		logger.info("Player '" + p.getName() + "' logged out, " + countPlayers() + " players online");
		playerLogger.info("LOGOUT " + p.getName() + " played=" + p.getLoggedInTime() + " playedtotal=" + p.getPlayedTotal() + " level=" + p.getLevel() + " exp=" + p.getExperience());
		sendGlobalNotice("Player " + p.getName() + " logged out");
	}
	
	public static String getPlayerDirectory(String playerName) {
		return PLAYERDIR + playerName + File.separatorChar;
	}

	public static String getPlayerSaveFileName(String playerName) {
		return getPlayerDirectory(playerName) + playerName;
	}

	public static Player loadFromFile(String name, ClientThread ct) throws Exception {
		logger.trace("Trying to load player '" + name + "' from file");

		Player p;
		try {
			p = loadFromFileXml(name);
		} catch (FileNotFoundException e) {
			logger.trace("XML savefile not found, trying TXT");
			try {
				p = loadFromFileOld(name, ct);
			} catch (FileNotFoundException e2) {
				logger.trace("No savefile found");
				return null;
			}
		}

		logger.trace("Player '" + name + "' successfully loaded from file");
		return p;
	}

	public static Player loadFromFileOld(String name, ClientThread ct) throws IOException {
		Player p = new Player();
		p.setClientThread(ct);
		ct.setPlayer(p);
		p.setName(name);

		final String filename = getPlayerDirectory(p.getName()) + p.getName();
		BufferedReader in = new BufferedReader(new FileReader(filename));
		p.setFullyLoaded(false);
		try {
			p.loadFromStream(in, false);
		}
		finally {
			in.close();
		}
		
		loadPlayerQuestLog(p);

		p.setFullyLoaded(true);
		return p;
	}

	public static Player loadFromFileXml(String playerName) throws Exception {
		final String filename = getPlayerSaveFileName(playerName) + ".xml";

		XMLStreamReader streamReader = XMLInputFactory.newInstance().createXMLStreamReader(new FileInputStream(filename));
		Player player = XmlReaders.processPlayer(streamReader);
		streamReader.close();

		loadPlayerQuestLog(player);

		player.moveToInstanceAfterLoading();

		player.setFullyLoaded(true);

		return player;
	}

	public static void loadPlayerQuestLog(Player p) {
		// QuestLog
		try {
			QuestLog questLog = XMLUtil.fromXML(QuestLog.class, PLAYERDIR + p.getName() + File.separatorChar + QuestLog.FILENAME);
			p.setQuestLog(questLog);
		} catch (FileNotFoundException e) {
			logger.trace("Questlog not found");
		} catch (JAXBException e) {
			logger.error("Failed to load questlog", e);
			throw new RuntimeException("Failed to load questlog", e);
		}
	}

	public static void saveOrLogError(final Player p) {
		if (!p.isFullyLoaded()) {
			logger.error("Refusing to save player '" + p.getName() + "' because it was not fully loaded.");
			return;
		}

		createPlayerDir(p.getName());

		try {
			saveToFile(p);
		} catch (Exception e) {
			logger.error("Failed to save player " + p.getName() + " to file");
		}
		try {
			saveToFileXml(p);
		} catch (Exception e) {
			logger.error("Failed to save player " + p.getName() + " to XML file");
		}

		saveQuestLog(p);
	}

	/**
	 * Save player to data/players/<name>/<name>.
	 * 
	 * @note This should only be called from ClientThread of the owning player.
	 */
	private static void saveToFile(final Player p) throws Exception {
		logger.debug("Saving player '" + p.getName() + "' to file");
		
		final String filename = getPlayerSaveFileName(p.getName());
		
		SaveHelper.saveToFileWithBackup(new Saveable() {
			public void saveToFile(String filename) throws Exception {
				Writer out = new BufferedWriter(new FileWriter(filename));
				try {
					p.saveToStream(out);
				} finally {
					out.close();
				}

			}

		}, filename);
	}
	
	private static void saveToFileXml(final Player p) throws Exception {
		logger.debug("Saving player '" + p.getName() + "' to XML file");

		final String filename = getPlayerSaveFileName(p.getName()) + ".xml";

		SaveHelper.saveToFileWithBackup(new Saveable() {
			public void saveToFile(String filename) throws Exception {
				XMLEntityWriter xmlEntityWriter = new XMLEntityWriter(new FileOutputStream(filename));
				try {
					xmlEntityWriter.writeEntity("player", p);
				} finally {
					xmlEntityWriter.close();
				}
			}
		}, filename);
	}

	public static boolean saveQuestLog(Player p) {
		if (!p.getQuestLog().isNeedingSave()) {
			return true;
		}
		final String questlogfilename = getPlayerDirectory(p.getName()) + QuestLog.FILENAME;
		final String questlogtempfile = questlogfilename + ".tmp";
		if (!p.getQuestLog().saveToFile(questlogtempfile)) {
			return false;
		}

		FileUtil.renameAsBackup(questlogfilename);
		if (new File(questlogtempfile).renameTo(new File(questlogfilename))) {
			return false;
		}

		return true;
	}

	public static void createPlayerDir(String playerName) {
		new File(getPlayerDirectory(playerName)).mkdirs();
	}
	
	/**
	 * Sets player's password, effectively registering the player if it doesn't exist. 
	 */
	public static synchronized boolean setPlayerPassword(String playerName, String password) {
		createPlayerDir(playerName);
		String filename = PLAYERDIR + playerName + "/password";
		try {
			Writer out = new BufferedWriter(new FileWriter(filename));
			out.write(Sha1Util.getSha1ForString(password));
			out.close();
		} catch (IOException e) {
			logger.error("Failed to set password for player " + playerName, e);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Send a "server notice" text for all clients
	 */
	public void sendGlobalNotice(String text) {
		for (Player p : players) {
			p.getClientThread().sendNotice(text);
		}
	}

	public List<Player> getPlayers() {
		return players;
	}
	
	public int countPlayers() {
		return players.size();
	}
	
	/**
	 * Count all players, online or not.
	 */
	public int countRegisteredPlayers() {
		File dir = new File(PLAYERDIR);
		String[] entries = dir.list();
		return entries.length;
	}
	
	/**
	 * Get player by name, or null if not found.
	 */
	public Player getPlayerByName(String name) {
		synchronized (getPlayers()) {
			for (Player pl : getPlayers()) {
				if (pl.getName().equals(name)) {
					return pl;
				}
			}
		}
		return null;
	}

	public Player getPlayerById(Long id) {
		synchronized (getPlayers()) {
			for (Player pl : getPlayers()) {
				if (pl.getID() == id) {
					return pl;
				}
			}
		}
		return null;
	}

	public int getMaxPlayers() {
		return ServerConfig.getInstance().getPropertyAsInt("server.max_players");
	}

	/**
	 * Get an array of all registered player names.
	 */
	public String[] getAllPlayerNames() {
		File dir = new File(PLAYERDIR);
		String[] names = dir.list();
		Arrays.sort(names);
		return names;
	}
	
	public long getLastLoginFor(String player_name) {
		File file = new File(PLAYERDIR + player_name + File.separator + player_name);
		return file.lastModified();
	}

	public static String getNextFreeGuestName() {
		// find initial free name by multiplying base with *2 - log(n)
		final String BASENAME = "guest";
		int firstFree = 1;
		while (isPlayerRegistered(BASENAME + firstFree)) {
			firstFree *= 2;
		}

		// now find the first free name - modified binary search
		int start = 1;
		int end = firstFree;
		while (start <= end) {
			int mid = (start + end) / 2;
			if (isPlayerRegistered(BASENAME + mid)) {
				start = mid + 1;
			}
			else {
				end = mid - 1;
			}
		}

		return BASENAME + (end + 1);
	}
}
