package org.fealdia.orpg.server;

public final class GameRules {
	static final int[] EXPERIENCE_TABLE = { 0, 100, 300, 500, 1000, 2000, 4000, 8000, 14000, 20000 /* 10 */ };
	public static final int COMBAT_BUFF_DURATION = 5; // secs

	/**
	 * Get chance to hit the given creature, 0-100.
	 */
	public static int getChanceToHit(int our_level, int target_level) {
		int chance = 0;
		
		int diff = our_level - target_level;
		if (diff < 0) {
			// For higher level targets, chance is 20% - 80% with -10% / level
			chance = Math.max(20, 80 + (diff * 10));
		}
		else {
			// For lower level targets, chance is 80% - 95% with +5% / level
			chance = Math.min(95, 80 + (diff * 5));
		}
		
		return chance;
	}
	
	/**
	 * Health calculation for Player.
	 */
	public static int getHealthForLevel(int level) {
		return 30 + 10 * (level - 1);
	}
	
	/**
	 * Damage Reduction % calculation for given AP and enemy level.
	 *
	 * @return 0-100
	 */
	public static int getDamageReduction(int armor_points, int enemy_level) {
		return (100 * armor_points) / (armor_points + 6 + 6 * enemy_level);
	}

	/**
	 * Get experience needed for the given level.
	 */
	public static long getExperienceForLevel(int level) {
		if (level > getHighestLevel()) {
			level = getHighestLevel();
		}
		return EXPERIENCE_TABLE[level - 1];
	}

	public static int getLevelFromExperience(long experience) {
		for (int i = 1; i < EXPERIENCE_TABLE.length; i++) {
			if (EXPERIENCE_TABLE[i] > experience) {
				return i;
			}
		}
		
		return EXPERIENCE_TABLE.length;
	}
	
	public static int getHighestLevel() {
		return EXPERIENCE_TABLE.length;
	}
}
