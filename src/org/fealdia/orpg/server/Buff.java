package org.fealdia.orpg.server;

/**
 * Buffs are temporary status effects for a Creature.
 *
 * TODO:
 * - buffsource = { SPELL, DM, ITEM }
 * - String src_player
 * - boolean expires_on_death
 * - boolean visible
 * - allow saving to & loading from stream
 * - buffs that are removed on logout (DM)
 */
public class Buff {
	public enum BuffDuration {
		AURA, // permanent until removed by something
		GAMEDURATION, // wears out when logged in long enough
		REALDURATION, // wears out whether or not logged in
	}
	
	private BuffType buffType;
	private long created = System.currentTimeMillis();
	private int duration_secs = 0;
	
	public Buff(BuffType type, int duration_secs) {
		this.buffType = type;
		this.duration_secs = duration_secs;
	}
	
	public BuffType getBuffType() {
		return buffType;
	}
	
	public int getDurationLeft() {
		long now = System.currentTimeMillis();
		
		return Math.max(0, duration_secs - (int)(now - created));
	}
	
	public boolean hasExpired() {
		long now = System.currentTimeMillis();
		return (now > created + duration_secs * 1000);
	}
	
	public void setDuration(int duration_secs) {
		this.duration_secs = duration_secs;
		created = System.currentTimeMillis();
	}
	
	@Override
	public String toString() {
		return getBuffType().name() + " [" + getDurationLeft() + "s]";
	}

}
