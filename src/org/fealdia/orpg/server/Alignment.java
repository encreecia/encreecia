package org.fealdia.orpg.server;

import java.awt.Color;

public enum Alignment {
	GOOD(80000),
	NEUTRAL(0),
	EVIL(-80000);

	private final int border;

	public final static int MAX_ALIGNMENT = 100000;
	public final static int GOOD_KILL_NPC = 250; // 20 mobs = 1 PK -> 320 mobs from neutral -> good
	public final static int BAD_KILL_NPC = -2500; // 32 (40) npc kills from neutral -> evil
	public final static int BAD_KILL_PVP = -5000; // 16 (20) pvp kills from neutral -> evil

	Alignment(int limit) {
		this.border = limit;
	}

	public int getLimit() {
		return border;
	}

	public static Color getColorFor(int value) {
		if (value < NEUTRAL.getLimit()) {
			int pct = (int) (((((float) value)) / EVIL.getLimit()) * 255);
			return new Color(255, 255 - pct, 0);
		}
		int pct = (int) (((((float) value)) / GOOD.getLimit()) * 255);
		return new Color(255 - pct, 255, 0);
	}
}
