package org.fealdia.orpg.server;

public class ServerMain {

	public static void main(String[] args) {
		System.setProperty("log4j.configuration", "log4j-server.properties");

		Server server = new Server();
		
		for (int i = 0; i < args.length; i++) {
			if ("--help".equals(args[i]) || "-h".equals(args[i])) {
				System.out.println("Options:\n-p <port>\tListen on <port>\n\n");
				System.exit(1);
			}
			else if ("--port".equals(args[i]) || "-p".equals(args[i])) {
				server.setPort(Integer.parseInt(args[i+1]));
				i++;
			}
		}
		
		server.run();
	}

}
