package org.fealdia.orpg.server;

import org.fealdia.orpg.common.util.StringUtil;

/**
 * @see Buff
 */
public enum BuffType {
	//DM ("Dungeon Master"),
	CRIMINAL,
	COMBAT(null, true),
	//INVISIBILITY,
	HASTE,
	SLOW,
	/*
	SEEINVISIBLE,
	POISONED,
	*/
	;
	
	private String name = null;
	private boolean hidden = false;
	
	BuffType() {}
	
	BuffType(String name, boolean hidden) {
		this.name = name;
		this.hidden = hidden;
	}
	
	public String getName() {
		if (name == null) {
			return StringUtil.capitalize(toString());
		}
		return name;
	}

	public boolean isHidden() {
		return hidden;
	}
}
