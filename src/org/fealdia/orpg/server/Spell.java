package org.fealdia.orpg.server;

import org.fealdia.orpg.common.util.StringUtil;

/**
 * List of spells.
 * TODO constructor with name, points required, level
 */
public enum Spell {
	HASTE,
	
	// Offensive spells
	SLOW (null, true),
	;
	
	private boolean offensive = false;
	private String name = null;
	
	Spell() {}
	
	Spell(String name, boolean offensive) {
		this.name = name;
		this.offensive = offensive;
	}
	
	public String getName() {
		if (name != null) {
			return name;
		}
		return StringUtil.capitalize(toString());
	}
	
	public boolean isOffensive() {
		return offensive;
	}
}
