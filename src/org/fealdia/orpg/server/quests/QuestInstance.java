package org.fealdia.orpg.server.quests;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.Logger;

@XmlRootElement(name = "questInstance")
public class QuestInstance {
	private static Logger logger = Logger.getLogger(QuestInstance.class);
	private Quest quest;
	@XmlElementWrapper
	@XmlElementRef
	private List<QuestProgress> progresses = new LinkedList<QuestProgress>();
	@XmlElement
	private long createdStamp = System.currentTimeMillis();
	
	public QuestInstance() {}

	public QuestInstance(Quest quest) {
		this.quest = quest;
		createProgresses();
	}

	private void createProgresses() {
		for (QuestObjective questObjective : quest.getObjectives()) {
			progresses.add(questObjective.createQuestProgress());
		}
	}
	
	@XmlTransient
	public Quest getQuest() {
		return quest;
	}
	
	public boolean isFinished() {
		// finished if each QuestObjective / QuestProgress is finished
		int index = 0;
		for (QuestObjective questObjective : quest.getObjectives()) {
			if (!questObjective.isCompleted(progresses.get(index))) {
				return false;
			}
			index++;
		}
		return true;
	}
	
	public void setQuest(Quest quest) {
		this.quest = quest;
		createProgresses();
	}

	@XmlElement
	public int getQuestId() {
		return quest.getId();
	}

	public long getCreatedStamp() {
		return createdStamp;
	}

	public boolean onKill(final String archetype) {
		int index = 0;
		boolean changed = false;
		for (QuestObjective questObjective : quest.getObjectives()) {
			if (questObjective instanceof KillObjective) {
				KillObjective killObjective = (KillObjective) questObjective;
				if (killObjective.getArchetype().equals(archetype)) {
					logger.debug("Incrementing progress of quest " + quest.getId() + " objective " + index);
					killObjective.incrementQuestProgress(progresses.get(index));
					changed = true;
				}
			}
			index++;
		}
		return changed;
	}

	// for JAXB
	@SuppressWarnings("unused")
	private void setQuestId(final int questId) {
		Quest quest = QuestManager.getInstance().getQuest(questId);
		this.quest = quest;
	}
}
