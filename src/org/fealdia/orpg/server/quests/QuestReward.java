package org.fealdia.orpg.server.quests;

import javax.xml.bind.annotation.XmlSeeAlso;

import org.fealdia.orpg.server.Player;

@XmlSeeAlso(ExperienceReward.class)
public abstract class QuestReward {
	public abstract void giveRewardTo(Player player);
}
