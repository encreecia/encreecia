package org.fealdia.orpg.server.quests;

/**
 * Cue to show for quest givers. Most important last. 
 */
public enum QuestAvailability {
	NONE,
	STARTED("misc/question_gray"), // Quests are in progress, gray "?"
	AVAILABLE("misc/exclamation_yellow"), // Quests are available to be started, yellow "!"
	RETURNABLE("misc/question_yellow"), // Quests can be returned, yellow "?"
	// Could add "repeatable" quest markers too
	;
	private final String face;
	
	private QuestAvailability() {
		face = null;
	}
	
	private QuestAvailability(String face) {
		this.face = face;
	}

	public String getFace() {
		return face;
	}
}