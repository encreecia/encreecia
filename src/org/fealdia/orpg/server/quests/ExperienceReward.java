package org.fealdia.orpg.server.quests;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.fealdia.orpg.server.Player;

@XmlRootElement(name = "experience")
public class ExperienceReward extends QuestReward {
	@XmlAttribute
	private long amount = 0;

	@Override
	public void giveRewardTo(Player player) {
		player.addExperience(amount, false);
	}

	@Override
	public String toString() {
		return "" + amount + " experience";
	}
}
