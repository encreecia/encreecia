package org.fealdia.orpg.server.quests;

import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Base class for QuestObjective progress tracking.
 */
@XmlSeeAlso(CountableQuestProgress.class)
public abstract class QuestProgress {

}
