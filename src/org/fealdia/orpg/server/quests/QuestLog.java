package org.fealdia.orpg.server.quests;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.xml.XMLUtil;

/**
 * Quest log of a Player.
 * 
 * Saved into data/players/<player>/questlog.xml
 */
@XmlRootElement
public class QuestLog {
	private static Logger logger = Logger.getLogger(QuestLog.class);
	public static final String FILENAME = "questlog.xml";

	@XmlElementWrapper(name="active")
	@XmlElementRef
	private List<QuestInstance> activeQuests = new LinkedList<QuestInstance>();
	@XmlElementWrapper(name="finished")
	@XmlElementRef
	private List<QuestInstance> finishedQuests = new LinkedList<QuestInstance>();
	@XmlTransient
	private boolean needSave = false; // Whether questlog has been changed since loading

	public boolean abandonQuest(int questId) {
		QuestInstance qi = getQuestInstance(questId);
		return activeQuests.remove(qi);
	}

	public boolean finishQuest(Quest quest) {
		QuestInstance qi = getQuestInstance(quest);
		if (!activeQuests.remove(qi)) {
			return false;
		}
		finishedQuests.add(qi);
		needSave = true;
		return true;
	}
	
	public QuestInstance getQuestInstance(int questId) {
		for (QuestInstance q : activeQuests) {
			if (q.getQuestId() == questId) {
				return q;
			}
		}
		return null;
	}

	public QuestInstance getQuestInstance(Quest quest) {
		for (QuestInstance qi : activeQuests) {
			if (qi.getQuest() == quest) {
				return qi;
			}
		}
		for (QuestInstance qi : finishedQuests) {
			if (qi.getQuest() == quest) {
				return qi;
			}
		}
		return null;
	}

	public QuestAvailability getQuestStatus(int questId) {
		Quest quest = QuestManager.getInstance().getQuest(questId);
		
		QuestInstance qi = getQuestInstance(quest);
		if (qi != null) {
			if (finishedQuests.contains(qi)) {
				return QuestAvailability.NONE;
			}
			// if all objectives have been done -> RETURNABLE
			if (qi.isFinished()) {
				return QuestAvailability.RETURNABLE;
			}
			return QuestAvailability.STARTED;
		}

		// Quest is available, now we have to check dependencies
		for (Integer dep : quest.getDependencies()) {
			boolean ok = false;
			for (QuestInstance questInstance : finishedQuests) {
				if (questInstance.getQuestId() == dep) {
					ok = true;
					break;
				}
			}
			if (!ok) {
				return QuestAvailability.NONE;
			}
		}
		return QuestAvailability.AVAILABLE;
	}
	
	public boolean isNewQuest(Quest quest) {
		return getQuestInstance(quest) == null;
	}

	public void startQuest(Quest quest) {
		activeQuests.add(new QuestInstance(quest));
		needSave = true;
	}

	/**
	 * Called when player kills a Monster of type archetype.
	 */
	public void onKill(final String archetype) {
		for (QuestInstance qi : activeQuests) {
			if (!qi.isFinished()) {
				if (qi.onKill(archetype)) {
					needSave = true;
				}
			}
		}
	}

	public boolean isNeedingSave() {
		return needSave;
	}

	public boolean saveToFile(final String filename) {
		logger.debug("Saving quest log to " + filename);
		try {
			FileOutputStream out = new FileOutputStream(filename);
			XMLUtil.toXML(this, out);
			out.close();
		} catch (FileNotFoundException e) {
			return false;
		} catch (JAXBException e) {
			logger.warn("QuestLog.saveToFile(" + filename +") failed", e);
			return false;
		} catch (IOException e) {
			logger.warn("QuestLog.saveToFile(" + filename + ") failed", e);
			return false;
		}
		needSave = false;
		return true;
	}

	public List<QuestInstance> getActiveQuests() {
		return activeQuests;
	}

	public List<QuestInstance> getFinishedQuests() {
		return finishedQuests;
	}

	public void setNeedSave(boolean needSave) {
		this.needSave = needSave;
	}
}
