package org.fealdia.orpg.server.quests;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Typical Quest progress, counting things (eg. kills).
 */
@XmlRootElement(name = "countableProgress")
public class CountableQuestProgress extends QuestProgress {
	@XmlAttribute
	private int count = 0;
	
	public int getCount() {
		return count;
	}
	
	public void increment() {
		count++;
	}
}
