package org.fealdia.orpg.server.quests;

import javax.xml.bind.annotation.XmlSeeAlso;


@XmlSeeAlso(KillObjective.class)
public abstract class QuestObjective {
	public abstract QuestProgress createQuestProgress();
	public abstract boolean isCompleted(QuestProgress questProgress);
}
