package org.fealdia.orpg.server.quests;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fealdia.orpg.server.Player;

@XmlRootElement
public class Quest {
	private int id = 0;
	private String title;
	private String description;
	@XmlElementWrapper
	@XmlElementRef
	private List<QuestObjective> objectives = new LinkedList<QuestObjective>();
	@XmlElementWrapper
	@XmlElementRef
	private List<QuestReward> rewards = new LinkedList<QuestReward>();
	@XmlElement(name = "dependency")
	private List<Integer> dependencies = new LinkedList<Integer>();

	public Quest() {
	}

	// TODO repeatable, required player level (if any)

	@XmlAttribute
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public String getFullDescription() {
		StringBuffer buf = new StringBuffer(description + "\n\nRewards:\n");
		for (QuestReward questReward : rewards) {
			buf.append("* " + questReward.toString() + "\n");
		}
		return buf.toString();
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<QuestObjective> getObjectives() {
		return objectives;
	}

	public List<QuestReward> getRewards() {
		return rewards;
	}

	public void giveRewardTo(Player player) {
		for (QuestReward reward : rewards) {
			reward.giveRewardTo(player);
			player.sendText("Got reward: " + reward);
		}
	}

	public void addDependency(int questId) {
		this.dependencies.add(questId);
	}

	public List<Integer> getDependencies() {
		return dependencies;
	}

	public void addObjective(QuestObjective objective) {
		objectives.add(objective);
	}
}
