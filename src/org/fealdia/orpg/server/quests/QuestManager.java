package org.fealdia.orpg.server.quests;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.xml.XMLUtil;

/**
 * Caching quest loader.
 */
public class QuestManager {
	private static QuestManager instance = null;
	
	Map<Integer, Quest> quests = new HashMap<Integer,Quest>();
	
	synchronized public static QuestManager getInstance() {
		if (instance == null) {
			instance = new QuestManager();
		}
		return instance;
	}
	
	private QuestManager() {}
	
	public Quest getQuest(int questId) {
		if (!quests.containsKey(questId)) {
			// load from maps/quests/<id>.xml
			try {
				Quest q = XMLUtil.<Quest>fromXML(Quest.class, PathConfig.getMapDir() + File.separator + "quests" + File.separator + questId + ".xml");
				quests.put(questId, q);
			}
			catch (Exception e) {
				throw new RuntimeException("Could not load quest id=" + questId, e);
			}

		}
		return quests.get(questId);
	}

}
