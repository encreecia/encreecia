package org.fealdia.orpg.server.quests;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="kill")
public class KillObjective extends QuestObjective {
	@XmlAttribute
	private String archetype;
	@XmlAttribute
	private int count;
	
	public KillObjective() {}
	
	public KillObjective(final String archetype, final int count) {
		this.archetype = archetype;
		this.count = count;
	}

	public String getArchetype() {
		return archetype;
	}

	@Override
	public QuestProgress createQuestProgress() {
		return new CountableQuestProgress();
	}

	public void incrementQuestProgress(QuestProgress questProgress) {
		((CountableQuestProgress)questProgress).increment();
	}

	@Override
	public boolean isCompleted(QuestProgress questProgress) {
		return ((CountableQuestProgress)questProgress).getCount() >= count;
	}
}
