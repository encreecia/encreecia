package org.fealdia.orpg.server.maps;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.StreamUtil;

/**
 * Handles creation of loot for monsters, based on treasure lists.
 * 
 * For now, it supports only simple format:
 * <pre>
 * treasure treasurename
 *   archetype dagger 5
 *   archetype sword 2
 * &lt;/treasure>
 * </pre>
 * 
 * In other words, list of archetypes and their chances.
 * 
 * TODO Should eventually support Crossfire-style rich treasure lists.
 */
public class TreasureManager {
	private static TreasureManager instance = null;
	
	private static Logger logger = Logger.getLogger(TreasureManager.class);
	
	// treasure -> {archetype,chance}
	private Map<String,Map<String,Integer>> treasures = new HashMap<String,Map<String,Integer>>();
	
	public static TreasureManager getInstance() {
		if (instance == null) {
			instance = new TreasureManager();
		}
		return instance;
	}
	
	public TreasureManager() {
		loadFromFile();
	}
	
	public void loadFromFile() {
		String filename = PathConfig.getXmlDir() + "treasures.txt";
		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			logger.error("Treasure file not found, nothing loaded");
			return;
		}
		
		String treasure = null;
		Map<String,Integer> treasureMap = null;
		try {
			for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
				String[] parts = line.split(" +");
				if ("treasure".equals(parts[0])) {
					treasure = parts[1];
					treasureMap = new HashMap<String,Integer>();
				}
				else if (parts[0].startsWith("</")) {
					logger.trace("Treasure '" + treasure + "' added with " + treasureMap.size() + " archetypes");
					treasures.put(treasure, treasureMap);
					treasure = null;
					treasureMap = null;
				}
				else if (treasure != null && "archetype".equals(parts[0])) {
					treasureMap.put(parts[1], Integer.parseInt(parts[2]));
				}
				else {
					logger.warn("Unhandled line: " + line);
				}
			}
			logger.info(treasures.size() + " treasures loaded.");
		} catch (IOException e) {
			logger.error("loadFromFile() IOException", e);
		}
		
		try {
			in.close();
		} catch (IOException e) {
			logger.warn("loadFromFile() IOException on close", e);
		}
	}
	
	/**
	 * Pick randomly and return list of archetypes generated for the given treasure.
	 */
	public List<String> getTreasureList(String name) {
		List<String> res = new LinkedList<String>();
		
		Map<String,Integer> archetypes = treasures.get(name);
		//for (String archetype : archetypes.keySet()) {
		for (Entry<String, Integer> pair : archetypes.entrySet()) {
			int chance = pair.getValue();
			
			int random = new Random().nextInt(100) + 1; // 1..100
			if (random <= chance) {
				res.add(pair.getKey());
			}
		}
		
		logger.trace("Treasure '" + name + "' generated the following: " + res);
		return res;
	}
}
