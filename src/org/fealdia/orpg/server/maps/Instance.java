package org.fealdia.orpg.server.maps;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Mobility;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.maps.SceneEntity;
import org.fealdia.orpg.common.util.FileUtil;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.SaveHelper;
import org.fealdia.orpg.common.util.SaveHelper.Saveable;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.maps.entities.ActionEntity;
import org.fealdia.orpg.server.maps.entities.Creature;
import org.fealdia.orpg.server.maps.entities.Item;
import org.fealdia.orpg.server.maps.entities.MapEntity;
import org.fealdia.orpg.server.maps.entities.Monster;
import org.fealdia.orpg.server.maps.entities.persistence.XMLEntityWriter;
import org.fealdia.orpg.server.maps.entities.persistence.XmlReaders;

/**
 * Instance of a Scene. This is a "live" map, used by the server.
 * 
 * Overlays are way to save items in unique instances. The entities are loaded as usual: all entities are marked non-saveable, except for the
 * ones that are marked "overlay once". These entities are loaded only the first time when there's no saved overlay. After that the overlay entities
 * are loaded. When the instance is unloaded, the overlay entities (saveable ones) are saved again.
 */
public class Instance {
	private static Logger logger = Logger.getLogger(Instance.class);
	
	/**
	 * Determines how many instances are created of one Scene, and on what basis. 
	 */
	public enum Uniqueness {
		NORMAL (false), // Only one instance for everyone
		PLAYER (true), // One instance for each player. These are saved.
		//PARTY (false),
		//GUILD (true),
		;
		private boolean saved = false;
		
		private Uniqueness(boolean saved) {
			this.saved = saved;
		}
		
		public boolean isSaved() {
			return saved;
		}
	}
	
	private Uniqueness uniqueness = Uniqueness.NORMAL;
	
	private String uniquenessKey = null;
	
	private Scene scene;
	
	// List of entities in this instance
	private List<MapEntity> entities = Collections.synchronizedList(new LinkedList<MapEntity>());
	
	private int playerCount = 0; // How many players are in this instance
	
	private long activeStamp = 0;
	
	public Instance(Scene scene, Uniqueness uniqueness, String key) {
		this.scene = scene;
		scene.incrementUsageCount();
		
		updateActiveStamp();
		
		setUniqueness(uniqueness);
		setUniquenessKey(key);
		
		// Load entities
		loadEntitiesFromXml();
	}
	
	public Scene getScene() {
		return scene;
	}
	
	public void addEntity(MapEntity entity) {
		// For debugging. Could also remove the earlier instance.
		if (entities.contains(entity)) {
			logger.error("Trying to add an entity to instance where it already is: " + entity.getName());
			return;
		}
		entities.add(entity);
		logger.trace("Added new entity to instance '" + getName() + "', " + entities.size() + " entities on it");
		
		if (entity instanceof Player) {
			incrementPlayerCount();
		}
	}
	
	public void removeEntity(MapEntity entity) {
		if (entity instanceof Player) {
			try {
				saveOverlayXml();
			} catch (Exception e) {
				logger.error("Failed to save overlay", e);
			}
		}
		
		entities.remove(entity);
		logger.trace("Removed entity, instance '" + getName() + "' has " + entities.size() + " entities on it");
		
		if (entity instanceof Player) {
			decrementPlayerCount();
			updateActiveStamp();
		}
	}
	
	public String getName() {
		return getScene().getName();
	}
	
	/**
	 * Check whether the given position is blocked by background, NPCs, or something else.
	 */
	public boolean isBlockedFor(int x, int y, Mobility mobility) {
		if (getScene().isBlockedFor(x, y, mobility)) {
			return true;
		}
		return isBlockedByEntities(x, y);
	}
	
	public boolean isBlockedByEntities(int x, int y) {
		// Check whether blocked by NPCs
		List<MapEntity> entities = getEntities();
		synchronized (entities) {
			for (MapEntity e : entities) {
				if (e.getX() == x && e.getY() == y && (e instanceof Monster || e instanceof Player)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Get list of entities in this instance. Do not modify the returned list. 
	 */
	public List<MapEntity> getEntities() {
		return entities;
	}
	
	/**
	 * Get entity by ID, or null if not found in this instance. 
	 */
	public MapEntity getEntityByID(long id) {
		for (MapEntity e: getEntities()) {
			if (e.getID() == id) {
				return e;
			}
		}
		return null;
	}
	
	/**
	 * Get Creature by id, or null if not found or not Creature.
	 */
	public Creature getCreatureByID(long id) {
		MapEntity en = getEntityByID(id);
		if (en instanceof Creature) {
			return (Creature) en;
		}
		return null;
	}
	
	/**
	 * Animate all Creatures in this instance.
	 * @note If an entity is moved to another instance here, it may get two actions this "tick".
	 */
	public void action() {
		// Make a copy of the entities list as it may change
		List<MapEntity> entities = new LinkedList<MapEntity>(getEntities());
		
		for (MapEntity e : entities) {
			try {
				// Animate creatures & spawns
				if (e instanceof ActionEntity) {
					if (e instanceof Creature) {
						Creature c = (Creature) e;
						if (!c.isDead()) {
							c.action();
						} else {
							c.die();
						}
					}
					else {
						((ActionEntity)e).action();
					}

				}

				// Decay items in instances that are not saved
				if (!getUniqueness().isSaved() && e instanceof Item) {
					Item i = (Item) e;
					if (i.canDecay()) {
						i.decay();
					}
				}
			}
			catch (Exception ex) {
				logger.error("Caught exception while animating entity '" + e.getName() + "' in instance '" + getUniqueName() + "'", ex);
			}
		}
	}

	public void loadEntitiesFromXml() {
		// Try to load entities unless they were loaded already
		if (scene.getEntityManager().getEntities().size() == 0) {
			if (!scene.loadEntities()) {
				return;
			}
		}

		for (SceneEntity sceneEntity : scene.getEntityManager().getEntities()) {
			// If this Instance already has an overlay, and the Scene contains overlay items (items created only once
			// and then saved into overlay if they still exist), then skip the entity.
			if (sceneEntity.containsKey("overlay") && hasOverlay()) {
				continue;
			}
			MapEntity mapEntity = MapEntity.createFromSceneEntity(sceneEntity);
			mapEntity.setInstance(this);
			if (mapEntity instanceof Item) {
				// Items originally on Scene (not dropped by players) should not decay
				((Item) mapEntity).setDecayable(false);
			}
		}
	}

	public MapEntity getFirstEntityAt(int x, int y) {
		List<MapEntity> entities = getEntities();
		synchronized (entities) {
			for (MapEntity e : entities) {
				if (e.getX() == x && e.getY() == y) {
					return e;
				}
			}
		}
		return null;
	}

	public List<MapEntity> getEntitiesAt(int x, int y) {
		List<MapEntity> result = new LinkedList<MapEntity>();
		List<MapEntity> entities = getEntities();
		synchronized (entities) {
			for (MapEntity e : entities) {
				if (e.getX() == x && e.getY() == y) {
					result.add(e);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Get entities within radius of the given position.
	 */
	public List<MapEntity> getEntitiesAtRadius(int x, int y, int radius) {
		List<MapEntity> result = new LinkedList<MapEntity>();
		List<MapEntity> entities = getEntities();
		synchronized (entities) {
			for (MapEntity e : entities) {
				if (e.distanceTo(x, y) <= radius) {
					result.add(e);
				}
			}
		}
		
		return result;
	}
	
	public MapEntity getEntityByTypeAt(Class<? extends MapEntity> type, int x, int y) {
		List<MapEntity> entities = getEntities();
		synchronized (entities) {
			for (MapEntity e : entities) {
				if (e.getX() == x && e.getY() == y && type.isInstance(e)) {
					return e;
				}
			}
		}
		return null;
	}
	
	public Uniqueness getUniqueness() {
		return uniqueness;
	}

	public void setUniqueness(Uniqueness uniqueness) {
		this.uniqueness = uniqueness;
	}
	
	/**
	 * Get a name that sets this instance apart from others.
	 */
	public String getUniqueName() {
		StringBuffer result = new StringBuffer(getName());
		if (getUniqueness() != Uniqueness.NORMAL) {
			result.append("[" + getUniqueness().toString() + ":" + getUniquenessKey() + "]");
		}
		
		return result.toString();
	}

	public String getUniquenessKey() {
		return uniquenessKey;
	}

	public void setUniquenessKey(String uniquenessKey) {
		this.uniquenessKey = uniquenessKey;
	}
	
	/**
	 * Whether this instance is permanent, eg. saved to a file.
	 */
	public boolean isPermanent() {
		return getUniqueness().isSaved();
	}
	
	/**
	 * Overlay directory for this instance.
	 */
	public String getOverlayDir() {
		return PathConfig.getServerDataDir() + "instances/" + getUniqueness().toString().toLowerCase() + "/" + getUniquenessKey() + "/";
	}
	
	/**
	 * data/instances/<type>/<key>/<mapname>.txt
	 */
	public String getOverlayFilename() {
		return getOverlayDir() + getName() + ".txt";
	}
	
	public String getOverlayFilenameXml() {
		return getOverlayDir() + getName() + ".xml";
	}

	/**
	 * If this is a permanent instance, save the overlay.
	 */
	@Deprecated
	public boolean saveOverlay() throws IOException {
		if (!isPermanent()) {
			return false;
		}
		try {
			saveOverlayXml();
		} catch (Exception e) {
			logger.error("XML overlay saving failed", e);
		}
		logger.debug("Saving overlay for " + getUniqueName());
		
		// Make the directories
		String dir = getOverlayDir();
		new File(dir).mkdirs();
		
		String filename = getOverlayFilename();
		BufferedWriter out = new BufferedWriter(new FileWriter(filename + ".tmp"));
		try {
			// Save all entities
			synchronized (getEntities()) {
				for (MapEntity en : getEntities()) {
					if (!en.isSaveable()) {
						continue;
					}

					// Save each entity
					en.saveToStreamWithHeader(out);
				}
			}
		} finally {
			out.close();
		}
		
		// Save old file as a backup
		FileUtil.renameAsBackup(filename);
		
		// rename filename.tmp -> filename
		File newFile = new File(filename + ".tmp");
		return newFile.renameTo(new File(filename));
	}

	public void saveOverlayXml() throws Exception {
		if (!isPermanent()) {
			return;
		}
		logger.debug("Saving overlay XML for " + getUniqueName());

		// Make the directories
		String dir = getOverlayDir();
		new File(dir).mkdirs();

		// XML
		SaveHelper.saveToFileWithBackup(new Saveable() {
			public void saveToFile(String filename) throws Exception {
				FileOutputStream out2 = new FileOutputStream(filename);
				XMLEntityWriter xmlEntityWriter = new XMLEntityWriter(out2);
				try {
					List<MapEntity> filteredEntities = new LinkedList<MapEntity>();
					for (MapEntity entity : getEntities()) {
						if (entity.isSaveable()) {
							filteredEntities.add(entity);
						}
					}
					xmlEntityWriter.writeEntities(filteredEntities);
				} finally {
					xmlEntityWriter.close();
					out2.close();
				}
			}
		}, getOverlayFilenameXml());

	}

	public void loadOverlay() throws Exception {
		if (!isPermanent()) {
			return;
		}
		logger.debug("Loading overlay for " + getUniqueName());

		try {
			loadOverlayXml();
		} catch (FileNotFoundException e) {
			logger.debug("XML overlay not found, trying TXT");
			try {
				loadOverlayTxt();
			} catch (FileNotFoundException e2) {
				logger.debug("No overlay found");
			}
		}
	}

	private void loadOverlayTxt() throws IOException {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(getOverlayFilename()));

			List<MapEntity> ents = MapEntity.loadEntitiesFromStream(in);
			for (MapEntity en : ents) {
				en.moveToInstance(this, en.getX(), en.getY());
				if (en instanceof Item) {
					((Item) en).setDecayable(false);
				}
			}
		} finally {
			in.close();
		}
	}

	private void loadOverlayXml() throws Exception {
		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new FileInputStream(getOverlayFilenameXml()));
		final List<MapEntity> entities = XmlReaders.processEntities(reader);
		reader.close();
		for (MapEntity en : entities) {
			en.moveToInstance(this, en.getX(), en.getY());
			if (en instanceof Item) {
				((Item) en).setDecayable(false);
			}
		}
	}

	public int getPlayerCount() {
		return playerCount;
	}
	
	public synchronized void incrementPlayerCount() {
		playerCount++;
	}
	
	public synchronized void decrementPlayerCount() {
		playerCount--;
	}

	public long getActiveStamp() {
		return activeStamp;
	}
	
	public void updateActiveStamp() {
		activeStamp = System.currentTimeMillis();
	}
	
	/**
	 * Check whether this Instance yet has a saved overlay.
	 */
	public boolean hasOverlay() {
		// If this instance is never saved, it does not have an overlay
		if (!isPermanent()) {
			return false;
		}

		// Now check if the overlay has been saved yet
		File f = new File(getOverlayFilename());
		return f.exists();
	}
	
	/**
	 * Should be called when this Instance is unloaded.
	 */
	public void unload() {
		scene.decrementUsageCount();
	}
}