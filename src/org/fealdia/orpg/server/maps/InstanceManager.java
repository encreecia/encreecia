package org.fealdia.orpg.server.maps;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Scene;
import org.fealdia.orpg.common.maps.SceneManager;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.ServerConfig;
import org.fealdia.orpg.server.maps.Instance.Uniqueness;
import org.fealdia.orpg.server.maps.entities.MapEntity;

/**
 * In charge of creation, saving and destruction of instances (live) of scenes.
 * 
 * There may be multiple Instances of the same Scene present, depending on Instance.Uniqueness. The uniqueness is decided by whatever
 * takes the player to another instance, Teleport for example. Thus, there may be both global and player-specific instances of the same Scene,
 * for example.
 */
public class InstanceManager {
	private static InstanceManager instance;
	
	private static Logger logger = Logger.getLogger(InstanceManager.class);
	
	// How long (in secs) to wait until getting rid of the instance after last player has left it
	private static final int UNLOAD_DELAY = ServerConfig.getInstance().getPropertyAsInt("instance.unload_delay");
	
	private SceneManager sceneManager;
	
	// All instances
	private List<Instance> instances = new LinkedList<Instance>();
	
	// uniqueness -> key -> name -> instance
	// NORMAL -> null
	// PLAYER -> PlayerName
	// PARTY  -> PartyName
	// GUILD  -> GuildName
	private Map<Instance.Uniqueness,Map<String,Map<String,Instance>>> instanceMap = new HashMap<Instance.Uniqueness,Map<String,Map<String,Instance>>>();
	
	public static InstanceManager getInstance() {
		if (instance == null) {
			instance = new InstanceManager(SceneManager.getInstance());
		}
		return instance;
	}

	public InstanceManager(SceneManager sceneManager) {
		this.sceneManager = sceneManager;
	}

	public SceneManager getSceneManager() {
		return sceneManager;
	}
	
	/**
	 * Get or create an instance matching the given scene name, assuming Uniqueness = NORMAL. 
	 */
	public Instance getInstance(String name) {
		return getInstance(name, Uniqueness.NORMAL, null);
	}
	
	/**
	 * Get or create an instance matching the given scene name.
	 */
	public synchronized Instance getInstance(String name, Uniqueness uniqueness, MapEntity entity) {
		String key = null;
		if (uniqueness == Uniqueness.PLAYER) {
			if (entity instanceof Player) {
				key = entity.getName();
			} else {
				logger.warn("instance is player-unique, but entity is not player");
				return null;
			}
		}
		
		try {
			Instance inst = instanceMap.get(uniqueness).get(key).get(name);
			if (inst != null) {
				return inst;
			}
		}
		catch (NullPointerException e) {}
		
		// Create new instance
		logger.debug("Creating new instance for: " + name);
		
		Scene scene = getSceneManager().getScene(name);
		if (scene == null) {
			return null;
		}
		Instance instance = new Instance(scene, uniqueness, key);
		try {
			instance.loadOverlay();
		} catch (Exception e) {
			logger.error("Instance overlay loading failed", e);
			throw new RuntimeException("Instance overlay loading failed", e);
		}
		
		instances.add(instance);
		
		// Add to the instance map
		if (!instanceMap.containsKey(uniqueness)) {
			instanceMap.put(uniqueness, new HashMap<String,Map<String,Instance>>());
		}
		if (!instanceMap.get(uniqueness).containsKey(key)) {
			instanceMap.get(uniqueness).put(key, new HashMap<String,Instance>());
		}
		instanceMap.get(uniqueness).get(key).put(name, instance);
		
		logger.info("Successfully created instance '" + name + "', " + instances.size() + " instances present");
		
		// If WorldThread is hibernating, wake it up after we leave this method
		notify(); 
		
		return instance;
	}
	
	public List<Instance> getInstances() {
		return instances;
	}

	public int countInstances() {
		return instances.size();
	}
	
	/**
	 * Unload any instances that have not been used for a while.
	 * @param force whether to force unload regardless of how long the instance has been unused
	 * 
	 * @return true if any instances were unloaded
	 */
	public boolean unloadInstances(boolean force) {
		long now = System.currentTimeMillis();
		boolean res = false;
		synchronized (getInstances()) {
			List<Instance> instances = getInstances();
			Iterator<Instance> iter = instances.iterator();
			while (iter.hasNext()) {
				Instance instance = iter.next();
				if (instance.getPlayerCount() == 0) {
					long stamp = instance.getActiveStamp();
					if (force || stamp + UNLOAD_DELAY * 1000 < now) {
						logger.info("Unloading instance: " + instance.getUniqueName());
						
						// Remove from the map
						if (instanceMap.get(instance.getUniqueness()).get(instance.getUniquenessKey()).remove(instance.getName()) == null) {
							logger.error("Failed to remove instance from instanceMap");
						} else {
							iter.remove();
							instance.unload();
							res = true;
						}
					}
				}
			}
		}
		return res;
	}
}
