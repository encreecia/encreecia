package org.fealdia.orpg.server.maps.entities;

import org.fealdia.orpg.server.Player;

/**
 * A simple sign, containing text, that can be read. Used to label towns, roads etc.
 */
public class Sign extends MapEntity {
	private String text;
	
	@Override
	public boolean onGroundUse(Player player) {
		if (distanceTo(player) <= 2) {
			player.sendText("You see a sign reading: " + text);
		} else {
			player.sendText("You see a sign, but you can't quite tell what it says.");
		}
		return true;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("text".equals(key)) {
			setText(value);
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
