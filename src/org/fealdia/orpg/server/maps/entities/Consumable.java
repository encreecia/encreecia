package org.fealdia.orpg.server.maps.entities;

import org.fealdia.orpg.server.Player;

/**
 * Base class for consumable items.
 */
public abstract class Consumable extends Item {
	/**
	 * Attempt to use the consumable.
	 *
	 * @return true if the item was consumed in the process, and should be destroyed.
	 */
	public abstract boolean use(Player player);
}
