package org.fealdia.orpg.server.maps.entities;

import java.io.IOException;
import java.io.Writer;

import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;

/**
 * Returns health when used. 
 */
public class Potion extends Consumable {
	private int health = 0;

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("health".equals(key)) {
			setHealth(Integer.parseInt(value));
		}
		else {		
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}
	
	@Override
	public boolean use(Player player) {
		int newHealth = Math.min(player.getMaxHealth(), player.getHealth() + getHealth());
		
		if (newHealth > player.getHealth()) {
			player.setHealth(newHealth);
			player.sendText("You gain " + getHealth() + " from the potion.");
		}
		
		return true;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	@Override
	public boolean isStackable() {
		return true;
	}

	@Override
	public boolean stacksWith(Item other) {
		if (other instanceof Potion) {
			Potion p = (Potion) other;
			return p.getHealth() == getHealth();
		}
		return false;
	}

	/*
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Object o = super.clone();
		
		Potion p = (Potion) o;
		p.setHealth(p.getHealth());

		return o;
	}
	*/

	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		
		savePropertyToStream(out, "health", getHealth(), false);
	}
	
	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		saveProperty(listener, "health", getHealth());
	}
}
