package org.fealdia.orpg.server.maps.entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;

/**
 * An item that holds other items.
 */
public class Container extends Item {
	private List<Item> items = new LinkedList<Item>();
	
	private int itemCapacity = 0;
	private long weightCapacity = 0;

	public List<Item> getItems() {
		return items;
	}

	public void addItem(Item i) {
		i.moveOutOfWorld();
		if (i instanceof Wearable) {
			((Wearable)i).setEquipped(false);
		}
		items.add(i);
	}
	
	/**
	 * Whether this container can hold the given item.
	 */
	public boolean canReceive(Item item) {
		if (getItemCapacity() != 0 && getContentCount() == getItemCapacity()) {
			return false;
		}
		if (getWeightCapacity() != 0 && item.getTotalWeight() + getContentWeight() > getWeightCapacity()) {
			return false;
		}
		return true;
	}
	
	public Item getItemByID(long item_id) {
		for (Item i : items) {
			if (i.getID() == item_id) {
				return i;
			}
		}
		return null;
	}
	
	public void removeItem(Item i) {
		items.remove(i);
	}
	
	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		savePropertyToStream(out, "capacity_items", getItemCapacity(), getItemCapacity() == 0);
		savePropertyToStream(out, "capacity_weight", getWeightCapacity(), getWeightCapacity() == 0);
		
		out.write("<items>\n");
		for (Item i : getItems()) {
			i.saveToStreamWithHeader(out);
		}
		out.write("</items>\n");
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		if (getItemCapacity() != 0) {
			saveProperty(listener, "capacityItems", getItemCapacity());
		}
		if (getWeightCapacity() != 0) {
			saveProperty(listener, "capacityWeight", getWeightCapacity());
		}

		listener.startElement("items");
		for (Item i : getItems()) {
			i.saveWithHeader(listener);
		}
		listener.endElement();
	}

	@Override
	public boolean handlePropertyChild(String type, BufferedReader in) throws IOException {
		if ("items".equals(type)) {
			List<MapEntity> ents = MapEntity.loadEntitiesFromStream(in);
			for (MapEntity en : ents) {
				addItem((Item)en);
			}
		} else {
			return super.handlePropertyChild(type, in);
		}
		return true;
	}

	@Override
	public List<String> getExamineProperties() {
		List<String> props = super.getExamineProperties();
		props.add("capacity " + getContentWeight() + "/" + getWeightCapacity() + " g");
		props.add(getContentCount() + "/" + getItemCapacity() + " items");
		return props;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("capacity_items".equals(key)) {
			setItemCapacity(Integer.parseInt(value));
		}
		else if ("capacity_weight".equals(key)) {
			setWeightCapacity(Long.parseLong(value));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	public int getItemCapacity() {
		return itemCapacity;
	}

	public void setItemCapacity(int itemCapacity) {
		this.itemCapacity = itemCapacity;
	}

	public long getWeightCapacity() {
		return weightCapacity;
	}

	public void setWeightCapacity(long weightCapacity) {
		this.weightCapacity = weightCapacity;
	}

	@Override
	public long getTotalWeight() {
		return super.getTotalWeight() + getContentWeight();
	}
	
	public int getContentCount() {
		return getItems().size();
	}
	
	/**
	 * Get the weight of our contents.
	 */
	public long getContentWeight() {
		long res = 0;
		
		for (Item i : getItems()) {
			res += i.getTotalWeight();
		}
		
		return res;
	}
	
}
