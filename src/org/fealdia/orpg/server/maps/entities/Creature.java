package org.fealdia.orpg.server.maps.entities;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Mobility;
import org.fealdia.orpg.server.Alignment;
import org.fealdia.orpg.server.Buff;
import org.fealdia.orpg.server.BuffType;
import org.fealdia.orpg.server.CreatureController;
import org.fealdia.orpg.server.CreatureViewer;
import org.fealdia.orpg.server.GameRules;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.Spell;
import org.fealdia.orpg.server.maps.Instance;
import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;


/**
 * Common base for Monster and Player. 
 */
public abstract class Creature extends ActionEntity implements PropertyChangeListener {
	private static Logger logger = Logger.getLogger(Creature.class);
	
	private final static String DEFAULT_NAME = "creature";
	
	public enum Race {
		NONE,
		HUMANOID,
		UNDEAD,
		DEMON,
		DRAGON,
	}
	
	// base position
	// level
	// wait_attack, wait_move, wait_...?
	// bool moveRelative
	
	private int attackTimer = 0;
	private String faceEngaged = null;
	private int health = 0;
	private int maxHealth = 10;
	private Mobility mobility = Mobility.NORMAL;
	private int movementTimer = 0;
	private Race race = Race.NONE;
	private int recoverTimer = 0;
	private int speedBonus = 0;
	private int visibleRadius = VISIBLE_RADIUS;
	private CreatureController controller = null;
	private CreatureController activeController = null;
	private List<CreatureViewer> viewers = new LinkedList<CreatureViewer>();
	
	private List<Buff> buffs = new LinkedList<Buff>();
	
	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	public enum WatchableProperty {
		HEALTH,
		MAXHEALTH,
		INSTANCE,
		TARGET,
	}

	public Creature() {
		setName(DEFAULT_NAME);
	}
	
	@Override
	public void action() {
		// Movement
		if (movementTimer <= 0) {
			//if (doMovement()) {
			if ((getActiveController() != null && getActiveController().doMovement(this))) {
				// Get terrain movement cost - speed bonus. Minimum cost is 1.
				int cost = Math.max(getInstance().getScene().getSpeed(getX(), getY()) - getSpeedBonusPlusBuffs(), 1);
				movementTimer += cost;
			}
		} else {
			movementTimer -= 10;
		}
		
		// Attack target if next to it
		if (attackTimer <= 0) {
			Creature target = getTarget();
			if (target != null && canAttack(target)) {
				attackCreature(target);
				attackTimer += getWeaponSpeed();
			}
		} else {
			attackTimer -= 10;
		}
		
		// Regain health every N tick
		if (getHealth() < getMaxHealth() && recoverTimer == 0 && !isInCombat()) {
			setHealth(getHealth() + Math.min(5, getMaxHealth() - getHealth()));
			recoverTimer += 20;
		}
		else if (recoverTimer > 0) {
			recoverTimer--;
		}
		
		// TODO don't expire every tick, just every 1 sec or so
		expireBuffs();
	}
	
	public boolean canAttack(Creature target) {
		return distanceTo(target) <= getAttackRange();
	}
	
	public int getHealth() {
		return health;
	}
	
	public Creature getTarget() {
		// If ActiveController is null, eg. this player is possessing someone else, then return the original controller's target
		if (getController() == null) {
			return null;
		}
		if (getActiveController() == null) {
			return getController().getTarget();
		}
		
		// otherwise return the ActiveController's target, eg. target of whoever is possessing this Creature
		return getActiveController().getTarget();
	}
	
	/**
	 * Sets health, and notifies creatures that are targeting us.
	 */
	public void setHealth(int health) {
		if (health == this.health) {
			return;
		}
		int oldhealth = this.health;
		this.health = health;
		
		pcs.firePropertyChange(WatchableProperty.HEALTH.toString(), oldhealth, health);
	}
	
	/**
	 * Change target to the given id.
	 * 
	 * @param id entity id or 0 to clear target.
	 */
	public synchronized void setTarget(long id) {
		setTarget(getInstance().getCreatureByID(id));
	}
	
	public synchronized void setTarget(final Creature target) {
		// No change -> return
		if (target == getTarget()) {
			return;
		}
		final Creature oldTarget = getTarget();
		
		// Stop listening to old target's property changes
		Creature c = getTarget();
		if (c != null) {
			c.removePropertyChangeListener(this);
		}

		long id = target == null ? 0 : target.getID();
		logger.trace(getID() + "[" + getName() + "]: Target set to id = " + id);
		
		getController().setTarget(target);
		
		// Add to new target's targetters
		c = getTarget();
		if (c != null) {
			c.addPropertyChangeListener(this);
		}
		
		// Notify clients of possible face change
		if (getFaceEngaged() != null) {
			notifyPlayersOfEntityChange();
		}

		pcs.firePropertyChange(WatchableProperty.TARGET.toString(), oldTarget, target);
	}
	
	public boolean isDead() {
		return getHealth() <= 0;
	}
	
	/**
	 * Check whether the given creature is our enemy.
	 */
	public boolean isEnemy(Creature creature) {
		return false;
	}
	
	/**
	 * Called when another entity is moved (either within the instance or to/from another instance).
	 * Also when another entity becomes visible or "invisible" because of our movement.
	 * 
	 * @param src_visible whether or not the old position was visible
	 * @param dst_visible whether or not the new position is visible
	 * @param we_moved whether the visibility change happened because this Creature moved
	 */
	protected void onEntityVisibilityChange(MapEntity mapEntity, boolean src_visible, boolean dst_visible, boolean we_moved) {
		for (CreatureViewer viewer : viewers) {
			viewer.onEntityMovement(mapEntity, src_visible, dst_visible);
		}
	}

	@Override
	public void move(int x, int y) {
		int oldx = getX();
		int oldy = getY();
		
		super.move(x, y);
		
		// Check all entities that were not visible but are now, and vice versa, and notice self.
		List<MapEntity> entities = getInstance().getEntities();
		synchronized (entities) {
			for (MapEntity e : entities) {
				if (e == this) {
					continue;
				}
				boolean src_visible = false;
				boolean dst_visible = false;
				if ((e.isVisible() || canSeeInvisible()) && e.distanceTo(oldx, oldy) <= getVisibleRadius()) {
					src_visible = true;
				}
				if ((e.isVisible() || canSeeInvisible()) && e.distanceTo(x, y) <= getVisibleRadius()) {
					dst_visible = true;
				}
				// Visibility change
				if (src_visible != dst_visible) {
					onEntityVisibilityChange(e, src_visible, dst_visible, true);
				}
			}
		}
	}
	
	/**
	 * Receive given damage from source.
	 * 
	 * @return true if this creature died because of the damage. 
	 */
	public boolean receiveDamage(Creature source, int damage) {
		if (isDead()) {
			return false;
		}
		onDamageReceived(source, damage, getHealth() - damage <= 0);
		setHealth(getHealth() - damage);
		giveBuff(BuffType.COMBAT, GameRules.COMBAT_BUFF_DURATION);
		
		// Notify nearby creatures that can see us
		for (MapEntity en : getInstance().getEntities()) {
			if (en instanceof Creature && en.canSee(this)) {
				((Creature)en).onCreatureHit(source, this, damage);
			}
		}
		
		return isDead();
	}
	
	public void receiveSpell(Creature source, Spell spell) {
		// Notify nearby creatures that can see us
		for (MapEntity en : getInstance().getEntities()) {
			if (en instanceof Creature && en.canSee(this)) {
				((Creature) en).onSpellCast(source, this, spell);
			}
		}
	}

	/**
	 * Called when a nearby Creature is hit
	 */
	public void onCreatureHit(Creature attacker, Creature target, int damage) {}
	
	public void onSpellCast(Creature source, Creature target, Spell spell) {}

	/**
	 * Called when this Creature dies and any related business should be attended to.
	 */
	public void die() {
		// Clear targetters so that guards won't keep spawnkilling us (if player)
		// We do this by moving us out of this world, and then back in - it will trigger PropertyChangeListeners
		Instance inst = getInstance();
		int x = getX();
		int y = getY();
		moveOutOfWorld();
		moveToInstance(inst, x, y);
		
		// Also clear our target
		setTarget(null);
		
		// Clear buffs that expire on death
		buffs.clear();
		onBuffsChanged();
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		int oldHealth = this.maxHealth;
		this.maxHealth = maxHealth;

		pcs.firePropertyChange(WatchableProperty.MAXHEALTH.toString(), oldHealth, maxHealth);
	}
	
	public void attackCreature(Creature target) {
		// calculate whether we hit
		int chanceToHit = getChanceToHit(target);
		boolean doWeHit = new Random().nextInt(100) <= (chanceToHit - 1);
		
		int damage = 0;
		long experience = 0;
		if (doWeHit) {

			// Calculate amount of damage done
			damage = getWeaponDamage(target);

			// subtract the amount blocked by the enemy's armor
			damage = target.getDamageAfterReductions(damage, getLevel());

			if (target.receiveDamage(this, damage)) {
				// Calculate real experience to get
				experience = 0;
				if (target instanceof Monster) {
					Monster m = (Monster) target;
					experience = m.getExperienceGiven();

					// make sure the target does not drop loot if not killed by player
					if (!(this instanceof Player)) {
						m.setTreasure(null);
					}
				}
			}
		}
		
		giveBuff(BuffType.COMBAT, GameRules.COMBAT_BUFF_DURATION);

		onAttack(target, damage, experience);
	}
	
	/**
	 * Called when we attack a creature.
	 */
	protected void onAttack(Creature target, int damage, long experience) {
		
	}
	
	/**
	 * Try moving relatively based on given vector. 
	 */
	public boolean tryMoveVector(int dx, int dy) {
		int x = getX();
		int y = getY();
		
		// Randomly try to move first horizontally, then vertically
		// This is a bit cryptic code to understand, but try reading it with assumption r1=1, r2=0 first
		int r1 = new Random().nextInt(2); // 0-1
		int r2 = 0;
		if (r1 == 0) { r2 = 1; }
		
		if (canMoveTo(x + r1*dx, y + r2*dy)) {
			x += r1*dx;
			y += r2*dy;
			
			if (canMoveTo(x + r2*dx, y + r1*dy)) {
				x += r2*dx;
				y += r1*dy;
			}
		}
		else if (canMoveTo(getX() + r2*dx, getY() + r1*dy)) {
			x += r2*dx;
			y += r1*dy;
			
			if (canMoveTo(getX() + r1*dx, getY() + r2*dy)) {
				x += r1*dx;
				y += r2*dy;
			}
		} else {
			return false;
		}
		
		// If moving to where a creature already is, swap places with it
		MapEntity blocker = getInstance().getEntityByTypeAt(Creature.class, x, y);
		if (blocker != null) {
			blocker.moveBelow(this);
		}
		
		move(x, y);
		return true;
	}
	
	public boolean canMoveTo(int x, int y) {
		// If blocked by scene, nothing we can do -> FAIL
		if (getInstance().getScene().isBlockedFor(x, y, getMobility())) {
			return false;
		}
		
		// If not blocked by scene or entities -> OK
		if (!getInstance().isBlockedByEntities(x, y)) {
			return true;
		}
		
		// If we have the ability to swap places, check if target square has a creature, if yes, return true
		MapEntity blocker = getInstance().getEntityByTypeAt(Creature.class, x, y);
		if (blocker != null) {
			return canSwapPlaceWith((Creature) blocker);
		}
		return false;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("health".equals(key)) {
			setHealth(Integer.parseInt(value));
			setMaxHealth(getHealth());
		}
		else if ("faceengaged".equals(key)) {
			setFaceEngaged(value);
		}
		else if ("mobility".equals(key)) {
			setMobility(Mobility.valueOf(value.toUpperCase()));
		}
		else if ("race".equals(key)) {
			setRace(Race.valueOf(value.toUpperCase()));
		}
		else if ("speed".equals(key)) {
			setSpeedBonus(Integer.parseInt(value));
		}
		else if ("target".equals(key)) {
			setTarget(Long.parseLong(value));
		}
		else if ("visibleradius".equals(key)) {
			setVisibleRadius(Integer.parseInt(value));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		
		savePropertyToStream(out, "health", getHealth(), false);
		savePropertyToStream(out, "race", getRace().toString().toLowerCase(), getRace() == Race.NONE);
		savePropertyToStream(out, "speed", getSpeedBonus(), getSpeedBonus() == 0);
		savePropertyToStream(out, "visibleradius", getVisibleRadius(), getVisibleRadius() == VISIBLE_RADIUS);
		savePropertyToStream(out, "faceengaged", getFaceEngaged(), getFaceEngaged() == null);
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		saveProperty(listener, "health", getHealth());
		if (getRace() != Race.NONE) {
			saveProperty(listener, "race", getRace().toString().toLowerCase());
		}
		if (getSpeedBonus() != 0) {
			saveProperty(listener, "speed", getSpeedBonus());
		}
		if (getVisibleRadius() != VISIBLE_RADIUS) {
			saveProperty(listener, "visibleradius", getVisibleRadius());
		}
		if (getFaceEngaged() != null) {
			saveProperty(listener, "faceengaged", getFaceEngaged());
		}
	}

	/**
	 * Speed bonus this creature has on all terrains. Positive bonus means faster speed, negative bonus slower.
	 */
	public int getSpeedBonus() {
		return speedBonus;
	}
	
	public int getSpeedBonusPlusBuffs() {
		int bonus = 0;
		if (hasBuff(BuffType.HASTE)) {
			bonus += 10;
		}
		if (hasBuff(BuffType.SLOW)) {
			bonus += -20;
		}
		return getSpeedBonus() + bonus;
	}

	public void setSpeedBonus(int speedBonus) {
		this.speedBonus = speedBonus;
	}
	
	/**
	 * How many 1/100 of second the equipped weapon (if any) takes to attack again. Smaller = faster.
	 */
	public int getWeaponSpeed() {
		return 50;
	}

	/**
	 * Get the damage this Creature can do with whatever means it has. The returned result may wary (eg. may be random).
	 * @param target 
	 */
	public int getWeaponDamage(Creature target) {
		return 1;
	}
	
	/**
	 * How many tiles away this creature can attack from.
	 */
	public int getAttackRange() {
		return 1;
	}
	
	/**
	 * Called when the health of our target changes.
	 * @param difference negative if target's health decreased
	 */
	protected void onTargetHealthChange(Creature target, int difference) {}

	/**
	 * 0-100.
	 */
	public int getHealthPercentage() {
		return Math.min(100, (getHealth() * 100) / getMaxHealth());
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}

	@Override
	public int getVisibleRadius() {
		return visibleRadius;
	}

	public void setVisibleRadius(int visibleRadius) {
		this.visibleRadius = visibleRadius;
	}
	
	/**
	 * Called when we receive damage.
	 * @param died whether we died because of the damage
	 */
	protected void onDamageReceived(Creature source, int damage, boolean died) {}
	
	/**
	 * Create a corpse typical to this race.
	 */
	public Item createCorpse() {
		Item corpse = new Item();
		corpse.setName("Corpse of " + getName());
		corpse.setPickUp(false);
		
		// Set face based on race
		String face = null;
		if (getRace() == Race.HUMANOID) {
			// humanoid[1-4]
			int num = new Random().nextInt(4) + 1;
			face = "bones/humanoid" + num;
		}
		else {
			String[] choices = { "bones", "skull", "small1", "small2" };
			int num = new Random().nextInt(choices.length);
			face = "bones/" + choices[num];
		}
		corpse.setFace(face);
		
		return corpse;
	}
	
	/**
	 * Whether this creature can swap places with other creatures. Currently only guards can.
	 */
	public boolean canSwapPlaceWith(Creature creature) {
		return false;
	}

	public String getFaceEngaged() {
		return faceEngaged;
	}

	public void setFaceEngaged(String faceEngaged) {
		this.faceEngaged = faceEngaged;
	}
	
	@Override
	public String getVisibleFace() {
		if (getTarget() != null && getFaceEngaged() != null) {
			return getFaceEngaged();
		}
		return getFace();
	}
	
	/**
	 * Get chance to hit the given creature, 0-100.
	 */
	public int getChanceToHit(Creature target) {
		return GameRules.getChanceToHit(getLevel(), target.getLevel());
	}
	
	public int getLevel() {
		return 1;
	}
	
	@Override
	public Mobility getMobility() {
		return mobility;
	}

	public void setMobility(Mobility mobility) {
		this.mobility = mobility;
	}

	public CreatureController getController() {
		return controller;
	}

	public void setController(CreatureController controller) {
		this.controller = controller;
		setActiveController(controller);
	}

	public CreatureController getActiveController() {
		return activeController;
	}

	public void setActiveController(CreatureController activeController) {
		this.activeController = activeController;
	}
	
	public void addViewer(CreatureViewer viewer) {
		viewers.add(viewer);
	}
	
	public boolean removeViewer(CreatureViewer viewer) {
		return viewers.remove(viewer);
	}
	
	public List<CreatureViewer> getViewers() {
		return viewers;
	}

	@Override
	protected void onPositionChanged(int x, int y) {
		super.onPositionChanged(x, y);
		
		for (CreatureViewer viewer : viewers) {
			viewer.onViewablePositionChanged(x, y);
		}
	}
	
	public void resyncVisibleEntities() {
		// Find all creatures that we see now, and send notifications of them
		if (getInstance() != null) {
			synchronized (getInstance().getEntities()) {
				for (MapEntity e : getInstance().getEntities()) {
					if (e != this && canSee(e)) {
						onEntityVisibilityChange(e, false, true, true);
					}
				}
			}
		}
	}
	
	public List<Buff> getBuffs() {
		return buffs;
	}
	
	/**
	 * Called when buffs have changed.
	 */
	protected void onBuffsChanged() {}
	
	protected void onBuffReceived(BuffType buff) {
		if (buff.isHidden()) {
			return;
		}
		sendTextToNearbyViewers(getName() + " gains " + buff.getName());
	}
	
	protected void onBuffExpired(BuffType buff) {
		if (buff.isHidden()) {
			return;
		}
		sendTextToNearbyViewers(buff.getName() + " wears off " + getName());
	}
	
	public void giveBuff(BuffType buffType, int duration_secs) {
		// If there is an existing Buff of the same type, renew it or don't buff at all
		for (Buff b : getBuffs()) {
			if (b.getBuffType() == buffType) {
				// If new buff lasts longer, renew
				if (b.getDurationLeft() < duration_secs) {
					b.setDuration(duration_secs);
					onBuffsChanged();
				}
				return;
			}
		}
		
		// New Buff
		Buff buff = new Buff(buffType, duration_secs);
		buffs.add(buff);
		onBuffReceived(buffType);
		onBuffsChanged();
	}
	
	/**
	 * Check all Buffs for expired ones, and remove those.
	 */
	public void expireBuffs() {
		boolean changed = false;
		for (Iterator<Buff> iter = buffs.iterator(); iter.hasNext();) {
			Buff b = iter.next();
			if (b.hasExpired()) {
				onBuffExpired(b.getBuffType());
				iter.remove();
				changed = true;
			}
		}
		if (changed) {
			onBuffsChanged();
		}
	}
	
	public boolean hasBuff(BuffType buff) {
		for (Buff b : getBuffs()) {
			if (b.getBuffType() == buff) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Send the given text to all viewers of Creatures nearby.
	 */
	public void sendTextToNearbyViewers(String text) {
		for (MapEntity e : getInstance().getEntities()) {
			if (e instanceof Creature && e.canSee(this)) {
				Creature c = (Creature) e;
				for (CreatureViewer v : c.getViewers()) {
					v.sendText(text);
				}
			}
		}
	}
	
	/**
	 * Get how much damage is left after armor reductions.
	 */
	public int getDamageAfterReductions(int damage, int enemy_level) {
		return damage;
	}

	public abstract Alignment getAlignment();

	public boolean isInCombat() {
		return hasBuff(BuffType.COMBAT);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	@Override
	public void moveToInstance(Instance instance, int x, int y) {
		setTarget(null);
		super.moveToInstance(instance, x, y);
		resyncVisibleEntities();
	}

	@Override
	public void setInstance(Instance instance) {
		Instance oldInstance = getInstance();

		super.setInstance(instance);

		pcs.firePropertyChange(WatchableProperty.INSTANCE.toString(), oldInstance, instance);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() == getTarget()) {
			// If our target changes instance, clear target
			if (WatchableProperty.INSTANCE.toString().equals(evt.getPropertyName())) {
				setTarget(null);
			}
			else if (WatchableProperty.HEALTH.toString().equals(evt.getPropertyName())) {
				onTargetHealthChange(getTarget(), ((Integer) evt.getNewValue()) - ((Integer) evt.getOldValue()));
			}
		}
		else {
			logger.warn("PropertyChangeEvent from non-target");
		}
	}
}
