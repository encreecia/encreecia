package org.fealdia.orpg.server.maps.entities;


public class Money extends Item {
	public Money() {
		setFace("valuables/coins_copper");
		setName("coin");
		setValue(1);
	}

	@Override
	public boolean stacksWith(Item other) {
		return (other instanceof Money);
	}

	@Override
	public boolean isStackable() {
		return true;
	}
	
}
