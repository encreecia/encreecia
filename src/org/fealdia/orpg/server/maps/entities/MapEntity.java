package org.fealdia.orpg.server.maps.entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.ArchetypeManager2;
import org.fealdia.orpg.common.maps.Mobility;
import org.fealdia.orpg.common.maps.SceneEntity;
import org.fealdia.orpg.common.util.StreamUtil;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.maps.Instance;
import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;

/**
 * Describes an entity in an Instance.
 * 
 * Note: this class is responsible for notifying any creatures/players when it moves/reappears.
 */
public abstract class MapEntity {
	private static long nextID = 0;
	private final static Logger logger = Logger.getLogger(MapEntity.class);
	
	private final static String DEFAULT_NAME = "entity";
	
	public enum Slot {
		NONE,
		WEAPON,
		AMMO,
		// Armor slots
		HELMET,
		CHEST,
		SHIELD,
	}
	
	public class OverlayOnlyException extends IOException {
		private static final long serialVersionUID = 1L;
	};
	
	private int x = 0;
	private int y = 0;
	private Instance instance;
	private long id;
	
	private boolean visible = true; // whether this is visible to creatures
	
	private String archetype = null;
	
	private String face = "misc/orb";
	private String name = DEFAULT_NAME;
	
	// whether this entity can be saved to file. False for entities loaded from scene entity list. True for everything created later on.
	private boolean saveable = true;
	
	/**
	 * How far players are able to see. The visible area on client side is 2*x+1.
	 */
	public static final int VISIBLE_RADIUS = 10;
	
	public MapEntity() {
		setID(createNextID());
		logger.trace("Map entity created with id " + id + " (" + this.getClass().getName() + ")");
	}
	
	protected static long createNextID() {
		long result = 0;
		synchronized (MapEntity.class) {
			// Check for overflow and throw exception
			if (nextID == Long.MAX_VALUE) {
				throw new RuntimeException("Can not create more map entities; maximum ID reached");
			}
			nextID++;
			result = nextID;
		}
		return result;
	}
	
	/**
	 * Get a number specific for this map entity. No other map entity on the same server has the same ID.
	 * This is mostly used for Client<->Server communication. 
	 */
	final public long getID() {
		return id;
	}
	
	/**
	 * Change this entity's ID. This must only be done when splitting stacks.
	 */
	final protected void setID(long id) {
		this.id = id;
	}
	
	final public int getX() {
		return x;
	}
	
	final public int getY() {
		return y;
	}
	
	/**
	 * Get distance to a coordinate. This means bigger of the differences in x/y coordinates. 
	 */
	final public int distanceTo(int x, int y) {
		return Math.max(Math.abs(this.x - x), Math.abs(this.y - y));
	}
	
	/**
	 * Get distance to another MapEntity.
	 * 
	 * @return distance or -1 if in another instance.
	 */
	final public int distanceTo(MapEntity e) {
		if (e.getInstance() != getInstance()) {
			return -1;
		}
		return Math.max(Math.abs(this.x - e.getX()), Math.abs(this.y - e.getY()));
	}
	
	/**
	 * Return true if this entity can see the target coordinates.
	 * 
	 * No obstruction checks are done (eg. for walls).
	 */
	public boolean canSee(int x, int y) {
		return distanceTo(x, y) <= getVisibleRadius();
	}
	
	/**
	 * Return true if this entity can see the target entity.
	 */
	public boolean canSee(MapEntity e) {
		if (e.getInstance() != getInstance() || (!e.isVisible() && !canSeeInvisible())) {
			return false;
		}
		return canSee(e.getX(), e.getY());
	}
	
	/**
	 * Whether this entity can see invisible entities.
	 */
	public boolean canSeeInvisible() {
		return false;
	}
	
	/**
	 * How far this MapEntity can see. Currently the same for all entities, but should perhaps be zero for anything except Creature. 
	 */
	public int getVisibleRadius() {
		return VISIBLE_RADIUS;
	}
	
	/**
	 * Moves this entity, notifying interested parties. This means interested Creatures and most importantly clients. 
	 */
	public void move(int x, int y) {
		int oldx = getX();
		int oldy = getY();
		
		if (oldx == x && oldy == y) {
			return;
		}
		
		// check destination for a Teleport
		MapEntity tele = getInstance().getEntityByTypeAt(Teleport.class, x, y);
		if (tele != null) {
			Teleport teleport = (Teleport) tele;
			if (teleport.teleportEntity(this)) {
				return;
			}
		}
		
		setLocation(x, y);
		onPositionChanged(x, y);
		
		// Send notification to all Creatures that see either source or destination of our movement.
		for (MapEntity e : getInstance().getEntities()) {
			if (e != this && e instanceof Creature) {
				boolean src_visible = false;
				boolean dst_visible = false;
				if (e.canSee(oldx, oldy)) {
					src_visible = true;
				}
				if (e.canSee(x, y)) {
					dst_visible = true;
				}
				if (src_visible || dst_visible) {
					((Creature)e).onEntityVisibilityChange(this, src_visible, dst_visible, false);
				}
			}
		}
	}
	
	/**
	 * Moves this entity to the same instance and coordinates the target is in.
	 * 
	 * This is used when dropping items for example. 
	 */
	public void moveBelow(MapEntity target) {
		moveToInstance(target.getInstance(), target.getX(), target.getY());
	}
	
	/**
	 * Attempt to "place" this entity at the target square. If blocked, the nearby squares are tried instead.
	 * If all of those are blocked, then the original position is used.
	 *
	 * @param instance instance, or null when moving in current
	 */
	public void moveToOrNearby(Instance inst, int x, int y) {
		if (inst == null) {
			inst = getInstance();
		}
		if (inst == null) {
			moveToInstance(inst, x, y);
			return;
		}
		
		int[][] points_to_check = {
				{ x, y },
				{ x, y - 1 }, // left
				{ x - 1, y }, // up
				{ x + 1, y }, // right
				{ x, y + 1 }  // down
		};
		
		// Try all points
		for (int i = 0; i < points_to_check.length; i++) {
			int tx = points_to_check[i][0];
			int ty = points_to_check[i][1];
			if (!inst.isBlockedFor(tx, ty, getMobility())) {
				moveToInstance(inst, tx, ty);
				return;
			}
		}
		
		// Fall back to moving to the given target
		moveToInstance(inst, x, y);
	}
	
	/**
	 * Moves this entity into another instance and position, notifying interested parties.
	 */
	public void moveToInstance(Instance instance, int x, int y) {
		// If instance does not change, use move() instead
		if (instance != null && instance == getInstance()) {
			move(x, y);
			return;
		}
		Instance oldInstance = getInstance();
		
		// Send disappearance notification to all Creatures that see us in old instance
		if (getInstance() != null) {
			List<MapEntity> entities = getInstance().getEntities();
			synchronized (entities) {
				for (MapEntity e : entities) {
					if (e != this && e instanceof Creature && e.canSee(getX(), getY())) {
						((Creature)e).onEntityVisibilityChange(this, true, false, false);
					}
				}
			}
		}
		
		setInstance(instance);
		setLocation(x, y);
		onPositionChanged(x, y);
		
		// Send appearance notification to all Creatures that see us in the new instance
		if (getInstance() != null) {
			List<MapEntity> entities = getInstance().getEntities();
			synchronized (entities) {
				for (MapEntity e : entities) {
					if (e != this && e instanceof Creature && e.canSee(getX(), getY())) {
						((Creature)e).onEntityVisibilityChange(this, false, true, false);
					}
				}
			}
		}
	}
	
	/**
	 * Move this entity out of any instance, in preparation to being picked up or deleted.
	 */
	public void moveOutOfWorld() {
		moveToInstance(null, 0, 0);
	}

	/**
	 * Changes the location of this entity, without notifying anyone. 
	 */
	protected void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		// When instance changes, remove this from it and add to new
		if (this.instance != null && this.instance != instance) {
			this.instance.removeEntity(this);
		}
		
		this.instance = instance;
		
		// Add to new instance, if it's not null
		if (instance != null) {
			instance.addEntity(this);
		}
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
		
		// TODO notify nearby creatures
	}
	
	/**
	 * Name of this entity. Not unique!
	 */
	public String getName() {
		return name;
	}
	
	public String getFace() {
		return face;
	}
	
	public void setFace(String face) {
		this.face = face;

		if (getVisibleFace() == getFace()) {
			notifyPlayersOfEntityChange();
		}
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Load this entity from the given stream.
	 * 
	 * When &lt;/entity> is encountered, this method returns.
	 * @param ignore_overlay whether to throw exception when "overlay" property is encountered
	 * @throws IOException 
	 */
	public void loadFromStream(BufferedReader in, boolean ignore_overlay) throws IOException {
		boolean overlay = false;
		for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
			if (line.startsWith("</")) {
				if (overlay) {
					throw new OverlayOnlyException();
				}
				return;
			}
			
			// The stream contains some sub-element?
			if (line.startsWith("<")) {
				handlePropertyChild(line.substring(1, line.indexOf('>')), in);
				continue;
			}
			
			String[] parts = line.split(" +", 2);
			if ("overlay".equals(parts[0])) {
				overlay = ignore_overlay;
				setSaveable(true); // Only loaded once when there is no overlay, so must be saveable
			}
			else if (!handlePropertyEdit(parts[0], parts[1])) {
				logger.warn("Unhandled line (entitytype " + getEntityType() + "): " + line);
			}
		}
	}
	
	/**
	 * When '<foo>' is encountered while loading entity, this is called with 'foo' as type.
	 * Should return when a line with '</' is encountered.
	 */
	public boolean handlePropertyChild(String type, BufferedReader in) throws IOException {
		logger.error("Unhandled property child: " + type);
		return false;
	}
	
	/**
	 * Handle property edit, eg. when loading from file.
	 * 
	 * Subclasses should override this, handle any added properties and then call super.
	 */
	public boolean handlePropertyEdit(String key, String value) {
		if ("x".equals(key)) {
			setLocation(Integer.parseInt(value), getY());
		}
		else if ("y".equals(key)) {
			setLocation(getX(), Integer.parseInt(value));
		}
		else if ("face".equals(key)) {
			setFace(value);
		}
		else if ("name".equals(key)) {
			setName(value);
		}
		else if ("visible".equals(key)) {
			setVisible(Boolean.parseBoolean(value));
		}
		else {
			return false;
		}
		return true;
	}
	
	public void save(PropertySaveListener listener) throws Exception {
		if (getInstance() != null) {
			listener.saveProperty("x", getX());
			listener.saveProperty("y", getY());
		}
		if (!DEFAULT_NAME.equals(getName()) && (getArchetype() == null || !getArchetype().equals(getName()))) {
			saveProperty(listener, "name", getName());
		}
		saveProperty(listener, "face", getFace());
	}

	final public void saveWithHeader(PropertySaveListener listener) throws Exception {
		if (getArchetype() != null) {
			listener.startEntity(getArchetype());
			save(listener);
			listener.endEntity();
		}
		else {
			listener.startClass(getEntityType());
			save(listener);
			listener.endClass();
		}
	}

	final public void saveProperty(PropertySaveListener listener, final String name, final Object value) throws Exception {
		// Check against the archetype default (if any), if same -> don't save
		if (getArchetype() != null) {
			Map<String,String> arch = ArchetypeManager2.getInstance().getFullArchetype(getArchetype());

			// Same as in archetype, don't save
			if (String.valueOf(value).equals(arch.get(name))) {
				return;
			}
		}

		// Different from archetype -> save
		listener.saveProperty(name, value);
	}

	/**
	 * Save properties of this entity to the given stream.
	 * 
	 * Subclasses should override this, call super and then write any added properties.
	 * @throws IOException 
	 */
	public void saveToStream(Writer out) throws IOException {
		// If in instance, write location properties
		if (getInstance() != null) {
			out.write("x " + getX() + "\n");
			out.write("y " + getY() + "\n");
		}
		if (!DEFAULT_NAME.equals(getName()) && (getArchetype() == null || !getArchetype().equals(getName()))) {
			savePropertyToStream(out, "name", getName(), false);
		}
		savePropertyToStream(out, "face", getFace(), false);
	}

	/**
	 * Save property to stream, if different from archetype and global default.
	 */
	public void savePropertyToStream(Writer out, String key, Object value, boolean equals_global_default) throws IOException {
		// Check against the archetype default (if any), if same -> don't save
		if (getArchetype() != null) {
			Map<String,String> arch = ArchetypeManager2.getInstance().getFullArchetype(getArchetype());
			
			// Same as in archetype, don't save
			if (String.valueOf(value).equals(arch.get(key))) {
				return;
			}
		}
		
		// Check against the global default, if same -> don't save
		if (equals_global_default) {
			return;
		}
		
		// Different from archetype and the global default -> save
		out.write(key + " " + value + "\n");
	}
	
	/**
	 * Create an entity from archetype, or return null on failure.
	 */
	public static MapEntity createFromArchetype(String name) {
		MapEntity result = null;
		String className = null;
		
		Map<String,String> archetype = ArchetypeManager2.getInstance().getFullArchetype(name);
		// If no such archetype, assume type is class name
		if (archetype == null) {
			className = name;
		}
		// Get class name from archetype "class" property
		else {
			className = archetype.get("class");
		}

		if (className != null) {
			MapEntity en = createInstanceByEntityType(className);
			if (en != null) {
				result = en;
				
				// Set default name to archetype name
				result.setName(name);
				result.setArchetype(name);
			}
			else {
				logger.error("createFromArchetype does not support class '" + className + "' (archetype = " + name + ")");
			}
			
			if (result != null) {
				for (Map.Entry<String,String> pair : archetype.entrySet()) {
					if ("class".equals(pair.getKey())) {
						continue;
					}
					if (!result.handlePropertyEdit(pair.getKey(), pair.getValue())) {
						logger.warn("createFromArchetype: failed to set property " + pair.getKey() + " (archetype = " + name + ")");
					}
				}
			}
		}
		
		return result;
	}
	
	public static MapEntity createFromSceneEntity(final SceneEntity sceneEntity) {
		// create MapEntity based on archetype first
		MapEntity result = createFromArchetype(sceneEntity.getArchetype());

		// Items originally on Scene (not dropped by players) should not decay
		if (result instanceof Item) {
			((Item) result).setDecayable(false);
		}

		// Entities created from Scene are not saveable by default (otherwise they would multiply every time instance is loaded)
		result.setSaveable(false);

		// Override all properties from SceneEntity
		result.setLocation(sceneEntity.getX(), sceneEntity.getY());
		for (Entry<String,String> entry : sceneEntity.getProperties().entrySet()) {
			if (entry.getKey().equals("overlay")) {
				result.setSaveable(true); // Only loaded once when there is no overlay, so must be saveable
				continue;
			}
			result.handlePropertyEdit(entry.getKey(), entry.getValue());
		}

		return result;
	}

	/**
	 * Inheriting classes may need to override this.
	 * 
	 * This implementation returns last part of the class name in lowercase.
	 * 
	 * @see createInstanceByEntityType
	 */
	public String getEntityType() {
		String[] parts = getClass().getName().split("\\.");
		return parts[parts.length - 1].toLowerCase(); // last part
	}
	
	/**
	 * Create a MapEntity by name.
	 */
	public static MapEntity createInstanceByEntityType(String entityType) {
		if ("item".equals(entityType)) {
			return new Item();
		}
		else if ("armor".equals(entityType)) {
			return new Armor();
		}
		else if ("container".equals(entityType)) {
			return new Container();
		}
		else if ("money".equals(entityType)) {
			return new Money();
		}
		else if ("monster".equals(entityType)) {
			return new Monster();
		}
		else if ("potion".equals(entityType)) {
			return new Potion();
		}
		else if ("scroll".equals(entityType)) {
			return new Scroll();
		}
		else if ("shopkeeper".equals(entityType)) {
			return new ShopKeeper();
		}
		else if ("sign".equals(entityType)) {
			return new Sign();
		}
		else if ("spawn".equals(entityType)) {
			return new Spawn();
		}
		else if ("teleport".equals(entityType)) {
			return new Teleport();
		}
		else if ("playerspawn".equals(entityType)) {
			return new PlayerSpawn();
		}
		else if ("weapon".equals(entityType)) {
			return new Weapon();
		}
		else if ("wearable".equals(entityType)) {
			return new Wearable();
		}
		else if ("player".equals(entityType)) {
			return new Player();
		}
		
		logger.error("No class found for entity type: " + entityType);
		return null;
	}
	
	/**
	 * Get a list of creatures who see me.
	 */
	public List<Creature> getCreaturesWhoSeeMe() {
		List<Creature> result = new LinkedList<Creature>();
		
		if (getInstance() == null) {
			return result;
		}
		
		List<MapEntity> entities = getInstance().getEntities();
		synchronized (entities) {
			for (MapEntity en : entities) {
				if (en instanceof Creature) {
					Creature c = (Creature) en;
					if (c.canSee(this)) {
						result.add(c);
					}
				}
			}
		}
		
		return result;
	}

	public boolean isSaveable() {
		return saveable;
	}

	public void setSaveable(boolean saveable) {
		this.saveable = saveable;
	}

	/**
	 * Get the original archetype name this entity was created from.
	 */
	public String getArchetype() {
		return archetype;
	}

	public void setArchetype(String archetype) {
		this.archetype = archetype;
	}
	
	public void saveToStreamWithHeader(Writer out) throws IOException {
		// New format when archetype is known
		if (getArchetype() != null) {
			out.write("entity " + getArchetype() + "\n");
			saveToStream(out);
			out.write("</entity>\n\n");
		}
		// Old compatibility format when archetype is not known
		else {
			out.write("<" + getEntityType() + ">\n");
			saveToStream(out);
			out.write("</" + getEntityType() + ">\n\n");
		}
	}
	
	/**
	 * Called after our position has changed.
	 */
	protected void onPositionChanged(int x, int y) {}
	
	/**
	 * Get the face that is seen by clients.
	 */
	public String getVisibleFace() {
		return getFace();
	}
	
	/**
	 * Notify all nearby players of our changed status.
	 */
	public void notifyPlayersOfEntityChange() {
		// Notify all nearby players
		List<Creature> creatures = getCreaturesWhoSeeMe();
		for (Creature c : creatures) {
			if (c instanceof Player) {
				Player pl = (Player) c;
				pl.onEntityChanged(this);
			}
		}
	}
	
	/**
	 * Get the Mobility of this entity, NORMAL for everything except Creature.
	 */
	public Mobility getMobility() {
		return Mobility.NORMAL;
	}
	
	/**
	 * Load a number of entities from stream. Used by: container, instance overlay, player inventory.
	 * This should be called after '<inventory>', '<items>' line or similar has been read. Returns when a line starting '</' is found.
	 */
	public static List<MapEntity> loadEntitiesFromStream(BufferedReader in) throws IOException {
		List<MapEntity> res = new LinkedList<MapEntity>();
		
		for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
			if (line.startsWith("</")) {
				return res;
			}
			else if (line.startsWith("entity ")) {
				String archetype = line.substring("entity ".length());
				MapEntity en = createFromArchetype(archetype);
				en.loadFromStream(in, false);
				res.add(en);
				continue;
			}
			else if (line.startsWith("<")) {
				String entityType = line.substring(1, line.indexOf('>'));
				MapEntity en = createInstanceByEntityType(entityType);
				en.loadFromStream(in, false);
				res.add(en);
				continue;
			}
		}

		return res;
	}
	
	/**
	 * Called when an entity is doubleclicked (used) on client side. This can mean talking to NPCs or using items.
	 */
	public boolean onGroundUse(Player player) {
		return false;
	}
	
	/**
	 * Dump all private properties to the stream using reflection.
	 */
	public StringBuffer dumpToBuffer() {
		StringBuffer buf = new StringBuffer();
		
		for (Class<?> cl = getClass(); cl != Object.class; cl = cl.getSuperclass()) {
			buf.append("=== Class " + cl.getName() + "\n");
			
			for (Field field : cl.getDeclaredFields()) {
				field.setAccessible(true);
				buf.append(field.getName());
				try {
					buf.append(" = " + field.get(this));
				} catch (Exception e) {
					logger.error(e);
				}
				buf.append(" [" + field.toString() + "]");
				buf.append("\n");
				field.setAccessible(false);
			}
		}
		
		// TODO do the same for private members of parent classes
		return buf;
	}
}
