package org.fealdia.orpg.server.maps.entities;


/**
 * Entity that must be animated by the instance: Monster, Player, Spawn.
 */
public abstract class ActionEntity extends MapEntity {
	/**
	 * Called every 100ms to give this entity a chance to act. For monsters
	 * this means moving towards target, for players to act out current orders
	 * (like move) etc.
	 */
	public void action() {
	}
}
