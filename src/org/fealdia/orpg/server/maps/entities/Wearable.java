package org.fealdia.orpg.server.maps.entities;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;


/**
 * Base class for wearables (armor, weapon, ammo, etc)
 */
public class Wearable extends Item {
	// Needed only for ammo & ranged weapons, should perhaps be moved into a subclass
	private AmmoType ammoType = AmmoType.NONE;
	
	private boolean equipped = false;
	
	private Slot slot = Slot.NONE;
	
	private int level = 1;
	
	public Slot getSlot() {
		return slot;
	}

	public void setSlot(Slot slot) {
		this.slot = slot;
	}

	public boolean isEquipped() {
		return equipped;
	}

	public void setEquipped(boolean equipped) {
		this.equipped = equipped;
	}

	public AmmoType getAmmoType() {
		return ammoType;
	}

	public void setAmmoType(AmmoType ammoType) {
		this.ammoType = ammoType;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("ammotype".equals(key)) {
			setAmmoType(AmmoType.valueOf(value.toUpperCase()));
		}
		else if ("equipped".equals(key)) {
			setEquipped(Integer.parseInt(value) > 0);
		}
		else if ("level".equals(key)) {
			setLevel(Integer.parseInt(value));
		}
		else if ("slot".equals(key)) {
			setSlot(Slot.valueOf(value.toUpperCase()));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		
		savePropertyToStream(out, "ammotype", getAmmoType().toString().toLowerCase(), getAmmoType() == AmmoType.NONE);
		savePropertyToStream(out, "level", getLevel(), getLevel() == 1);
		if (isEquipped()) {
			out.write("equipped 1\n");
		}
		savePropertyToStream(out, "slot", getSlot().toString().toLowerCase(), getSlot() == getDefaultSlot());
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		if (getAmmoType() != AmmoType.NONE) {
			saveProperty(listener, "ammotype", getAmmoType().toString().toLowerCase());
		}
		if (getLevel() != 1) {
			saveProperty(listener, "level", getLevel());
		}
		if (isEquipped()) {
			saveProperty(listener, "equipped", "1");
		}
		if (getSlot() != getDefaultSlot()) {
			saveProperty(listener, "slot", getSlot().toString().toLowerCase());
		}
	}

	@Override
	public List<String> getExamineProperties() {
		List<String> props = super.getExamineProperties();
		if (getLevel() > 1) {
			props.add("level " + getLevel());
		}
		if (getSlot() != Slot.NONE) {
			props.add(getSlot().toString().toLowerCase() + " slot");
		}
		return props;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
}
