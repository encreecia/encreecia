package org.fealdia.orpg.server.maps.entities.persistence;

import java.io.OutputStream;
import java.util.Collection;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.fealdia.orpg.server.maps.entities.MapEntity;

public class XMLEntityWriter implements PropertySaveListener {
	private XMLStreamWriter writer;
	private int indent = 0;

	public XMLEntityWriter(final OutputStream out) throws XMLStreamException, FactoryConfigurationError {
		writer = XMLOutputFactory.newInstance().createXMLStreamWriter(out);
	}

	public void close() throws XMLStreamException {
		writer.flush();
		writer.close();
	}

	public void writeEntities(Collection<MapEntity> entities) throws Exception {
		startElement("entities");
		for (MapEntity mapEntity : entities) {
			mapEntity.saveWithHeader(this);
		}
		endElement();
	}

	public void writeEntity(String rootElement, MapEntity entity) throws Exception {
		startElement(rootElement);
		entity.save(this);
		endElement();
	}

	public void saveProperty(String name, Object value) throws XMLStreamException {
		writeIndent();
		writer.writeStartElement(name);
		writer.writeCharacters(value.toString());
		writer.writeEndElement();
		writer.writeCharacters("\n");
	}

	public void startElement(String element) throws Exception {
		writeIndent();
		writer.writeStartElement(element);
		writer.writeCharacters("\n");
		indent++;
	}

	public void startEntity(String archetype) throws XMLStreamException {
		writeIndent();
		writer.writeStartElement("entity");
		writer.writeAttribute("archetype", archetype);
		writer.writeCharacters("\n");
		indent++;
	}

	public void endEntity() throws XMLStreamException {
		indent--;
		writeIndent();
		writer.writeEndElement();
		writer.writeCharacters("\n");
	}

	private void writeIndent() throws XMLStreamException {
		for (int i = 0; i < indent; i++) {
			writer.writeCharacters("  ");
		}
	}

	public void startClass(String clazz) throws Exception {
		writeIndent();
		writer.writeStartElement("entity");
		writer.writeAttribute("type", clazz);
		writer.writeCharacters("\n");
		indent++;
	}

	public void endClass() throws Exception {
		indent--;
		writeIndent();
		writer.writeEndElement();
		writer.writeCharacters("\n");
	}

	public void endElement() throws Exception {
		indent--;
		writeIndent();
		writer.writeEndElement();
		writer.writeCharacters("\n");
	}
}
