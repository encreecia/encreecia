package org.fealdia.orpg.server.maps.entities.persistence;

public interface PropertySaveListener {
	public void startElement(String element) throws Exception;

	public void startEntity(String archetype) throws Exception;

	public void startClass(String clazz) throws Exception;

	public void saveProperty(String name, Object value) throws Exception;

	public void endElement() throws Exception;

	public void endEntity() throws Exception;

	public void endClass() throws Exception;
}
