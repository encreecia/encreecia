package org.fealdia.orpg.server.maps.entities.persistence;

import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.maps.entities.Item;
import org.fealdia.orpg.server.maps.entities.MapEntity;

public class XmlReaders {
	public static Player processPlayer(XMLStreamReader reader) throws XMLStreamException {
		while (reader.hasNext()) {
			reader.next();

			switch (reader.getEventType()) {
				case XMLStreamReader.START_ELEMENT:
					String name = reader.getLocalName();
					if ("player".equals(name)) {
						return (Player) processEntity(reader);
					}
					else {
						throw new RuntimeException("Unexpected start element: " + name);
					}
				default:
					throw new RuntimeException("Unexpected eventtype for player: " + reader.getEventType());
			}
		}

		throw new RuntimeException("Unexpected reader exit");
	}

	public static List<MapEntity> processEntities(XMLStreamReader reader) throws XMLStreamException {
		return processEntities(reader, "entities");
	}

	public static List<MapEntity> processEntities(XMLStreamReader reader, final String rootElement) throws XMLStreamException {
		List<MapEntity> entities = new LinkedList<MapEntity>();

		while (reader.hasNext()) {
			reader.next();

			switch (reader.getEventType()) {
				case XMLStreamReader.START_ELEMENT:
					String name = reader.getLocalName();
					if (rootElement.equals(name)) {
						// Ok
					}
					else if ("entity".equals(name)) {
						entities.add(processEntity(reader));
					}
					else {
						throw new RuntimeException("Unexpected element: " + name);
					}
					break;
				case XMLStreamReader.END_ELEMENT:
					String name2 = reader.getLocalName();
					if (rootElement.equals(name2)) {
						// Ok
						return entities;
					}
					else {
						throw new RuntimeException("Unexpected end element: " + name2);
					}
				case XMLStreamReader.CHARACTERS:
					String text = reader.getText();
					if (!text.matches("[ \n]*")) {
						throw new RuntimeException("Expected whitespace, got: " + text);
					}
					break;
				default:
					throw new RuntimeException("Unexpected eventtype: " + reader.getEventType());
			}
		}

		throw new RuntimeException("Unexpected end for processEntities");
	}

	// Called after <entity> element has been found
	public static MapEntity processEntity(XMLStreamReader reader) throws XMLStreamException {
		final String archetype = reader.getAttributeValue(null, "archetype");
		final String type = reader.getAttributeValue(null, "type");

		MapEntity entity;

		if (archetype != null) {
			entity = MapEntity.createFromArchetype(archetype);
		}
		else if (type != null) {
			entity = MapEntity.createInstanceByEntityType(type);
		}
		else if ("player".equals(reader.getLocalName())) {
			entity = new Player();
		}
		else {
			throw new RuntimeException("Entity contains neither archetype nor type, and is not a player");
		}

		while (reader.hasNext()) {
			reader.next();

			switch (reader.getEventType()) {
				case XMLStreamReader.START_ELEMENT:
					String name = reader.getLocalName();
					if ("inventory".equals(name) && entity instanceof Player) {
						Player player = (Player) entity;
						List<MapEntity> entities = processEntities(reader, "inventory");
						player.getInventory().clear();
						for (MapEntity e : entities) {
							player.getInventory().add((Item) e);
						}
					}
					else {
						String value = reader.getElementText();
						entity.handlePropertyEdit(name, value);
					}
					break;
				case XMLStreamReader.END_ELEMENT:
					String endName = reader.getLocalName();
					if ("entity".equals(endName) || "player".equals(endName)) {
						return entity;
					}
					else {
						throw new RuntimeException("Unexpected end element: " + endName);
					}
				case XMLStreamReader.CHARACTERS:
					String text = reader.getText();
					if (!text.matches("[ \n]*")) {
						throw new RuntimeException("Expected whitespace, got: " + text);
					}
					break;
				default:
					throw new RuntimeException("Unexpected eventtype in entity: " + reader.getEventType());
			}
		}

		return entity;
	}
}
