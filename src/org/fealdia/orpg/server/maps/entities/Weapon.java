package org.fealdia.orpg.server.maps.entities;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Random;

import org.fealdia.orpg.server.maps.entities.Creature.Race;
import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;


/**
 * Equippable weapon.
 */
public class Weapon extends Wearable {
	public final static int DEFAULT_SPEED = 50; // 50/100 = 0.5 sec
	
	private int weaponDamage_min = 1;
	private int weaponDamage_max = 1;
	private int weaponRange = 1;
	private int weaponSpeed = DEFAULT_SPEED;
	
	private Race slay = Race.NONE;
	
	public Weapon() {
		setSlot(Slot.WEAPON);
	}
	
	/**
	 * Calculate randomly how much damage the weapon does.
	 */
	public int calculateWeaponDamage(Creature target) {
		int min = getWeaponDamageMin();
		int max = getWeaponDamageMax();
		int diff = max - min;
		
		int damage = min + new Random().nextInt(diff + 1);
		
		if (getSlay() != Race.NONE && target.getRace() == getSlay()) {
			damage = 2 * damage;
		}
		
		return damage;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("weaponrange".equals(key)) {
			setWeaponRange(Integer.parseInt(value));
		}
		else if ("weapondamage".equals(key)) {
			String[] parts = value.split(" +", 2);
			
			setWeaponDamage(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
		}
		else if ("weaponspeed".equals(key)) {
			setWeaponSpeed(Integer.parseInt(value));
		}
		else if ("slay".equals(key)) {
			setSlay(Race.valueOf(value.toUpperCase()));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		
		savePropertyToStream(out, "slay", getSlay().toString().toLowerCase(), getSlay() == Race.NONE);
		savePropertyToStream(out, "weapondamage", getWeaponDamageMin() + " " + getWeaponDamageMax(), false);
		savePropertyToStream(out, "weaponrange", getWeaponRange(), getWeaponRange() == 1);
		savePropertyToStream(out, "weaponspeed", getWeaponSpeed(), getWeaponSpeed() == DEFAULT_SPEED);
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		if (getSlay() != Race.NONE) {
			saveProperty(listener, "slay", getSlay().toString().toLowerCase());
		}
		saveProperty(listener, "weapondamage", getWeaponDamageMin() + " " + getWeaponDamageMax());
		if (getWeaponRange() != 1) {
			saveProperty(listener, "weaponrange", getWeaponRange());
		}
		if (getWeaponSpeed() != DEFAULT_SPEED) {
			saveProperty(listener, "weaponspeed", getWeaponSpeed());
		}
	}

	public int getWeaponDamageMin() {
		return weaponDamage_min;
	}
	
	public int getWeaponDamageMax() {
		return weaponDamage_max;
	}

	public int getWeaponSpeed() {
		return weaponSpeed;
	}

	public void setWeaponDamage(int weaponDamage_min, int weaponDamage_max) {
		this.weaponDamage_min = weaponDamage_min;
		this.weaponDamage_max = weaponDamage_max;
	}

	public void setWeaponSpeed(int weaponSpeed) {
		this.weaponSpeed = weaponSpeed;
	}

	public int getWeaponRange() {
		return weaponRange;
	}

	public void setWeaponRange(int weaponRange) {
		this.weaponRange = weaponRange;
	}
	
	public boolean isNeedingAmmo() {
		return getAmmoType() != AmmoType.NONE;
	}

	@Override
	public List<String> getExamineProperties() {
		List<String> props = super.getExamineProperties();
		props.add(getWeaponDamageMin() + "-" + getWeaponDamageMax() + " damage");
		props.add(getWeaponSpeed() + " speed");

		// DPS
		float speedFactor = 1 / ((float) getWeaponSpeed() / 10);
		float mindps = getWeaponDamageMin() * speedFactor;
		float maxdps = getWeaponDamageMax() * speedFactor;
		props.add(mindps + "-" + maxdps + " dps");
		
		if (getWeaponRange() != 1) {
			props.add(getWeaponRange() + " range");
		}
		if (getSlay() != Race.NONE) {
			props.add("slays " + getSlay().toString().toLowerCase());
		}
		
		return props;
	}

	public Race getSlay() {
		return slay;
	}

	public void setSlay(Race slay) {
		this.slay = slay;
	}

	@Override
	public Slot getDefaultSlot() {
		return Slot.WEAPON;
	}
	
}
