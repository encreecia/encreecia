package org.fealdia.orpg.server.maps.entities;

import java.util.LinkedList;
import java.util.List;


/**
 * NPC that buys and sells items.
 */
public class ShopKeeper extends Monster {
	private List<Item> shopList = new LinkedList<Item>();
	private int ratio = 75; // % of value to give for sold items
	
	/**
	 * Get price that this ShopKeeper asks for the item.
	 */
	public int getBuyPrice(Item item) {
		return item.getValue();
	}

	/**
	 * Get price that this ShopKeeper would pay for the item.
	 */
	public int getSellPrice(Item item) {
		return (item.getStackValue() * ratio) / 100;
	}
	
	public int getRatio() {
		return ratio;
	}
	
	public List<Item> getShopList() {
		return shopList;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("item".equals(key)) {
			Item i = (Item) MapEntity.createFromArchetype(value);
			shopList.add(i);
		}
		else if ("items".equals(key)) {
			shopList.clear();
			String[] parts = value.split(" ");
			for (String p : parts) {
				Item i = (Item) MapEntity.createFromArchetype(p);
				shopList.add(i);
			}
		}
		else if ("ratio".equals(key)) {
			setRatio(Integer.parseInt(value));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}
	
	public void setRatio(int ratio) {
		this.ratio = ratio;
	}

	public Item getItemByID(long id) {
		for (Item i : getShopList()) {
			if (i.getID() == id) {
				return i;
			}
		}
		return null;
	}
	
}
