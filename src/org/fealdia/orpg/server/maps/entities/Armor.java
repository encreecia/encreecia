package org.fealdia.orpg.server.maps.entities;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;


/**
 * Wearable item that blocks damage.
 */
public class Armor extends Wearable {
	private int block = 0;

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("block".equals(key)) {
			setBlock(Integer.parseInt(value));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	/**
	 * How many points of damage this armor can block.
	 */
	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		
		savePropertyToStream(out, "block", getBlock(), block == 0);
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		if (getBlock() != 0) {
			saveProperty(listener, "block", getBlock());
		}
	}

	@Override
	public List<String> getExamineProperties() {
		List<String> props = super.getExamineProperties();
		props.add("block " + getBlock());
		return props;
	}
	
}
