package org.fealdia.orpg.server.maps.entities;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.Spell;
import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;

public class Scroll extends Consumable {
	private Spell spell;
	private String text;
	
	public Spell getSpell() {
		return spell;
	}

	/**
	 * Get text on the scroll, if any.
	 */
	public String getText() {
		return text;
	}
	
	@Override
	public List<String> getExamineProperties() {
		List<String> props = super.getExamineProperties();
		if (getSpell() != null) {
			props.add("contains a spell");
		}
		if (getText() != null) {
			props.add("says '" + getText() + "'");
		}
		
		return props;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("text".equals(key)) {
			setText(value);
		}
		else if ("spell".equals(key)) {
			setSpell(Spell.valueOf(value.toUpperCase()));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		
		savePropertyToStream(out, "text", getText(), false);
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		saveProperty(listener, "text", getText());
	}

	public void setSpell(Spell spell) {
		this.spell = spell;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public boolean use(Player player) {
		player.sendText("You read the scroll");
		
		if (getText() != null) {
			player.sendText("It says: '" + getText() + "'");
		}
		
		// Cast the spell, if any
		if (getSpell() != null) {
			return player.castSpell(getSpell());
		}
		return false;
	}

}
