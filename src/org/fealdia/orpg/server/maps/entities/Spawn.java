package org.fealdia.orpg.server.maps.entities;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

/**
 * Responsible for spawning monsters.
 *
 * TODO Spawn should not be spawning monsters when there are no players around - it should be woken up when someone comes nearby
 */
public class Spawn extends ActionEntity {
	private static Logger logger = Logger.getLogger(Spawn.class);
	
	private static int DEFAULT_INTERVAL = 450; // 45 secs
	
	private int interval = DEFAULT_INTERVAL;
	private int intervalD = 0; // next interval is always interval + rand(intervald)
	private int interval_left = 0;
	
	private long lastSpawn_id = 0;
	
	private int radius = 0;
	
	private String spawnArchetype = null;
	
	private Map<String,String> spawnProperties = new HashMap<String,String>();
	
	public Spawn() {
		setVisible(false);
	}
	
	@Override
	public void action() {
		// Return if our last spawn is alive
		if (lastSpawn_id != 0 && getInstance().getEntityByID(lastSpawn_id) != null) {
			return;
		}
		
		if (interval_left == 0) {
			doSpawn();
			interval_left = getInterval();
			if (getIntervalD() > 0) {
				interval_left += new Random().nextInt(getIntervalD());
			}
		}
		else {
			interval_left--;
		}
	}

	public void doSpawn() {
		String archetype = getArchetype();
		if (archetype == null) {
			return;
		}
		
		Monster monster = (Monster) MapEntity.createFromArchetype(archetype);
		if (monster == null) {
			logger.warn("Unable to spawn monster");
			return;
		}
		
		// set the overridden properties
		for (Entry<String,String> i : spawnProperties.entrySet()) {
			monster.handlePropertyEdit(i.getKey(), i.getValue());
		}
		
		int spawn_x = getX();
		int spawn_y = getY();
		
		if (getRadius() > 0) {
			// Random position:
			// x = baseX - r + [0,2*r]
			
			Random r = new Random();
			spawn_x = getX() - getRadius() + r.nextInt(getRadius()*2 + 1);
			spawn_y = getY() - getRadius() + r.nextInt(getRadius()*2 + 1);
			
			// Check if the target position is blocked
			if (getInstance().isBlockedFor(spawn_x, spawn_y, monster.getMobility())) {
				spawn_x = getX();
				spawn_y = getY();
			}
		}
		logger.trace("Spawning " + archetype + " at " + spawn_x + "," + spawn_y);
		
		monster.setSaveable(false);
		monster.setSpawnLocation(spawn_x, spawn_y);
		// Body decays 1/3 of interval before next spawn, but not before 10 secs has passed
		monster.setBodyDecayTime(Math.max(10, ((getInterval() / 10) * 2) / 3));
		monster.moveToOrNearby(getInstance(), spawn_x, spawn_y);
		monster.checkTarget(null);
		
		lastSpawn_id = monster.getID();
	}
	
	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
		interval_left = 0;
	}

	public String getSpawnArchetype() {
		return spawnArchetype;
	}

	public void setSpawnArchetype(String archetype) {
		this.spawnArchetype = archetype;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("archetype".equals(key)) {
			setArchetype(value);
		}
		else if ("interval".equals(key)) {
			setInterval(Integer.parseInt(value));
		}
		else if ("intervald".equals(key)) {
			setIntervalD(Integer.parseInt(value));
		}
		else if ("radius".equals(key)) {
			setRadius(Integer.parseInt(value));
		}
		// Properties for the spawned creature
		else if (key.startsWith("_")) {
			spawnProperties.put(key.substring(1), value);
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getIntervalD() {
		return intervalD;
	}

	public void setIntervalD(int intervald) {
		this.intervalD = intervald;
	}
	
	
}
