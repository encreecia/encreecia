package org.fealdia.orpg.server.maps.entities;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.util.StringUtil;
import org.fealdia.orpg.server.ServerConfig;
import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;

/**
 * Item that can be picked up, dropped on ground etc. This includes pretty much everything that doesn't move, apart from the background.
 */
public class Item extends MapEntity implements Cloneable {
	// For ranged weapons + ammo
	public enum AmmoType {
		NONE,
		ARROW,
	}

	private static Logger logger = Logger.getLogger(Item.class);

	/**
	 * How many seconds items stay dropped on the ground until they disappear for eternity.
	 */
	protected final static long DECAY_TIME = ServerConfig.getInstance().getPropertyAsInt("instance.decay_time");
	
	private final static long DEFAULT_WEIGHT = 1;
	
	private long decayTime = DECAY_TIME;
	private boolean decayable = true;
	
	private boolean pickup = true;

	// How many items are in this stack
	private int quantity = 1;
	
	private boolean stackable = false;
	
	// When this item was dropped/put on the ground
	private long stamp = 0;
	
	private int value = 0;
	
	private long weight = DEFAULT_WEIGHT;
	
	public Item() {
		updateStamp();
	}
	
	public boolean canPickUp() {
		return pickup;
	}

	public long getStamp() {
		return stamp;
	}

	@Override
	protected void setLocation(int x, int y) {
		super.setLocation(x, y);
	}
	
	public void updateStamp() {
		stamp = System.currentTimeMillis() / 1000;
	}
	
	/**
	 * Whether or not it is time to remove this object from the instance.
	 */
	public boolean canDecay() {
		if (getInstance() == null) {
			return false;
		}
		long now = System.currentTimeMillis() / 1000;
		return (isDecayable() && getStamp() + getDecayTime() < now);
	}
	
	public void decay() {
		logger.trace(getID() + "[" + getName() + "]: decayed from the world");
		moveOutOfWorld();
	}
	
	public void setPickUp(boolean pickup) {
		this.pickup = pickup;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("pickup".equals(key)) {
			setPickUp(Integer.parseInt(value) > 0);
		}
		else if ("quantity".equals(key)) {
			setQuantity(Integer.parseInt(value));
		}
		else if ("stackable".equals(key)) {
			setStackable(Integer.parseInt(value) > 0);
		}
		else if ("value".equals(key)) {
			setValue(Integer.parseInt(value));
		}
		else if ("weight".equals(key)) {
			setWeight(Long.parseLong(value));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	@Override
	public void saveToStream(Writer out) throws IOException {
		super.saveToStream(out);
		
		if (getQuantity() != 1) {
			out.write("quantity " + getQuantity() + "\n");
		}
		savePropertyToStream(out, "stackable", "1", !isStackable());
		savePropertyToStream(out, "value", getValue(), getValue() == 0);
		savePropertyToStream(out, "weight", getWeight(), getWeight() == DEFAULT_WEIGHT);
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		super.save(listener);

		if (getQuantity() != 1) {
			saveProperty(listener, "quantity", getQuantity());
		}
		if (isStackable()) {
			saveProperty(listener, "stackable", "1");
		}
		if (getValue() != 0) {
			saveProperty(listener, "value", getValue());
		}
		if (getWeight() != DEFAULT_WEIGHT) {
			saveProperty(listener, "weight", getWeight());
		}
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isStackable() {
		return stackable;
	}

	public void setStackable(boolean stackable) {
		this.stackable = stackable;
	}
	
	/**
	 * Check whether this item stacks with the other one.
	 */
	public boolean stacksWith(Item other) {
		return (isStackable() && other.getName().equals(getName()));
	}

	/**
	 * Attempt to split from the stack given amount of items.
	 *
	 * @return new item if splitting was successful.
	 */
	public Item splitFromStack(int quantity) {
		int origquantity = getQuantity();
		
		// Can't split none, all, or more
		if (quantity <= 0 || quantity >= origquantity) {
			return null;
		}
		
		Item newItem = cloneItem();
		newItem.setQuantity(quantity);
		setQuantity(origquantity - quantity);
		return newItem;
	}
	
	public Item cloneItem() {
		try {
			return (Item) clone();
		}
		catch (CloneNotSupportedException e) {
			return null;
		}
	}

	/**
	 * Return a clone of this item, but with new ID, and without putting the item in world.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Object o = super.clone();
		
		Item item = (Item) o;
		item.setID(createNextID());
		item.setInstance(null);
		
		return o;
	}
	
	/**
	 * Get a "You see a dagger (2-3 damage)" text or similar.
	 */
	public final String getExamineText() {
		StringBuffer result = new StringBuffer();
		result.append("You see ");
		
		if (getQuantity() == 1) {
			result.append("a ");
		} else {
			result.append(getQuantity() + " ");
		}
		
		result.append(getName());
		if (getQuantity() > 1) {
			result.append('s');
		}
		
		String props = StringUtil.join(getExamineProperties(), ", ");
		if (props != null) {
			result.append(" (");
			result.append(props);
			result.append(")");
		}
		
		return result.toString();
	}
	
	/**
	 * Get properties typical for this item. Varies depending on the subclass type, and should be overridden.
	 */
	public List<String> getExamineProperties() {
		return new LinkedList<String>();
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	public int getStackValue() {
		return getValue() * getQuantity();
	}
	
	/**
	 * Get default slot for this class.
	 */
	public Slot getDefaultSlot() {
		return Slot.NONE;
	}

	public long getDecayTime() {
		return decayTime;
	}

	/**
	 * Set how many seconds this item stays on ground before getting decayed.
	 */
	public void setDecayTime(long decayTime) {
		this.decayTime = decayTime;
	}

	public boolean isDecayable() {
		return decayable;
	}

	public void setDecayable(boolean decayable) {
		this.decayable = decayable;
	}
	
	/**
	 * Should be called when this item is dropped on ground.
	 */
	public void onDrop() {
		setDecayable(true);
		updateStamp();
	}

	/**
	 * Get weight of a single item, in grams.
	 */
	public long getWeight() {
		return weight;
	}

	public void setWeight(long weight) {
		this.weight = weight;
	}
	
	public long getTotalWeight() {
		return getWeight() * getQuantity();
	}
}
