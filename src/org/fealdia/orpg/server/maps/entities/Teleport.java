package org.fealdia.orpg.server.maps.entities;

import org.apache.log4j.Logger;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.maps.Instance;
import org.fealdia.orpg.server.maps.InstanceManager;

/**
 * A portal that leads to another instance.
 * 
 * In the future, this may also hold information regarding whether it is a player-unique portal etc.
 */
public class Teleport extends MapEntity {
	private static Logger logger = Logger.getLogger(Teleport.class);
	
	private String target_map = null;
	private int target_x = 0;
	private int target_y = 0;
	
	private Instance.Uniqueness targetUniqueness = Instance.Uniqueness.NORMAL; 

	/**
	 * Damned teleport sets Player's spawn to the destination
	 */
	private boolean damned = false;
	
	public Teleport() {
		setFace("effects/aqua_cloud");
	}
	
	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("target_map".equals(key)) {
			setTargetMap(value);
		}
		else if ("target_x".equals(key)) {
			setTargetX(Integer.parseInt(value));
		}
		else if ("target_y".equals(key)) {
			setTargetY(Integer.parseInt(value));
		}
		else if ("target_uniqueness".equals(key)) {
			setTargetUniqueness(Instance.Uniqueness.valueOf(value.toUpperCase()));
		}
		else if ("damned".equals(key)) {
			setDamned(Boolean.parseBoolean(value));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	public String getTargetMap() {
		return target_map;
	}
	
	public void setTargetMap(String target_map) {
		this.target_map = target_map;
	}
	
	public int getTargetX() {
		return target_x;
	}
	
	public void setTargetX(int target_x) {
		this.target_x = target_x;
	}
	
	public int getTargetY() {
		return target_y;
	}
	
	public void setTargetY(int target_y) {
		this.target_y = target_y;
	}
	
	public Instance getTargetInstanceFor(MapEntity entity) {
		if (getTargetMap() == null) {
			return null;
		}
		
		return InstanceManager.getInstance().getInstance(getTargetMap(), getTargetUniqueness(), entity);
	}
	
	/**
	 * Teleport the given entity, creating a target instance if necessary.
	 * 
	 * @return true if entity was teleported.
	 */
	public boolean teleportEntity(MapEntity entity) {
		Instance inst = getTargetInstanceFor(entity);
		if (inst != null) {
			logger.debug("Teleporting " + entity.getName() + " to instance " + inst.getUniqueName());
			entity.moveToInstance(inst, getTargetX(), getTargetY());

			if (damned && entity instanceof Player) {
				((Player) entity).setSpawnHere();
			}
			return true;
		}
		return false;
	}

	public Instance.Uniqueness getTargetUniqueness() {
		return targetUniqueness;
	}

	public void setTargetUniqueness(Instance.Uniqueness targetUniqueness) {
		this.targetUniqueness = targetUniqueness;
	}

	public void setDamned(boolean damned) {
		this.damned = damned;
	}
}
