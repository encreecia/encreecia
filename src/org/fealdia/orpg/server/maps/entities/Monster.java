package org.fealdia.orpg.server.maps.entities;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.fealdia.orpg.server.AIController;
import org.fealdia.orpg.server.Alignment;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.maps.TreasureManager;
import org.fealdia.orpg.server.quests.QuestAvailability;

/**
 * Hostile creature or NPC.
 */
public class Monster extends Creature {
	private static Logger logger = Logger.getLogger(Monster.class);

	private Alignment alignment = Alignment.NEUTRAL;
	private AttackOrders attackOrders = AttackOrders.DEFEND;
	private MovementOrders movementOrders = MovementOrders.NONE;

	public enum AttackOrders {
		NONE,
		DEFEND, // Only attack when attacked
		ATTACK, // Attack enemies
		ATTACKNEUTRAL, // Attack enemies and neutrals
	}

	/**
	 * Determines movement of Monster when not engaging targets.
	 */
	public enum MovementOrders {
		NONE,
		STAND, // return to spawn position when target lost, useful for guards
		WANDER, // wander within wanderRadius of spawn
		//PATROL,
	}
	
	private int attackSpeed = Weapon.DEFAULT_SPEED;
	private long bodyDecayTime = Item.DECAY_TIME;
	private int damage_min = 1;
	private int damage_max = 1;
	private long experienceGiven = 0;
	private int level = 1;
	private int spawn_x = 0;
	private int spawn_y = 0;
	private int wanderRadius = 7;
	private List<Integer> questsGiven = new LinkedList<Integer>();
	private List<Integer> questsTaken = new LinkedList<Integer>();
	
	/**
	 * Treasure list name, if any.
	 * @see TreasureManager
	 */
	private String treasure = null;
	
	public Monster() {
		setController(new AIController());
	}

	@Override
	public void die() {
		super.die();
		
		// Create a corpse
		Item corpse = createCorpse();
		corpse.moveBelow(this);
		corpse.setDecayTime(getBodyDecayTime());

		// Drop loot
		if (getTreasure() != null) {
			List<String> items = TreasureManager.getInstance().getTreasureList(getTreasure());
			logger.trace("Creating loot: " + items);
			for (String archetype : items) {
				MapEntity item = MapEntity.createFromArchetype(archetype);
				if (item != null) {
					item.moveBelow(this);
				}
			}
		}
		
		// Remove us from this instance
		moveOutOfWorld();
	}

	@Override
	protected void onDamageReceived(Creature source, int damage, boolean died) {
		// Change target to attacker if 1) we have no target 2) our target is farther away than the attacker
		if (getTarget() == null || distanceTo(getTarget()) > distanceTo(source)) {
			setTarget(source.getID());
		}
	}

	/**
	 * Try to move towards the target location. This won't go past obstacles.
	 */
	public boolean moveTowards(int x, int y) {
		// Get movement vector relative to target
		int dx = x - getX();
		int dy = y - getY();
		
		// Normalize the vector, eg. abs(dx) <= 1
		if (dx != 0) {
			dx = dx / Math.abs(dx);
		}
		if (dy != 0) {
			dy = dy / Math.abs(dy);
		}
		return tryMoveVector(dx, dy);
	}
	
	/**
	 * Movement logic for "wander" order.
	 */
	public boolean doMovementWander() {
		// 80% chance not to move at all
		if (new Random().nextInt(5) != 0) {
			return false;
		}
		
		// Pick direction randomly and move towards it
		int dx = new Random().nextInt(3) - 1;
		int dy = new Random().nextInt(3) - 1;
		return tryMoveVector(dx, dy);
	}

	@Override
	protected void onEntityVisibilityChange(MapEntity mapEntity, boolean src_visible, boolean dst_visible, boolean we_moved) {
		super.onEntityVisibilityChange(mapEntity, src_visible, dst_visible, we_moved);
		
		if (dst_visible) {
			checkTarget(mapEntity);
		}
	}
	
	/**
	 * Check for potential new prey.
	 */
	public void checkTarget(MapEntity mapEntity) {
		if (getTarget() != null) {
			return;
		}
		//logger.debug(getID() + "[" + getName() + "]: Checking target " + mapEntity.getID() + "[" + mapEntity.getName() + "]");
		
		// Target the entity
		/*
		if (mapEntity instanceof Creature) {
			if (isEnemy((Creature)mapEntity)) {
				logger.trace("Noticed new target: " + mapEntity.getName());
				setTarget(mapEntity.getID());
			}
		}
		*/
		targetClosestEnemy();
	}

	@Override
	public Alignment getAlignment() {
		return alignment;
	}

	public AttackOrders getAttackOrders() {
		return attackOrders;
	}

	public MovementOrders getMovementOrders() {
		return movementOrders;
	}

	public void setAlignment(Alignment alignment) {
		this.alignment = alignment;
	}

	public void setAttackOrders(AttackOrders attackOrders) {
		this.attackOrders = attackOrders;
	}

	public void setMovementOrders(MovementOrders movementOrders) {
		this.movementOrders = movementOrders;
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("alignment".equals(key)) {
			setAlignment(Alignment.valueOf(value.toUpperCase()));
		}
		else if ("attack".equals(key)) {
			setAttackOrders(AttackOrders.valueOf(value.toUpperCase()));
		}
		else if ("attackspeed".equals(key)) {
			setAttackSpeed(Integer.parseInt(value));
		}
		else if ("damage".equals(key)) {
			String[] parts = value.split(" +", 2);
			setDamage(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
		}
		else if ("experience".equals(key)) {
			setExperienceGiven(Long.parseLong(value));
		}
		else if ("level".equals(key)) {
			setLevel(Integer.parseInt(value));
		}
		else if ("movement".equals(key)) {
			setMovementOrders(MovementOrders.valueOf(value.toUpperCase()));
		}
		else if ("quests".equals(key)) {
			String[] parts = value.split(",");
			List<Integer> quests = new LinkedList<Integer>();
			for (String p : parts) {
				quests.add(Integer.parseInt(p));
			}
			this.questsGiven = new LinkedList<Integer>(quests);
			this.questsTaken = new LinkedList<Integer>(quests);
		}
		else if ("questsgiven".equals(key)) {
			String[] parts = value.split(",");
			List<Integer> questsGiven = new LinkedList<Integer>();
			for (String p : parts) {
				questsGiven.add(Integer.parseInt(p));
			}
			this.questsGiven = questsGiven;
		}
		else if ("queststaken".equals(key)) {
			String[] parts = value.split(",");
			List<Integer> questsTaken = new LinkedList<Integer>();
			for (String p : parts) {
				questsTaken.add(Integer.parseInt(p));
			}
			this.questsTaken = questsTaken;
		}
		else if ("treasure".equals(key)) {
			setTreasure(value);
		}
		else if ("wanderradius".equals(key)) {
			setWanderRadius(Integer.parseInt(value));
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}

	@Override
	public boolean isEnemy(Creature creature) {
		if (getAlignment() == Alignment.GOOD) {
			// Good-aligned creatures only consider criminal players enemies
			if (creature instanceof Player) {
				return ((Player) creature).isCriminal();
			}

			// And EVIL monsters
			if (creature.getAlignment() == Alignment.EVIL) {
				return true;
			}

			// Neutrals are "enemies" too if ATTACKNEUTRAL order
			if (getAttackOrders() == AttackOrders.ATTACKNEUTRAL && creature.getAlignment() == Alignment.NEUTRAL) {
				return true;
			}
		}
		else if (getAlignment() == Alignment.EVIL) {
			if (creature instanceof Player) {
				return true;
			}

			// EVIL attacks GOOD
			if (creature.getAlignment() == Alignment.GOOD) {
				return true;
			}

			// EVIL attacks NEUTRALs as well, if ATTACKNEUTRAL order
			if (getAttackOrders() == AttackOrders.ATTACKNEUTRAL && creature.getAlignment() == Alignment.NEUTRAL) {
				return true;
			}
		}
		else {
			// Neutrals only attack EVIL if ATTACK order
			return getAttackOrders() == AttackOrders.ATTACK && creature.getAlignment() == Alignment.EVIL;
		}
		return false;
	}

	public void setAttackSpeed(int attackSpeed) {
		this.attackSpeed = attackSpeed;
	}

	public void setDamage(int damage_min, int damage_max) {
		this.damage_min = damage_min;
		this.damage_max = damage_max;
	}

	@Override
	public int getWeaponDamage(Creature target) {
		int min = getDamageMin();
		int max = getDamageMax();
		int diff = max - min;
		
		return min + new Random().nextInt(diff + 1);
	}
	
	public int getDamageMin() {
		return damage_min;
	}
	
	public int getDamageMax() {
		return damage_max;
	}

	@Override
	public int getWeaponSpeed() {
		return attackSpeed;
	}

	public String getTreasure() {
		return treasure;
	}

	public void setTreasure(String treasure) {
		this.treasure = treasure;
	}
	
	public void setSpawnLocation(int x, int y) {
		this.spawn_x = x;
		this.spawn_y = y;
	}
	
	public int getSpawnX() {
		return spawn_x;
	}
	
	public int getSpawnY() {
		return spawn_y;
	}

	@Override
	public void onCreatureHit(Creature attacker, Creature target, int damage) {
		// If our enemy attacks our friend, attack it. Mostly for guards.
		if (getAttackOrders() == AttackOrders.ATTACK && !isEnemy(target) && isEnemy(attacker)) {
			setTarget(attacker.getID());
		}
	}

	public int getWanderRadius() {
		return wanderRadius;
	}

	public void setWanderRadius(int wanderRadius) {
		this.wanderRadius = wanderRadius;
	}

	public long getExperienceGiven() {
		return experienceGiven;
	}

	public void setExperienceGiven(long experienceGiven) {
		this.experienceGiven = experienceGiven;
	}

	public long getBodyDecayTime() {
		return bodyDecayTime;
	}

	public void setBodyDecayTime(long bodyDecayTime) {
		this.bodyDecayTime = bodyDecayTime;
	}

	@Override
	public boolean canSwapPlaceWith(Creature creature) {
		// GOOD-aligned monsters that have ATTACK orders can swap places with some creatures. Mostly useful for guards when they chase.
		// NOTE: Don't allow swapping places with another guard, because if it's possible, two guards chasing after something
		// can swap places with each other all the time, preventing both from moving :)
		if (getAlignment() == Alignment.GOOD && getAttackOrders() == AttackOrders.ATTACK) {
			if (creature instanceof Player) {
				return true;
			}
			Monster monster = (Monster) creature;
			if (monster.getAttackOrders() != AttackOrders.ATTACK) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Target closest enemy.
	 */
	public boolean targetClosestEnemy() {
		List<MapEntity> entities = getInstance().getEntitiesAtRadius(getX(), getY(), getVisibleRadius());
		
		Creature closest = null;
		
		for (MapEntity en : entities) {
			if (en instanceof Creature) {
				Creature c = (Creature) en;
				if (!canEngage(c)) {
					continue;
				}
				if (closest == null || distanceTo(c) < distanceTo(closest)) {
					closest = c;
				}
			}
		}
		if (closest != null) {
			setTarget(closest.getID());
			return true;
		}
		return false;
	}
	
	/**
	 * Check whether this monster can engage the target creature.
	 * 
	 * This takes into account hostility of target and visibility.
	 */
	public boolean canEngage(Creature target) {
		return (isEnemy(target) && canSee(target));
	}

	@Override
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public QuestAvailability getQuestAvailabilityFor(Player player) {
		if (questsGiven.size() == 0 && questsTaken.size() == 0) {
			return QuestAvailability.NONE;
		}
		QuestAvailability availability = QuestAvailability.NONE;

		// Check if any quests can be received
		for (int questId : questsGiven) {
			QuestAvailability status = player.getQuestLog().getQuestStatus(questId);
			switch (status) {
				case RETURNABLE:
					continue;
				default:
					break;
			}
			if (status.ordinal() > availability.ordinal()) {
				availability = status;
			}
		}

		// Check if any quests can be returned
		for (int questId : questsTaken) {
			QuestAvailability status = player.getQuestLog().getQuestStatus(questId);
			switch (status) {
				case AVAILABLE:
					continue;
				default:
					break;
			}
			if (status.ordinal() > availability.ordinal()) {
				availability = status;
			}
		}
		return availability;
	}
	
	public boolean talkWith(Player player) {
		QuestAvailability availability = getQuestAvailabilityFor(player);
		if (availability != QuestAvailability.NONE) {
			// Any quests available?
			for (int questId : questsGiven) {
				QuestAvailability playerStatus = player.getQuestLog().getQuestStatus(questId);
				switch (playerStatus) {
					case AVAILABLE:
						player.offerQuest(questId, getName());
						return true;
					default:
						break;
				}
			}

			// Any quests started or returnable?
			for (int questId : questsTaken) {
				QuestAvailability playerStatus = player.getQuestLog().getQuestStatus(questId);
				switch (playerStatus) {
					case RETURNABLE:
						player.finishQuest(questId);
						return true;
					case STARTED:
						player.sendText("You have unfinished quest(s) for " + getName());
					default:
						break;
				}
			}
		}
		
		return false;
	}
	
}
