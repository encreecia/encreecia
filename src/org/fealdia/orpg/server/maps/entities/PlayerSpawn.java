package org.fealdia.orpg.server.maps.entities;


/**
 * A spot where players respawn when they die.
 */
public class PlayerSpawn extends MapEntity {
	public PlayerSpawn() {
		setFace("misc/skull");
		setName("player spawn");
	}

}
