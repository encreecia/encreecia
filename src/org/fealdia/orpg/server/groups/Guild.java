package org.fealdia.orpg.server.groups;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.fealdia.orpg.common.net.data.ChatType;
import org.fealdia.orpg.common.util.FileUtil;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.xml.XMLUtil;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.PlayerManager;

/**
 * Guild is a permanent group for players. Each Player can belong to only one Guild.
 * 
 * Guilds have a "tag" and a name. Tag is 1-4 characters.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Guild {
	private static Logger logger = Logger.getLogger(Guild.class);
	
	@XmlTransient
	private boolean changed = false;
	private long experience = 0;
	private long founded = 0; // timestamp (secs)
	private String leader;
	@XmlTransient
	private Logger guildLog = null;
	@XmlElement(name = "member")
	@XmlElementWrapper(name = "members")
	private List<String> members = new LinkedList<String>();
	@XmlElementWrapper(name = "memberJoinTime")
	private HashMap<String,Long> membersJoined = new HashMap<String,Long>();
	@XmlTransient
	private List<Player> members_online = new LinkedList<Player>();
	private String motd = null;
	private String name;
	@XmlTransient
	private List<String> players_invited = new LinkedList<String>();
	private String tag;
	
	public static boolean isValidName(String name) {
		return name.matches("[A-Za-z][A-Za-z0-9' ]{4,30}[A-Za-z0-9']");
	}
	
	public static boolean isValidTag(String tag) {
		return tag.matches("[A-Za-z0-9]{1,4}");
	}

	public static Guild loadFromFile(final String filename) throws FileNotFoundException, JAXBException {
		return XMLUtil.fromXML(Guild.class, filename);
	}

	public static String getFilenameForTag(final String tag) {
		return PathConfig.getServerDataDir() + "guilds/" + tag + "/" + tag + ".xml";
	}

	public Guild() {
		setFounded(System.currentTimeMillis() / 1000);
	}
	
	/**
	 * How many guild members in total.
	 */
	public synchronized int countMembers() {
		return members.size();
	}
	
	public synchronized int countMembersOnline() {
		return members_online.size();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
		
		createLogger();
	}
	public String getLeader() {
		return leader;
	}
	public void setLeader(String leader) {
		this.leader = leader;
	}
	public List<String> getMembers() {
		return members;
	}
	
	public synchronized boolean addMember(String name) {
		if (!PlayerManager.isPlayerRegistered(name)) {
			return false;
		}
		if (isMember(name)) {
			return false;
		}
		
		members.add(name);
		membersJoined.put(name, System.currentTimeMillis() / 1000);
		players_invited.remove(name);
		sendNotice(name + " has joined the guild.");
		log("Member added: " + name);
		
		// if online, set the guild as well
		Player player = PlayerManager.getInstance().getPlayerByName(name);
		if (player != null) {
			player.setGuild(this);
			loginPlayer(player);
			
			player.sendText("You have joined the '" + getName() + "' guild");
		}
		setChanged(true);
		return true;
	}
	
	public synchronized boolean changeLeader(String new_leader) {
		if (!isMember(new_leader)) {
			return false;
		}
		String old_leader = getLeader();
		setLeader(new_leader);
		sendNotice(new_leader + " has been set as the guild leader by " + old_leader);
		log("Leader changed: " + old_leader + " -> " + new_leader);
		
		setChanged(true);
		return true;
	}
	
	public String getFilename() {
		return getFilenameForTag(getTag());
	}
	
	public long getMemberJoined(String name) {
		if (membersJoined.containsKey(name)) {
			return membersJoined.get(name);
		}
		return 0;
	}
	
	public List<Player> getMembersOnline() {
		return new LinkedList<Player>(members_online);
	}
	
	public synchronized boolean invitePlayer(Player pl) {
		if (pl.isInGuild()) {
			return false;
		}
		if (isInvited(pl.getName())) {
			return false;
		}
		
		String name = pl.getName();
		players_invited.add(name);
		
		pl.sendText("You have been invited to join the guild '" + getTag() + "' (" + getName() + "). Type '/guild join " + getTag() + "' to join the guild.");
		
		return true;
	}
	
	public synchronized boolean isInvited(String player) {
		return players_invited.contains(player);
	}
	
	public synchronized boolean isMember(String player) {
		return members.contains(player);
	}
	
	public synchronized boolean isMemberOnline(String member) {
		for (Player pl : getMembersOnline()) {
			if (pl.getName().equals(member)) {
				return true;
			}
		}
		return false;
	}
	
	public synchronized boolean kickMember(String victim) {
		// Is the victim online?
		for (Player pl : getMembersOnline()) {
			if (pl.getName().equals(victim)) {
				leaveGuild(pl);
				log("Kicked: " + victim);
				return true;
			}
		}
		
		// Victim is offline
		for (String m : getMembers()) {
			if (m.equals(victim)) {
				sendNotice(victim + " has been kicked out of the guild.");
				getMembers().remove(victim);
				setChanged(true);
				log("Kicked: " + victim);
				return true;
			}
		}
		return false;
	}
	
	public synchronized void leaveGuild(Player player) {
		members_online.remove(player);
		
		members.remove(player.getName());
		
		sendNotice(player.getName() + " left the guild.");
		player.sendText("You have left the guild.");
		player.setGuild(null);
		setChanged(true);
		log("Left: " + player.getName());
	}
	
	public synchronized void loginPlayer(Player player) {
		sendNotice(player.getName() + " logged in.");
		
		members_online.add(player);
		
		if (getMotd() != null) {
			player.sendText("Guild message of the day:\n" + getMotd());
		}
	}
	
	public synchronized void logoutPlayer(Player player) {
		members_online.remove(player);
		
		sendNotice(player.getName() + " logged out.");
		
		if (members_online.size() == 0) {
			tryToSave();
		}
	}

	public synchronized void saveToFile() throws JAXBException, IOException {
		logger.debug("Saving " + getTag());
		File dir = new File(PathConfig.getServerDataDir() + "guilds/" + getTag());
		dir.mkdirs();

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(getFilename() + ".tmp");
			XMLUtil.toXML(this, fos, true);
		} finally {
			fos.close();
		}

		FileUtil.renameAsBackupWithException(getFilename());

		File f = new File(getFilename() + ".tmp");
		boolean res = f.renameTo(new File(getFilename()));
		if (!res) {
			throw new IOException("Rename from tmp to actual filename failed: " + getFilename());
		}
		setChanged(false);
	}

	public synchronized boolean tryToSave() {
		try {
			saveToFile();
		} catch (Exception e) {
			logger.error("Failed to save guild", e);
			return false;
		}
		return true;
	}

	public synchronized void sendChat(String who, String text) {
		for (Player pl : getMembersOnline()) {
			pl.getClientThread().sendChat(who, text, ChatType.GUILD);
		}
	}
	
	/**
	 * Disband this Guild. At this point the guild should have no members, apart from the leader.
	 */
	public synchronized void disband() {
		log("Disbanded");
		// Clear members list
		members.clear();
		setChanged(true);
	}

	/**
	 * Send a notice to all members online.
	 */
	public synchronized void sendNotice(String text) {
		for (Player pl : members_online) {
			pl.sendText("[GUILD] " + text);
		}
	}

	public long getFounded() {
		return founded;
	}

	public void setFounded(long founded) {
		this.founded = founded;
	}

	public long getExperience() {
		return experience;
	}

	public synchronized void setExperience(long experience) {
		this.experience = experience;
	}
	
	public synchronized void addExperience(long experience) {
		this.experience += experience;
		
		setChanged(true);
	}

	public boolean isChanged() {
		return changed;
	}

	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	public String getMotd() {
		return motd;
	}

	public synchronized void setMotd(String motd) {
		this.motd = motd;
		
		if (motd != null) {
			sendNotice("Message of the Day changed to:\n" + motd);
		}
		log("Motd: " + motd);
	}
	
	private void createLogger() {
		String filename = PathConfig.getServerDataDir() + "guilds/" + tag + "/log.txt";
		try {
			FileAppender file = new FileAppender(new PatternLayout("%d{ISO8601} %m%n"), filename);
			
			guildLog = Logger.getLogger("guild:" + tag);
			guildLog.addAppender(file);
		} catch (IOException e) {
			logger.error(e);
		}
	}
	
	private void log(String message) {
		if (guildLog != null) {
			guildLog.info(message);
		}
	}
	
	public void unload() {
		if (guildLog != null) {
			guildLog.removeAllAppenders();
			guildLog = null;
		}
	}
}
