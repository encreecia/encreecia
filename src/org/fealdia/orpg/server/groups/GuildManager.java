package org.fealdia.orpg.server.groups;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.PlayerManager;

/**
 * Guild loading, saving, and guild maintenance related operations.
 * 
 * Guilds are saved to data/guilds/<abbr>/<abbr>
 */
public class GuildManager {
	private static GuildManager instance;
	private static Logger logger = Logger.getLogger(GuildManager.class);
	
	// Currently loaded guilds
	private List<Guild> guilds = new LinkedList<Guild>();
	
	public static GuildManager getInstance() {
		if (instance == null) {
			instance = new GuildManager();
		}
		return instance;
	}
	
	/**
	 * Attempt to create a new guild.
	 */
	public synchronized Guild createGuild(final String tag, final String name, final String leader) throws Exception {
		if (!Guild.isValidTag(tag)) {
			throw new IllegalArgumentException("Guild tag is not valid, must be 1-4 characters: " + tag);
		}
		if (!Guild.isValidName(name)) {
			throw new IllegalArgumentException("Guild name is not valid: " + name);
		}
		
		if (isRegisteredGuild(tag)) {
			throw new IllegalArgumentException("Guild is already registered: " + tag);
		}
		
		Guild result = new Guild();
		result.setTag(tag);
		result.setName(name);
		result.setLeader(leader);
		result.addMember(leader);
		
		// immediately save the guild
		result.saveToFile();
		
		guilds.add(result);
		
		logger.debug("New guild created: " + tag + ": " + name + " (leader: " + leader + ")");
		return result;
	}
	
	public synchronized void disbandGuild(Guild guild) {
		guild.disband();
		try {
			guild.saveToFile();
		} catch (Exception e) {
			logger.error("Failed to save guild after disbanding", e);
		}
		// TODO actually rename/remove the guild savefile?
		guilds.remove(guild);
	}
	
	/**
	 * Get or load the given guild.
	 */
	public synchronized Guild getGuild(String tag) {
		for (Guild guild : guilds) {
			if (tag.equals(guild.getTag())) {
				return guild;
			}
		}
		
		// load from file
		if (isRegisteredGuild(tag)) {
			logger.debug("Loading guild from file: " + tag);
			Guild guild;
			try {
				guild = Guild.loadFromFile(Guild.getFilenameForTag(tag));
			} catch (Exception e) {
				logger.error("Failed to load guild", e);
				return null;
			}
			guilds.add(guild);
			
			return guild;
		}
		
		return null;
	}
	
	public void handleCommand(Player pl, String[] params) {
		String cmd = params[0];
		logger.debug("handleCommand: " + Arrays.toString(params));
		
		// create <abbr> <name>
		if ("create".equals(cmd) && !pl.isInGuild()) {
			try {
				Guild guild = createGuild(params[1], params[2], pl.getName());
				if (guild != null) {
					pl.sendText("New guild created");
					pl.setGuild(guild);
				}
			} catch (Exception e) {
				pl.sendText("Failed to create guild: " + e.getMessage());
				logger.warn("Failed to create guild: ", e);
			}
		}
		else if ("disband".equals(cmd) && pl.isGuildLeader()) {
			// Disband the guild if it only has one member
			Guild g = pl.getGuild();
			if (g != null && g.countMembers() == 1) {
				g.logoutPlayer(pl);
				disbandGuild(g);
				pl.setGuild(null);
				pl.sendText("Your guild has been disbanded");
			}
		}
		else if ("invite".equals(cmd) && params.length >= 2 && pl.isGuildLeader()) {
			Player target = PlayerManager.getInstance().getPlayerByName(params[1]);
			if (target != null) {
				if (!pl.getGuild().invitePlayer(target)) {
					pl.sendText("Failed to invite " + params[1]);
				}
			} else {
				pl.sendText("No such player. Members can only be invited to a guild when they are online.");
			}
		}
		else if ("who".equals(cmd) && pl.isInGuild()) {
			Guild g = pl.getGuild();
			for (String name : g.getMembers()) {
				if (g.isMemberOnline(name)) {
					Player m = PlayerManager.getInstance().getPlayerByName(name);
					
					pl.sendText(m.getName() + " - level " + m.getLevel() + " (" + m.getExperience() + " exp) - " + m.getInstance().getName());
				} else {
					pl.sendText(name + " [OFFLINE]");
				}
			}
			pl.sendText(g.countMembersOnline() + " members online. " + g.countMembers() + " members in total. Leader is " + g.getLeader());
		}
		else if ("leader".equals(cmd) && params.length >= 2 && pl.isGuildLeader()) {
			if (!pl.getGuild().changeLeader(params[1])) {
				pl.sendText("Failed to change guild leader.");
			}
		}
		else if ("leave".equals(cmd) && pl.isInGuild() && !pl.isGuildLeader()) {
			pl.getGuild().leaveGuild(pl);
		}
		else if ("kick".equals(cmd) && params.length >= 2 && pl.isGuildLeader()) {
			String victim = params[1];
			if (pl.getName().equals(victim)) {
				pl.sendText("You can't kick yourself out of a guild. Try disbanding the guild instead.");
			} else {
				Guild g = pl.getGuild();
				g.kickMember(victim);
			}
		}
		else if ("join".equals(cmd) && params.length >= 2 && !pl.isInGuild()) {
			// Check that we have been invited, and then join the guild
			Guild g = getGuild(params[1]);
			if (g != null && g.isInvited(pl.getName())) {
				g.addMember(pl.getName());
			}
		}
		else if ("motd".equals(cmd) && pl.isGuildLeader()) {
			String motd = null;
			if (params.length >= 2) {
				motd = params[1];
			}
			pl.getGuild().setMotd(motd);
		}
		else {
			pl.sendText("Unhandled guild command: " + cmd);
		}
	}
	
	public boolean isRegisteredGuild(String tag) {
		File dir = new File(Guild.getFilenameForTag(tag));
		return dir.exists();
	}
	
	public synchronized void unloadEmptyGuilds() {
		for (Iterator<Guild> itr = guilds.iterator(); itr.hasNext(); ) {
			Guild g = itr.next();
			synchronized (g) {
				if (g.countMembersOnline() == 0) {
					if (g.isChanged()) {
						try {
							g.saveToFile();
						} catch (Exception e) {
							logger.error("Failed to save guild " + g.getName(), e);
							continue;
						}
					}
					logger.debug("Unloading empty guild " + g.getTag() + " (" + g.getName() + ")");
					g.unload();
					itr.remove();
				}
			}
		}
	}
	
	public synchronized void autoSaveGuilds() {
		for (Guild g : guilds) {
			if (g.isChanged()) {
				logger.debug("Autosaving guild: " + g.getName());
				g.tryToSave();
			}
		}
	}
	
}
