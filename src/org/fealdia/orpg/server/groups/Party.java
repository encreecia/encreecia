package org.fealdia.orpg.server.groups;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.PlayerManager;

/**
 * A temporary group of players.
 * 
 * Parties can be created, and then other players can be invited to join them.
 * 
 * Members can leave the party, and after the last ones the party is automatically removed.
 *
 * Experience players receive in a party is shared among the party members.
 */
public class Party {
	private static Logger logger = Logger.getLogger(Party.class);
	
	// Party minimap marker
	private String marker_map = null;
	private int marker_x = 0;
	private int marker_y = 0;
	
	private String name;
	
	private List<Long> players = Collections.synchronizedList(new LinkedList<Long>());
	private List<String> players_invited = new LinkedList<String>();
	
	private int remainder = 0; // Who gets next remainder exp
	
	public Party(String name) {
		setName(name);
		logger.debug("Created");
	}
	
	public synchronized void addMember(long player_id) {
		players.add(player_id);
		
		Player pl = PlayerManager.getInstance().getPlayerById(player_id);
		sendText(pl.getName() + " has joined the party.");

		players_invited.remove(pl.getName());

		sendPartyList();
		
		sendMemberPositionsFor(pl);
		sendPositionForOthers(pl);
		sendMarkerTo(pl);
	}
	
	public synchronized void removeMember(long player_id) {
		players.remove(player_id);
		
		Player pl = PlayerManager.getInstance().getPlayerById(player_id);
		sendText(pl.getName() + " has left the party.");
		
		// Party is destroyed when last player leaves
		if (players.size() == 0) {
			PartyManager.getInstance().removeParty(this);
		}
		
		sendPartyList();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int size() {
		return players.size();
	}
	
	/**
	 * Get list of members. The result can is a new list that can be iterated freely.
	 */
	public List<Player> getPlayers() {
		List<Player> result = new LinkedList<Player>();
		synchronized (players) {
			for (Long id : players) {
				result.add(PlayerManager.getInstance().getPlayerById(id));
			}
		}
		return result;
	}

	/**
	 * Share experience between party members.
	 */
	public synchronized void addExperience(long experience) {
		long each = experience / size();
		long left = experience - each * size();
		//logger.debug("Sharing " + experience + " exp: " + each + " each, " + left + " left");
		
		List<Player> players = getPlayers();
		for (Player pl : players) {
			if (each > 0) {
				pl.addExperience(each, true);
			}
		}
		if (left == 0) {
			return;
		}
		
		if (remainder >= size()) {
			remainder = 0;
		}
		Player lucky = players.get(remainder);
		lucky.addExperience(left, true);
		remainder++;
	}
	
	/**
	 * Send text to all party members.
	 */
	public void sendText(String text) {
		for (Player pl : getPlayers()) {
			pl.sendText(text);
		}
	}
	
	/**
	 * Send party list to all members.
	 */
	public void sendPartyList() {
		for (Player p : getPlayers()) {
			p.getClientThread().sendPartyMembers();
		}
	}
	
	/**
	 * Send positions of all members in the same instance to the given player.
	 */
	public void sendMemberPositionsFor(Player pl) {
		logger.debug("Sending all party member positions to " + pl.getName());
		for (Player member : getPlayers()) {
			if (member == pl) {
				continue;
			}
			if (member.getInstance() == pl.getInstance()) {
				pl.getClientThread().sendPartyMemberPos(member.getID(), member.getX(), member.getY());
			}
		}
	}
	
	/**
	 * Send position of the given player for all other members in the same instance.
	 */
	public void sendPositionForOthers(Player target) {
		List<Player> members = getPlayers();
		for (Player p : members) {
			if (p == target) {
				continue;
			}
			if (p.getInstance() == target.getInstance()) {
				p.getClientThread().sendPartyMemberPos(target.getID(), target.getX(), target.getY());
			}
		}
	}
	
	/**
	 * Notify all members in same instance of disappearance of the given member.
	 */
	public void sendPositionClearFor(Player target) {
		List<Player> members = getPlayers();
		for (Player p : members) {
			if (p == target) {
				continue;
			}
			if (p.getInstance() == target.getInstance()) {
				p.getClientThread().sendPartyMemberPosClear(target.getID());
			}
		}
	}
	
	public void invitePlayer(Player inviter, Player target) {
		sendText(inviter.getName() + " has invited " + target.getName() + " to join the party.");
		
		players_invited.remove(target.getName());
		players_invited.add(target.getName());
		
		target.sendText("You have been invited to join a party by " + inviter.getName() + ". Type /party join " + getName() + " to join it.");
	}
	
	public boolean wasInvited(String name) {
		return players_invited.contains(name);
	}
	
	/**
	 * Set party minimap market to point to the player's map and given coordinates.
	 */
	public void setPartyMarker(Player who, int x, int y) {
		marker_map = who.getInstance().getScene().getName();
		marker_x = x;
		marker_y = y;
		
		sendText("Party minimap marker has been set to " + x + "," + y + "@" + marker_map + " by " + who.getName());
		
		sendMarkerToAll();
	}
	
	/**
	 * Send party marker to everyone.
	 */
	public void sendMarkerToAll() {
		List<Player> members = getPlayers();
		for (Player p : members) {
			sendMarkerTo(p);
		}
	}
	
	public void sendMarkerTo(Player pl) {
		pl.getClientThread().sendPartyMarker(marker_map, marker_x, marker_y);
	}
}
