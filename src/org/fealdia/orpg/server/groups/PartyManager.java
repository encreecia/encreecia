package org.fealdia.orpg.server.groups;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.fealdia.orpg.server.Player;
import org.fealdia.orpg.server.PlayerManager;

/**
 * Manages parties
 */
public class PartyManager {
	private static PartyManager instance = null;
	
	private static Logger logger = Logger.getLogger(PartyManager.class);
	
	private List<Party> parties = new LinkedList<Party>();
	
	public static PartyManager getInstance() {
		if (instance == null) {
			instance = new PartyManager();
		}
		return instance;
	}
	
	public synchronized Party getPartyByName(String name) {
		for (Party p : parties) {
			if (name.equals(p.getName())) {
				return p;
			}
		}
		return null;
	}
	
	/**
	 * Creates a new party with the given name.
	 * 
	 * @return new party, or null if it exists already
	 */
	public synchronized Party createParty(String name) {
		if (getPartyByName(name) != null) {
			return null;
		}
		Party party = new Party(name);
		parties.add(party);
		return party;
	}
	
	public void handleCommand(Player pl, String[] params) {
		logger.debug("handleCommand: " + Arrays.toString(params));
		
		String cmd = params[0];
		if ("create".equals(cmd) && params.length >= 2 && !pl.isInParty()) {
			pl.setParty(PartyManager.getInstance().createParty(params[1]));
		}
		else if ("list".equals(cmd)) {
			List<Party> parties = PartyManager.getInstance().getList();
			
			for (Party party : parties) {
				pl.sendText(party.getName() + " (" + party.size() + " members)");
			}
			pl.sendText(parties.size() + " parties.");
		}
		else if ("join".equals(cmd) && params.length >= 2 && !pl.isInParty()) {
			Party party = PartyManager.getInstance().getPartyByName(params[1]);
			if (party != null) {
				if (party.wasInvited(pl.getName())) {
					pl.setParty(party);
				} else {
					pl.sendText("You have not been invited to join that party.");
				}
			} else {
				pl.sendText("No such party.");
			}
		}
		else if ("leave".equals(cmd) && pl.isInParty()) {
			pl.setParty(null);
		}
		else if ("who".equals(cmd) && pl.isInParty()) {
			List<Player> players = pl.getParty().getPlayers();
			for (Player o : players) {
				pl.sendText(o.getName() + " - level " + o.getLevel() + " (" + o.getExperience() + " exp) - " + o.getInstance().getName());
			}
			pl.sendText(players.size() + " members.");
		}
		else if ("invite".equals(cmd) && params.length >= 2 && pl.isInParty()) {
			Player target = PlayerManager.getInstance().getPlayerByName(params[1]);
			if (target != null) {
				pl.getParty().invitePlayer(pl, target);
			} else {
				pl.sendText("No such player");
			}
		}
		else {
			pl.sendText("Usage: party create <name>|list|invite <player>|join <name>|leave|who");
		}
	}
	
	public synchronized void removeParty(Party party) {
		parties.remove(party);
	}
	
	public synchronized List<Party> getList() {
		return new LinkedList<Party> (parties); 
	}
}
