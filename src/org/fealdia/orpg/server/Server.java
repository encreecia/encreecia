package org.fealdia.orpg.server;

import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.ArchetypeManager2;
import org.fealdia.orpg.common.maps.BackgroundMapper;

/**
 * The game server. Listens to connections from clients, and creates or loads necessary parts
 * of world to accommodate them. 
 */
public class Server implements Runnable {
	private static Logger logger = Logger.getLogger(Server.class);
	
	private int port = ServerConfig.getInstance().getPropertyAsInt("server.port");
	private ServerSocket socket;
	
	private long bytesIn = 0;
	private long bytesOut = 0;
	
	public Server() {
	}
	
	public void run() {
		logger.info("Starting server");
		
		BackgroundMapper.getInstance();
		PlayerManager.getInstance();
		ArchetypeManager2.getInstance();
		
		logger.info("Initialization done");
		
		// Start listening for incoming connections
		try {
			socket = new ServerSocket(port);
		}
		catch (Exception e) {
			logger.error("Listen failed: " + e.getMessage());
			return;
		}
		
		WorldThread.getInstance().start();
		
		logger.info("Listening on port " + socket.getLocalPort());
		
		// Create a ClientThread for each
		while (!socket.isClosed()) {
			Socket client = null;
			try {
				client = socket.accept();
				logger.info("New connection from " + client.getRemoteSocketAddress());
				Thread ct = new Thread(new ClientThread(client, this));
				ct.setName("ClientThread");
				ct.start();
			}
			catch (Exception e) {
				logger.error("Accept failed", e);
			}
		}
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public synchronized void incrementBytesTransferred(long in, long out) {
		bytesIn += in;
		bytesOut += out;
	}
	
	public long getBytesIn() {
		return bytesIn;
	}
	
	public long getBytesOut() {
		return bytesOut;
	}
}
