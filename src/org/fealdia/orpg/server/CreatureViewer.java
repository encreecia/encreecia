package org.fealdia.orpg.server;

import org.fealdia.orpg.server.maps.entities.MapEntity;

/**
 * CreatureViewer is capable of viewing the world with the eyes of a Creature. Player implements a CreatureViewer,
 * and each Creature (Monster/Player) can have 0+ viewers. By default Player becomes its own viewer.
 */
public interface CreatureViewer {
	/**
	 * Called when an entity moves near the viewed Creature.
	 */
	public void onEntityMovement(MapEntity mapEntity, boolean src_visible, boolean dst_visible);
	
	/**
	 * Called when the viewed Creature's position is changed.
	 */
	public void onViewablePositionChanged(int x, int y);
	
	/**
	 * Send text to this viewer.
	 */
	public void sendText(String text);
}
