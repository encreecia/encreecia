package org.fealdia.orpg.server;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.SceneManager;
import org.fealdia.orpg.server.groups.GuildManager;
import org.fealdia.orpg.server.maps.Instance;
import org.fealdia.orpg.server.maps.InstanceManager;

/**
 * This thread is the one mainly responsible for animating a World.
 * 
 * The ServerThread creates a WorldThread, and then a ClientThread for all
 * connecting players. Once a player logs in, the ClientThread puts the player
 * in an instance. After an instance has been created, the WorldThread will
 * continue "animating" it (moving monsters, etc).
 */
public class WorldThread extends Thread {
	private static Logger logger = Logger.getLogger(WorldThread.class);
	
	private static WorldThread instance = null;
	
	public static WorldThread getInstance() {
		if (instance == null) {
			instance = new WorldThread();
			logger.debug("Instance created");
		}
		return instance;
	}
	
	private WorldThread() {
		setName("WorldThread");
	}

	@Override
	public void run() {
		long time_used = 0;
		int tick = 0;
		
		try {
			while (true) {
				try {
					// compensate for execution time by sleeping only the time left to next "tick"
					long msec = Math.max(100 - time_used, 50);
					if (msec < 20) {
						logger.debug("Sleeping: " + msec + " used: " + time_used);
					}
					Thread.sleep(msec);
				} catch (InterruptedException e) {
					logger.warn("WorldThread sleep interrupted", e);
				}
				tick++;

				// Every 1 min: unload empty guilds
				if ((tick % 600) == 0) {
					unloadEmptyGuilds();
				}

				// Every 15 min
				if ((tick % 9000) == 0) {
					saveGuilds();
					memoryReport();
				}

				// Every 1 min: unload stale instances
				if ((tick % 600) == 0) {
					unloadStaleInstances();
				}

				long time_start = System.currentTimeMillis();
				// Go through all instances and "animate" them
				List<Instance> instances = new LinkedList<Instance>(InstanceManager.getInstance().getInstances());
				for (Instance i : instances) {
					i.action();
				}
				long time_end = System.currentTimeMillis();
				time_used = time_end - time_start;
			}
		}
		catch (Exception e) {
			logger.fatal("WorldThread exiting because of exception", e);
		}
	}

	private void memoryReport() {
		Runtime runtime = Runtime.getRuntime();
		long total = runtime.totalMemory() / 1024;
		long free = runtime.freeMemory() / 1024;
		long used = total - free;
		logger.info("Memory: " + used + "/" + total + " KB used/total, free: " + free + " KB");
	}

	private void unloadEmptyGuilds() {
		try {
			GuildManager.getInstance().unloadEmptyGuilds();
		}
		catch (Exception e) {
			logger.error("Failed to unload empty guilds", e);
		}
	}

	private void unloadStaleInstances() {
		try {
			InstanceManager im = InstanceManager.getInstance();
			synchronized (im) {
				im.unloadInstances(false);
				SceneManager.getInstance().unloadUnusedScenes();

				// Hibernate until we have some instances to animate.
				// We will be woken up by InstanceManager.getInstance()
				while (im.countInstances() == 0) {
					// Do final cleanup before hibernating
					System.gc();
					System.runFinalization();
					SceneManager.getInstance().unloadUnusedScenes();
					System.gc();

					logger.info("No instances, WorldThread hibernating");
					try {
						im.wait();
					} catch (InterruptedException e) {
						logger.warn("Hibernation interrupted", e);
					}
				}
			}
		}
		catch (Exception e) {
			logger.error("Failed to unload stale instances", e);
		}
	}

	private void saveGuilds() {
		try {
			GuildManager.getInstance().autoSaveGuilds();
		}
		catch (Exception e) {
			logger.error("Failed to autosave guilds", e);
		}
	}

}
