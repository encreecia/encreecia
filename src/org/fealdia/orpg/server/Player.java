package org.fealdia.orpg.server;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.fealdia.orpg.common.maps.Animation;
import org.fealdia.orpg.common.maps.ArchetypeManager2;
import org.fealdia.orpg.common.maps.Mobility;
import org.fealdia.orpg.common.maps.SceneManager;
import org.fealdia.orpg.common.net.data.ChatType;
import org.fealdia.orpg.common.net.data.MusicHint;
import org.fealdia.orpg.common.util.FileUtil;
import org.fealdia.orpg.common.util.PathConfig;
import org.fealdia.orpg.common.util.Sha1Util;
import org.fealdia.orpg.common.util.StreamUtil;
import org.fealdia.orpg.common.util.StringUtil;
import org.fealdia.orpg.common.util.TimeUtil;
import org.fealdia.orpg.server.commands.Command;
import org.fealdia.orpg.server.groups.Guild;
import org.fealdia.orpg.server.groups.GuildManager;
import org.fealdia.orpg.server.groups.Party;
import org.fealdia.orpg.server.groups.PartyManager;
import org.fealdia.orpg.server.maps.Instance;
import org.fealdia.orpg.server.maps.InstanceManager;
import org.fealdia.orpg.server.maps.entities.Armor;
import org.fealdia.orpg.server.maps.entities.Consumable;
import org.fealdia.orpg.server.maps.entities.Container;
import org.fealdia.orpg.server.maps.entities.Creature;
import org.fealdia.orpg.server.maps.entities.Item;
import org.fealdia.orpg.server.maps.entities.MapEntity;
import org.fealdia.orpg.server.maps.entities.Money;
import org.fealdia.orpg.server.maps.entities.Monster;
import org.fealdia.orpg.server.maps.entities.PlayerSpawn;
import org.fealdia.orpg.server.maps.entities.ShopKeeper;
import org.fealdia.orpg.server.maps.entities.Weapon;
import org.fealdia.orpg.server.maps.entities.Wearable;
import org.fealdia.orpg.server.maps.entities.persistence.PropertySaveListener;
import org.fealdia.orpg.server.quests.Quest;
import org.fealdia.orpg.server.quests.QuestInstance;
import org.fealdia.orpg.server.quests.QuestLog;
import org.fealdia.orpg.server.quests.QuestManager;

/**
 * A player that is currently connected and logged in. For each player, there is a controlling
 * ClientThread.
 *
 * TODO: stats, or stat experience, stamina, parry timer, carryingCapacity, isBurdened
 */
public class Player extends Creature implements CreatureViewer {
	private static Logger logger = Logger.getLogger(Player.class);
	private static Logger dmLogger = Logger.getLogger("DM");
	
	// Seconds, how long players are marked as criminal from last offense
	private static int CRIMINAL_PERIOD = ServerConfig.getInstance().getPropertyAsInt("player.criminal_period");
	
	private long activeContainer = 0;
	
	private boolean attackMode = false;
	
	private ClientThread clientThread = null;
	
	private boolean fullyLoaded = false;
	
	private Guild guild = null;
	
	private long experience = 0;
	
	List<Item> inventory = new LinkedList<Item>();
	List<Wearable> inventoryEquipped = new LinkedList<Wearable>();
	
	private long lastSaveTime = 0; // secs
	
	private boolean master = false;
	
	private Party party = null;
	
	private int played = 0;
	
	private long loginStamp = System.currentTimeMillis() / 1000;
	
	private String lastSpawnMap = null;
	private int lastSpawnX = -1;
	private int lastSpawnY = -1;
	
	private Creature possessedCreature = null;
	
	private boolean pvp = false; // Player vs. Player mode
	
	private int statsSessions = 0;
	private long statsDamageDone = 0;
	private long statsDamageRecv = 0;
	private int statsDeaths = 0; // all deaths
	private int statsDeathsPVP = 0;
	private int statsKills = 0; // all kills
	private int statsKillsPVP = 0; // player kills
	private int statsKillsPVPCriminal = 0; // player kills while target was not criminal
	
	private QuestLog questLog = new QuestLog();
	private Set<Integer> offeredQuests = new HashSet<Integer>(); // transitive
	
	private int alignment = 0;

	/**
	 * instanceUniqueness and sceneName are only used temporarily when loading XML.
	 */
	private Instance.Uniqueness instanceUniqueness = Instance.Uniqueness.NORMAL;
	private String sceneName;

	public Player() {
		logger.trace("New player created");
		setFace("monsters/warrior_human");
		setRace(Race.HUMANOID);
		setSaveable(false);
		setController(new PlayerController());
		addViewer(this);
	}
	
	public ClientThread getClientThread() {
		return clientThread;
	}

	public void setClientThread(ClientThread clientThread) {
		this.clientThread = clientThread;
	}
	
	public void setMovementVector(int dx, int dy) {
		if (Math.abs(dx) > 1) {
			logger.warn("Normalizing invalid movement vector dx = " + dx);
			dx = dx / Math.abs(dx);
		}
		if (Math.abs(dy) > 1) {
			logger.warn("Normalizing invalid movement vector dy = " + dy);
			dy = dy / Math.abs(dy);
		}
		if (getController() instanceof PlayerController) {
			PlayerController pc = (PlayerController) getController();
			
			pc.setVector(dx, dy);
		}
		//logger.debug("Movement vector set to dx = " + dx + ", dy = " + dy);
	}
	
	public long getExperience() {
		return experience;
	}

	public void setExperience(long experience) {
		this.experience = experience;
		
		setMaxHealth(GameRules.getHealthForLevel(getLevel()));
		
		// Notify client
		if (getClientThread() != null) {
			getClientThread().sendExperience();
		}
	}

	/**
	 * Attempt to apply (use, equip/unequip) the item if possible.
	 */
	public void tryToApplyItem(long item_id) {
		logger.trace("Trying to apply item " + item_id);
		if (isDead()) {
			sendText("Can not apply items when dead.");
			return;
		}
		
		// check if it's in our inventory, and if so, equip/unequip
		Item item = getInventoryItemByID(item_id);
		if (item != null) {
			if (item instanceof Consumable) {
				if (((Consumable)item).use(this)) {
					item.setQuantity(item.getQuantity() - 1);
					if (item.getQuantity() < 1) {
						removeInventoryItem(item);
						item.moveOutOfWorld();
					}
					else {
						onInventoryChanged();
					}
				}
				return;
			}
			else if (item instanceof Container) {
				if (getActiveContainerID() != item.getID()) {
					setActiveContainer(item.getID());
				} else {
					// If already open, close
					setActiveContainer(0);
				}
				return;
			}
			
			if (inventoryEquipped.contains(item)) {
				unequipItem((Wearable)item);
			}
			else if (item instanceof Wearable) {
				equipItem((Wearable)item);
			}
		} else {
			sendText("Can't apply an item that you don't have");
		}
	}
	
	/**
	 * Attempt to get the item, if possible. 
	 */
	public void tryToGetItem(long item_id, int quantity) {
		logger.trace("Trying to pick up item " + item_id);
		MapEntity en = getInstance().getEntityByID(item_id);
		
		// Can't do anything for the active container
		if (item_id == getActiveContainerID()) {
			sendText("Oops, you can't do that");
			return;
		}
		
		if (en instanceof Item) {
			Item i = (Item) en;
			if (distanceTo(i) == 0 && i.canPickUp()) {
				// TODO if we have an active container, put it straight there?
				
				// Split stack if requested
				Item split = i.splitFromStack(quantity);
				if (split != null) {
					addInventoryItem(split, true);
				}
				else {
					addInventoryItem(i, true);
				}
			}
		} else {
			// The item might be in our inventory
			Item i = getInventoryItemByID(item_id);
			if (i != null) {
				// If we have active container, move it there
				if (getActiveContainerID() != 0 && getActiveContainer() != null) {
					Container c = getActiveContainer();
					if (c != null) {
						if (c.canReceive(i)) {
							removeInventoryItem(i);
							c.addItem(i);
							onContainerChanged(activeContainer, c.getItems());
						} else {
							sendText(i.getName() + " does not fit into " + c.getName());
						}
					}
				}
				// Otherwise try to drop it
				else {
					tryToDropItem(item_id, quantity);
				}
			}
			// Neither on the ground nor in our inventory, so see if it's in the active container
			else {
				if (getActiveContainerID() != 0) {
					Container c = getActiveContainer();
					if (c != null) {
						Item i2 = c.getItemByID(item_id);
						if (i2 != null) {
							c.removeItem(i2);
							onContainerChanged(activeContainer, c.getItems());
							addInventoryItem(i2, true);
						}
					}
				}
			}
		}
		
		// Check if ShopKeeper is selling the item when shopping
		if (isShopping()) {
			ShopKeeper shop = getShopKeeper();
			
			List<Item> items = shop.getShopList();
			for (Item item : items) {
				if (item.getID() == item_id) {
					// Attempt to buy it
					int value = shop.getBuyPrice(item);
					if (removeMoney(value)) {
						Item cloned = item.cloneItem();
						addInventoryItem(cloned, true);
						sendText("You bought " + cloned.getName() + " for " + value);
					}
					else {
						sendText("You do not have enough money to buy " + item.getName() + " (" + item.getValue() + ")");
					}
				}
			}
		}
	}
	
	public void tryToDropItem(long item_id, int quantity) {
		logger.trace("Trying to drop item " + item_id);
		for (Item item : inventory) {
			if (item.getID() == item_id) {
				
				// Split stack if requested
				Item split = item.splitFromStack(quantity);
				if (split != null) {
					addInventoryItem(split, false);
					item = split;
				}
				
				// If shopping, sell the item
				if (isShopping()) {
					ShopKeeper shop = getShopKeeper();
					
					int worth = shop.getSellPrice(item);
					if (worth == 0) {
						sendText("The shopkeeper does not want that.");
					}
					else {
						sendText("You get " + worth + " for " + item.getQuantity() + " " + item.getName());
						removeInventoryItem(item);
						item.moveOutOfWorld();
						addMoney(worth);
					}
				}
				// Otherwise drop it to ground
				else {
					removeInventoryItem(item);
				}
				
				break;
			}
		}
	}
	
	/**
	 * Find a money stack. If none, create one, and add to it.
	 */
	private void addMoney(int worth) {
		// Add to existing stack
		for (Item item : getInventory()) {
			if (item instanceof Money) {
				item.setQuantity(item.getQuantity() + worth);
				onInventoryChanged();
				return;
			}
		}
		
		// Create new money stack
		Money money = new Money();
		money.setQuantity(worth);
		addInventoryItem(money, true);
	}

	public void addInventoryItem(Item item, boolean merge_stacks) {
		item.moveOutOfWorld();
		
		logger.trace("Item added to inventory: " + item.getName());

		// Look for any existing stacks to merge with
		boolean merged = false;
		if (merge_stacks && item.isStackable()) {
			for (Item other : getInventory()) {
				if (other.stacksWith(item)) {
					logger.debug("Merging two item stacks");
					other.setQuantity(other.getQuantity() + item.getQuantity());
					merged = true;
					break;
				}
			}
		}
		if (!merged) {
			item.setSaveable(true);
			inventory.add(item);
		}

		onInventoryChanged();
	}

	/**
	 * Remove an item from inventory and drop it below us.
	 */
	public void removeInventoryItem(Item item) {
		if (item instanceof Wearable) {
			unequipItem((Wearable)item);
		}
		inventory.remove(item);
		item.moveBelow(this);
		item.onDrop();

		if (logger.isTraceEnabled()) {
			logger.trace(getID() + ": Item removed from inventory " + item.getID() + "(" + item.getName() + ")");
		}

		onInventoryChanged();
	}

	@Override
	protected void onAttack(Creature target, int damage, long experience) {
		if (experience > 0) {
			// If in a party, experience is shared
			if (isInParty()) {
				getParty().addExperience(experience);
			}
			else {
				addExperience(experience, false);
			}
		}
		
		setStatsDamageDone(getStatsDamageDone() + damage);
		if (target.isDead()) {
			onKill(target);
		}
		
		sendText("You hit " + target.getName() + " for " + damage);
		if (experience > 0 && !isInParty()) {
			sendText("Got " + experience + " experience");
		}
		
		// If the target is not our enemy (eg. it's friendly), mark us as criminal
		if (!isEnemy(target)) {
			markCriminal();
		}
	}
	
	protected void onKill(Creature target) {
		setStatsKills(getStatsKills() + 1);
		questLog.onKill(target.getArchetype());
		if (target instanceof Player) {
			setStatsKillsPVP(getStatsKillsPVP() + 1);
			Player p = (Player) target;
			if (!p.isCriminal()) {
				setStatsKillsPVPCriminal(getStatsKillsPVPCriminal() + 1);
				alignment += Alignment.BAD_KILL_PVP;
			}
		}
		else {
			if (isEnemy(target)) {
				alignment += Alignment.GOOD_KILL_NPC;
			}
			else {
				alignment += Alignment.BAD_KILL_NPC;
			}
		}
	}

	/**
	 * Called when a nearby Creature is hit
	 */
	@Override
	public void onCreatureHit(Creature attacker, Creature target, int damage) {
		long id = target.getID();
		if (target == this) {
			id = 0;
		}
		
		String face = "effects/red_explosion";
		if (damage == 0) {
			face = "effects/aqua_explosion_small";
		}

		getClientThread().sendEntityEffect(id, face, 200, null);
	}

	@Override
	public void onSpellCast(Creature source, Creature target, Spell spell) {
		String face = Animation.PREFIX + "magic";
		if (spell.isOffensive()) {
			face = Animation.PREFIX + "magic_harmful";
		}

		getClientThread().sendEntityEffect(source == this ? 0 : source.getID(), Animation.PREFIX + "magic", 500, null);
		if (source != target) {
			getClientThread().sendEntityEffect(target == this ? 0 : target.getID(), face, 500, null);
		}
	}

	@Override
	public void saveToStream(Writer out) throws IOException {
		// save before "map" property
		if (getInstance().getUniqueness() != Instance.Uniqueness.NORMAL) {
			out.write("instance_uniqueness " + getInstance().getUniqueness().toString() + "\n");
		}
		out.write("map " + getInstance().getName() + "\n");
		
		super.saveToStream(out);
		
		out.write("experience " + getExperience() + "\n");
		out.write("played " + getPlayedTotal() + "\n");
		savePropertyToStream(out, "alignment", alignment, false);
		savePropertyToStream(out, "sessions", getStatsSessions(), false);
		savePropertyToStream(out, "deaths", getStatsDeaths(), false);
		savePropertyToStream(out, "deaths_pvp", getStatsDeathsPVP(), false);
		savePropertyToStream(out, "damage_done", getStatsDamageDone(), false);
		savePropertyToStream(out, "damage_recv", getStatsDamageRecv(), false);
		savePropertyToStream(out, "kills", getStatsKills(), false);
		savePropertyToStream(out, "kills_pvp", getStatsKillsPVP(), false);
		savePropertyToStream(out, "kills_pvp_criminal", getStatsKillsPVPCriminal(), false);
		if (isInGuild()) {
			out.write("guild " + getGuild().getTag() + "\n");
		}
		if (lastSpawnMap != null) {
			out.write("lastspawnmap " + getLastSpawnMap() + "\n");
			out.write("lastspawnx " + getLastSpawnX() + "\n");
			out.write("lastspawny " + getLastSpawnY() + "\n");
		}
		
		// Save inventory
		if (inventory.size() > 0) {
			out.write("<inventory>\n");
			for (Item item : inventory) {
				item.saveToStreamWithHeader(out);
			}
			out.write("</inventory>\n");
		}
		
		updateLastSaveTime();
	}

	@Override
	public void save(PropertySaveListener listener) throws Exception {
		if (getInstance() != null) {
			// save before "map" property
			if (getInstance().getUniqueness() != Instance.Uniqueness.NORMAL) {
				listener.saveProperty("instance_uniqueness", getInstance().getUniqueness());
			}
			listener.saveProperty("map", getInstance().getName());
		}

		super.save(listener);

		listener.saveProperty("experience", getExperience());
		listener.saveProperty("played", getPlayedTotal());
		saveProperty(listener, "alignment", alignment);
		saveProperty(listener, "sessions", getStatsSessions());
		saveProperty(listener, "deaths", getStatsDeaths());
		saveProperty(listener, "deaths_pvp", getStatsDeathsPVP());
		saveProperty(listener, "damage_done", getStatsDamageDone());
		saveProperty(listener, "damage_recv", getStatsDamageRecv());
		saveProperty(listener, "kills", getStatsKills());
		saveProperty(listener, "kills_pvp", getStatsKillsPVP());
		saveProperty(listener, "kills_pvp_criminal", getStatsKillsPVPCriminal());
		if (isInGuild()) {
			listener.saveProperty("guild", getGuild().getTag());
		}
		if (lastSpawnMap != null) {
			saveProperty(listener, "lastspawnmap", getLastSpawnMap());
			saveProperty(listener, "lastspawnx", getLastSpawnX());
			saveProperty(listener, "lastspawny", getLastSpawnY());
		}

		// Save inventory
		if (inventory.size() > 0) {
			listener.startElement("inventory");
			for (Item item : inventory) {
				item.saveWithHeader(listener);
			}
			listener.endElement();
		}

		updateLastSaveTime();
	}

	// TODO figure out a way to merge this with MapEntity?
	@Override
	public void loadFromStream(BufferedReader in, boolean has_overlay) throws IOException {
		int x = 0;
		int y = 0;
		Instance instance = null;
		Instance.Uniqueness uniqueness = Instance.Uniqueness.NORMAL;
		
		for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
			if (line.startsWith("</")) {
				return;
			}
			
			if (line.startsWith("<inventory>")) {
				List<MapEntity> ents = MapEntity.loadEntitiesFromStream(in);
				for (MapEntity en : ents) {
					Item item = (Item) en;
					addInventoryItem(item, true);
					if (item instanceof Wearable && ((Wearable)item).isEquipped()) {
						equipItem((Wearable)item);
					}
				}
				continue;
			}
			
			String[] parts = line.split(" +", 2);
			if ("map".equals(parts[0])) {
				instance = InstanceManager.getInstance().getInstance(parts[1], uniqueness, this);
			}
			else if ("x".equals(parts[0])) {
				x = Integer.parseInt(parts[1]);
			}
			else if ("y".equals(parts[0])) {
				y = Integer.parseInt(parts[1]);
			}
			else if ("instance_uniqueness".equals(parts[0])) {
				uniqueness = Instance.Uniqueness.valueOf(parts[1].toUpperCase());
			}
			else if (!handlePropertyEdit(parts[0], parts[1])) {
				logger.warn("Unhandled line: " + line);
			}
		}
		moveToInstance(instance, x, y);
		
		updateLastSaveTime();
	}
	
	/**
	 * Handle a textual command from client. The input should be validated properly.
	 */
	public boolean handleClientCommand(String command) {
		logger.trace("Client command: " + command);
		
		String[] parts = StringUtil.splitIntoParameters(command);
		if (parts.length == 0) {
			return true;
		}
		String cmd = parts[0];
		
		if (handleClientCommandNew(cmd, command, Arrays.copyOfRange(parts, 1, parts.length))) {
			return true;
		}
		logger.debug("Falling back to old implementation");

		if ("guild".equals(cmd) && parts.length >= 2) {
			GuildManager.getInstance().handleCommand(this, Arrays.copyOfRange(parts, 1, parts.length));
		}
		else if ("party".equals(cmd) && parts.length >= 2) {
			PartyManager.getInstance().handleCommand(this, Arrays.copyOfRange(parts, 1, parts.length));
		}
		else if (Emote.getEmote(cmd) != null) {
			handleEmotion(cmd, Arrays.copyOfRange(parts, 1, parts.length));
		}
		
		else {
			sendText("Invalid command: " + command);
			return false;
		}
		
		return true;
	}

	/**
	 * Get file content in a string, or null if not found.
	 */
	private static String getFileContent(String filename) {
		StringBuffer result = new StringBuffer();
		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(filename));
			for (String line = StreamUtil.readLine(in); line != null; line = StreamUtil.readLine(in)) {
				result.append(line);
				result.append("\n");
			}
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			logger.warn("Error getting contents for file " + filename, e);
			return null;
		}
		try {
			in.close();
		} catch (Exception e) {}
		
		return result.toString();
	}

	@Override
	public boolean handlePropertyEdit(String key, String value) {
		if ("alignment".equals(key)) {
			alignment = Integer.parseInt(value);
		}
		else if ("damage_done".equals(key)) {
			setStatsDamageDone(Long.parseLong(value));
		}
		else if ("damage_recv".equals(key)) {
			setStatsDamageRecv(Long.parseLong(value));
		}
		else if ("deaths".equals(key)) {
			setStatsDeaths(Integer.parseInt(value));
		}
		else if ("deaths_pvp".equals(key)) {
			setStatsDeathsPVP(Integer.parseInt(value));
		}
		else if ("experience".equals(key)) {
			setExperience(Long.parseLong(value));
		}
		else if ("guild".equals(key)) {
			Guild g = GuildManager.getInstance().getGuild(value);
			if (g == null) {
				throw new RuntimeException("Failed to load guild");
			}
			// Check whether we are still in the guild
			if (g.isMember(getName())) {
				setGuild(g);
				g.loginPlayer(this);
			} else {
				sendText("You were kicked out of your guild while you were offline.");
			}
		}
		else if ("kills".equals(key)) {
			setStatsKills(Integer.parseInt(value));
		}
		else if ("kills_pvp".equals(key)) {
			setStatsKillsPVP(Integer.parseInt(value));
		}
		else if ("kills_pvp_criminal".equals(key)) {
			setStatsKillsPVPCriminal(Integer.parseInt(value));
		}
		else if ("lastspawnmap".equals(key)) {
			setLastSpawnMap(value);
		}
		else if ("lastspawnx".equals(key)) {
			setLastSpawnX(Integer.parseInt(value));
		}
		else if ("lastspawny".equals(key)) {
			setLastSpawnY(Integer.parseInt(value));
		}
		else if ("played".equals(key)) {
			setPlayed(Integer.parseInt(value));
		}
		else if ("sessions".equals(key)) {
			setStatsSessions(Integer.parseInt(value));
		}
		else if ("instance_uniqueness".equals(key)) {
			instanceUniqueness = Instance.Uniqueness.valueOf(value);
		}
		else if ("map".equals(key)) {
			sceneName = value;
		}
		else {
			return super.handlePropertyEdit(key, value);
		}
		return true;
	}
	
	@Override
	public void die() {
		super.die();
		logger.debug("Dying");
		
		//criminalLast = 0;
		
		// TODO drop items?
		
		// Create a corpse
		createCorpse().moveBelow(this);
		
		respawn();
		getClientThread().sendNotice("You have died.");
		getClientThread().sendMusicHint(MusicHint.DEATH);
	}
	
	/**
	 * Send text to client
	 */
	public void sendText(String text) {
		getClientThread().sendNotice(text);
	}
	
	public String getRemoteIP() {
		return getClientThread().getRemoteIP();
	}

	@Override
	public int getLevel() {
		return GameRules.getLevelFromExperience(getExperience());
	}
	
	/**
	 * Whether this player is a dungeon master
	 */
	public boolean isMaster() {
		return master;
	}

	public void setMaster(boolean master) {
		if (this.master == master) {
			return;
		}
		this.master = master;
		String logIdentity = getName() + "@" + getRemoteIP() + "[" + getInstance().getName() + "]";
		if (master) {
			sendText("You are now a dungeon master.");
			PlayerManager.getInstance().sendGlobalNotice("A dungeon master has logged in.");
			resyncVisibleEntities();
			dmLogger.info("+ DM " + logIdentity);
		} else {
			sendText("You are no longer a dungeon master.");
			resyncVisibleEntities();
			dmLogger.info("- DM " + logIdentity);
		}
		getClientThread().sendMaster();
		notifyPlayersOfEntityChange(); // name color change
	}
	
	public List<Item> getInventory() {
		return inventory;
	}
	
	public Item getInventoryItemByID(long id) {
		for (Item item : getInventory()) {
			if (item.getID() == id) {
				return item;
			}
		}
		return null;
	}
	
	public boolean equipItem(Wearable item) {
		if (item.getSlot() == Slot.NONE) {
			sendText("That item cannot be equipped");
			return false;
		}
		if (getLevel() < item.getLevel()) {
			sendText("You need to be level " + item.getLevel() + " to use that item.");
			return false;
		}
		if (getEquippedBySlot(item.getSlot()) != null) {
			logger.debug("Slot already used");
			sendText("You already have something equipped in that slot.");
			return false;
		}
		
		inventoryEquipped.add(item);
		item.setEquipped(true);
		logger.trace("Equipped item: " + item.getName());
		
		// Notify client
		onInventoryChanged();
		
		return true;
	}
	
	public void unequipItem(Wearable item) {
		inventoryEquipped.remove(item);
		item.setEquipped(false);
		logger.trace("Unequipped item: " + item.getName());
		
		// Notify client
		onInventoryChanged();
	}
	
	/**
	 * Get equipped item by slot if any.
	 */
	public Wearable getEquippedBySlot(Slot slot) {
		for (Wearable item : inventoryEquipped) {
			if (item.getSlot() == slot) {
				return item;
			}
		}
		return null;
	}
	
	public Weapon getWeapon() {
		Item w = getEquippedBySlot(Slot.WEAPON);
		if (w instanceof Weapon) {
			return (Weapon) w;
		}
		return null;
	}

	@Override
	public int getWeaponDamage(Creature target) {
		Weapon w = getWeapon();
		if (w != null) {
			return w.calculateWeaponDamage(target);
		}
		return super.getWeaponDamage(target);
	}

	@Override
	public int getWeaponSpeed() {
		Weapon w = getWeapon();
		if (w != null) {
			return w.getWeaponSpeed();
		}
		return super.getWeaponSpeed();
	}

	/**
	 * Get played time when we logged in.
	 */
	public int getPlayed() {
		return played;
	}
	
	public void setPlayed(int played) {
		this.played = played;
	}
	
	/**
	 * How many seconds we have been logged in.
	 */
	public int getLoggedInTime() {
		return (int) (System.currentTimeMillis() / 1000 - loginStamp);
	}
	
	public int getPlayedTotal() {
		return getLoggedInTime() + getPlayed();
	}

	/**
	 * Called when player tries to look at something.
	 */
	public void lookAt(int x, int y) {
		List<MapEntity> entities = getInstance().getEntitiesAt(x, y);
		for (MapEntity en : entities) {
			if (!en.isVisible() && !canSeeInvisible()) {
				continue;
			}
			
			StringBuffer description = new StringBuffer("You see ");
			if (en instanceof Item) {
				Item item = (Item) en;
				if (item.getQuantity() > 1) {
					description.append(item.getQuantity() + " ");
				}
			}
			description.append(en.getName());
			if (en instanceof Player) {
				Player pl = (Player) en;
				if (pl.isInGuild()) {
					description.append(" of " + pl.getGuild().getName());
				}
			}
			
			if (isMaster()) {
				description.append(" (id=" + en.getID() + ")");
			}
			
			sendText(description.toString());
		}
	}
	
	public boolean isItemEquipped(Item item) {
		return inventoryEquipped.contains(item);
	}

	@Override
	public int getAttackRange() {
		Weapon w = getWeapon();
		if (w != null) {
			return w.getWeaponRange(); 
		}
		return 1;
	}
	
	/**
	 * Get equipped ammo, if any.
	 */
	public Item getAmmo() {
		return getEquippedBySlot(Slot.AMMO);
	}

	@Override
	public boolean canAttack(Creature target) {
		boolean ok = super.canAttack(target);
		if (!ok) {
			return false;
		}
		// Do not attack if 1) attack mode is off 2) PvP is off and target is not enemy
		if (!isAttackMode() || (!isPvp() && !isEnemy(target))) {
			return false;
		}
		
		// Do we have a weapon needing ammo?
		Weapon w = getWeapon();
		if (w == null || !w.isNeedingAmmo()) {
			// Nope, it's ok to attack
			return true;
		}
		
		// Do we have the right type of ammo equipped?
		Wearable ammo = getEquippedBySlot(Slot.AMMO);
		if (ammo == null) {
			return false;
		}
		return ammo.getAmmoType() == w.getAmmoType();
	}

	@Override
	public void attackCreature(Creature target) {
		super.attackCreature(target);
		
		// If our weapon needs ammo, decrement it
		Weapon w = getWeapon();
		if (w == null || !w.isNeedingAmmo()) {
			return;
		}
		Item ammo = getEquippedBySlot(Slot.AMMO);
		if (ammo == null) {
			logger.error("Attacking with a ranged weapon without ammo?");
			return;
		}
		int quantity = ammo.getQuantity() - 1;
		if (quantity <= 0) {
			// None left, remove the whole stack
			removeInventoryItem(ammo);
			ammo.moveOutOfWorld();
		} else {
			ammo.setQuantity(quantity);
		}
		
		// Notify client of inventory change
		onInventoryChanged();
	}

	@Override
	public boolean canSeeInvisible() {
		// DM can see invisible entities (spawns etc)
		return isMaster();
	}

	public boolean isFullyLoaded() {
		return fullyLoaded;
	}

	public void setFullyLoaded(boolean fullyLoaded) {
		this.fullyLoaded = fullyLoaded;
	}

	@Override
	protected void onTargetHealthChange(Creature target, int difference) {
		// Send to client
		getClientThread().sendTargetHealth(target.getID(), target.getHealthPercentage());
	}

	public void examineItem(long id) {
		Item i = findItem(id);
		if (i != null) {
			sendText(i.getExamineText());
			if (isShopping()) {
				sendText("The shopkeeper would buy it for " + getShopKeeper().getSellPrice(i));
			}
			return;
		}
		
		// Shopping?
		if (isShopping()) {
			ShopKeeper shop = getShopKeeper();
			Item i2 = shop.getItemByID(id);
			if (i2 != null) {
				sendText(i2.getExamineText());
				sendText("The shopkeeper would sell it for " + shop.getBuyPrice(i2));
				return;
			}
		}
		
		sendText("No such item");
		logger.warn("Player " + getName() + " tried to examine item " + id + " which could not be found");
		return;
	}
	
	public Item findItem(long id) {
		// Inventory?
		Item i = getInventoryItemByID(id);
		if (i != null) {
			return i;
		}
		
		// in active container?
		Container con = getActiveContainer();
		if (con != null) {
			Item i2 = con.getItemByID(id);
			if (i2 != null) {
				return i2;
			}
		}
		
		// Below us?
		MapEntity en = getInstance().getEntityByID(id);
		if (en instanceof Item && distanceTo(en) == 0) {
			return (Item) en;
		}
		
		return null;
	}

	public boolean isAttackMode() {
		return attackMode;
	}

	public void setAttackMode(boolean attackMode) {
		this.attackMode = attackMode;
		getClientThread().sendAttackMode(isAttackMode());
	}

	public int getArmorPoints() {
		int blocked = 0;
		for (Item item : inventoryEquipped) {
			if (item instanceof Armor) {
				Armor armor = (Armor) item;
				blocked += armor.getBlock();
			}
		}
		
		return blocked;
	}
	
	/**
	 * Find closest player spawn in this instance.
	 */
	public PlayerSpawn findClosestPlayerSpawn() {
		PlayerSpawn spawn = null;
		int distance = -1;
		for (MapEntity en : getInstance().getEntities()) {
			// Is this PlayerSpawn closer than the earlier one?
			if (en instanceof PlayerSpawn && (distance == -1 || distanceTo(en) < distance)) {
				spawn = (PlayerSpawn) en;
				distance = distanceTo(en);
			}
		}
		return spawn;
	}

	public long getActiveContainerID() {
		return activeContainer;
	}
	
	public Container getActiveContainer() {
		return (Container) getInventoryItemByID(getActiveContainerID());
	}

	public void setActiveContainer(long activeContainer) {
		if (this.activeContainer == activeContainer) {
			return;
		}
		long oldContainer = this.activeContainer;
		
		this.activeContainer = activeContainer;

		// Closed
		if (activeContainer == 0) {
			getClientThread().sendContainerClosed(oldContainer);
			sendText("Container closed");
		}
		else {
			List<Item> items = null;
			
			MapEntity en = getInstance().getEntityByID(activeContainer);
			if (en instanceof ShopKeeper) {
				items = ((ShopKeeper)en).getShopList();
			}
			else if (getActiveContainer() != null) {
				items = getActiveContainer().getItems();
			}
			else {
				logger.warn("Unsupported active container type, sending empty list");
				// TODO send actual container contents
				items = new LinkedList<Item>();
			}

			onContainerChanged(activeContainer, items);
		}
	}

	/**
	 * Called when a map position is double-clicked on client side.
	 */
	public void handleUse(int x, int y) {
		// Look for shopkeeper first
		ShopKeeper en = (ShopKeeper) getInstance().getEntityByTypeAt(ShopKeeper.class, x, y);
		if (en != null && distanceTo(en) <= 2) {
			sendText("You are shopping, drop items to sell them.");
			setActiveContainer(en.getID());
			return;
		}
		Monster monster = (Monster) getInstance().getEntityByTypeAt(Monster.class, x, y);
		if (monster != null && distanceTo(monster) <= 1) {
			// If talking with the Monster works, then return
			if (monster.talkWith(this)) {
				getClientThread().sendMusicHint(MusicHint.TOWN);
				return;
			}
		}
		
		// Look for all entities at the destination, and call onGroundUse() until one of them accepts it
		for (MapEntity e : getInstance().getEntitiesAt(x, y)) {
			if (canSee(e) && e.onGroundUse(this)) {
				return;
			}
		}
	}

	/**
	 * Are we shopping right now?
	 */
	public boolean isShopping() {
		long id = getActiveContainerID();
		if (id == 0) {
			return false;
		}
		MapEntity en = getInstance().getEntityByID(id);
		return (en instanceof ShopKeeper);
	}
	
	public void onInventoryChanged() {
		onContainerChanged(0, inventory);
	}

	public void onContainerChanged(long containerId, List<Item> items) {
		// Don't send containers while still loading player
		if (!isFullyLoaded()) {
			return;
		}

		if (logger.isTraceEnabled()) {
			logger.trace("Sending container #" + containerId);
		}
		getClientThread().sendContainer(containerId, items);
	}

	public void onEntityChanged(MapEntity mapEntity) {
		try {
			if (getClientThread() != null) {
				getClientThread().sendEntityInfo(mapEntity.getID());
			}
		} catch (IOException e) {
			logger.error("noticeEntityChanged() caused exception", e);
		}
	}

	public void handleMapClicked(int x, int y) {
		if (isMaster()) {
			move(x, y);
		}
		else if (isInParty()) {
			getParty().setPartyMarker(this, x, y);
		}
	}

	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		Party oldParty = this.party;
		if (this.party == party) {
			return;
		}
		
		this.party = party;
		
		if (party != null) {
			sendText("You have joined a party. Any experience gained is shared among members.");
			party.addMember(getID());
		} else {
			sendText("You have left the party.");
			oldParty.removeMember(getID());
			getClientThread().sendPartyLeft();
		}
	}

	public boolean isInParty() {
		return (getParty() != null);
	}
	
	/**
	 * Should be called before the socket is closed, by ClientThread.logout().
	 */
	public void logout() {
		unpossess();
		if (isInGuild()) {
			getGuild().logoutPlayer(this);
		}
		setParty(null);
		setStatsSessions(getStatsSessions() + 1);
	}
	
	/**
	 * Called when player really gains experience.
	 */
	public synchronized void addExperience(long experience, boolean fromParty) {
		int oldLevel = getLevel();
		
		setExperience(getExperience() + experience);
		
		int newLevel = getLevel();

		if (fromParty) {
			sendText("You got " + experience + " experience from the party.");
		}
		
		if (newLevel > oldLevel) {
			sendText("Level up! You are now level " + getLevel() + ". Experience required for level "+ (getLevel() + 1) + " is " + GameRules.getExperienceForLevel(getLevel() + 1));
		}
		
		if (isInGuild()) {
			getGuild().addExperience(experience);
		}
	}

	@Override
	protected void onDamageReceived(Creature source, int damage, boolean died) {
		setStatsDamageRecv(getStatsDamageRecv() + damage);
		if (damage > 0) {
			sendText(source.getName() + " hits you for " + damage);
		}
		if (died) {
			setStatsDeaths(getStatsDeaths() + 1);
			if (source instanceof Player) {
				setStatsDeathsPVP(getStatsDeathsPVP() + 1);
			}
		}
	}
	
	public boolean isCriminal() {
		return (hasBuff(BuffType.CRIMINAL));
	}
	
	public void markCriminal() {
		if (!isCriminal()) {
			sendText("You have been marked as a criminal. Other players and guards can freely attack you.");
		}
		
		giveBuff(BuffType.CRIMINAL, CRIMINAL_PERIOD);
		
		// Guards don't need to be notified here as they will already react to anyone friendly being attacked
	}

	@Override
	public boolean isEnemy(Creature creature) {
		if (creature instanceof Monster) {
			Monster m = (Monster) creature;
			if (m.getAlignment() == Alignment.EVIL) {
				return true;
			}
		}
		
		if (creature instanceof Player) {
			Player p = (Player) creature;
			if (p.isCriminal()) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public void setInstance(Instance instance) {
		if (isInParty()) {
			getParty().sendPositionClearFor(this);
		}
		
		super.setInstance(instance);
		
		// If instance is not null, send the name to client
		if (instance != null && getClientThread() != null) {
			getClientThread().sendInstanceName(instance.getName());
		}
		
		// If in party, send positions of all other members
		if (isInParty()) {
			getParty().sendMemberPositionsFor(this);
		}
	}

	@Override
	public void onPositionChanged(int x, int y) {
		super.onPositionChanged(x, y);
		
		//getClientThread().sendPosition(x, y);
		
		// Close active container, if any (shopkeeper etc)
		setActiveContainer(0);
		
		// If in a party, notify all members that are in same instance
		if (isInParty()) {
			getParty().sendPositionForOthers(this);
		}
	}
	
	/**
	 * Attempt to remove given amount of money from inventory.
	 * 
	 * @return false if not enough money
	 */
	public boolean removeMoney(int amount) {
		// Add to existing stack
		for (Item item : getInventory()) {
			if (item instanceof Money) {
				int quantity = item.getQuantity();
				if (quantity > amount) {
					item.setQuantity(item.getQuantity() - amount);
					onInventoryChanged();
					return true;
				}
				else if (quantity == amount) {
					removeInventoryItem(item);
					item.moveOutOfWorld();
					return true;
				}
			}
		}
		
		return false;
	}

	public Guild getGuild() {
		return guild;
	}

	public void setGuild(Guild guild) {
		this.guild = guild;

		if (guild != null && guild.getMotd() != null) {
			sendText("Guild motd: " + guild.getMotd());
		}
	}
	
	public boolean isInGuild() {
		return getGuild() != null;
	}
	
	public boolean isGuildLeader() {
		return isInGuild() && getGuild().getLeader().equals(getName());
	}

	public long getLastSaveTime() {
		return lastSaveTime;
	}
	
	public void updateLastSaveTime() {
		lastSaveTime = System.currentTimeMillis() / 1000;
	}

	@Override
	public boolean canSwapPlaceWith(Creature creature) {
		return !isAttackMode() && creature instanceof Player;
	}

	public void handleEmotion(String cmd, String[] params) {
		String text = null;
		String who = null;
		
		Emote em = Emote.getEmote(cmd);
		
		if (getTarget() != null) {
			who = getTarget().getName();
		}
		if (params.length > 0) {
			who = params[0];
			
			// check that player name is valid
			Player pl = PlayerManager.getInstance().getPlayerByName(who);
			if (pl == null || !canSee(pl)) {
				sendText("You can't see that player");
				return;
			}
		}
		
		if (who == null) {
			text = em.withoutTarget();
		}
		else {
			text = em.withTarget(who);
		}
		
		List<MapEntity> entities = getInstance().getEntitiesAtRadius(getX(), getY(), VISIBLE_RADIUS);
		for (MapEntity e : entities) {
			if (e instanceof Player) {
				Player p = (Player) e;
				p.getClientThread().sendChat(getName(), text, ChatType.EMOTE);
			}
		}

	}
	
	/**
	 * Respawn the player either at closest spawn, the last spawn, or server's global starting spawn.
	 */
	public void respawn() {
		// Does this instance have a player spawn?
		PlayerSpawn spawn = findClosestPlayerSpawn();
		if (spawn != null) {
			move(spawn.getX(), spawn.getY());
		}
		
		// No spawn in this instance -> try to spawn at the last player spawn we spawned at
		else if (spawn == null && getLastSpawnMap() != null) {
			Instance inst = InstanceManager.getInstance().getInstance(getLastSpawnMap());
			if (inst != null) {
				moveToInstance(inst, getLastSpawnX(), getLastSpawnY());
			} else {
				logger.error("Failed to spawn player " + getName() + " at last saved spawn");
			}
		}
		
		// spawn at server's global spawn
		else {
			logger.warn("Spawning player " + getName() + " at server spawn");
			
			respawnAtStartingSpawn();
		}
		
		// Respawn with half the health, but 1 at minimum
		setHealth(Math.max(getMaxHealth() / 2, 1));
		
		logger.info("Player " + getName() + " respawned at " + getX() + "," + getY() + "@" + getInstance().getName());
		
		// Save the spawn
		setSpawnHere();
	}

	public void setSpawnHere() {
		setLastSpawnMap(getInstance().getName());
		setLastSpawnX(getX());
		setLastSpawnY(getY());
	}
	
	/**
	 * Respawn the player at the server's starting spawn.
	 */
	public void respawnAtStartingSpawn() {
		// Move player into the instance
		Instance i = InstanceManager.getInstance().getInstance(ServerConfig.getInstance().getProperty("starting_map"));
		
		// find the spawn closest to 1,1
		moveToInstance(i, 1, 1);
		PlayerSpawn spawn = findClosestPlayerSpawn();
		if (spawn != null) {
			moveToOrNearby(i, spawn.getX(), spawn.getY());
		}
	}

	public String getLastSpawnMap() {
		return lastSpawnMap;
	}

	public int getLastSpawnX() {
		return lastSpawnX;
	}

	public int getLastSpawnY() {
		return lastSpawnY;
	}

	public void setLastSpawnMap(String lastSpawnMap) {
		this.lastSpawnMap = lastSpawnMap;
	}

	public void setLastSpawnX(int lastSpawnX) {
		this.lastSpawnX = lastSpawnX;
	}

	public void setLastSpawnY(int lastSpawnY) {
		this.lastSpawnY = lastSpawnY;
	}

	@Override
	public Mobility getMobility() {
		if (isMaster()) {
			return Mobility.DM;
		}
		return super.getMobility();
	}
	
	public boolean handleClientCommandNew(String command, String raw_line, String[] args) {
		//logger.debug("handleClientCommandNew(command = " + command + ", raw_line = " + raw_line + ", args = " + args + ")");
		
		Command cmd = null;
		
		for (Command c : Command.values()) {
			if (command.startsWith(c.toString().toLowerCase())) {
				cmd = c;
			}
		}
		if (cmd == null) {
			return false;
		}
		
		// If DM command and we don't have DM, bail out 
		if (cmd.isDMCommand() && !isMaster()) {
			sendText("Only dungeon master can use that command");
			return false;
		}
		
		// Not enough parameters?
		if (cmd.getMinArgs() > args.length) {
			sendText("Not enough parameters for " + command + ", " + cmd.getMinArgs() + " required");
			return true;
		}
		
		// check paramspec if specified
		if (cmd.getParamSpec() != null) {
			String[] expectedType = cmd.getParamSpec().split(" ");
			if (expectedType.length != cmd.getMinArgs()) {
				logger.error("Paramspec parts length does not match min args");
			}
			for (int i = 0; i < expectedType.length; i++) {
				boolean ok = true;
				if ("ID".equals(expectedType[i])) {
					try {
						long id = Long.parseLong(args[i]);
						MapEntity en = getInstance().getEntityByID(id);
						if (en == null) {
							ok = false;
						}
					} catch (NumberFormatException e) {
						ok = false;
					}
				}
				else if ("INT".equals(expectedType[i])) {
					try {
						Integer.parseInt(args[i]);
					} catch (NumberFormatException e) {
						ok = false;
					}
				}
				else if ("PLAYER".equals(expectedType[i])) {
					Player pl = PlayerManager.getInstance().getPlayerByName(args[i]);
					if (pl == null) {
						ok = false;
					}
				}
				else if ("STRING".equals(expectedType[i])) {
					if (args[i].length() == 0) {
						ok = false;
					}
				}
				else {
					logger.error("Unhandled paramspec type, so not checked: " + expectedType[i]);
				}
				
				if (!ok) {
					sendText(command + ": Parameter " + (i + 1) + " should be of type " + expectedType[i]);
					return true;
				}
			}
		}
		
		// Log DM commands
		if (cmd.isDMCommand()) {
			logger.info("DM[@" + getInstance().getName() + "]: " + raw_line);
			dmLogger.info(getName() + "@" + getRemoteIP() + "[" + getInstance().getName() + "] >> " + raw_line);
		}
		
		// Okay, now handle the command
		if (Command.WHO == cmd) {
			PlayerManager pm = PlayerManager.getInstance();
			List<Player> players = pm.getPlayers();
			synchronized (players) {
				for (Player p : players) {
					String info = p.getName() + " - level " + p.getLevel() + " @" + p.getRemoteIP()
					+ " (Java " + p.getClientThread().getJavaVersion() + " on " + p.getClientThread().getOsName() + " " + p.getClientThread().getOsVersion() + ")"
					+" [" + p.getInstance().getUniqueName() + "]";
					if (isMaster() && p.isMaster()) {
						info = info + " [DM]";
					}
					
					sendText(info);
				}
			}
			sendText(pm.countPlayers() + " players online");
		}
		else if (Command.ABANDONQUEST == cmd) {
			int questId = Integer.parseInt(args[0]);
			QuestInstance qi = questLog.getQuestInstance(questId);
			if (qi != null && questLog.abandonQuest(questId)) {
				sendText("Abandoned quest: " + qi.getQuest().getTitle());
			}
		}
		else if (Command.ACCEPTQUEST == cmd) {
			int questId = Integer.parseInt(args[0]);
			if (offeredQuests.contains(questId)) {
				Quest quest = QuestManager.getInstance().getQuest(questId);
				startQuest(quest);
			} else {
				sendText("Can not accept a quest that has not been offered, id = " + questId);
			}
		}
		else if (Command.REJECTQUEST == cmd) {
			offeredQuests.remove(Integer.parseInt(args[0]));
		}
		else if (Command.ATTACKMODE == cmd) {
			setAttackMode(!isAttackMode());
		}
		else if (Command.CAST == cmd) {
			// cast a spell
			if (args.length >= 1) {
				try {
					Spell sp = Spell.valueOf(args[0].toUpperCase());
					castSpell(sp);
				} catch (IllegalArgumentException e) {
					sendText("No such spell");
				}
			}
			// list spells available
			else {
				sendText("Available spells:");
				for (Spell s : Spell.values()) {
					sendText(s.getName());
				}
			}
		}
		else if (Command.EMOTES == cmd) {
			sendText("Available emotes: " + Emote.getEmoteList());
		}
		else if (Command.PLAYED == cmd) {
			sendText("Played: " + TimeUtil.getDHMSStringFromSecs(getPlayedTotal()));
			sendText("Session: " + TimeUtil.getDHMSStringFromSecs(getLoggedInTime()));
		}
		else if (Command.PVP == cmd) {
			setPvp(!isPvp());
		}
		else if (Command.HELP == cmd) {
			// Help on topic
			if (args.length >= 1) {
				String topic = args[0];
				if (topic.matches("^[a-z0-9]+$")) {
					String filename = PathConfig.getEtcDir() + "help" + File.separator + topic;
					String help = getFileContent(filename);
					if (help != null) {
						sendText(help);
					} else {
						sendText("No help available for " + topic);
					}
				}
			}
			// Show list of files in help
			else {
				StringBuffer res = new StringBuffer();
				File dir = new File(PathConfig.getEtcDir() + "help");
				for (File f : dir.listFiles()) {
					res.append(f.getName() + " ");
				}
				sendText("Available topics: " + res.toString());
			}
		}
		else if (Command.INSPECT == cmd) {
			Creature c = getTarget();
			if (c == null) {
				c = this;
			}
			
			if (c instanceof Player) {
				Player pl = (Player) c;
				
				sendText("Inspecting player " + pl.getName() + ":");
				for (Wearable item : pl.getInventoryEquipped()) {
					sendText(item.getSlot() + ": " + item.getExamineText());
				}
			} else {
				sendText("You can only inspect other players.");
			}
		}
		else if (Command.LOGOUT == cmd) {
			getClientThread().logout();
		}
		else if (Command.PLAYERS == cmd) {
			String[] names = PlayerManager.getInstance().getAllPlayerNames();
			Format formatter = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
			for (String name : names) {
				long lastlogin = PlayerManager.getInstance().getLastLoginFor(name);
				String stamp = formatter.format(new Date(lastlogin));
				sendText(stamp + " " + name);
			}
			sendText(names.length + " players in total.");
		}
		else if (Command.QUESTLOG == cmd) {
			getClientThread().sendQuestLog();
		}
		else if (Command.SAVE == cmd) {
			PlayerManager.saveOrLogError(this);
		}
		else if (Command.TARGETTARGET == cmd) {
			Creature currentTarget = getTarget();
			if (currentTarget != null) {
				Creature t = currentTarget.getTarget();
				if (t != null) {
					setTarget(t.getID());
				} else {
					setTarget(0);
				}
			}
		}
		else if (Command.WHERE == cmd) {
			sendText("You are at " + getInstance().getName() + " " + getX() + "," + getY());
		}
		else if (Command.DM == cmd) {
			String password = Sha1Util.getSha1ForString(args[0]);
			
			String passInFile = FileUtil.getFirstLineAsString(PathConfig.getDMPasswordPath());
			if (passInFile == null) {
				sendText("DM password has not been set (sha1 hash of it should be in " + PathConfig.getDMPasswordPath() + ").");
			}
			else if (passInFile.equals(password)) {
				setMaster(true);
			} else {
				String msg = "Player " + getName() + " failed to become a master";
				logger.warn(msg);
				dmLogger.warn(msg);
				sendText("Wrong password. This incident has been logged.");
			}
		}
		
		// DM commands
		else if (Command.BANDWIDTH == cmd) {
			StringBuffer res = new StringBuffer();
			PlayerManager pm = PlayerManager.getInstance();
			List<Player> players = pm.getPlayers();
			long totalIn = 0;
			long totalOut = 0;
			synchronized (players) {
				for (Player p : players) {
					long bytesSent = p.getClientThread().getBytesSent();
					int bytesSentPerSec = p.getClientThread().getBytesSentPerSecond();
					long bytesReceived = p.getClientThread().getBytesReceived();
					int bytesReceivedPerSec = p.getClientThread().getBytesReceivedPerSecond();
					res.append(p.getName() + " [sent: " + bytesSentPerSec + " bytes/sec, " + bytesSent + " total, received: " + bytesReceivedPerSec + " bytes/sec, " + bytesReceived + " total]\n");
					
					totalIn += bytesReceived;
					totalOut += bytesSent;
				}
			}
			totalIn += getClientThread().getServer().getBytesIn();
			totalOut += getClientThread().getServer().getBytesOut();
			res.append("Server runtime total: " + totalIn + " in, " + totalOut + " out");
			sendText(res.toString());
		}
		else if (Command.CLEARQUESTLOG == cmd) {
			questLog = new QuestLog();
			questLog.setNeedSave(true);
			resyncVisibleEntities();
		}
		// create <archetype> [num] - create entities of archetype at our position
		else if (Command.CREATE == cmd) {
			int count = 1;
			if (args.length > 1) {
				count = Integer.parseInt(args[1]);
			}
			String archetype = args[0];
			
			boolean done = false;
			for (int i = 0; i < count; i++) {
				MapEntity en = createFromArchetype(archetype);
				if (en == null) {
					sendText("Failed to create MapEntity of archetype '" + archetype + "'");
					break;
				}
				if (en instanceof Item) {
					Item item = (Item) en;
					if (item.isStackable()) {
						item.setQuantity(count);
						done = true;
					}
					addInventoryItem(item, true);
				}
				else if (en instanceof Monster) {
					Monster m = (Monster) en;
					m.setSpawnLocation(getX(), getY());
					en.moveToOrNearby(getInstance(), getX(), getY());
				}
				else {
					en.moveBelow(this);
				}
				if (done) {
					break;
				}
			}
		}
		// Dump an entity like it would be saved to stream
		else if (Command.DUMP == cmd) {
			long id = Long.parseLong(args[0]);
			MapEntity en = getInstance().getEntityByID(id);
			if (en != null) {
				sendText("Dumping entity " + id + ":");
				StringWriter info = new StringWriter();
				try {
					en.saveToStream(info);
				} catch (IOException e) {
					logger.warn("IOException while dumping entity", e);
				}
				sendText(info.toString());
			}
		}
		// Dump an entity using reflection
		else if (Command.DUMPX == cmd) {
			long id = Long.parseLong(args[0]);
			MapEntity en = getInstance().getEntityByID(id);
			if (en != null) {
				StringBuffer buf = new StringBuffer();
				buf.append("Dumping entity " + id + " using reflection:\n");
				buf.append(en.dumpToBuffer());
				sendPopup(buf.toString());
			}
		}
		// List entities in this instance
		else if (Command.ENTITIES == cmd) {
			List<MapEntity> entities = getInstance().getEntities();
			synchronized (entities) {
				for (MapEntity en : entities) {
					sendText(en.getID() + " " + en.getEntityType() + " \"" + en.getName() + "\"");
				}
				sendText(entities.size() + " entities in this instance.");
			}
		}
		// goto <mapname> <x> <y>
		else if (Command.GOTO == cmd) {
			String name = args[0];
			int x = Integer.parseInt(args[1]);
			int y = Integer.parseInt(args[2]);
			
			Instance targetInstance = InstanceManager.getInstance().getInstance(name);
			if (targetInstance != null) {
				moveToInstance(targetInstance, x, y);
			} else {
				sendText("Failed to load instance: " + name);
			}
		}
		else if (Command.KICK == cmd) {
			String name = args[0];
			
			Player pl = PlayerManager.getInstance().getPlayerByName(name);
			if (pl != null) {
				pl.getClientThread().disconnect();
				sendText("Kicked player " + name);
			}
		}
		// Kill an entity by removing it from the world
		else if (Command.KILL == cmd) {
			long id = Long.parseLong(args[0]);
			
			MapEntity en = getInstance().getEntityByID(id);
			if (en != null) {
				en.moveOutOfWorld();
			}
		}
		else if (Command.MAPLIST == cmd) {
			File dir = new File(PathConfig.getMapDir());
			String[] children = dir.list();
			StringBuffer buf = new StringBuffer(children.length + " files in maps directory:\n");
			int i = 0;
			for (String s : children) {
				i++;
				buf.append(s + " ");
				if ((i % 5) == 0) {
					buf.append("\n");
				}
			}
			sendText(buf.toString());
		}
		else if (Command.NODM == cmd) {
			setMaster(false);
		}
		else if (Command.PATCH == cmd) {
			String key = args[1];
			String value = args[2];
			String[] ids = args[0].split(",");
			for (int i = 0; i < ids.length; i++) {
				long id = Long.parseLong(ids[i]);
				MapEntity en = getInstance().getEntityByID(id);
				if (en != null) {
					if (!en.handlePropertyEdit(key, value)) {
						sendText("Failed to edit property " + key + " on " + id);
					}
				}
			}
		}
		else if (Command.POSSESS == cmd) {
			long id = Long.parseLong(args[0]);
			
			MapEntity en = getInstance().getEntityByID(id);
			if (en instanceof Creature) {
				Creature c = (Creature) en;
				possess(c);
			}
		}
		else if (Command.RELOAD == cmd) {
			sendText("Reloading archetypes");
			ArchetypeManager2.getInstance().reload();
		}
		else if (Command.RESPAWN == cmd) {
			if (args.length >= 1) {
				Player pl = PlayerManager.getInstance().getPlayerByName(args[0]);
				if (pl != null) {
					pl.respawn();
					sendText("Player " + pl.getName() + " respawned");
				}
			} else {
				respawn();
			}
		}
		else if (Command.RESYNC == cmd) {
			resyncVisibleEntities();
		}
		else if (Command.SERVERSTATUS == cmd) {
			// Entity statistics
			int players = PlayerManager.getInstance().countPlayers();
			int instances = InstanceManager.getInstance().countInstances();
			int scenes = SceneManager.getInstance().countScenes();
			
			sendText(players + " players online, " + instances + " instances, " + scenes + " scenes");
			sendText(PlayerManager.getInstance().countRegisteredPlayers() + " players in total.");
			
			// Memory
			Runtime rt = Runtime.getRuntime();
			sendText("JVM: freeMemory=" + rt.freeMemory() + ", totalMemory=" + rt.totalMemory() + ", maxMemory=" + rt.maxMemory());
		}
		else if (Command.SUMMON == cmd) {
			String name = args[0];
			Player targetPlayer = PlayerManager.getInstance().getPlayerByName(name);
			if (targetPlayer != null) {
				targetPlayer.moveToOrNearby(getInstance(), getX(), getY());
				targetPlayer.sendText("You have been summoned.");
				sendText("Successfully summoned " + name);
			} else {
				sendText("Summon failed: no such player");
			}
		}
		else if (Command.POPUP == cmd) {
			String player = args[0];
			String text = args[1];

			Player targetPlayer = PlayerManager.getInstance().getPlayerByName(player);
			if (targetPlayer != null) {
				targetPlayer.sendPopup("Message from DM " + getName() + ":\n" + text);
			} else {
				sendText("No such player");
			}
		}
		else if (Command.TELEPORT == cmd) {
			String name = args[0];
			Player targetPlayer = PlayerManager.getInstance().getPlayerByName(name);
			if (targetPlayer != null) {
				moveToOrNearby(targetPlayer.getInstance(), targetPlayer.getX(), targetPlayer.getY());
				sendText("Successfully teleported to where " + name + " is");
			} else {
				sendText("Teleport failed: no such player");
			}
		}
		else if (Command.UNLOADALL == cmd) {
			InstanceManager.getInstance().unloadInstances(true);
			SceneManager.getInstance().unloadUnusedScenes();
		}
		else if (Command.UNPOSSESS == cmd) {
			unpossess();
		}
		else {
			return false;
		}
		return true;
	}

	private void sendPopup(String text) {
		getClientThread().sendPopup(text);
	}
	
	public void possess(Creature creature) {
		creature.setActiveController(getController());
		setActiveController(null);
		setPossessedCreature(creature);
		removeViewer(this);
		creature.addViewer(this);
		onViewablePositionChanged(creature.getX(), creature.getY());
		creature.resyncVisibleEntities();
	}
	
	public void unpossess() {
		if (getPossessedCreature() != null) {
			setActiveController(getController());

			// Restore target creature's controller
			Creature victim = getPossessedCreature();
			victim.removeViewer(this);
			addViewer(this);
			victim.setActiveController(victim.getController());
			onViewablePositionChanged(getX(), getY());
			resyncVisibleEntities();
			setPossessedCreature(null);
		}
	}

	public Creature getPossessedCreature() {
		return possessedCreature;
	}

	public void setPossessedCreature(Creature possessedCreature) {
		this.possessedCreature = possessedCreature;
	}

	public void onViewablePositionChanged(int x, int y) {
		getClientThread().sendPosition(x, y);
	}

	public void onEntityMovement(MapEntity mapEntity, boolean src_visible, boolean dst_visible) {
		// Notify client of the movement
		if (getClientThread() != null) {
			getClientThread().sendNoticeOfEntityMovement(mapEntity.getID(), mapEntity.getX(), mapEntity.getY(), src_visible, dst_visible);
		}
	}

	@Override
	public void resyncVisibleEntities() {
		getClientThread().sendEntityCacheClear();
		super.resyncVisibleEntities();
	}
	
	public boolean castSpell(Spell spell) {
		// For target, use the selected Creature or self
		Creature target = getTarget();
		if (target == null) {
			if (spell.isOffensive()) {
				sendText("Cannot cast offensive spell on itself");
				return false;
			}
			target = this;
		} else {
			// Do not cast harmful spells on friendlies when pvp is off
			if (spell.isOffensive() && !isPvp() && !isEnemy(target)) {
				sendText("Cannot cast offensive spell on friendly target when pvp is off");
				return false;
			}
		}
		
		sendText("Casting spell " + spell.getName() + " on " + target.getName());
		
		if (spell == Spell.HASTE){
			target.giveBuff(BuffType.HASTE, 1 * getLevel());
		}
		else if (spell == Spell.SLOW){
			target.giveBuff(BuffType.SLOW, 1 * getLevel());
		}
		
		// If casting an offensive spell on a non-enemy, mark us as a criminal
		if (spell.isOffensive() && !isEnemy(target)) {
			markCriminal();
		}
		
		target.receiveSpell(this, spell);
		return true;
	}

	@Override
	protected void onBuffReceived(BuffType buff) {
		super.onBuffReceived(buff);
		if (buff.isHidden()) {
			return;
		}
		sendText("You gain " + buff.getName());
		if (buff == BuffType.CRIMINAL) {
			notifyPlayersOfEntityChange(); // name color change
		}
	}

	@Override
	protected void onBuffExpired(BuffType buff) {
		super.onBuffExpired(buff);
		if (buff.isHidden()) {
			return;
		}
		sendText(buff.getName() + " wears off");
		if (buff == BuffType.CRIMINAL) {
			notifyPlayersOfEntityChange(); // name color change
		}
	}

	@Override
	public int getDamageAfterReductions(int damage, int enemy_level) {
		int armor_points = getArmorPoints();
		int dr = GameRules.getDamageReduction(armor_points, enemy_level);
		
		return ((100 - dr) * damage) / 100;
	}
	
	public List<Wearable> getInventoryEquipped() {
		return inventoryEquipped;
	}

	public boolean isPvp() {
		return pvp;
	}

	public void setPvp(boolean pvp) {
		if (this.pvp == pvp) {
			return;
		}
		this.pvp = pvp;
		sendText("PVP mode is now " + (pvp ? "on" : "off"));
	}

	public int getStatsSessions() {
		return statsSessions;
	}

	public void setStatsSessions(int statsSessions) {
		this.statsSessions = statsSessions;
	}

	public int getStatsDeaths() {
		return statsDeaths;
	}

	public void setStatsDeaths(int statsDeaths) {
		this.statsDeaths = statsDeaths;
	}

	public long getStatsDamageDone() {
		return statsDamageDone;
	}

	public void setStatsDamageDone(long statsDamageDone) {
		this.statsDamageDone = statsDamageDone;
	}

	public long getStatsDamageRecv() {
		return statsDamageRecv;
	}

	public void setStatsDamageRecv(long statsDamageRecv) {
		this.statsDamageRecv = statsDamageRecv;
	}

	public int getStatsKills() {
		return statsKills;
	}

	public void setStatsKills(int statsKills) {
		this.statsKills = statsKills;
	}

	public int getStatsKillsPVP() {
		return statsKillsPVP;
	}

	public void setStatsKillsPVP(int statsKillsPVP) {
		this.statsKillsPVP = statsKillsPVP;
	}

	public int getStatsKillsPVPCriminal() {
		return statsKillsPVPCriminal;
	}

	public void setStatsKillsPVPCriminal(int statsKillsPVPCriminal) {
		this.statsKillsPVPCriminal = statsKillsPVPCriminal;
	}

	public int getStatsDeathsPVP() {
		return statsDeathsPVP;
	}

	public void setStatsDeathsPVP(int statsDeathsPvp) {
		this.statsDeathsPVP = statsDeathsPvp;
	}
	
	/**
	 * Get ShopKeeper we are shopping with.
	 * @return
	 */
	public ShopKeeper getShopKeeper() {
		long id = getActiveContainerID();
		ShopKeeper shop = (ShopKeeper) getInstance().getCreatureByID(id);
		return shop;
	}

	/**
	 * Get the color that should be shown on client side for our name.
	 */
	public Color getNameColor() {
		if (isMaster()) {
			return Color.WHITE;
		}
		if (isCriminal()) {
			return Color.GRAY;
		}
		return Alignment.getColorFor(alignment);
	}

	public boolean offerQuest(int questId, String fromWho) {
		// If the player does not yet have the quest, offer it
		Quest quest = QuestManager.getInstance().getQuest(questId);
		if (questLog.isNewQuest(quest)) {
			if (!offeredQuests.contains(questId)) {
				offeredQuests.add(questId);
			}
			getClientThread().sendQuestOffer(questId, fromWho);
		}
		return false;
	}

	private void startQuest(Quest quest) {
		sendText("Started a new quest: " + quest.getTitle());
		questLog.startQuest(quest);
		// We may have gotten the quest from a nearby NPC with a quest marker which now needs refreshing.
		// TODO optimize this, but for now just refresh nearby entities
		resyncVisibleEntities();
	}

	public QuestLog getQuestLog() {
		return questLog;
	}

	public void finishQuest(int questId) {
		logger.debug("finishQuest(" + questId + ")");
		Quest quest = QuestManager.getInstance().getQuest(questId);
		if (questLog.finishQuest(quest)) {
			sendText("Finished quest: " + quest.getTitle());
			resyncVisibleEntities();
			quest.giveRewardTo(this);
		}
	}

	void setQuestLog(QuestLog questLog) {
		this.questLog = questLog;
	}

	public void moveToInstanceAfterLoading() {
		setInstance(InstanceManager.getInstance().getInstance(sceneName, instanceUniqueness, this));
	}

	@Override
	public Alignment getAlignment() {
		Alignment result = Alignment.EVIL;
		for (Alignment a : Alignment.values()) {
			if (a.getLimit() >= result.getLimit() && alignment >= a.getLimit()) {
				result = a;
			}
		}
		return result;
	}
}
