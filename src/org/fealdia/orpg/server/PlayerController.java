package org.fealdia.orpg.server;

import org.fealdia.orpg.server.maps.entities.Creature;

public class PlayerController extends CreatureController {
	private int moveX = 0;
	private int moveY = 0;
	
	@Override
	public boolean doMovement(Creature creature) {
		if (moveX != 0 || moveY != 0) {
			return creature.tryMoveVector(moveX, moveY);
		}
		return false;
	}
	
	public void setVector(int dx, int dy) {
		moveX = dx;
		moveY = dy;
	}

}
