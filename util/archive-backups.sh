#!/bin/bash
# JFealdia server data backup archiver

trap "echo 'ERROR running last command, exiting'; exit 1" ERR

if [ ! -d data ]; then
	echo "Must be run from the parent directory of data/"
	exit 1
fi

cd data/

stamp=$(date +%Y%m%d-%H%M%S)

list="tmp-backup.txt"
archive="archive-$stamp.tar.gz"

find -type f |egrep '[[:digit:]]{13,}$' > "$list"
xargs -a "$list" -r tar -zcf "$archive"

if [ -f "$archive" ]; then
	echo "data/$archive created"
fi

echo "Press enter to delete the archived files, Ctrl-C to abort"
read
xargs -a "$list" -r rm
rm "$list"
